package io.renren.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysRefundLogEntity;

/**
 * 退款日志表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface RefundLogService extends IService<SysRefundLogEntity> {

}

