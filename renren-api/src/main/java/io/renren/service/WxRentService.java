package io.renren.service;

import java.util.List;
import java.util.Map;

import io.renren.common.utils.PageUtils;
import io.renren.modules.request.BaseReq;
import io.renren.modules.request.ImmediatelyRentReq;
import io.renren.modules.request.ImmediatelyReturnReq;
import io.renren.modules.request.MapHomeReq;
import io.renren.modules.request.OrderDetailReq;
import io.renren.modules.response.AboutUsResp;
import io.renren.modules.response.DepositRuleResp;
import io.renren.modules.response.MapDataResp;
import io.renren.modules.response.OrderDetailResp;
import io.renren.modules.response.RentPageResp;
import io.renren.modules.response.RentResultResp;
import io.renren.modules.response.ReturnResultResp;
import io.renren.modules.response.ScanQrResult;

public interface WxRentService {

	MapDataResp index(MapHomeReq param);
	
	ScanQrResult scanQrcode(BaseReq param);
	
	RentPageResp toRentPage(BaseReq param);
	
	RentResultResp immediatelyRent(ImmediatelyRentReq param);
	
	ReturnResultResp immediatelyReturn(ImmediatelyReturnReq param);
	
	OrderDetailResp orderDetail(OrderDetailReq param);
	
	PageUtils queryOrderPage(Map<String, Object> params);
	
	PageUtils queryRechargeCardPage(Map<String, Object> params);
	
	DepositRuleResp queryDepositRuleInfo(Map<String, Object> params);
	
	PageUtils queryPaymentLogPage(Map<String, Object> params);
	
	PageUtils queryNearByMachinePage(Map<String, Object> params);
	
	RentPageResp rentSuc(OrderDetailReq param);
		
	OrderDetailResp returnSuc(OrderDetailReq param);
	
	List<AboutUsResp> aboutUs();
}
