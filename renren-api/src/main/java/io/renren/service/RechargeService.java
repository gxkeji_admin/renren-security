package io.renren.service;

import com.baomidou.mybatisplus.service.IService;

import io.renren.entity.SysRechargeEntity;

/**
 * 充值
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface RechargeService extends IService<SysRechargeEntity> {

}

