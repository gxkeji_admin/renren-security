package io.renren.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysIotCallbackLogEntity;

/**
 * 操作iot获取数据日志记录
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-15 18:45:53
 */
public interface IotCallbackLogService extends IService<SysIotCallbackLogEntity> {

}

