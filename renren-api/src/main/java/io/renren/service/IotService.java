package io.renren.service;

import com.alibaba.fastjson.JSONObject;

import io.renren.entity.SysOrderEntity;
import io.renren.modules.request.IotReq;
import io.renren.modules.request.TranslateSceneStr;

public interface IotService {

	IotReq translateIotReturnReq(String token, TranslateSceneStr translateSceneStr, SysOrderEntity order);
	
	IotReq translateIotReq(String token, TranslateSceneStr translateSceneStr, SysOrderEntity order);
	
	JSONObject operDevice(IotReq iotReq);
	
}
