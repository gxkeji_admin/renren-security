package io.renren.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.request.MinaUserLocation;

/**
 * 微信用户
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface WxUserService extends IService<SysWxUserEntity> {
	
	SysWxUserEntity updateWxUserInfo(WxMaJscode2SessionResult session);

	SysWxUserEntity updateWxUserInfo(WxMaUserInfo maUserInfo);
	
	SysWxUserEntity updateWxUserLocation(MinaUserLocation minaUserLocation);
	
	SysWxUserEntity queryByMinaOpenid(String minaOpenid);

	SysWxUserEntity queryByOpenid(String openid);
	
	Map<String, Boolean> checkDepositAndBalance(String token);
}

