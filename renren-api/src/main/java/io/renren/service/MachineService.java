package io.renren.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysMachineEntity;
import io.renren.modules.request.MapHomeReq;
import io.renren.modules.request.TranslateSceneStr;

/**
 * 机器
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface MachineService extends IService<SysMachineEntity> {

    PageUtils queryNearByMachinePage(Map<String, Object> params);
    
	SysMachineEntity queryMachineByStatus(String machineCode, int machineStatus);
	
	List<SysMachineEntity> queryList(MapHomeReq param);
	
	List<SysMachineEntity> queryListByMap(Map<String, Object> params);
	
	TranslateSceneStr translateSceneStr(String sceneStr);
	
}


