package io.renren.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.dao.SysPaymentLogDao;
import io.renren.entity.SysPaymentLogEntity;
import io.renren.service.PaymentLogService;


@Service("sysPaymentLogService")
public class PaymentLogServiceImpl extends ServiceImpl<SysPaymentLogDao, SysPaymentLogEntity> implements PaymentLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysPaymentLogEntity> page = this.selectPage(
                new Query<SysPaymentLogEntity>(params).getPage(),
                new EntityWrapper<SysPaymentLogEntity>()
        );

        return new PageUtils(page);
    }

}
