package io.renren.service.impl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.request.WxPayRefundRequest;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.WxPayRefundResult;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import io.renren.common.Constants;
import io.renren.emum.DepositStatus;
import io.renren.emum.OrderStatus;
import io.renren.emum.PageCode;
import io.renren.emum.PayLogStatus;
import io.renren.emum.RechargeStatus;
import io.renren.emum.ResultCode;
import io.renren.emum.RuleStatus;
import io.renren.emum.TradeType;
import io.renren.entity.SysDepositEntity;
import io.renren.entity.SysDepositRuleEntity;
import io.renren.entity.SysOrderEntity;
import io.renren.entity.SysPaymentLogEntity;
import io.renren.entity.SysRechargeCardEntity;
import io.renren.entity.SysRechargeEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.BaseParam;
import io.renren.modules.request.DepositPayReq;
import io.renren.modules.response.RechargePayReq;
import io.renren.modules.response.RefundResultResp;
import io.renren.service.DepositRuleService;
import io.renren.service.DepositService;
import io.renren.service.DictService;
import io.renren.service.PayService;
import io.renren.service.WxUserService;

@Service("wxPayService")
public class PayServiceImpl extends WxPayServiceImpl implements PayService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DepositRuleService		depositRuleService;
	
	@Autowired
	private DepositService		depositService;
	
	@Autowired
	private RechargeServiceImpl			rechargeService;
	
	@Autowired
	private WxUserService			wxUserService;
	
	@Autowired
	private OrderServiceImpl			orderService;
	
	@Autowired
	private RechargeCardServiceImpl			rechargeCardService;

	@Autowired
	private PaymentLogServiceImpl		paymentLogService;
	
	@Autowired
	private DictService dictService;

	public PayServiceImpl() {
	}

	@PostConstruct
	public void init() {
		Map<String, String> configMap = dictService.queryDicByType("minipay");
		final WxPayConfig payConfig = new WxPayConfig();
		if(configMap != null) {
			payConfig.setAppId(configMap.get("appid"));
			payConfig.setMchId(configMap.get("mchid"));
			payConfig.setMchKey(configMap.get("mchkey"));
			payConfig.setSubAppId(configMap.get("subappid"));
			payConfig.setSubMchId(configMap.get("submchid"));
			payConfig.setKeyPath(configMap.get("keypath"));
			payConfig.setNotifyUrl(configMap.get("notifyurl"));
		}
		super.setConfig(payConfig);
	}
	
	@Transactional
	public Map<String, String> payDeposit(DepositPayReq param) {
		Map<String, String> result = new HashMap<String, String>();
		if (param != null && StringUtils.isNotBlank(param.getToken())) {
			String openid = param.getToken();
			SysDepositRuleEntity depositRule = depositRuleService.selectById(param.getDepositRuleId().longValue());
			if(depositRule == null) {
				depositRule = depositRuleService.queryDepositRuleByMap(new HashMap<String, Object>());
			} else {
				if(RuleStatus.EXECUTING.getValue().intValue() != depositRule.getRuleStatus().intValue()) {
					depositRule = depositRuleService.queryDepositRuleByMap(new HashMap<String, Object>());
				}
			}
			
			if (depositRule.getDepositPrice().compareTo(BigDecimal.ZERO) > 0) {
				SysDepositEntity insertDeposit = new SysDepositEntity();
				insertDeposit.setOpenid(openid);
				insertDeposit.setDeptId(depositRule.getDeptId().longValue());
				insertDeposit.setPayScene(WxPayConstants.TradeType.JSAPI);
				SysDepositEntity deposit = depositService.selectOne(new EntityWrapper<SysDepositEntity>()
						.eq("openid", insertDeposit.getOpenid()).eq("dept_id", insertDeposit.getDeptId().longValue())
						.eq("pay_scene", insertDeposit.getPayScene()));
				
				if(deposit != null) {
					int depositStatus = deposit.getDepositStatus().intValue();
					if(DepositStatus.PAY_BUT_NOREFUND.getValue().intValue() == depositStatus) {
						SysWxUserEntity wxUser = wxUserService.queryByMinaOpenid(openid);
						BigDecimal userDepositValue = wxUser.getDeposit();
						if(userDepositValue.compareTo(BigDecimal.ZERO) > 0) {
							logger.info("支付押金[openid={}]已支付过押金.....", wxUser.getMinaOpenid());
							result.put("msg", "支付押金[openid={"+ wxUser.getMinaOpenid()+"}]已支付过押金");
							return result;
						}
					}
				} else {
					insertDeposit.setPayPrice(depositRule.getDepositPrice());
					insertDeposit.setDepositStatus(DepositStatus.UNPAID.getValue().intValue());
					insertDeposit.setCreateTime(new Date());
					insertDeposit.setUpdateTime(insertDeposit.getCreateTime());
					boolean insertDepositResult = depositService.insert(insertDeposit);
					if (insertDepositResult) {
						deposit = depositService.selectOne(new EntityWrapper<SysDepositEntity>()
								.eq("openid", insertDeposit.getOpenid()).eq("dept_id", insertDeposit.getDeptId().longValue())
								.eq("pay_scene", insertDeposit.getPayScene()));
					}
				}
										
				SysPaymentLogEntity paymentLog = new SysPaymentLogEntity();
				paymentLog.setOrderId(deposit.getId().longValue());
				paymentLog.setOpenid(deposit.getOpenid());
				paymentLog.setTradeNo(IdWorker.getId() + ""); // 财付通订单号
				paymentLog.setDeptId(depositRule.getDeptId().longValue());
				paymentLog.setPayStatus(PayLogStatus.UNPAID.getValue().intValue());
				paymentLog.setTradeType(TradeType.BIZ_DEPOSIT.getValue().intValue());
				paymentLog.setPayPrice(depositRule.getDepositPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
				paymentLog.setRemark(deposit.getOpenid() + " 支付押金日志 " + " 金额=" + paymentLog.getPayPrice());
				paymentLog.setCreateTime(new Date());
				paymentLog.setUpdateTime(paymentLog.getCreateTime());
				boolean paymentLogResult = paymentLogService.insert(paymentLog);
				if (paymentLogResult) {
					BigDecimal depositValue = NumberUtil.mul(depositRule.getDepositPrice().setScale(2, BigDecimal.ROUND_HALF_UP), 100);
					String body = "押金";
					try {
						if (StringUtils.isNotBlank(depositRule.getName())) {
							body = depositRule.getName().length() > 64 ? depositRule.getName().substring(0, 64) : depositRule.getName();
						}
						body = new String(body.toString().getBytes("UTF-8"), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					WxPayUnifiedOrderRequest payInfo = WxPayUnifiedOrderRequest.newBuilder()
							.openid(openid).outTradeNo(paymentLog.getTradeNo())
							.totalFee(depositValue.intValue()).body(body)
							.tradeType(WxPayConstants.TradeType.JSAPI)
							.spbillCreateIp(Constants.SERVER_IP).build();
					logger.info("支付统一下单请求参数[openid={}],[outTradeNo={}],[totalFee={}],[body={}],[tradeType={}],[spbillCreateIp={}],[notifyURL={}]......",
							payInfo.getOpenid(), payInfo.getOutTradeNo(), payInfo.getTotalFee(),
							payInfo.getBody(), payInfo.getTradeType(), payInfo.getSpbillCreateIp(),
							payInfo.getNotifyUrl());
					try {
						result = this.getPayInfo(payInfo);
						result.put("id", deposit.getId().toString());
					} catch (WxPayException e) {
						e.printStackTrace();
					}
					logger.info("支付统一下单返回结果[result={}]", JSON.toJSONString(result));
				}
			} else {
				// 不设置押金
			}
		}
		return result;
	}
	
	@Transactional
	public Map<String, String> payRecharge(RechargePayReq param) {
		Map<String, String> result = new HashMap<String, String>();
		if (param != null && StringUtils.isNotBlank(param.getToken()) && param.getRechargeCardId() != null && param.getRechargeCardId().longValue() > 0L) {
			String openid = param.getToken();
			SysRechargeCardEntity rechargeCard = rechargeCardService.selectById(param.getRechargeCardId().longValue());
			if (rechargeCard != null && StringUtils.isNotBlank(openid)) {
				SysWxUserEntity wxUser = wxUserService.queryByMinaOpenid(openid);
				if (wxUser != null) {
					if (rechargeCard.getMarketPrice().compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal rechargeValue = NumberUtil.mul(rechargeCard.getMarketPrice().setScale(2, BigDecimal.ROUND_HALF_UP), 100);
						SysRechargeEntity insertRecharge = new SysRechargeEntity();
						insertRecharge.setOpenid(openid);
						insertRecharge.setPayScene(WxPayConstants.TradeType.JSAPI);
						insertRecharge.setRechargeStatus(RechargeStatus.UNPAID.getValue().intValue());
						SysRechargeEntity recharge = rechargeService.selectOne(new EntityWrapper<SysRechargeEntity>()
								.eq("openid", insertRecharge.getOpenid()).eq("dept_id", wxUser.getDeptId().longValue())
								.eq("recharge_status", insertRecharge.getRechargeStatus()));
						
						if(recharge == null) {
							insertRecharge.setFrontBalance(wxUser.getBalance());
							insertRecharge.setTotalPrice(rechargeCard.getTotalPrice());
							insertRecharge.setActualPrice(rechargeCard.getMarketPrice());
							insertRecharge.setDeptId(wxUser.getDeptId().longValue());
							boolean insertRechargeResult = rechargeService.insert(insertRecharge);
							if (insertRechargeResult) {
								recharge = rechargeService.selectOne(new EntityWrapper<SysRechargeEntity>()
										.eq("openid", insertRecharge.getOpenid()).eq("dept_id", wxUser.getDeptId().longValue())
										.eq("recharge_status", insertRecharge.getRechargeStatus()));
							}
						}
						SysPaymentLogEntity paymentLog = new SysPaymentLogEntity();
						if (recharge != null) {
							paymentLog.setOrderId(recharge.getId().longValue());
							paymentLog.setTradeNo(IdWorker.getId() + ""); // 财付通订单号
							paymentLog.setOpenid(recharge.getOpenid());
							paymentLog.setDeptId(recharge.getDeptId().longValue());
							paymentLog.setPayStatus(PayLogStatus.UNPAID.getValue().intValue());
							paymentLog.setTradeType(TradeType.BIZ_RECHARGE.getValue().intValue());
							paymentLog.setPayPrice(recharge.getActualPrice());
							paymentLog.setRemark(recharge.getOpenid() + " 支付充值日志 " + " 金额=" + paymentLog.getPayPrice());
							paymentLog.setCreateTime(new Date());
							paymentLog.setUpdateTime(paymentLog.getCreateTime());
							boolean paymentLogResult = paymentLogService.insert(paymentLog);
							if (paymentLogResult) {
								String body = recharge.getTotalPrice() + "有伞充值";
								try {
									body = new String(body.toString().getBytes("UTF-8"), "UTF-8");
									WxPayUnifiedOrderRequest payInfo = WxPayUnifiedOrderRequest.newBuilder()
										.openid(openid).outTradeNo(paymentLog.getTradeNo())
										.totalFee(rechargeValue.intValue()).body(body)
										.tradeType(WxPayConstants.TradeType.JSAPI)
										.spbillCreateIp(Constants.SERVER_IP).build();
									logger.info("支付统一下单请求参数[openid={}],[outTradeNo={}],[totalFee={}],[body={}],[tradeType={}],[spbillCreateIp={}],[notifyURL={}]......",
										payInfo.getOpenid(), payInfo.getOutTradeNo(), payInfo.getTotalFee(),
										payInfo.getBody(), payInfo.getTradeType(), payInfo.getSpbillCreateIp(),
										payInfo.getNotifyUrl());
										result = this.getPayInfo(payInfo);
								} catch (Exception e) {
									logger.error("payRecharge err...", e.getMessage());
								}
								logger.info("支付统一下单返回结果[result={}]", JSON.toJSONString(result));
							}
						}
					}
				}

			}
		}
		return result;
	}
	
	@Transactional
    public RefundResultResp refundDeposit(BaseParam param) {
		synchronized (this) {
			WxPayRefundResult refundResult = new WxPayRefundResult();
			RefundResultResp result = new RefundResultResp();
			result.setTimestamp(System.currentTimeMillis());
			result.setToken(param.getToken());
			SysWxUserEntity wxUser = wxUserService.queryByMinaOpenid(param.getToken());
			if(wxUser != null && wxUser.getDeposit().compareTo(BigDecimal.ZERO) > 0) {
				SysDepositEntity deposit = new SysDepositEntity();
				deposit.setOpenid(wxUser.getMinaOpenid());
				deposit.setPayScene(WxPayConstants.TradeType.JSAPI);
				deposit.setPayPrice(wxUser.getDeposit());
				deposit.setDepositStatus(DepositStatus.PAY_BUT_NOREFUND.getValue().intValue());
				deposit = depositService.selectOne(new EntityWrapper<SysDepositEntity>()
						.eq("openid", deposit.getOpenid())
						.eq("deposit_status", deposit.getDepositStatus())
						.eq("pay_scene", deposit.getPayScene())
						.eq("pay_price", deposit.getPayPrice()));
				//1.判断押金金额是否正确
				if(deposit == null) {
					result.setPageCode(PageCode.DEPOSIT_PAGE.getValue().intValue());
					result.setResultCode(ResultCode.UNPAY_DEPOSIT.getValue().intValue());
					result.setResultMsg(ResultCode.UNPAY_DEPOSIT.getMsg());
					return result;
				}
				
				if(wxUser.getBalance().compareTo(BigDecimal.ZERO) == -1) {
					result.setPageCode(PageCode.RECHARGE_PAGE.getValue().intValue());
					result.setResultCode(ResultCode.ARREARAGE.getValue().intValue());
					result.setResultMsg(ResultCode.ARREARAGE.getMsg());
					return result;
				}
				
				//2.判断是否有未完成的订单，未付款的订单
				
				Integer[] statusArray = {OrderStatus.RETURN_UN_PAY.getValue().intValue(), OrderStatus.TO_BE_RETURN.getValue().intValue()};
				List<SysOrderEntity> orderList = orderService.queryListByStatus(param.getToken(), statusArray);
				if (orderList != null && !orderList.isEmpty() && orderList.size() > 0) {
					//2.1 有未归还的雨伞
					result.setPageCode(PageCode.ORDER_DETAIL_PAGE.getValue().intValue());
					result.setId(orderList.get(0).getId().longValue());
					result.setResultCode(ResultCode.TO_BE_RETURN.getValue().intValue());
					result.setResultMsg(ResultCode.TO_BE_RETURN.getMsg());
					return result;
				}
				
				//3.插入退款日志记录
				if(deposit != null) {
					SysPaymentLogEntity paymentLog = new SysPaymentLogEntity();
					paymentLog.setOrderId(deposit.getId().longValue());
					paymentLog.setOpenid(deposit.getOpenid());
					paymentLog.setTradeNo(deposit.getTradeNo());
					paymentLog.setTradeType(TradeType.BIZ_REFUND.getValue().intValue());
					paymentLog.setPayStatus(PayLogStatus.PAY.getValue().intValue());
					paymentLog.setPayPrice(deposit.getPayPrice());
					paymentLog.setRemark(deposit.getOpenid() + " 退款押金日志 " + " 金额=" + paymentLog.getPayPrice());
					paymentLog.setCreateTime(new Date());
					paymentLog.setUpdateTime(paymentLog.getCreateTime());
					boolean paymentLogResult = paymentLogService.insert(paymentLog);
					if(paymentLogResult) {
						String refundDesc = "退还有伞押金";
						BigDecimal depositValue = NumberUtil.mul(wxUser.getDeposit().setScale(2, BigDecimal.ROUND_HALF_UP), 100);
						WxPayRefundRequest refund = WxPayRefundRequest.newBuilder().outTradeNo(deposit.getTradeNo()).totalFee(depositValue.intValue())
								.refundFee(depositValue.intValue()).outRefundNo(IdWorker.getId()+"").refundAccount("REFUND_SOURCE_UNSETTLED_FUNDS").refundDesc(refundDesc).build();
						logger.info("微信退款请求参数[openid={}],[outTradeNo={}],[totalFee={}],[refundFee={}],[refundDesc={}]......",
								wxUser.getOpenid(), refund.getOutTradeNo(), refund.getTotalFee(),
								refund.getRefundFee(), refund.getRefundDesc()); 
						try {
							refundResult = this.refund(refund);
						} catch (WxPayException e) {
							if("SUCCESS".equalsIgnoreCase(e.getReturnCode()) && "FAIL".equalsIgnoreCase(e.getResultCode()) && "NOTENOUGH".equalsIgnoreCase(e.getErrCode())) {
								WxPayRefundRequest secondeRefund = WxPayRefundRequest.newBuilder().outTradeNo(deposit.getTradeNo()).totalFee(depositValue.intValue())
										.refundFee(depositValue.intValue()).outRefundNo(IdWorker.getId()+"").refundAccount("REFUND_SOURCE_RECHARGE_FUNDS").refundDesc(refundDesc).build();
								logger.info("第二次微信退款请求参数[openid={}],[outTradeNo={}],[totalFee={}],[refundFee={}],[refundDesc={}]......",
										wxUser.getOpenid(), refund.getOutTradeNo(), refund.getTotalFee(),
										refund.getRefundFee(), refund.getRefundDesc()); 
								try {
									refundResult = this.refund(secondeRefund);
									logger.info("第二次微信退款请求结果[refundResult={}]", JSON.toJSONString(refundResult));
								} catch(WxPayException ex) {
									String errMsg = "申请退押金失败，调用微信退款接口失败！" + ex.toString();
									refundResult.setReturnMsg(errMsg);
									result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
									result.setResultCode(ResultCode.BUSY.getValue().intValue());
									result.setResultMsg(errMsg);
									logger.info("第二次微信退款请求结果[errMsg={}]", JSON.toJSONString(errMsg));
								}
							}
						}
						logger.info("微信退款返回结果[result={}]", JSON.toJSONString(result));
						if(refundResult != null && "SUCCESS".equals(refundResult.getResultCode())) {
							Date date = new Date();
							deposit.setDepositStatus(DepositStatus.PAY_AND_REFUND.getValue().intValue());
							deposit.setRefundPrice(NumberUtil.div(refundResult.getRefundFee(), "100", -1));
							deposit.setRefundNo(Long.parseLong(
									refundResult.getOutRefundNo()));
							deposit.setRefundId(refundResult.getRefundId());
							deposit.setTransactionId(refundResult.getTransactionId());
							deposit.setRefundTime(date);
							deposit.setUpdateTime(date);
							depositService.insertOrUpdate(deposit);
							wxUser.setDeposit(BigDecimal.ZERO);
							wxUserService.insertOrUpdate(wxUser);
							paymentLog.setTransactionId(refundResult.getTransactionId());
							paymentLogService.insertOrUpdate(paymentLog);
							
							result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
							result.setResultCode(ResultCode.OK.getValue().intValue());
							result.setResultMsg(ResultCode.OK.getMsg());
						}
					}
				}
			}
			return result;
		}
    }

	//@Transactional(事务不起作用？)
	public WxPayOrderNotifyResult parseOrderNotifyResult(String xmlData) {
		try {
			synchronized (this) {
				WxPayOrderNotifyResult result = super.parseOrderNotifyResult(xmlData);
				if ("SUCCESS".equals(result.getResultCode())) {
					// 微信服务器通知此回调接口支付成功后，通知给业务系统做处理
					SysPaymentLogEntity paymentLog = new SysPaymentLogEntity();
					Wrapper<SysPaymentLogEntity> wrapperRecharge = new EntityWrapper<SysPaymentLogEntity>();
					wrapperRecharge.eq("trade_no", result.getOutTradeNo()).eq("pay_status", PayLogStatus.UNPAID.getValue().intValue());
					paymentLog = paymentLogService.selectOne(wrapperRecharge);
					if(paymentLog == null) {
						logger.info("==> 微信支付成功回调，查找支付请求日志为空: " + xmlData);
						return null;
					}
					paymentLog.setUpdateTime(new Date());
					paymentLog.setTransactionId(result.getTransactionId());
					if(StringUtils.isNotBlank(result.getTimeEnd())) {
						paymentLog.setPayTime(DateUtil.parse(result.getTimeEnd(), DatePattern.PURE_DATETIME_PATTERN));
					}
					paymentLog.setPayStatus(PayLogStatus.PAY.getValue().intValue());
					boolean updateResult = paymentLogService.update(paymentLog, wrapperRecharge);
					BigDecimal depositValue = BigDecimal.ZERO;
					BigDecimal  rechargeValue = BigDecimal.ZERO;
					boolean updateDepositFlag = Boolean.FALSE;
					boolean updateBalanceFlag = Boolean.FALSE;
					if(paymentLog != null && updateResult) {
						int tradeType = paymentLog.getTradeType().intValue();
						switch (tradeType) {
							case 1:// 押金
								SysDepositEntity deposit = depositService.selectById(paymentLog.getOrderId().longValue());
								deposit.setTradeNo(result.getOutTradeNo());
								deposit.setTransactionId(result.getTransactionId());
								deposit.setPayTime(paymentLog.getPayTime());
								deposit.setPayPrice(paymentLog.getPayPrice());
								deposit.setDepositStatus(DepositStatus.PAY_BUT_NOREFUND.getValue().intValue());
								deposit.setUpdateTime(new Date());
								if(deposit != null && deposit.getId() != null) {
									depositService.insertOrUpdate(deposit);
									depositValue = deposit.getPayPrice();
									updateDepositFlag = Boolean.TRUE;
								}
								break;
							case 2:// 充值
								SysRechargeEntity recharge = rechargeService.selectById(paymentLog.getOrderId().longValue());
								recharge.setTradeNo(result.getOutTradeNo());
								recharge.setTransactionId(result.getTransactionId());
								recharge.setPayTime(paymentLog.getPayTime());
								recharge.setRechargeStatus(RechargeStatus.PAY.getValue().intValue());
								recharge.setUpdateTime(new Date());
								if(recharge != null && recharge.getId() != null) {
									rechargeService.insertOrUpdate(recharge);
									rechargeValue = recharge.getTotalPrice();
									updateBalanceFlag = Boolean.TRUE;
								}
								break;
							default:
								break;
						}
						SysWxUserEntity wxUser = wxUserService.queryByMinaOpenid(result.getOpenid());
						if(wxUser != null) {
							if(updateBalanceFlag) {
								BigDecimal balance = NumberUtil.add(wxUser.getBalance(), rechargeValue);
								wxUser.setBalance(balance);
							}
							
							if(updateDepositFlag) {
								BigDecimal deposit = depositValue;
								wxUser.setDeposit(deposit);
							}
							wxUser.setUpdateTime(new Date());
							wxUserService.insertOrUpdate(wxUser);
						}
					}
					logger.info("out_trade_no: " + result.getOutTradeNo() + " pay SUCCESS!");
					return result;
				} else {
					return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
