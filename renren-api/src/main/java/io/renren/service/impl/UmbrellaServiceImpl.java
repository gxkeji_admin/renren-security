package io.renren.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.dao.UmbrellaDao;
import io.renren.entity.SysUmbrellaEntity;
import io.renren.service.UmbrellaService;


@Service("sysUmbrellaService")
public class UmbrellaServiceImpl extends ServiceImpl<UmbrellaDao, SysUmbrellaEntity> implements UmbrellaService {

}
