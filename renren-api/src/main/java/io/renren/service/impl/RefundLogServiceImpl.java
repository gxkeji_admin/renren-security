package io.renren.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.dao.SysRefundLogDao;
import io.renren.entity.SysRefundLogEntity;
import io.renren.service.RefundLogService;


@Service("sysRefundLogService")
public class RefundLogServiceImpl extends ServiceImpl<SysRefundLogDao, SysRefundLogEntity> implements RefundLogService {

}
