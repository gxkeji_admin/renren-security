package io.renren.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.dao.IotCallbackLogDao;
import io.renren.entity.SysIotCallbackLogEntity;
import io.renren.service.IotCallbackLogService;


@Service("sysIotCallbackLogService")
public class IotCallbackLogServiceImpl extends ServiceImpl<IotCallbackLogDao, SysIotCallbackLogEntity> implements IotCallbackLogService {


}
