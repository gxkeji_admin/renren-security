package io.renren.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.dao.SysRechargeDao;
import io.renren.entity.SysRechargeEntity;
import io.renren.service.RechargeService;


@Service("sysRechargeService")
public class RechargeServiceImpl extends ServiceImpl<SysRechargeDao, SysRechargeEntity> implements RechargeService {


}
