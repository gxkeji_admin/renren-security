package io.renren.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.dao.SysMachineChanncelDao;
import io.renren.entity.SysMachineChanncelEntity;
import io.renren.service.MachineChanncelService;


@Service("sysMachineChanncelService")
public class MachineChanncelServiceImpl extends ServiceImpl<SysMachineChanncelDao, SysMachineChanncelEntity> implements MachineChanncelService {

}
