package io.renren.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.dao.DepositRuleDao;
import io.renren.emum.RuleStatus;
import io.renren.entity.SysDepositRuleEntity;
import io.renren.service.DepositRuleService;


@Service("sysDepositRuleService")
public class DepositRuleServiceImpl extends ServiceImpl<DepositRuleDao, SysDepositRuleEntity> implements DepositRuleService {

	@Override
	public SysDepositRuleEntity queryDepositRuleByDeptId(long deptId) {
		SysDepositRuleEntity result = this.selectOne(new EntityWrapper<SysDepositRuleEntity>().eq("dept_id", deptId)
				.eq("rule_status", RuleStatus.EXECUTING.getValue().intValue()));
		if (result != null) {
			return result;
		} else {
			return this.selectList(new EntityWrapper<SysDepositRuleEntity>().orderBy("id", Boolean.TRUE)).get(0);
		}
	}

	@DataFilter(wxUser = false)
	@Override
	public SysDepositRuleEntity queryDepositRuleByMap(Map<String, Object> params) {
		SysDepositRuleEntity result = this.selectOne(new EntityWrapper<SysDepositRuleEntity>()
				.eq("rule_status", RuleStatus.EXECUTING.getValue().intValue())
				.addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String) params.get(Constant.SQL_FILTER)));
		if (result != null) {
			return result;
		} else {
			return this.selectList(new EntityWrapper<SysDepositRuleEntity>().orderBy("id", Boolean.TRUE)).get(0);
		}
	}
}
