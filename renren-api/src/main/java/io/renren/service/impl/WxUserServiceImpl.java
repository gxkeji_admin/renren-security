package io.renren.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import io.renren.common.utils.HttpContextUtils;
import io.renren.dao.SysWxUserDao;
import io.renren.emum.DepositStatus;
import io.renren.entity.SysDepositEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.interceptor.AuthorizationInterceptor;
import io.renren.modules.request.MinaUserLocation;
import io.renren.service.DepositService;
import io.renren.service.WxUserService;


@Service("wxUserService")
public class WxUserServiceImpl extends ServiceImpl<SysWxUserDao, SysWxUserEntity> implements WxUserService {

	@Autowired
	private DepositService depositService;
	
    @Autowired
    private WxMaService wxMaService;
	
	@Override
	public SysWxUserEntity updateWxUserInfo(WxMaJscode2SessionResult session) {
		SysWxUserEntity wxUser = this.queryByMinaOpenid(session.getOpenid());
		Date date = new Date();
		if(wxUser == null) {
			wxUser = new SysWxUserEntity();
			wxUser.setMinaOpenid(session.getOpenid());
			wxUser.setAppid(wxMaService.getWxMaConfig().getAppid());
			wxUser.setCreateTime(date);
			wxUser.setUpdateTime(date);
			this.insert(wxUser);
		} 
		return wxUser; 
	}
	
    @Transactional
	@Override
	public SysWxUserEntity updateWxUserInfo(WxMaUserInfo maUserInfo) {
		SysWxUserEntity wxUser = this.queryByMinaOpenid(maUserInfo.getOpenId());
		if(wxUser == null) {
			wxUser = new SysWxUserEntity();
		}
		wxUser.setMinaOpenid(maUserInfo.getOpenId());
		wxUser.setNickName(maUserInfo.getNickName());
		wxUser.setSex(StringUtils.isNotBlank(maUserInfo.getGender()) ? Integer.parseInt(maUserInfo.getGender()) : 0);
		wxUser.setLanguage(maUserInfo.getLanguage());
		wxUser.setCity(maUserInfo.getCity());
		wxUser.setProvince(maUserInfo.getProvince());
		wxUser.setCountry(maUserInfo.getCountry());
		wxUser.setAvatar(maUserInfo.getAvatarUrl());
		wxUser.setUnionid(maUserInfo.getUnionId());
		wxUser.setMinaAppid(maUserInfo.getWatermark().getAppid());
		this.insertOrUpdate(wxUser);
		return wxUser; 
	}
    
	@Transactional
	@Override
	public SysWxUserEntity updateWxUserLocation(MinaUserLocation minaUserLocation) {
		SysWxUserEntity wxUser = new SysWxUserEntity();
		if (minaUserLocation != null && StringUtils.isNotBlank(minaUserLocation.getToken())) {
			wxUser = this.queryByMinaOpenid(minaUserLocation.getToken());
			if(wxUser != null) {
				wxUser.setLatitude(minaUserLocation.getLatitude());
				wxUser.setLongitude(minaUserLocation.getLongitude());
				this.updateById(wxUser);
				return wxUser;
			}
		}
		return wxUser;
	}
	
	@Override
	public SysWxUserEntity queryByMinaOpenid(String minaOpenid) {
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		SysWxUserEntity wxUser = (SysWxUserEntity) request.getAttribute(AuthorizationInterceptor.USER_KEY);
		if (wxUser != null) {
			return wxUser;
		} else {
			return this.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", minaOpenid));
		}
	}
	
	@Override
	public SysWxUserEntity queryByOpenid(String openid) {
		return this.selectOne(new EntityWrapper<SysWxUserEntity>().eq("openid", openid));
	}
	
	/**
	 * 
	 * @Title: checkDepositAndBalance
	 * @Description: TODO<判断是否已经缴纳押金：false|未缴纳，true|已缴纳; 余额充足：true；欠费，false>
	 * @author oslive 2018年4月12日 上午12:03:14
	 *
	 * @param token
	 * @return
	 */
	@Override
	public Map<String, Boolean> checkDepositAndBalance(String token) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		if (StringUtils.isNotBlank(token)) {
			SysWxUserEntity wxUser = this.queryByMinaOpenid(token);
			if (wxUser != null) {
				BigDecimal wxUserDeposit = wxUser.getDeposit();
				BigDecimal wxUserBalance = wxUser.getBalance();
				SysDepositEntity deposit = depositService.selectOne(new EntityWrapper<SysDepositEntity>().eq("openid", token).eq("deposit_status", DepositStatus.PAY_BUT_NOREFUND.getValue().intValue()));
				boolean depositLog = Boolean.FALSE;
				if(deposit != null) {
					depositLog = Boolean.TRUE;
				}
				if (wxUserDeposit != null && wxUserDeposit.compareTo(BigDecimal.ZERO) > 0) {
					result.put("deposit", Boolean.TRUE && depositLog);
				}else {
					result.put("deposit", Boolean.FALSE);
				}
				if (wxUserBalance != null && wxUserBalance.compareTo(BigDecimal.ZERO) > -1) {
					result.put("balance", Boolean.TRUE);
				} else {
					result.put("balance", Boolean.FALSE);
				}
			}
		}
		return result;
	}
}
