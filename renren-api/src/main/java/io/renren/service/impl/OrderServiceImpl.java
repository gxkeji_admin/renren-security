package io.renren.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.dao.OrderDao;
import io.renren.emum.OrderStatus;
import io.renren.entity.SysDepositRuleEntity;
import io.renren.entity.SysOrderEntity;
import io.renren.entity.SysRentRuleEntity;
import io.renren.modules.request.OrderDetailReq;
import io.renren.modules.response.OrderDetailResp;
import io.renren.modules.response.OrderDetailResp.FeeDetail;
import io.renren.modules.response.OrderDetailResp.OrderDetail;
import io.renren.modules.response.OrderDetailResp.RentRuleDetail;
import io.renren.service.DepositRuleService;
import io.renren.service.OrderService;
import io.renren.service.RentRuleService;

@Service("sysOrderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, SysOrderEntity> implements OrderService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RentRuleService rentRuleService;
	
	@Autowired
	private DepositRuleService depositRuleService;
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysOrderEntity> page = this.selectPage(
                new Query<SysOrderEntity>(params).getPage(),
                new EntityWrapper<SysOrderEntity>()
        );
		List<SysOrderEntity> orderList = page.getRecords();
		orderList.forEach(order -> {
			if (order != null) {
				this.calculateOrder(order);
			}
		});
        return new PageUtils(page);
    }

	@Override
	public List<SysOrderEntity> queryListByStatus(String openid, int orderStatus) {
	       List<SysOrderEntity> result = this.selectList(new EntityWrapper<SysOrderEntity>().eq("openid", openid).eq("chip_code", "unuse").eq("order_status", orderStatus).orderBy("id", Boolean.FALSE));
	       return result;
	}
	
	@Override
	public List<SysOrderEntity> queryListByStatus(String openid, Integer array[]) {
	       List<SysOrderEntity> result = this.selectList(new EntityWrapper<SysOrderEntity>().eq("openid", openid).in("order_status", array).orderBy("id", Boolean.FALSE));
	       return result;
	}

	@Override
	public void calculateOrder(SysOrderEntity order) {
		if (order != null) {
			Date startTime = order.getRentTime();
			Date returnTime = order.getReturnTime() != null ? order.getReturnTime() : new Date();
			double useTime = 0.0;
			int orderStatus = order.getOrderStatus().intValue();
			if (orderStatus == OrderStatus.SYSTEM_CANNCEL.getValue().intValue() || orderStatus == OrderStatus.WAIT_TAKE_OUT.getValue().intValue()) {
				return;
			}
			SysRentRuleEntity rentRule = rentRuleService.selectOne(new EntityWrapper<SysRentRuleEntity>().eq("dept_id", order.getDeptId().longValue()));

			int freeType = rentRule.getFreeType();
			int freeValue = rentRule.getFreeValue();
			int perTypeValue = rentRule.getPerTypeValue();
			int rentType = rentRule.getRentType();
			BigDecimal rentValue = rentRule.getRentValue();

			if (OrderStatus.TO_BE_RETURN.getValue().intValue() == order.getOrderStatus().intValue() ||
			    OrderStatus.RETURN_UN_PAY.getValue().intValue() == order.getOrderStatus().intValue()) {
				// 免费时长单位:1|分，2|小时，3|天
				Date endTime = null;
				if (1 == freeType) {
					endTime = DateUtil.offsetMinute(returnTime, -freeValue);
				} else if (2 == freeType) {
					endTime = DateUtil.offsetHour(returnTime, -freeValue);
				} else if (3 == freeType) {
					endTime = DateUtil.offsetDay(returnTime, -freeValue);
				}

				if (startTime.before(endTime)) {
					// 计价单位：1|按小时计费，2|按天计费
					long diffMin = DateUtil.between(startTime, endTime, DateUnit.MINUTE, Boolean.TRUE);;
					if (1 == rentType) {
						double diffHours = diffMin / 60.0 / perTypeValue;
						useTime = Math.ceil(diffHours);
					} else if (2 == rentType) {
						double diffDays = diffMin / 60.0 / 24.0 / perTypeValue;
						useTime = Math.ceil(diffDays);
					}
					BigDecimal totalPrice = NumberUtil.mul(useTime, rentValue);
					order.setTotalPrice(totalPrice);
				} else {
					order.setTotalPrice(BigDecimal.ZERO);
				}
			}
		}
	}

	@Override
	public OrderDetailResp orderDetail(OrderDetailReq param) {
		OrderDetailResp result = new OrderDetailResp();
		if (param != null && param.getOrderId() > 0L) {
			SysOrderEntity order = this.selectById(param.getOrderId());
			if (order != null) {
				this.calculateOrder(order);
				result.setOrderId(order.getId());
				result.setChipCode(order.getChipCode());
				result.setOrderStatus(order.getOrderStatus().intValue());
				OrderDetail orderDetail = result.newOrderDetailInstance();
				orderDetail.setRentPlace(order.getRentPlace());
				orderDetail.setReturnPlace(order.getReturnPlace());
				orderDetail.setRentTime(order.getRentTime());
				orderDetail.setReturnTime(order.getReturnTime());
				FeeDetail feeDetail = result.newFeeDetailInstance();
				if (order.getTotalPrice() != null && order.getTotalPrice().compareTo(BigDecimal.ZERO) > 0
						&& order.getDiscountPrice() != null
						&& order.getDiscountPrice().compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal actualPrice = NumberUtil.sub(order.getTotalPrice(), order.getDiscountPrice());
					feeDetail.setActualPrice(actualPrice);
				} else {
					feeDetail.setActualPrice(order.getTotalPrice());
				}
				feeDetail.setTotalPrice(order.getTotalPrice());
				feeDetail.setBalanceDeductionPrice(order.getDiscountPrice());
				feeDetail.setDiscountPrice(order.getDiscountPrice());
				String chipCode = order.getChipCode();
				if (StringUtils.isNotBlank(chipCode)) {
					SysDepositRuleEntity depositRule = depositRuleService.selectOne(new EntityWrapper<SysDepositRuleEntity>().eq("dept_id", order.getDeptId().longValue()));
					SysRentRuleEntity rentRule = rentRuleService.selectOne(new EntityWrapper<SysRentRuleEntity>().eq("dept_id", order.getDeptId().longValue()));
					RentRuleDetail rentRuleDetail = result.newRentRuleDetailInstance();
					rentRuleDetail.setDepositValue(depositRule.getDepositPrice());
					rentRuleDetail.setFreeType(rentRule.getFreeType());
					rentRuleDetail.setFreeValue(rentRule.getFreeValue());
					rentRuleDetail.setRentType(rentRule.getRentType());
					rentRuleDetail.setRentValue(rentRule.getRentValue());
				} 
			}
		} 
		return result;
	}
	
	@Transactional
	@Override
	public OrderDetailResp orderLost(OrderDetailReq params) {
		OrderDetailResp result = new OrderDetailResp();
/*		if (params != null && params.getOrderId() != null && params.getOrderId().longValue() > 0L) {
			Order order = this.queryById(params.getOrderId());
			if (order != null && OrderStatus.TO_BE_RETURN.getValue().intValue() == order.getOrderStatus().intValue()) {
				String umbrellaCode = order.getUmbrellaCode();
				if (StringUtils.isNotBlank(umbrellaCode)) {
					Wrapper<Umbrella> umbrellaWrapper = new EntityWrapper<Umbrella>();
					umbrellaWrapper.eq("umbrella_code", umbrellaCode);
					Umbrella umbrella = umbrellaService.selectOne(umbrellaWrapper);
					if (umbrella != null && UmbrellaStatus.RENT_OUT.getValue().intValue() == umbrella.getUmbrellaState()
							.intValue()) {
						// 进入报失流程
						// 1.计算订单花费，扣减余额，扣减押金，备注押金表
						this.calculateOrder(order);
						BigDecimal totalPrice = order.getTotalPrice();
						WxUser wxUser = wxUserService.selectByMinaOpenid(params.getToken());
						if (wxUser != null) {
							BigDecimal balance = wxUser.getBalance();
							double diff = MathUtil.subtract(balance, totalPrice);
							if (diff > 0L) {
								balance = BigDecimal.valueOf(diff);
								wxUser.setBalance(balance);
							} else {
								balance = BigDecimal.ZERO;
								wxUser.setDeposit(BigDecimal.ZERO);
								List<Deposit> depositList = depositService
										.selectList(new EntityWrapper<Deposit>().eq("openid", params.getToken())
												.eq("deposit_status",
														DepositStatus.PAY_BUT_NOREFUND.getValue().intValue())
												.orderBy("create_time", false));
								if (depositList != null && !depositList.isEmpty() && depositList.size() > 0) {
									Deposit deposit = depositList.get(0);
									deposit.setDepositStatus(DepositStatus.LOST.getValue().intValue());
									deposit.setRemark(DepositStatus.LOST.getMsg());
									depositService.update(deposit);
								}
							}
							wxUserService.update(wxUser);
						}
						umbrella.setUmbrellaState(UmbrellaStatus.LOST.getValue().intValue());
						umbrella.setRemark(UmbrellaStatus.LOST.getMsg());
						umbrellaService.update(umbrella);
						order.setOrderStatus(OrderStatus.DONE.getValue().intValue());
						order.setRemark(UmbrellaStatus.LOST.getMsg());
						order.setPartLost(2);
						order.setReturnPlace("遗失");
						order.setReturnTime(new Date());
						this.update(order);

					} else {
						// 当前雨伞状态不允许报失
					}
				} else {
					// 当前订单信息不完整
				}
			} else {
				// 当前订单状态不允许报失
			}
		} else {
			throw new IllegalParameterException("==>orderLost，非法参数，[请求参数params不能为空]......");
		}*/
		return result;
	}
}
