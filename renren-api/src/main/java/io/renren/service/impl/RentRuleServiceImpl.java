package io.renren.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.dao.RentRuleDao;
import io.renren.emum.RuleStatus;
import io.renren.entity.SysRentRuleEntity;
import io.renren.service.RentRuleService;


@Service("sysRentRuleService")
public class RentRuleServiceImpl extends ServiceImpl<RentRuleDao, SysRentRuleEntity> implements RentRuleService {

	@Override
	public SysRentRuleEntity queryRentRuleByDeptId(long deptId) {
		SysRentRuleEntity result = this.selectOne(new EntityWrapper<SysRentRuleEntity>().eq("dept_id", deptId)
				.eq("rule_status", RuleStatus.EXECUTING.getValue().intValue()));
		if (result != null) {
			return result;
		} else {
			return this.selectList(new EntityWrapper<SysRentRuleEntity>().orderBy("id", Boolean.TRUE)).get(0);
		}
	}

	@DataFilter(wxUser = false)
	@Override
	public SysRentRuleEntity queryRentRuleByMap(Map<String, Object> params) {
		SysRentRuleEntity result = this.selectOne(new EntityWrapper<SysRentRuleEntity>()
				.eq("rule_status", RuleStatus.EXECUTING.getValue().intValue())
				.addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String) params.get(Constant.SQL_FILTER)));
		if (result != null) {
			return result;
		} else {
			return this.selectList(new EntityWrapper<SysRentRuleEntity>().orderBy("id", Boolean.TRUE)).get(0);
		}
	}
}
