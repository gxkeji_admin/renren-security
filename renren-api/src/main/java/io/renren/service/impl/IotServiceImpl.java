package io.renren.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;

import io.renren.common.Constants;
import io.renren.common.utils.HttpUtil;
import io.renren.common.utils.HttpUtil.HttpReturn;
import io.renren.common.utils.HttpUtil.Params;
import io.renren.common.utils.HttpUtil.ReqParam;
import io.renren.common.utils.JsonUtil;
import io.renren.emum.MachineStatus;
import io.renren.emum.OrderStatus;
import io.renren.emum.UmbrellaStatus;
import io.renren.entity.SysMachineEntity;
import io.renren.entity.SysOrderEntity;
import io.renren.entity.SysUmbrellaEntity;
import io.renren.modules.request.IotReq;
import io.renren.modules.request.TranslateSceneStr;
import io.renren.service.IotService;
import io.renren.service.MachineService;
import io.renren.service.UmbrellaService;

/**
 * 
 * @author WLDevelop
 *
 */
@Service
public class IotServiceImpl implements IotService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Resource
	private HttpUtil httpUtil;
	
	@Autowired
	private MachineService machineService;
	
	@Autowired
	private UmbrellaService sysUmbrellaService;
	
	public IotReq translateIotReturnReq(String token, TranslateSceneStr translateSceneStr, SysOrderEntity order) {
		IotReq iotReq = new IotReq();
		String actionOperation = Constants.SCAN_QR_OPERATION.RETURN;
		SysMachineEntity machine = machineService.queryMachineByStatus(translateSceneStr.getMachineCode(), MachineStatus.ON_LINE.getValue().intValue());
		if(machine != null && order != null) {
			if(order != null && OrderStatus.TO_BE_RETURN.getValue().intValue() == order.getOrderStatus().intValue()) {
				String chipCode = order.getChipCode();
				SysUmbrellaEntity umbrella = sysUmbrellaService.selectOne(new EntityWrapper<SysUmbrellaEntity>().eq("chip_code", chipCode));
				if (umbrella != null) {
					if (UmbrellaStatus.RENT_OUT.getValue().intValue() == umbrella.getUmbrellaStatus().intValue()) {
						actionOperation = Constants.SCAN_QR_OPERATION.RETURN;
						// 处理扫描伞道租伞逻辑
						iotReq.setProductKey(machine.getProductKey());//产品key
						iotReq.setDeviceName(machine.getDeviceName());//设备ID
						iotReq.setMachineId(machine.getId().longValue());
						iotReq.setOrderId(order.getId().longValue() + "");
						iotReq.setOper(actionOperation);//操作
						iotReq.setUmbrellaRoadId(translateSceneStr.getMachineChanncel());//伞道ID
						iotReq.setTime(System.currentTimeMillis()/1000);
						iotReq.setUserId(token);//微信粉丝ID
					} else {
						this.logger.error("==>translateIotReturnReq，雨伞状态不正确，[umbrellaStatus={"+ umbrella.getUmbrellaStatus().intValue() +"}]");
					}
				} else {
					this.logger.error("==>translateIotReturnReq，雨伞不存在，[chipCode={"+ chipCode +"}]");
				}
			}
		} else {
			this.logger.error("==>translateIotReturnReq，机器状态不在线, [machineCode={" + translateSceneStr != null ? translateSceneStr.getMachineCode() : "translateSceneStr为空" + "}]");
		}
		return iotReq;
	}


	@Override
	public IotReq translateIotReq(String token, TranslateSceneStr translateSceneStr, SysOrderEntity order) {
		IotReq iotReq = new IotReq();
		SysMachineEntity machine = machineService.queryMachineByStatus(translateSceneStr.getMachineCode(), MachineStatus.ON_LINE.getValue().intValue());
		if (StringUtils.isNotBlank(token) 
				&& translateSceneStr != null 
				&& order != null 
				&& machine != null) {
				// 处理扫描伞道租伞逻辑
				iotReq.setProductKey(machine.getProductKey());//产品key
				iotReq.setDeviceName(machine.getDeviceName());//设备ID
				iotReq.setMachineId(machine.getId().longValue());
				iotReq.setOrderId(order.getId().longValue() + "");
				iotReq.setOper(translateSceneStr.getActionOperation());//操作
				iotReq.setUmbrellaRoadId(translateSceneStr.getMachineChanncel());//伞道ID
				iotReq.setTime(System.currentTimeMillis()/1000);
				iotReq.setUserId(token);//微信粉丝ID
		} else {
			this.logger.error("==>translateIotRentReq， [token={" + token + "}], [machine={" + JSON.toJSONString(machine)
					+ "}], [order={" + JSON.toJSONString(order) + "}], [translateSceneStr={"
					+ JSON.toJSONString(translateSceneStr) + "}]");
		}
		return iotReq;
	}
	
	@Override
	public JSONObject operDevice(IotReq iotReq) {
	    List<ReqParam> paramList = new ArrayList<>();
	    paramList.add(new ReqParam("productKey", iotReq.getProductKey()));
	    paramList.add(new ReqParam("deviceName", iotReq.getDeviceName()));
	    String data = JsonUtil.toJSONString(iotReq);
	    paramList.add(new ReqParam("data", data));
	    System.out.println("dataJson:" + JSON.toJSONString(data));
	    Params params = Params.create().add(paramList);
        HttpReturn httpReturn = httpUtil.post(params, Constants.ALIYUN_IOT_DOMAIN);
        System.out.println("@@@httpReturn:" + httpReturn.getRetValue());
        JSONObject jsonObject = JSONObject.parseObject(httpReturn.getRetValue());
        return jsonObject;
	}
}
