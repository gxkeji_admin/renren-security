package io.renren.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.dao.SysDepositDao;
import io.renren.entity.SysDepositEntity;
import io.renren.service.DepositService;


@Service("sysDepositService")
public class DepositServiceImpl extends ServiceImpl<SysDepositDao, SysDepositEntity> implements DepositService {

}
