package io.renren.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.dao.SysWxUserFaultDao;
import io.renren.entity.SysWxUserFaultEntity;
import io.renren.service.WxUserFaultService;


@Service("sysWxUserFaultService")
public class SysWxUserFaultServiceImpl extends ServiceImpl<SysWxUserFaultDao, SysWxUserFaultEntity> implements WxUserFaultService {

	@Override
	public boolean save(SysWxUserFaultEntity wxUserFault) {
	    boolean result = this.insert(wxUserFault);
	    // 添加其他业务逻辑
		return result;
	}
}
