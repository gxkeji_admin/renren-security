package io.renren.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.google.common.collect.Lists;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaTemplateMessage;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import io.renren.common.Constants;
import io.renren.common.utils.MapUtils;
import io.renren.common.utils.PageUtils;
import io.renren.emum.AliyunIotStatus;
import io.renren.emum.MachineStatus;
import io.renren.emum.OrderStatus;
import io.renren.emum.PageCode;
import io.renren.emum.PayLogStatus;
import io.renren.emum.ResultCode;
import io.renren.emum.TradeType;
import io.renren.emum.UmbrellaStatus;
import io.renren.entity.SysDepositRuleEntity;
import io.renren.entity.SysDictEntity;
import io.renren.entity.SysIotCallbackLogEntity;
import io.renren.entity.SysMachineEntity;
import io.renren.entity.SysOrderEntity;
import io.renren.entity.SysPaymentLogEntity;
import io.renren.entity.SysRentRuleEntity;
import io.renren.entity.SysUmbrellaEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.request.BaseReq;
import io.renren.modules.request.ImmediatelyRentReq;
import io.renren.modules.request.ImmediatelyReturnReq;
import io.renren.modules.request.IotReq;
import io.renren.modules.request.MapHomeReq;
import io.renren.modules.request.OrderDetailReq;
import io.renren.modules.request.TranslateSceneStr;
import io.renren.modules.response.AboutUsResp;
import io.renren.modules.response.DepositRuleResp;
import io.renren.modules.response.IotResp;
import io.renren.modules.response.MapDataResp;
import io.renren.modules.response.OrderDetailResp;
import io.renren.modules.response.RentPageResp;
import io.renren.modules.response.RentResultResp;
import io.renren.modules.response.RentRuleResp;
import io.renren.modules.response.ReturnResultResp;
import io.renren.modules.response.ScanQrResult;
import io.renren.service.DepositRuleService;
import io.renren.service.DictService;
import io.renren.service.IotCallbackLogService;
import io.renren.service.IotService;
import io.renren.service.MachineService;
import io.renren.service.OrderService;
import io.renren.service.PaymentLogService;
import io.renren.service.RechargeCardService;
import io.renren.service.RentRuleService;
import io.renren.service.UmbrellaService;
import io.renren.service.WxRentService;
import io.renren.service.WxUserService;
import me.chanjar.weixin.common.exception.WxErrorException;

/**
 * 
 * @author WLDevelop
 *
 */
@Service
public class WxRentServiceImpl implements WxRentService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Autowired
    private WxMaService wxService;
    
    @Autowired
    private IotCallbackLogService iotCallbackLogService;

    @Autowired
    private WxUserService wxUserService;
	
    @Autowired
    private OrderService orderService;
    
    @Autowired
    private RechargeCardService rechargeCardService;
    
    @Autowired
    private PaymentLogService paymentLogService;
    
    @Autowired
    private MachineService machineService;
    
	@Autowired
	private UmbrellaService umbrellaService;
    
    @Autowired
    private DepositRuleService depositRuleService;
	
    @Autowired
    private RentRuleService rentRuleService;
    
    @Autowired
    private IotService iotService;
    
    @Autowired
	private DictService dictService;
    
	@Override
	public MapDataResp index(MapHomeReq param) {
		MapDataResp mapDataResp = new MapDataResp();
		if (param != null && StringUtils.isNotEmpty(param.getToken())) {
			SysWxUserEntity result = wxUserService.queryByMinaOpenid(param.getToken());
			if (result != null) {
				param.setLatitude(result.getLatitude() == null ? BigDecimal.valueOf(23.117055306224895) : result.getLatitude());
				param.setLongitude(result.getLongitude() == null ? BigDecimal.valueOf(113.2759952545166) : result.getLongitude());
				mapDataResp.setOpenid(result.getMinaOpenid());
				// 是否已经缴纳押金：1|未缴纳，2|已缴纳
				// 是否欠费：1|欠费，2|不欠费
				// 是否有进行中的订单：1|无，2|有
				BigDecimal wxUserDeposit = result.getDeposit();
				if (wxUserDeposit != null && wxUserDeposit.compareTo(BigDecimal.ZERO) > 0) {
					mapDataResp.setDeposit(2);
				} else {
					mapDataResp.setDeposit(1);
				}
				
				BigDecimal wxUserBalance = result.getBalance();
				if (wxUserBalance != null && wxUserBalance.compareTo(BigDecimal.ZERO) > -1) {
					mapDataResp.setBalance(2);
				} else {
					mapDataResp.setBalance(1);
				}
				
				List<SysOrderEntity> orderList = orderService.queryListByStatus(param.getToken(), OrderStatus.TO_BE_RETURN.getValue().intValue());
				if (orderList != null && !orderList.isEmpty() && orderList.size() > 0) {
					mapDataResp.setOrderFlag(2);
					mapDataResp.setOrderId(orderList.get(0).getId().longValue());
				} else {
					mapDataResp.setOrderFlag(1);
				}
			} else {
				param.setLatitude(BigDecimal.valueOf(23.117055306224895));
				param.setLongitude(BigDecimal.valueOf(113.2759952545166));
			}
		}
		List<SysMachineEntity> machineList = machineService.queryListByMap(MapUtils.transBean2Map(param));
		mapDataResp.setLatitude(param.getLatitude());
		mapDataResp.setLongitude(param.getLongitude());
		mapDataResp.setMachineList(machineList);
		return mapDataResp;
	}
	
	private ScanQrResult checkCondition(String token, String sceneStr) {
		ScanQrResult result = new ScanQrResult();
		// 1.判断二维码合法性
		if (sceneStr.length() != 10) {
			result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
			result.setResultCode(ResultCode.ERROR_QRCODE.getValue().intValue());
			result.setResultMsg(ResultCode.ERROR_QRCODE.getMsg());
			return result;
		}

		// 2.判断二维码最后一位字符是否是数字
		String onAndOffCodeStr = sceneStr.substring(9);
		if (!NumberUtil.isNumber(onAndOffCodeStr)) {
			result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
			result.setResultCode(ResultCode.ERROR_QRCODE.getValue().intValue());
			result.setResultMsg(ResultCode.ERROR_QRCODE.getMsg());
			return result;
		}

		// 3.判断二维码所属机器是否存在
		String channcelCode = sceneStr.substring(0, 9);
		String machineCode = channcelCode.replaceFirst("c", "m");
		SysMachineEntity machine = machineService.queryMachineByStatus(machineCode, MachineStatus.ON_LINE.getValue().intValue());
		if (machine == null) {
			result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
			result.setResultCode(ResultCode.ERROR_MACHINE_STATUS.getValue().intValue());
			result.setResultMsg(ResultCode.ERROR_MACHINE_STATUS.getMsg());
			return result;
		}

		// 4.判断是否缴押金;判断当前用户是否欠费
		Map<String, Boolean> checkDepositAndBalance = wxUserService.checkDepositAndBalance(token);
		if (checkDepositAndBalance != null) {
			boolean deposit = checkDepositAndBalance.get("deposit");
			boolean balance = checkDepositAndBalance.get("balance");
			if(!deposit) {
				result.setPageCode(PageCode.DEPOSIT_PAGE.getValue().intValue());
				result.setResultCode(ResultCode.UNPAY_DEPOSIT.getValue().intValue());
				result.setResultMsg(ResultCode.UNPAY_DEPOSIT.getMsg());
				return result;
			}
			if(!balance) {
				result.setPageCode(PageCode.RECHARGE_PAGE.getValue().intValue());
				result.setResultCode(ResultCode.ARREARAGE.getValue().intValue());
				result.setResultMsg(ResultCode.ARREARAGE.getMsg());
				return result;
			}
		}
		return null;
	}

	@Override
	public ScanQrResult scanQrcode(BaseReq param) {
		logger.info("==>scanQrcode，租伞界面传入参数[param={}]......", JSON.toJSONString(param));
		ScanQrResult result = new ScanQrResult();
		String token = param.getToken();
		String sceneStr = param.getSceneStr();
		result.setTimestamp(System.currentTimeMillis());
		result.setToken(token);
		if (!StringUtils.isAnyBlank(token, sceneStr)) {
			sceneStr = sceneStr.substring(sceneStr.lastIndexOf("/") + 1);
			ScanQrResult checkResult = this.checkCondition(token, sceneStr);
			if(checkResult != null) {
				checkResult.setTimestamp(System.currentTimeMillis());
				checkResult.setToken(token);
				return checkResult;
			} else {
				String actionOpertion = ""; // from sceneStr action
				String onAndOffCodeStr = sceneStr.substring(9);
				if (StringUtils.isNotBlank(onAndOffCodeStr)) {
					if (Constants.MACHINE_CHANNCEL_ON.contains(onAndOffCodeStr)) {
						actionOpertion = Constants.SCAN_QR_OPERATION.RENT;
					} else if (Constants.MACHINE_CHANNCEL_OFF.contains(onAndOffCodeStr)) {
						actionOpertion = Constants.SCAN_QR_OPERATION.RETURN;
					} else {
						actionOpertion = Constants.SCAN_QR_OPERATION.ERROR;
					}
				}
				
				// 判断二维码所属机器是否存在
				String channcelCode = sceneStr.substring(0, 9);
				String machineCode = channcelCode.replaceFirst("c", "m");
				SysMachineEntity machine = machineService.queryMachineByStatus(machineCode, MachineStatus.ON_LINE.getValue().intValue());
				if (machine == null) {
					result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
					result.setResultCode(ResultCode.ERROR_MACHINE_STATUS.getValue().intValue());
					result.setResultMsg(ResultCode.ERROR_MACHINE_STATUS.getMsg());
					return result;
				}
				
				//绑定微信用户所属代理:未绑定，则绑定
				if(machine != null) {
					int count = wxUserService.selectCount(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", token).isNull("dept_id"));
					Wrapper<SysWxUserEntity> wrapper = new EntityWrapper<SysWxUserEntity>();
					wrapper.eq("mina_openid", token);
					SysWxUserEntity wxUser = new SysWxUserEntity();
					if(count > 0) {
						wxUser.setDeptId(machine.getDeptId().longValue());
						wxUserService.update(wxUser, wrapper);
					}
				}
				
				Integer[] statusArray = {OrderStatus.RETURN_UN_PAY.getValue().intValue(), OrderStatus.TO_BE_RETURN.getValue().intValue()};
				List<SysOrderEntity> orderList = orderService.queryListByStatus(param.getToken(), statusArray);
				if (orderList != null && !orderList.isEmpty() && orderList.size() > 0) {
					if (Constants.SCAN_QR_OPERATION.RETURN.equalsIgnoreCase(actionOpertion)) {
						// 有未归还的雨伞
						result.setPageCode(PageCode.ORDER_DETAIL_PAGE.getValue().intValue());
						result.setOrderId(orderList.get(0).getId().longValue());
						result.setResultCode(ResultCode.TO_BE_RETURN.getValue().intValue());
						result.setResultMsg(ResultCode.TO_BE_RETURN.getMsg());
					} else if (Constants.SCAN_QR_OPERATION.RENT.equalsIgnoreCase(actionOpertion)) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setOrderId(orderList.get(0).getId().longValue());
						result.setResultCode(ResultCode.ERROR_CHANNCEL_OFF.getValue().intValue());
						result.setResultMsg(ResultCode.ERROR_CHANNCEL_OFF.getMsg());
					} else {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setOrderId(orderList.get(0).getId().longValue());
						result.setResultCode(ResultCode.ERROR_QRCODE.getValue().intValue());
						result.setResultMsg(ResultCode.ERROR_QRCODE.getMsg());
					}
					return result;
				} else {
					if (Constants.SCAN_QR_OPERATION.RENT.equalsIgnoreCase(actionOpertion)) {
						result.setPageCode(PageCode.MACHINE_CHANNCEL_RENT_PAGE.getValue().intValue());
						result.setResultCode(ResultCode.OK.getValue().intValue());
						result.setResultMsg(ResultCode.OK.getMsg());
					} else if (Constants.SCAN_QR_OPERATION.RETURN.equalsIgnoreCase(actionOpertion)) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(ResultCode.ERROR_CHANNCEL_ON.getValue().intValue());
						result.setResultMsg(ResultCode.ERROR_CHANNCEL_ON.getMsg());
						return result;
					} else {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(ResultCode.ERROR_QRCODE.getValue().intValue());
						result.setResultMsg(ResultCode.ERROR_QRCODE.getMsg());
						return result;
					}
				}
			}
		}
		return result;
	}
	
	@Override
	public RentPageResp toRentPage(BaseReq param) {
		RentPageResp result = new RentPageResp();
		// 押金
		SysDepositRuleEntity depositRule = depositRuleService.queryDepositRuleByMap(new HashMap<String, Object>());
		
		// 租金
		SysRentRuleEntity rentRule = rentRuleService.queryRentRuleByMap(new HashMap<String, Object>());

		DepositRuleResp depositRuleResp = new DepositRuleResp();
		RentRuleResp rentRuleResp = new RentRuleResp();

		// 2. 雨伞属性（租金规则）
		rentRuleResp.setId(rentRule.getId().longValue());
		rentRuleResp.setFreeType(rentRule.getFreeType());
		rentRuleResp.setFreeValue(rentRule.getFreeValue().intValue());
		rentRuleResp.setPerTypeValue(rentRule.getPerTypeValue().intValue());
		rentRuleResp.setRentType(rentRule.getRentType().intValue());
		rentRuleResp.setRentValue(rentRule.getRentValue());

		// 3.押金规则
		depositRuleResp.setId(depositRule.getId().longValue());
		depositRuleResp.setDepositPrice(depositRule.getDepositPrice());

		result.setRentRule(rentRuleResp);
		result.setDepositRule(depositRuleResp);
		return result;
	}
	
	@Override
	public RentResultResp immediatelyRent(ImmediatelyRentReq param) {
		logger.info("==>immediatelyRent，立即租伞传入参数[param={}]......", JSON.toJSONString(param));
		RentResultResp result = new RentResultResp();
		Date date = new Date();
		String token = param.getToken();
		String sceneStr = param.getSceneStr();
		result.setToken(token);
		result.setTimestamp(System.currentTimeMillis());
		if(!StringUtils.isAnyBlank(token, sceneStr)) {
			sceneStr = sceneStr.substring(sceneStr.lastIndexOf("/") + 1);
			TranslateSceneStr translateSceneStr = machineService.translateSceneStr(sceneStr);
			SysMachineEntity machine = machineService.selectOne(new EntityWrapper<SysMachineEntity>().eq("machine_code", translateSceneStr.getMachineCode()));
			
			//绑定微信用户所属代理:未绑定，则绑定
			if(machine != null) {
				int count = wxUserService.selectCount(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", token).isNull("dept_id"));
				Wrapper<SysWxUserEntity> wrapper = new EntityWrapper<SysWxUserEntity>();
				wrapper.eq("mina_openid", token);
				SysWxUserEntity wxUser = new SysWxUserEntity();
				if(count > 0) {
					wxUser.setDeptId(machine.getDeptId().longValue());
					wxUserService.update(wxUser, wrapper);
				}
			}
			
			List<SysOrderEntity> orderList = orderService.queryListByStatus(param.getToken(), OrderStatus.WAIT_TAKE_OUT.getValue().intValue());
			SysOrderEntity order = new SysOrderEntity();
			if (orderList != null && !orderList.isEmpty() && orderList.size() > 0) {
				order = orderList.get(0);
			} else {
				order.setOpenid(token);
				order.setRentMachineCode(machine.getMachineCode());
				order.setDeptId(machine.getDeptId().longValue());
				order.setRentPlace(machine.getLocation());
				order.setChipCode("unuse");
				order.setCreateTime(date);
				order.setUpdateTime(date);
				order.setOrderStatus(OrderStatus.WAIT_TAKE_OUT.getValue().intValue());
				orderService.insert(order);
				List<SysOrderEntity> orderListTemp = orderService.queryListByStatus(param.getToken(), OrderStatus.WAIT_TAKE_OUT.getValue().intValue());
				if (orderListTemp != null && !orderListTemp.isEmpty() && orderListTemp.size() > 0) {
					order = orderListTemp.get(0);
				}
			}
			
			IotReq iotReq = iotService.translateIotReq(token, translateSceneStr, order);;
			if(iotReq != null && Constants.SCAN_QR_OPERATION.RENT.equalsIgnoreCase(iotReq.getOper())) {
				JSONObject jsonObject = iotService.operDevice(iotReq);
				if(order != null && order.getId() != null && jsonObject != null) {
					result.setId(order.getId().longValue());
					if(!jsonObject.getBooleanValue("flag")) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(AliyunIotStatus.RESULT_FLAG_FALSE.getValue().intValue());
						result.setResultMsg(AliyunIotStatus.RESULT_FLAG_FALSE.getMsg());
						return result;
					} else {
						jsonObject = jsonObject.getJSONObject("data");
					}
					
					SysIotCallbackLogEntity iotCallbackLog = new SysIotCallbackLogEntity();
					iotCallbackLog.setProductKey(jsonObject.getString("productKey"));
					iotCallbackLog.setDeviceName(jsonObject.getString("deviceName"));
					iotCallbackLog.setOpenid(token);
					iotCallbackLog.setMessageType(Constants.SCAN_QR_OPERATION.RENT);
					iotCallbackLog.setReqTime(date);
					if(jsonObject != null && jsonObject.getLongValue("time") != 0) {
						iotCallbackLog.setRespTime(new DateTime(jsonObject.getLongValue("time")));
					}
					iotCallbackLog.setReqStr(JSON.toJSONString(iotReq));
					iotCallbackLog.setRespResult(JSON.toJSONString(jsonObject));
					iotCallbackLog.setCreateTime(date);
					iotCallbackLog.setOrderId(order.getId().longValue() + "");
					iotCallbackLogService.insert(iotCallbackLog);
					
					if(2 == jsonObject.getIntValue("operResult")) {
						if(0 == jsonObject.getIntValue("umbrellaId")) {
							result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
							result.setResultCode(AliyunIotStatus.TAKE_OUT_NOT_ACTION.getValue().intValue());
							result.setResultMsg(AliyunIotStatus.TAKE_OUT_NOT_ACTION.getMsg());
							return result;
						}else {
							result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
							result.setResultCode(AliyunIotStatus.TAKE_OUT_TIMEOUT.getValue().intValue());
							result.setResultMsg(AliyunIotStatus.TAKE_OUT_TIMEOUT.getMsg());
							return result;
						}
					}else if(3 == jsonObject.getIntValue("operResult")) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(AliyunIotStatus.MACHINE_NOT_ONLINE.getValue().intValue());
						result.setResultMsg(AliyunIotStatus.MACHINE_NOT_ONLINE.getMsg());
						return result;
					}else if(4 == jsonObject.getIntValue("operResult")) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(AliyunIotStatus.DUPLICATE_OPERATION.getValue().intValue());
						result.setResultMsg(AliyunIotStatus.DUPLICATE_OPERATION.getMsg());
						return result;
					}
			
					logger.info("==>immediatelyRent 开锁返回结果：" + jsonObject.toJSONString());
					//返回结果示例：
					//1.超时结果
					//{"msg":"操作成功","flag":true,"data":"{\"deviceName\":\"949897849160343553\",\"oper\":\"RENT\",\"orderId\":\"949647681836146689\",\"productKey\":\"4o5HzG9GaWx\",\"time\":1515248625,\"umbrellaRoadId\":\"1\",\"userId\":\"oQVv00GLqbTlufpgYtfJWvMH6Q6A\",\"operResult\":2,\"umbrellaId\":0}"}
					//2.正常结果
					//String temp = "{\"deviceName\": \"949897849160343553\",\"orderId\":\"949647681836146689\",\"oper\": \"RENT\",\"productKey\": \"4o5HzG9GaWx\",\"time\": 1515235397,\"umbrellaRoadId\": \"1\", \"userId\": \"oQVv00GLqbTlufpgYtfJWvMH6Q6A\", \"operResult\": 2,\"umbrellaId\": 9103543}";
					//jsonObject = JSONObject.parseObject(temp);
					//IotResp iotResp = JSON.toJavaObject(jsonObject.getJSONObject("data"), IotResp.class);
					IotResp iotResp = JSON.toJavaObject(jsonObject, IotResp.class);
						if (iotResp != null && "1".equals(iotResp.getOperResult())) {
							SysUmbrellaEntity umbrella = umbrellaService.selectOne(new EntityWrapper<SysUmbrellaEntity>().eq("chip_code", iotResp.getUmbrellaId()));
							logger.info("==>immediatelyRent 通过芯片获取雨伞对象[umbrella={}]......", JSON.toJSONString(umbrella));
								if (umbrella != null && UmbrellaStatus.FREE.getValue().intValue() == umbrella.getUmbrellaStatus().intValue()) {
									order.setChipCode(umbrella.getChipCode());
									order.setOrderStatus(OrderStatus.TO_BE_RETURN.getValue().intValue());
									order.setRentTime(date);
									orderService.updateById(order);
									logger.info("==>immediatelyRent 更新订单order，插入结果为[order={}]......", JSON.toJSONString(order));
									
									umbrella.setUmbrellaStatus(UmbrellaStatus.RENT_OUT.getValue().intValue());
									umbrellaService.updateById(umbrella);
									
									result.setResultCode(ResultCode.MACHINE_CHANNCEL_RENT_PAGE_SUC.getValue().intValue());
									
									// 更新机器上的伞的数量-减
									if(machine != null && machine.getLeftQuantity() != null) {
										int leftQuantity = machine.getLeftQuantity().intValue()- 1;
										machine.setLeftQuantity(leftQuantity);
										machineService.updateById(machine);
									}
									
									try {
										this.logger.info("==>sendTemplateMessage请求参数：[" + JSON.toJSONString(param) + "], 模版id=" + Constants.RENT_TEMPLATE);
										/* 	租借时间{{keyword1.DATA}}
											租借地点{{keyword2.DATA}}
											订单号{{keyword3.DATA}}*/
										WxMaTemplateMessage templateMessage = WxMaTemplateMessage.builder().toUser(order.getOpenid()).templateId(Constants.RENT_TEMPLATE)
												.formId(param.getFromId()).data(
														Lists.newArrayList(
									                            new WxMaTemplateMessage.Data("keyword1", DateUtil.format(order.getRentTime(), DatePattern.NORM_DATETIME_PATTERN) + "", "#173177"),
									                            new WxMaTemplateMessage.Data("keyword2", order.getRentPlace(), "#173177"),
									                            new WxMaTemplateMessage.Data("keyword3", order.getId().longValue() + "", "#173177"),
									                            new WxMaTemplateMessage.Data("keyword8", "点击查看详情", "#173177")
														)).build();
										this.logger.info("==>sendTemplateMessage请求参数：[" + JSON.toJSONString(templateMessage) + "], 模版id=" + Constants.RENT_TEMPLATE);
										wxService.getMsgService().sendTemplateMsg(templateMessage);
										
									} catch (WxErrorException e) {
										this.logger.error("==>sendTemplateMessage错误：[" + e.getMessage() + "], 模版id=" + Constants.RENT_TEMPLATE);
									}
								} else {
									result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
									result.setResultCode(ResultCode.ERROR_UMBRELLA_STATUS.getValue().intValue());
									result.setResultMsg("当前雨伞:" + UmbrellaStatus.getMsg(umbrella.getUmbrellaStatus().intValue()));
									return result;
								}
						} 
						result.setResultMsg(iotResp.getOperResult());
						result.setPageCode(PageCode.MACHINE_CHANNCEL_RENT_PAGE.getValue().intValue());
				} else {
					// 异常处理，失败处理
				}	
			}
		}else {
			this.logger.error("==>immediatelyRent，非法参数，[token={"+ token +"}], [scene_str={"+ sceneStr +"}]......不能为空");
		}
		return result;
	}
	
	@Transactional
	@Override
	public ReturnResultResp immediatelyReturn(ImmediatelyReturnReq param) {
		logger.info("==>immediatelyReturn，立即还伞传入参数[param={}]......", JSON.toJSONString(param));
		ReturnResultResp result = new ReturnResultResp();
		Date date = new Date();
		String offStr = Constants.MACHINE_CHANNCEL_OFF;
		String token = param.getToken();
		String sceneStr = param.getSceneStr();
		String orderId = param.getId().longValue() + "";
		result.setToken(token);
		result.setTimestamp(System.currentTimeMillis());
		if(!StringUtils.isAnyBlank(token, sceneStr) && StringUtils.isNotEmpty(orderId)) {
			sceneStr = sceneStr.substring(sceneStr.lastIndexOf("/") + 1);
			//1.判断是否缴押金|余额
			Map<String, Boolean> checkDepositAndBalance = wxUserService.checkDepositAndBalance(token);
			if (checkDepositAndBalance != null) {
				boolean deposit = checkDepositAndBalance.get("deposit");
				boolean balance = checkDepositAndBalance.get("balance");
				if(!deposit) {
					result.setPageCode(PageCode.DEPOSIT_PAGE.getValue().intValue());
					result.setResultCode(ResultCode.UNPAY_DEPOSIT.getValue().intValue());
					result.setResultMsg(ResultCode.UNPAY_DEPOSIT.getMsg());
					return result;
				}
				if(!balance) {
					result.setPageCode(PageCode.RECHARGE_PAGE.getValue().intValue());
					result.setResultCode(ResultCode.ARREARAGE.getValue().intValue());
					result.setResultMsg(ResultCode.ARREARAGE.getMsg());
					return result;
				}
			}
			
			SysOrderEntity order = orderService.selectById(param.getId().longValue());
			// 判断重复还伞
			if(order != null) {
				// 当前订单已经归还，无需重复还伞
				if(OrderStatus.DONE.getValue() == order.getOrderStatus() || OrderStatus.RETURN_UN_PAY.getValue() == order.getOrderStatus()){
					result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue());
					result.setResultCode(ResultCode.ERROR_TO_BE_RETURN.getValue());
					result.setResultMsg(ResultCode.ERROR_TO_BE_RETURN.getMsg());
					return result;
				}					
			} else {
				result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue());
				result.setResultCode(ResultCode.ERROR_CHANNCEL_ON.getValue());
				result.setResultMsg(ResultCode.ERROR_CHANNCEL_ON.getMsg());
				return result;
			}
			
			TranslateSceneStr translateSceneStr = machineService.translateSceneStr(sceneStr);
			IotReq iotReq = iotService.translateIotReturnReq(token, translateSceneStr, order);;
			if(iotReq != null && Constants.SCAN_QR_OPERATION.RETURN.equalsIgnoreCase(iotReq.getOper())) {
				String machineChanncel = iotReq.getUmbrellaRoadId();
				if(!offStr.contains(machineChanncel)) {
					result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
					result.setResultCode(ResultCode.ERROR_CHANNCEL_ON.getValue().intValue());
					result.setResultMsg(ResultCode.ERROR_CHANNCEL_ON.getMsg());
					return result;
				}
				
				JSONObject jsonObject = iotService.operDevice(iotReq);
				if(jsonObject != null) {
					result.setId(order.getId().longValue());
					if(!jsonObject.getBooleanValue("flag")) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(AliyunIotStatus.RESULT_FLAG_FALSE.getValue().intValue());
						result.setResultMsg(AliyunIotStatus.RESULT_FLAG_FALSE.getMsg());
						return result;
					} else {
						jsonObject = jsonObject.getJSONObject("data");
					}
					
					SysIotCallbackLogEntity iotCallbackLog = new SysIotCallbackLogEntity();
					iotCallbackLog.setProductKey(jsonObject.getString("productKey"));
					iotCallbackLog.setDeviceName(jsonObject.getString("deviceName"));
					iotCallbackLog.setOpenid(token);
					iotCallbackLog.setMessageType(Constants.SCAN_QR_OPERATION.RETURN);
					iotCallbackLog.setReqTime(date);
					if(jsonObject != null && jsonObject.getLongValue("time") != 0) {
						iotCallbackLog.setRespTime(new DateTime(jsonObject.getLongValue("time")));
					}
					iotCallbackLog.setReqStr(JSON.toJSONString(iotReq));
					iotCallbackLog.setRespResult(JSON.toJSONString(jsonObject));
					iotCallbackLog.setCreateTime(date);
					iotCallbackLog.setOrderId(order.getId().longValue() + "");
					iotCallbackLogService.insert(iotCallbackLog);
					
					if(2 == jsonObject.getIntValue("operResult")) {
						if(0 == jsonObject.getIntValue("umbrellaId")) {
							result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
							result.setResultCode(AliyunIotStatus.TO_BE_RETURN_NOT_ACTION.getValue().intValue());
							result.setResultMsg(AliyunIotStatus.TO_BE_RETURN_NOT_ACTION.getMsg());
							return result;
						}else {
							result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
							result.setResultCode(AliyunIotStatus.TO_BE_RETURN_TIMEOUT.getValue().intValue());
							result.setResultMsg(AliyunIotStatus.TO_BE_RETURN_TIMEOUT.getMsg());
							return result;
						}
					}else if(3 == jsonObject.getIntValue("operResult")) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(AliyunIotStatus.MACHINE_NOT_ONLINE.getValue().intValue());
						result.setResultMsg(AliyunIotStatus.MACHINE_NOT_ONLINE.getMsg());
						return result;
					}else if(4 == jsonObject.getIntValue("operResult")) {
						result.setPageCode(PageCode.MAP_INDEX_PAGE.getValue().intValue());
						result.setResultCode(AliyunIotStatus.DUPLICATE_OPERATION.getValue().intValue());
						result.setResultMsg(AliyunIotStatus.DUPLICATE_OPERATION.getMsg());
						return result;
					}
					
					logger.info("==>immediatelyReturn 开锁返回结果：" + jsonObject.toJSONString());
					//返回结果示例：
					//1.超时结果
					//{"msg":"操作成功","flag":true,"data":"{\"deviceName\":\"949897849160343553\",\"oper\":\"RENT\",\"orderId\":\"949647681836146689\",\"productKey\":\"4o5HzG9GaWx\",\"time\":1515248625,\"umbrellaRoadId\":\"1\",\"userId\":\"oQVv00GLqbTlufpgYtfJWvMH6Q6A\",\"operResult\":2,\"umbrellaId\":0}"}
					//2.正常结果
					//String temp = "{\"deviceName\": \"949897849160343553\",\"orderId\":\"949647681836146689\",\"oper\": \"RENT\",\"productKey\": \"4o5HzG9GaWx\",\"time\": 1515235397,\"umbrellaRoadId\": \"1\", \"userId\": \"oQVv00GLqbTlufpgYtfJWvMH6Q6A\", \"operResult\": 2,\"umbrellaId\": 9103543}";
					//jsonObject = JSONObject.parseObject(temp);
					//IotResp iotResp = JSON.toJavaObject(jsonObject.getJSONObject("data"), IotResp.class);
					IotResp iotResp = JSON.toJavaObject(jsonObject, IotResp.class);
					SysMachineEntity machine = machineService.selectOne(new EntityWrapper<SysMachineEntity>().eq("device_name", iotResp.getDeviceName()).eq("product_key", iotResp.getProductKey()));
					/*1.成功  借伞成功/还伞
					2.失败  
					1）伞ID为0，结果为2，用户没有拿伞导致超时，提示：没操作
					2）伞ID不为0，结果为2，已经读伞没有拿出来，提示：取伞超时
					3.设备不在线（单边机不在线）
					4.多人操作，请等待
					*/
					if("1".equals(iotResp.getOperResult())) {
						result.setResultCode(ResultCode.MACHINE_CHANNCEL_RETURN_PAGE_SUC.getValue().intValue());
						order.setOrderStatus(OrderStatus.RETURN_UN_PAY.getValue().intValue());
						order.setReturnMachineCode(machine.getMachineCode());
						order.setReturnPlace(machine != null ? machine.getLocation() : "");
						order.setReturnTime(date);
						order.setUpdateTime(date);
						boolean updateOrderResult = orderService.updateById(order);
						logger.info("==>immediatelyReturn 更新订单order，插入结果为[order={}]......", JSON.toJSONString(order));
						if(updateOrderResult) {
							String chipCode = order.getChipCode();
							SysUmbrellaEntity umbrella = umbrellaService.selectOne(new EntityWrapper<SysUmbrellaEntity>().eq("chip_code", chipCode));
							umbrella.setUmbrellaStatus(UmbrellaStatus.FREE.getValue().intValue());
							umbrella.setMachineId(machine != null ? machine.getId().longValue() : null);
							umbrellaService.updateById(umbrella);
							logger.info("==>immediatelyReturn 更新雨伞，插入结果为[umbrella={}]......", JSON.toJSONString(umbrella));
						}
						
						// 更新机器上的伞的数量-加
						if(machine != null && machine.getLeftQuantity() != null) {
							int leftQuantity = machine.getLeftQuantity().intValue() + 1;
							machine.setLeftQuantity(leftQuantity);
							machineService.updateById(machine);
						}
												
						// 扣余额操作：start
						orderService.calculateOrder(order); // 计算订单费用
						BigDecimal totalPrice = order.getTotalPrice();
						if (totalPrice != null && totalPrice.compareTo(BigDecimal.ZERO) > 0) {
							SysWxUserEntity wxUser = wxUserService.queryByMinaOpenid(token);
							BigDecimal balance =NumberUtil.sub(wxUser.getBalance(), totalPrice);
							wxUser.setBalance(balance);
							wxUser.setUpdateTime(new Date());
							wxUserService.updateById(wxUser);
							order.setOrderStatus(OrderStatus.DONE.getValue().intValue());
							order.setUpdateTime(date);
						} else {
							order.setOrderStatus(OrderStatus.DONE.getValue().intValue());
							order.setUpdateTime(date);
						}
						orderService.updateById(order);
						SysPaymentLogEntity paymentLog = new SysPaymentLogEntity();
						paymentLog.setOrderId(order.getId().longValue());
						paymentLog.setOpenid(token);
						paymentLog.setTradeNo(IdWorker.getId() + ""); // 财付通订单号, 此处非财付通订单号
						paymentLog.setPayStatus(PayLogStatus.PAY.getValue().intValue());
						paymentLog.setTradeType(TradeType.BIZ_CONSUME_PAY.getValue().intValue());
						paymentLog.setPayPrice(totalPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
						paymentLog.setRemark(order.getOpenid() + TradeType.BIZ_CONSUME_PAY.getMsg() + " 金额=" + paymentLog.getPayPrice());
						paymentLog.setCreateTime(date);
						paymentLog.setUpdateTime(date);
						paymentLogService.insert(paymentLog);
						// 扣余额操作: end
						
						try {
							this.logger.info("==>sendTemplateMessage请求参数：[" + JSON.toJSONString(param) + "], 模版id=" + Constants.RETURN_TEMPLATE);
/*								订单号{{keyword1.DATA}}
								租借时长{{keyword2.DATA}}
								产生费用{{keyword3.DATA}}
								归还时间{{keyword4.DATA}}
								归还地点{{keyword5.DATA}}
								租借时间{{keyword6.DATA}}
								租借地点{{keyword7.DATA}}
								备注{{keyword8.DATA}}*/
							WxMaTemplateMessage templateMessage = WxMaTemplateMessage.builder().toUser(order.getOpenid()).templateId(Constants.RETURN_TEMPLATE)
									.formId(param.getFromId()).data(
											Lists.newArrayList(
						                            new WxMaTemplateMessage.Data("keyword1", order.getId().longValue() + "", "#173177"),
						                            new WxMaTemplateMessage.Data("keyword2", DateUtil.between(order.getRentTime(), order.getReturnTime(), DateUnit.MINUTE, Boolean.TRUE) + "", "#173177"),
						                            new WxMaTemplateMessage.Data("keyword3", totalPrice + "", "#173177"),
						                            new WxMaTemplateMessage.Data("keyword4", DateUtil.format(order.getReturnTime(), DatePattern.NORM_DATETIME_PATTERN) + "", "#173177"),
						                            new WxMaTemplateMessage.Data("keyword5", order.getReturnPlace(), "#173177"),
						                            new WxMaTemplateMessage.Data("keyword6", DateUtil.format(order.getRentTime(), DatePattern.NORM_DATETIME_PATTERN) + "", "#173177"),
						                            new WxMaTemplateMessage.Data("keyword7", order.getRentPlace(), "#173177"),
						                            new WxMaTemplateMessage.Data("keyword8", "点击查看详情", "#173177")
											)).build();
							this.logger.info("==>sendTemplateMessage请求参数：[" + JSON.toJSONString(templateMessage) + "], 模版id=" + Constants.RETURN_TEMPLATE);
							wxService.getMsgService().sendTemplateMsg(templateMessage);
						} catch (WxErrorException e) {
							this.logger.error("==>sendTemplateMessage错误：[" + e.getMessage() + "], 模版id=" + Constants.RETURN_TEMPLATE);
						}
					}
					result.setResultMsg(iotResp.getOperResult());
					result.setPageCode(PageCode.MACHINE_CHANNCEL_RETURN_PAGE.getValue().intValue());
					result.setId(order.getId().longValue());
				} else {
					// 异常处理，失败处理
				}	
			}
		} else {
			this.logger.error("==>immediatelyReturn，非法参数，[token={"+ token +"}], [scene_str={"+ sceneStr +"}]， [orderId = {" + orderId + "}]......不能为空");
		}
		return result;
	}

	
	@Override
	public RentPageResp rentSuc(OrderDetailReq param) {
		logger.info("==>rentSuc，成功租伞界面传入参数[param={}]......", JSON.toJSON(param));
		RentPageResp result = new RentPageResp();
		//result.setTimestamp(System.currentTimeMillis());
		//result.setToken(param.getToken());
		if (StringUtils.isNotBlank(param.getToken()) && param.getOrderId() != 0l) {
			SysOrderEntity order = orderService.selectById(param.getOrderId());
			if (order != null) {
				// 押金
				SysDepositRuleEntity depositRule = depositRuleService.queryDepositRuleByMap(new HashMap<String, Object>());
				
				// 租金
				SysRentRuleEntity rentRule = rentRuleService.queryRentRuleByMap(new HashMap<String, Object>());
				
				DepositRuleResp depositRuleResp = new DepositRuleResp();
				RentRuleResp rentRuleResp = new RentRuleResp();

				// 2. 雨伞属性（租金规则）
				rentRuleResp.setId(rentRule.getId().longValue());
				rentRuleResp.setFreeType(rentRule.getFreeType());
				rentRuleResp.setFreeValue(rentRule.getFreeValue().intValue());
				rentRuleResp.setPerTypeValue(rentRule.getPerTypeValue().intValue());
				rentRuleResp.setRentType(rentRule.getRentType().intValue());
				rentRuleResp.setRentValue(rentRule.getRentValue());

				// 3.押金规则
				depositRuleResp.setId(depositRule.getId().longValue());
				depositRuleResp.setDepositPrice(depositRule.getDepositPrice());

				result.setRentRule(rentRuleResp);
				result.setDepositRule(depositRuleResp);
			}
			// 1. 填充公共参数
			//result.setPageCode(PageCode.MACHINE_CHANNCEL_RENT_PAGE.getValue().intValue());

		}
		return result;
	}
	

	@Override
	public OrderDetailResp returnSuc(OrderDetailReq param) {
		return orderService.orderDetail(param);
	}

	@Override
	public OrderDetailResp orderDetail(OrderDetailReq param) {
		return orderService.orderDetail(param);
	}

	@Override
	public PageUtils queryOrderPage(Map<String, Object> params) {
		return orderService.queryPage(params);
	}
	
	@Override
	public PageUtils queryRechargeCardPage(Map<String, Object> params) {
		return rechargeCardService.queryPage(params);
	}
	
	@Override
	public DepositRuleResp queryDepositRuleInfo(Map<String, Object> params) {
		DepositRuleResp depositRule = new DepositRuleResp();
		SysDepositRuleEntity result = depositRuleService.queryDepositRuleByMap(params);
		if(result != null) {
			depositRule.setId(result.getId().longValue());
			depositRule.setDepositPrice(result.getDepositPrice());
		}
		return depositRule;
	}

	@Override
	public PageUtils queryPaymentLogPage(Map<String, Object> params) {
		return paymentLogService.queryPage(params);
	}

	@Override
	public PageUtils queryNearByMachinePage(Map<String, Object> params) {
		return machineService.queryNearByMachinePage(params);
	}

	@Override
	public List<AboutUsResp> aboutUs() {
		List<AboutUsResp> result = new ArrayList<AboutUsResp>();
		List<SysDictEntity> dictList = dictService.queryDicByDitType("about");
		for(SysDictEntity dict : dictList){
			AboutUsResp about = new AboutUsResp();
			about.setId(dict.getId().longValue());
			about.setName(dict.getName());
			about.setCode(dict.getCode());
			about.setType(dict.getType());
			about.setValue(dict.getValue());
			about.setOrderNum(dict.getOrderNum());
			result.add(about);
		}
		return result;
	}
}
