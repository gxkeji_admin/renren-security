package io.renren.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.dao.SysRechargeCardDao;
import io.renren.entity.SysRechargeCardEntity;
import io.renren.service.RechargeCardService;


@Service("sysRechargeCardService")
public class RechargeCardServiceImpl extends ServiceImpl<SysRechargeCardDao, SysRechargeCardEntity> implements RechargeCardService {

	@DataFilter(wxUser = false)
    @Override
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysRechargeCardEntity> page = this.selectPage(new Query<SysRechargeCardEntity>(params).getPage(),
				new EntityWrapper<SysRechargeCardEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));

		return new PageUtils(page);
	}
}
