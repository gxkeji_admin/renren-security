package io.renren.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.hutool.core.util.NumberUtil;
import io.renren.annotation.DataFilter;
import io.renren.common.Constants;
import io.renren.common.Constants.SCAN_QR_OPERATION;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.dao.MachineDao;
import io.renren.emum.MachineStatus;
import io.renren.entity.SysMachineEntity;
import io.renren.modules.request.MapHomeReq;
import io.renren.modules.request.TranslateSceneStr;
import io.renren.service.MachineChanncelService;
import io.renren.service.MachineService;


@Service("sysMachineService")
public class MachineServiceImpl extends ServiceImpl<MachineDao, SysMachineEntity> implements MachineService {

	@Autowired
	private MachineChanncelService machineChanncelService;
	
    @Override
    public PageUtils queryNearByMachinePage(Map<String, Object> params) {
		BigDecimal latitude = null;
		BigDecimal longitude = null;
		Wrapper<SysMachineEntity> wrapper = new EntityWrapper<SysMachineEntity>();
		if (params != null) {
			if (params.get("latitude") != null) {
				latitude = new BigDecimal(params.get("latitude").toString());
		        if(latitude != null) {
		        	wrapper.ge(latitude != null,"latitude", latitude.subtract(BigDecimal.valueOf(0.1)));
		        	wrapper.le(latitude != null,"latitude", latitude.add(BigDecimal.valueOf(0.1)));
		        }
			}
			if (params.get("longitude") != null) {
				longitude = new BigDecimal(params.get("longitude").toString());
		        if(longitude != null) {
		        	wrapper.ge(longitude != null,"longitude", longitude.subtract(BigDecimal.valueOf(0.1)));
		        	wrapper.le(longitude != null,"longitude", longitude.add(BigDecimal.valueOf(0.1)));
		        }
			}
			wrapper.addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER));
		}
        Page<SysMachineEntity> page = this.selectPage(new Query<SysMachineEntity>(params).getPage(), wrapper);
        return new PageUtils(page);
    }
    
	private String getNextMachineCode() {
		String nextMachineCode = "";
		Wrapper<SysMachineEntity> wrapper = new EntityWrapper<SysMachineEntity>();
		wrapper.orderBy("machine_code", false);
		List<SysMachineEntity> machines = this.selectList(wrapper);
		if (machines != null && !machines.isEmpty() && machines.size() > 0) {
			SysMachineEntity machine = machines.get(0);
			if (machine != null && StringUtils.isNotEmpty(machine.getMachineCode())) {
				String temp = StringUtils.substringAfter(machine.getMachineCode(), Constants.MACHINE_CODE_PREFIX);
				if (NumberUtil.isNumber(temp)) {
					long nextCode = Long.valueOf(temp).longValue() + 1L;
					nextMachineCode = Constants.MACHINE_CODE_PREFIX + nextCode;
				}
			}
		}else {
			nextMachineCode = Constants.MACHINE_CODE_PREFIX + Constants.MACHINE_QR_CONTENT;
		}
		return nextMachineCode;
	}

	@Override
	public SysMachineEntity queryMachineByStatus(String machineCode, int machineStatus) {
		return this.selectOne(new EntityWrapper<SysMachineEntity>().eq("machine_code", machineCode).eq("machine_status", machineStatus));
	}

	@Override
	public List<SysMachineEntity> queryList(MapHomeReq param) {
		Wrapper<SysMachineEntity> wrapper = new EntityWrapper<SysMachineEntity>();
		if(param != null && param.getLatitude() != null && param.getLongitude() != null) {
			wrapper.ge("latitude", param.getLatitude().subtract(BigDecimal.valueOf(0.1)));
			wrapper.le("latitude", param.getLatitude().add(BigDecimal.valueOf(0.1)));
			wrapper.ge("longitude", param.getLongitude().subtract(BigDecimal.valueOf(0.1)));
			wrapper.le("longitude", param.getLongitude().add(BigDecimal.valueOf(0.1)));
			wrapper.eq("machine_status", MachineStatus.ON_LINE.getValue().intValue());
		} else {
			wrapper.last("limit 10");
		}
		return super.selectList(wrapper);
	}
	
	@DataFilter(wxUser = false)
	@Override
	public List<SysMachineEntity> queryListByMap(Map<String, Object> params) {
		Wrapper<SysMachineEntity> wrapper = new EntityWrapper<SysMachineEntity>();
		if(params != null && params.get("latitude") != null && params.get("longitude") != null) {
			wrapper.ge("latitude", ((BigDecimal) params.get("latitude")).subtract(BigDecimal.valueOf(0.1)));
			wrapper.le("latitude", ((BigDecimal) params.get("latitude")).add(BigDecimal.valueOf(0.1)));
			wrapper.ge("longitude", ((BigDecimal) params.get("longitude")).subtract(BigDecimal.valueOf(0.1)));
			wrapper.le("longitude", ((BigDecimal) params.get("longitude")).add(BigDecimal.valueOf(0.1)));
			wrapper.eq("machine_status", MachineStatus.ON_LINE.getValue().intValue());
			wrapper.addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String) params.get(Constant.SQL_FILTER));
		} else {
			wrapper.last("limit 10");
		}
		return super.selectList(wrapper);
	}

	@Override
	public TranslateSceneStr translateSceneStr(String sceneStr) {
		TranslateSceneStr translate = new TranslateSceneStr();
		if (StringUtils.isNotBlank(sceneStr)) {
			if (sceneStr.length() >= 9) {
				String channcelCode = sceneStr.substring(0, 9);
				String machineCode = channcelCode.replaceFirst("c", "m");
				String onAndOffCodeStr = sceneStr.substring(9);
				String actionOper = "";
				if (StringUtils.isNotBlank(onAndOffCodeStr)) {
					if (Constants.MACHINE_CHANNCEL_ON.contains(onAndOffCodeStr)) {
						actionOper = SCAN_QR_OPERATION.RENT;
					} else if (Constants.MACHINE_CHANNCEL_OFF.contains(onAndOffCodeStr)) {
						actionOper = SCAN_QR_OPERATION.RETURN;
					} else {
						actionOper = SCAN_QR_OPERATION.ERROR;
					}
				}
				translate.setMachineCode(machineCode);
				translate.setMachineChanncel(onAndOffCodeStr);
				translate.setActionOperation(actionOper);
			}
		}
		return translate;
	}
}
