/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.renren.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.dao.SysDictDao;
import io.renren.entity.SysDictEntity;
import io.renren.service.DictService;


@Service("sysDictService")
public class SysDictServiceImpl extends ServiceImpl<SysDictDao, SysDictEntity> implements DictService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String type = (String)params.get("type");

        Page<SysDictEntity> page = this.selectPage(
                new Query<SysDictEntity>(params).getPage(),
                new EntityWrapper<SysDictEntity>().eq("del_flag", 0)
                    .like(StringUtils.isNotBlank(type),"type", type)
        );

        return new PageUtils(page);
    }
    
    @Override
	public Map<String, String> queryDicByType(String type) {
		List<SysDictEntity> list = this.queryDicByDitType(type);
		Map<String, String> resultMap = new HashMap<String, String>();
		for (SysDictEntity sysDictEntity : list) {
			resultMap.put(sysDictEntity.getCode(), sysDictEntity.getValue());
		}
		return resultMap;
	}
    
    @Override
	public List<SysDictEntity> queryDicByDitType(String type) {
		List<SysDictEntity> result = this.selectList(new EntityWrapper<SysDictEntity>().eq("type", type).eq("del_flag", 0).orderBy("order_num", Boolean.TRUE));
		return result;
	}
}
