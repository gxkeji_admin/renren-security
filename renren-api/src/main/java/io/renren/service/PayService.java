package io.renren.service;

import java.util.Map;

import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.service.WxPayService;

import io.renren.modules.BaseParam;
import io.renren.modules.request.DepositPayReq;
import io.renren.modules.response.RechargePayReq;
import io.renren.modules.response.RefundResultResp;

public interface PayService extends WxPayService {
	
	Map<String, String> payDeposit(DepositPayReq param);
	
	Map<String, String> payRecharge(RechargePayReq param);
	
	RefundResultResp refundDeposit(BaseParam param);
	
	WxPayOrderNotifyResult parseOrderNotifyResult(String xmlData);
}
