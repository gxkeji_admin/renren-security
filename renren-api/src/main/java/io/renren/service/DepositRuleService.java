package io.renren.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.entity.SysDepositRuleEntity;

/**
 * 押金规则
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-30 02:50:36
 */
public interface DepositRuleService extends IService<SysDepositRuleEntity> {

    SysDepositRuleEntity queryDepositRuleByDeptId(long deptId);
    
    SysDepositRuleEntity queryDepositRuleByMap(Map<String, Object> params);
}

