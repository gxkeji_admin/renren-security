package io.renren.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.entity.SysRentRuleEntity;

/**
 * 租金规则
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-29 15:47:22
 */
public interface RentRuleService extends IService<SysRentRuleEntity> {

    SysRentRuleEntity queryRentRuleByDeptId(long deptId);
    
    SysRentRuleEntity queryRentRuleByMap(Map<String, Object> params);
}

