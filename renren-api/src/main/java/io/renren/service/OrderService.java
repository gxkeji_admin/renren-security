package io.renren.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysOrderEntity;
import io.renren.modules.request.OrderDetailReq;
import io.renren.modules.response.OrderDetailResp;

/**
 * 订单
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface OrderService extends IService<SysOrderEntity> {
	
    PageUtils queryPage(Map<String, Object> params);
    
    OrderDetailResp orderDetail(OrderDetailReq param);
    //SysOrderEntity orderDetail(OrderDetailReq param);
    
    OrderDetailResp orderLost(OrderDetailReq params);
    
	void calculateOrder(SysOrderEntity order);
    
    List<SysOrderEntity> queryListByStatus(String openid, int orderStatus);
    
    List<SysOrderEntity> queryListByStatus(String openid, Integer array[]);
}

