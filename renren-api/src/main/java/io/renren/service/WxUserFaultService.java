package io.renren.service;

import com.baomidou.mybatisplus.service.IService;

import io.renren.entity.SysWxUserFaultEntity;

/**
 * 故障表
 *
 * @author oslive
 * @email apple_live@qq.com
 * @date 2018-05-22 23:55:47
 */
public interface WxUserFaultService extends IService<SysWxUserFaultEntity> {

	boolean save(SysWxUserFaultEntity wxUserFault);
}

