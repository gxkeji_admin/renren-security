package io.renren.service;

import com.baomidou.mybatisplus.service.IService;

import io.renren.entity.SysDepositEntity;

/**
 * 押金
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface DepositService extends IService<SysDepositEntity> {

}

