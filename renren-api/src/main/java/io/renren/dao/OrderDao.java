package io.renren.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysOrderEntity;

/**
 * 订单
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface OrderDao extends BaseMapper<SysOrderEntity> {
	
}
