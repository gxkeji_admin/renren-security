package io.renren.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysWxUserFaultEntity;

/**
 * 故障表
 * 
 * @author oslive
 * @email apple_live@qq.com
 * @date 2018-05-22 23:55:47
 */
public interface SysWxUserFaultDao extends BaseMapper<SysWxUserFaultEntity> {
	
}
