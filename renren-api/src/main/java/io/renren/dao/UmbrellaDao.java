package io.renren.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysUmbrellaEntity;

/**
 * 雨伞
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface UmbrellaDao extends BaseMapper<SysUmbrellaEntity> {
	
}
