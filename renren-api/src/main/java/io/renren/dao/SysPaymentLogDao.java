package io.renren.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysPaymentLogEntity;

/**
 * 支付关系表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysPaymentLogDao extends BaseMapper<SysPaymentLogEntity> {
	
}
