package io.renren.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;

import io.renren.common.utils.R;
import io.renren.modules.request.DepositPayReq;
import io.renren.modules.response.RechargePayReq;
import io.renren.service.PayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * <pre>
 *  注意此controller类实现接口WxPayService（implements PayServiceImpl ），
 *  仅是为了方便演示所有接口的使用，以免漏掉某一个新增加的接口，实际使用时无需如此实现。
 *  </pre>
 *
 * @author Binary Wang
 */
@Api(value = "小程序支付接口", description = "小程序支付接口")
@RestController
@RequestMapping("/wechat/rent")
public class WxPayController extends AbstractController{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private PayService payService;

	@ApiOperation(value = "微信支付")
	@RequestMapping(value = "/pay/deposit", method = RequestMethod.PUT)
	public Object payDeposit(ModelMap modelMap, HttpServletRequest request, @RequestBody DepositPayReq param) {
		Map<String, String> result = payService.payDeposit(param);
		return R.ok().put("result", result);
	}
	
	
	@ApiOperation(value = "微信支付")
	@RequestMapping(value = "/pay/recharge", method = RequestMethod.PUT)
	public Object payRecharge(ModelMap modelMap, HttpServletRequest request, @RequestBody RechargePayReq param) {
		Map<String, String> result = payService.payRecharge(param);
		return R.ok().put("result", result);
	}

	/**
	 * 
	 * @Title: notify
	 * @Description: TODO<尽量简短描述其作用>
	 * @author oslive 2017年11月26日 上午12:12:54
	 *
	 * @param response
	 * @param xmlData
	 *  <xml><appid><![CDATA[wx7cb45a366a8752d4]]></appid>
	 *  <bank_type><![CDATA[CFT]]></bank_type>
	 *  <cash_fee><![CDATA[1]]></cash_fee>
	 *  <fee_type><![CDATA[CNY]]></fee_type>
	 *  <is_subscribe><![CDATA[N]]></is_subscribe>
	 *  <mch_id><![CDATA[1485189062]]></mch_id>
	 *  <nonce_str><![CDATA[1511601929918]]></nonce_str>
	 *  <openid><![CDATA[oQVv00GLqbTlufpgYtfJWvMH6Q6A]]></openid>
	 *  <out_trade_no><![CDATA[934352331265441793]]></out_trade_no>
	 *  <result_code><![CDATA[SUCCESS]]></result_code>
	 *  <return_code><![CDATA[SUCCESS]]></return_code>
	 *  <sign><![CDATA[2BF5349755CDC9C07B1902682F7B3304]]></sign>
	 *  <time_end><![CDATA[20171125172543]]></time_end>
	 *  <total_fee>1</total_fee>
	 *  <trade_type><![CDATA[JSAPI]]></trade_type>
	 *  <transaction_id><![CDATA[4200000003201711256986918414]]></transaction_id>
	 *  </xml>
	 */
	@ApiOperation(value = "微信支付回调通知")
	@RequestMapping(value = "/pay/notify", method = RequestMethod.POST)
	public void payNotify(HttpServletResponse response, @RequestBody String xmlData) {
		logger.info("==> 微信支付通知回调内容: " + xmlData);
		//xmlData ="<xml><appid><![CDATA[wx7cb45a366a8752d4]]></appid><bank_type><![CDATA[CFT]]></bank_type><cash_fee><![CDATA[1]]></cash_fee><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[N]]></is_subscribe><mch_id><![CDATA[1485189062]]></mch_id><nonce_str><![CDATA[1516039081451]]></nonce_str><openid><![CDATA[oQVv00GLqbTlufpgYtfJWvMH6Q6A]]></openid><out_trade_no><![CDATA[952963093693087746]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[54F4862C8051BE8DBA5D4C3A9E5C464B]]></sign><time_end><![CDATA[20180116015809]]></time_end><total_fee>1</total_fee><trade_type><![CDATA[JSAPI]]></trade_type><transaction_id><![CDATA[4200000065201801164894330485]]></transaction_id></xml>";
		xmlData = "<xml><appid><![CDATA[wxbd2f253d4bf3b1f0]]></appid><bank_type><![CDATA[CFT]]></bank_type><cash_fee><![CDATA[1]]></cash_fee><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[N]]></is_subscribe><mch_id><![CDATA[1501765151]]></mch_id><nonce_str><![CDATA[1529736849156]]></nonce_str><openid><![CDATA[ovz-Z5TGHM53mtWSyzXiyrXS4LcQ]]></openid><out_trade_no><![CDATA[1010415695420256258]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[7DAD47941EBCB2997E45EF3139E8FECC]]></sign><time_end><![CDATA[20180623145415]]></time_end><total_fee>1</total_fee><trade_type><![CDATA[JSAPI]]></trade_type><transaction_id><![CDATA[4200000127201806232848254116]]></transaction_id></xml>";
		WxPayOrderNotifyResult result = payService.parseOrderNotifyResult(xmlData);
		if ("SUCCESS".equals(result.getResultCode())) {
			String sucReslut = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[ok]]></return_msg></xml>";
			logger.info("out_trade_no: " + result.getOutTradeNo() + " pay SUCCESS!");
			super.renderString(response, sucReslut);
		} else {
			String failReslut = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[result_code is FAIL]]></return_msg></xml>";
			super.renderString(response, failReslut);
		}
	}
}

