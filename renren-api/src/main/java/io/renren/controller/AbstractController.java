package io.renren.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

/**
 * 
 * @ClassName: AbstractController
 * @Description: 控制器状态基类
 * @author oslive 2017年8月4日 上午2:47:20
 *
 */
public abstract class AbstractController {
	
    /**
     * 日志对象
     */
	protected final Logger logger = LogManager.getLogger(this.getClass());

	/**
     * 客户端返回JSON字符串
     *
     * @param response
     * @param object
     * @return
     */
    protected String renderString(HttpServletResponse response, Object object) {
        return renderString(response, new Gson().toJson(object), "application/json");
    }

    /**
     * 客户端返回字符串
     *
     * @param response
     * @param string
     * @return
     */
    protected String renderString(HttpServletResponse response, String string) {
    	try {
    		response.setCharacterEncoding("utf-8");
    		response.getWriter().print(string);
    		return null;
    	} catch (IOException e) {
    		return null;
    	}
    }
    
    /**
     * 客户端返回字符串
     *
     * @param response
     * @param string
     * @return
     */
    protected String renderString(HttpServletResponse response, String string, String type) {
        try {
            response.reset();
            response.setContentType(type);
            response.setCharacterEncoding("utf-8");
            //解决跨域问题
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.getWriter().print(string);
            return null;
        } catch (IOException e) {
            return null;
        }
    }
}
