package io.renren.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import io.renren.annotation.Login;
import io.renren.common.exception.RRException;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.request.BaseReq;
import io.renren.modules.request.ImmediatelyRentReq;
import io.renren.modules.request.ImmediatelyReturnReq;
import io.renren.modules.request.MapHomeReq;
import io.renren.modules.request.MinaUserLocation;
import io.renren.modules.request.OrderDetailReq;
import io.renren.modules.request.UserCredentialsReq;
import io.renren.modules.response.AboutUsResp;
import io.renren.modules.response.DepositRuleResp;
import io.renren.modules.response.MapDataResp;
import io.renren.modules.response.OrderDetailResp;
import io.renren.modules.response.RentPageResp;
import io.renren.modules.response.RentResultResp;
import io.renren.modules.response.ReturnResultResp;
import io.renren.modules.response.ScanQrResult;
import io.renren.oss.cloud.OSSFactory;
import io.renren.oss.entity.SysOssEntity;
import io.renren.oss.service.SysOssService;
import io.renren.service.DictService;
import io.renren.service.WxRentService;
import io.renren.service.WxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.exception.WxErrorException;

@Api(value = "共享晴雨伞接口", description = "共享晴雨伞接口")
@RestController
@RequestMapping("/wechat/rent")
public class WxRentController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final String DICTTYPE = "fault";
	
	@Autowired
	private SysOssService sysOssService;
	
    @Autowired
    private WxMaService wxService;
	
    @Autowired
    private WxUserService wxUserService;

    @Autowired
    private WxRentService wxRentService;
    
    @Autowired
    private DictService dictService;
    
	@ApiOperation(value = "登陆接口")
	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public R login(ModelMap modelMap, @RequestParam String code) {
        if (StringUtils.isBlank(code)) {
        	return R.error("empty jscode");
        }
        try {
            this.logger.info("==> login请求参数：code=" + code);
            WxMaJscode2SessionResult session = this.wxService.getUserService().getSessionInfo(code);
            if(session != null) {
            	wxUserService.updateWxUserInfo(session);
            }
            this.logger.info("==> login返回结果：[" + JSON.toJSONString(session) + "]");
            return R.ok().put("session", session);
        } catch (WxErrorException e) {
            this.logger.error(e.getMessage(), e);
            return R.error(e.toString());
        }
    }

	@ApiOperation(value = "获取用户信息接口")
	@RequestMapping(value = "/user", method = RequestMethod.PUT)
    public R info(ModelMap modelMap, @RequestBody UserCredentialsReq userCredentials) {
		this.logger.info("==>getUserInfo请求参数：[" + JSON.toJSONString(userCredentials) + "]");
		if(userCredentials != null && !StringUtils.isAnyBlank(
				userCredentials.getSessionKey(), 
				userCredentials.getRawData(), 
				userCredentials.getSignature(), 
				userCredentials.getEncryptedData(), 
				userCredentials.getIv(),
				userCredentials.getToken())) {
			 // 用户信息校验
	        if (!this.wxService.getUserService().checkUserInfo(userCredentials.getSessionKey(), userCredentials.getRawData(), userCredentials.getSignature())) {
	        	return R.error("user check failed");
	        }
		}else {
			return R.error("invalid param");
		}
        // 解密用户信息
        WxMaUserInfo userInfo = this.wxService.getUserService().getUserInfo(userCredentials.getSessionKey(), userCredentials.getEncryptedData(), userCredentials.getIv());
        this.logger.info("==>getUserInfo微信解密结果：[" + JSON.toJSONString(userInfo) + "]");
        SysWxUserEntity wxUser = new SysWxUserEntity();
        if(userInfo != null) {
        	wxUser = wxUserService.updateWxUserInfo(userInfo);
        }
        this.logger.info("==>getUserInfo查询返回结果：[" + JSON.toJSONString(wxUser) + "]");
        return R.ok().put("result", wxUser);
    }
	
	@Login
	@ApiOperation(value = "我的信息")
	@RequestMapping(value = "/user/info", method = RequestMethod.GET)
	public Object userInfo(ModelMap modelMap, HttpServletRequest request, @RequestParam String token) {
		SysWxUserEntity result = wxUserService.queryByMinaOpenid(token);
		return R.ok().put("result", result);
	}
	
	@Login
	@ApiOperation(value = "上传地理位置接口")
	@RequestMapping(value = "/location", method = RequestMethod.PUT)
    public R location(ModelMap modelMap, HttpServletRequest request, @RequestBody MinaUserLocation minaUserLocation) {
		this.logger.info("==>location请求参数：[" + JSON.toJSONString(minaUserLocation) + "]");
		SysWxUserEntity wxUser = new SysWxUserEntity();
		if(minaUserLocation != null) {
			String token = minaUserLocation.getToken();
			if(StringUtils.isNotBlank(token)) {
				wxUser = wxUserService.updateWxUserLocation(minaUserLocation);
				this.logger.info("==>location查询返回结果：[" + JSON.toJSONString(wxUser) + "]");
			}else {
				return R.error("token can not be null");
			}
		}else {
			return R.error("longitude and latitude can not be null");
		}
		return R.ok().put("result", wxUser);
    }
	
	@Login
	@ApiOperation(value = "有伞地图")
	@RequestMapping(value = "/index", method = RequestMethod.PUT)
	public R index(ModelMap modelMap, @RequestBody MapHomeReq param) {
		MapDataResp result = wxRentService.index(param);
		return R.ok().put("result", result);
	}
	
	@Login
	@ApiOperation(value = "扫一扫")
	@RequestMapping(value = "/scan", method = RequestMethod.PUT)
	public R baseReq(ModelMap modelMap, @RequestBody BaseReq param) {
		ScanQrResult result = wxRentService.scanQrcode(param);
		return R.ok().put("result", result);
	}
	
	@Login
	@ApiOperation(value = "租伞界面")
	@RequestMapping(value = "/rentPage", method = RequestMethod.PUT)
	public Object toRent(ModelMap modelMap, @RequestBody BaseReq param) {
		RentPageResp result = wxRentService.toRentPage(param);
		return R.ok().put("result", result);
	}
	@Login
	@ApiOperation(value = "立即租伞")
	@RequestMapping(value = "/immediatelyRent", method = RequestMethod.PUT)
	public R immediatelyRent(ModelMap modelMap, @RequestBody ImmediatelyRentReq param) {
		RentResultResp result = wxRentService.immediatelyRent(param);
		return R.ok().put("result", result);
	}
	@Login
	@ApiOperation(value = "立即还伞")
	@RequestMapping(value = "/immediatelyReturn", method = RequestMethod.PUT)
	public R immediatelyReturn(ModelMap modelMap, @RequestBody ImmediatelyReturnReq param) {
		ReturnResultResp result = wxRentService.immediatelyReturn(param);
		return R.ok().put("result", result);
	}
	
	@Login
	@ApiOperation(value = "租伞成功界面返回")
	@RequestMapping(value = "/rentSuc", method = RequestMethod.PUT)
	public R rentSuc(ModelMap modelMap, @RequestBody OrderDetailReq param) {
		Object result = wxRentService.rentSuc(param);
		return R.ok().put("result", result);
	}
	
	@Login
	@ApiOperation(value = "还伞成功界面返回")
	@RequestMapping(value = "/returnSuc", method = RequestMethod.PUT)
	public R returnSuc(ModelMap modelMap, @RequestBody OrderDetailReq param) {
		OrderDetailResp result = wxRentService.returnSuc(param);
		return R.ok().put("result", result);
	}
	
	@Login
	@ApiOperation(value = "订单详情")
	@RequestMapping(value = "/user/order/detail", method = RequestMethod.PUT)
	public Object userOrderDetail(ModelMap modelMap, @RequestBody OrderDetailReq params) {
		OrderDetailResp result = wxRentService.orderDetail(params);
		return R.ok().put("result", result);
	}
	
	@ApiOperation(value = "订单遗失")
	@RequestMapping(value = "/user/order/lost", method = RequestMethod.PUT)
	public Object userOrderLost(ModelMap modelMap, @RequestBody OrderDetailReq params) {
		//OrderDetailResp result = wxRentService.orderLost(params);
		return R.ok().put("result", null);
	}
	
	@Login
	@ApiOperation(value = "我的订单")
	@RequestMapping(value = "/user/order/list", method = RequestMethod.PUT)
    public R userOrder(@RequestBody Map<String, Object> params){
        PageUtils page = wxRentService.queryOrderPage(params);
        return R.ok().put("result", page);
    }
	
	@Login
	@ApiOperation(value = "充值卡")
	@RequestMapping(value = "/recharge/card/list", method = RequestMethod.PUT)
	public R rechargeCard(ModelMap modelMap, @RequestBody Map<String, Object> params) {
		PageUtils page = wxRentService.queryRechargeCardPage(params);
		return R.ok().put("result", page);
	}
	
	@Login
	@ApiOperation(value = "租赁押金")
	@RequestMapping(value = "/deposit/rule", method = RequestMethod.PUT)
	public R depositRuleInfo(ModelMap modelMap, @RequestBody Map<String, Object> params) {
		DepositRuleResp result = wxRentService.queryDepositRuleInfo(params);
		return R.ok().put("result", result);
	}
	
	@Login
	@ApiOperation(value = "交易明细")
	@RequestMapping(value = "/trade/detail", method = RequestMethod.PUT)
	public Object tradeDetail(ModelMap modelMap, @RequestBody Map<String, Object> params) {
		PageUtils page = wxRentService.queryPaymentLogPage(params);
		return R.ok().put("result", page);
	}
	
	@Login
	@ApiOperation(value = "查找附近")
	@RequestMapping(value = "/nearby", method = RequestMethod.PUT)
	public Object nearby(ModelMap modelMap, @RequestBody Map<String, Object> params) {
		PageUtils page = wxRentService.queryNearByMachinePage(params);
		return R.ok().put("result", page);
	}
	
	@Login
	@ApiOperation(value = "故障类型列表")
	@RequestMapping(value = "/fault/list", method = RequestMethod.PUT)
    public R list(ModelMap modelMap, @RequestBody Map<String, Object> params){
		params.put("type", DICTTYPE);
        PageUtils page = dictService.queryPage(params);

        return R.ok().put("result", page);
    }
	
	/**
	 * 上传文件
	 */
	@Login
	@RequestMapping("/upload")
	public R upload(@RequestParam("file") MultipartFile file) throws Exception {
		if (file.isEmpty()) {
			throw new RRException("上传文件不能为空");
		}

		//上传文件
		String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		String url = OSSFactory.build().uploadSuffix(file.getBytes(), suffix);

		//保存文件信息
		SysOssEntity ossEntity = new SysOssEntity();
		ossEntity.setUrl(url);
		ossEntity.setCreateDate(new Date());
		sysOssService.insert(ossEntity);

		return R.ok().put("result", url);
	}
	
/*	@ApiOperation(value = "帮助中心")
	@RequestMapping(value = "/help", method = RequestMethod.PUT)
	public Object userHelp(ModelMap modelMap, @RequestBody Map<String, Object> params) {
		Page<Help> pageHelp = helpService.query(params);
		return setSuccessModelMap(modelMap, pageHelp);
	}*/

	@ApiOperation(value = "关于我们")
	@Login
	@RequestMapping(value = "/about/us", method = RequestMethod.GET)
	public Object aboutInfo(ModelMap modelMap, @RequestParam String token) {
		List<AboutUsResp> result = wxRentService.aboutUs();
		return R.ok().put("result", result);
	}
}
