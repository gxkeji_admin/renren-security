package io.renren.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import io.renren.common.utils.R;
import io.renren.modules.BaseParam;
import io.renren.modules.response.RefundResultResp;
import io.renren.service.PayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @ClassName: WxRefundController
 * @Description: TODO<微信退款>
 * @author oslive 2017年11月28日 上午2:02:45
 *
 */
@Api(value = "小程序支付接口", description = "小程序支付接口")
@RestController
@RequestMapping("/wechat/rent")
public class WxRefundController extends AbstractController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private PayService payService;

	@ApiOperation(value = "微信退款")
	@RequestMapping(value = "/refund/deposit", method = RequestMethod.PUT)
	public Object payDeposit(ModelMap modelMap, HttpServletRequest request, @RequestBody BaseParam param) {
		logger.info("==> 微信退款请求内容: " + JSON.toJSONString(param));
		RefundResultResp result = payService.refundDeposit(param);
		if (result.getResultCode() == 0) {
			logger.info("out_trade_no: " + result.getId() + " refund SUCCESS!" + JSON.toJSONString(result));
		} else {
			logger.info("out_trade_no: " + result.getId() + " refund fail: " + JSON.toJSONString(result));
		}
		return R.ok().put("result", result);
	}
}

