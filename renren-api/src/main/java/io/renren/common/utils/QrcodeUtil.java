package io.renren.common.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.binarywang.utils.qrcode.BufferedImageLuminanceSource;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * 二维码工具类
 * 
 * @author oslive
 * @since 2017年2月21日 下午1:30:29
 */
public class QrcodeUtil {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(QrcodeUtil.class);
	
	// 机器二维码大小定义
	private static final int WIDTH = 300;
    private static final int HEIGHT = 300;
    private static final int BASE_IMG_HEIGHT=320;
    private static final int TITLE_Y = 316;
    private static final int FONT_SIZE = 35;
	
	
	// 雨伞二维码大小定义
/*	private static final int WIDTH = 168*5;
    private static final int HEIGHT = 168*5;
    private static final int BASE_IMG_HEIGHT=188*5;
    private static final int TITLE_Y = 185*5;
    private static final int FONT_SIZE = 25*5;*/
    
    //private static final int LOGO_HEIGHT = 90;
    //private static final int LOGO_WHDTH = 90;
    private static final String MARGIN = "1";
    private static final String CHARSET = "utf-8";
    private static final String FORMAT_NAME = "jpg";
    
    /**
     * 生成无标题无logo的二维码
     * 
     * @param dir 目录
     * @param _text 内容
     * @return
     */
    public static String createQrcodeWithoutLogo(String dir, String _text, String fileName) {
		String qrcodeFilePath = "";
		try {
			BufferedImage image = createQrcodeImg(_text);
			File qrcodeFile =newQrcodeFile(dir, fileName);
			ImageIO.write(image, FORMAT_NAME, qrcodeFile);
			qrcodeFilePath = qrcodeFile.getAbsolutePath();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return qrcodeFilePath;
	}
    
    /**
     * 生成图片
     * 
     * @param _text
     * @return
     */
    private static BufferedImage createQrcodeImg(String _text) {
    	HashMap<EncodeHintType, String> hints = new HashMap<EncodeHintType, String>();
    	hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q.name());// 矫错级别  
    	hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
		hints.put(EncodeHintType.MARGIN, MARGIN);
        BufferedImage image=null;
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(_text, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints);
            image = new BufferedImage(bitMatrix.getWidth(), bitMatrix.getHeight(), BufferedImage.TYPE_INT_RGB);
            for(int x=0;x<WIDTH;x++){
                for (int y=0;y<HEIGHT;y++){
                    image.setRGB(x,y,bitMatrix.get(x,y)? 0xFF000000
                            : 0xFFFFFFFF);
                }
            }
        } catch (WriterException e) {
            LOGGER.warn("==>生成二维码异常",e);
            return null;
        }
        return image;
    }
    
    public static String createQrcodeWidthBottomTitle(String dir, String _text, String title, String fileName){
    	String qrcodeFilePath = "";
        BufferedImage image = createQrcodeImg(_text);

        BufferedImage bufferdImage = new BufferedImage(WIDTH, BASE_IMG_HEIGHT, BufferedImage.TYPE_INT_RGB);
        // bufferdImage.setRGB(0,0,0);

        // Graphics2D base = bufferdImage.createGraphics();
        Graphics2D base = (Graphics2D)bufferdImage.getGraphics();
        base.setBackground(Color.WHITE);
        base.setColor(Color.black);
        base.clearRect(0,0,WIDTH,BASE_IMG_HEIGHT);
        base.drawImage(image, 0, 0, WIDTH, HEIGHT, null);
        Font font = new Font("WenQuanYi Zen Hei Mono", Font.BOLD, FONT_SIZE);
        base.setFont(font);
        FontMetrics fm = base.getFontMetrics();
        int strWight = fm.stringWidth(title);
        int x = (WIDTH - strWight) / 2;
        //在这里写mac
        base.drawString(title, x, TITLE_Y);
        base.dispose();
        bufferdImage.flush();
        try {
			File qrcodeFile =newQrcodeFile(dir, fileName);
			ImageIO.write(bufferdImage, FORMAT_NAME, qrcodeFile);
			qrcodeFilePath = qrcodeFile.getAbsolutePath();
        } catch (IOException e) {
        	LOGGER.warn("==>生成带标题的二维码异常",e);
        }
        return qrcodeFilePath;
    }
    
    /**
     * 生成目录
     * 
     * @param dir 不为空
     * @param fileName 不为空则以传入的文件命名
     * @return
     */
    private static File newQrcodeFile(String dir, String fileName) {
    	File qrcodeFile = new File(dir + "/" + UUID.randomUUID().toString() + "." + FORMAT_NAME);
    	if(StringUtils.isNotEmpty(fileName)) {
    		qrcodeFile = new File(dir + "/" + fileName + "." + FORMAT_NAME);
    	}
        if(!qrcodeFile.exists()){
        	qrcodeFile.mkdirs();
        }
        return qrcodeFile;
    }
    
	public static String decodeQr(String filePath) {
		String retStr = "";
		if ("".equalsIgnoreCase(filePath) && filePath.length() == 0) {
			return "图片路径为空!";
		}
		try {
			BufferedImage bufferedImage = ImageIO.read(new FileInputStream(filePath));
			LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
			Binarizer binarizer = new HybridBinarizer(source);
			BinaryBitmap bitmap = new BinaryBitmap(binarizer);
			HashMap<DecodeHintType, Object> hintTypeObjectHashMap = new HashMap<>();
			hintTypeObjectHashMap.put(DecodeHintType.CHARACTER_SET, "UTF-8");
			Result result = new MultiFormatReader().decode(bitmap, hintTypeObjectHashMap);
			retStr = result.getText();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retStr;
	}
	
	public static void main(String[] args) {
		//https://yosan.gzgxkj.com/c/c10001019_1_0
		Map<String, String> map = new HashMap<>();
		for(int i = 1; i<=20; i++) {
			int count = 10001000 +i;
			map.put("c"+count+ "_1_0", "https://yosan.gzgxkj.com/c/c" + count + "_1_0" );
			map.put("c"+count+ "_1_1" , "https://yosan.gzgxkj.com/c/c" + count + "_1_1" );
			map.put("c"+count+ "_2_0" , "https://yosan.gzgxkj.com/c/c" + count + "_2_0" );
			map.put("c"+count+ "_2_1" , "https://yosan.gzgxkj.com/c/c" + count + "_2_1" );
			map.put("c"+count+ "_3_0", "https://yosan.gzgxkj.com/c/c" + count + "_3_0" );
			map.put("c"+count + "_3_1" , "https://yosan.gzgxkj.com/c/c" + count + "_3_1" );
			
		}
		
		for (Map.Entry<String, String> entry : map.entrySet()) {
			 QrcodeUtil.createQrcodeWidthBottomTitle("D://qr//u", entry.getValue(), entry.getKey(), entry.getKey() );
		}
		
        //QRCodeKit.create("http://weixin.qq.com/q/xEyDsCXln9fmDWEgfGAG","/Users/vincent/Documents/qrcode.jpg","");
        //QrcodeUtil.createQrcodeWidthBottomTitle("D://afaa//a", "https://w.url.cn/s/ApeTeRq", "u10001001","u10001001" );
        //QrcodeUtil.createQrcodeWithoutLogo("D://afaa//a", "http://www.baidu.com", "cm10001001_3_1");
        }
}
