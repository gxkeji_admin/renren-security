package io.renren.common.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.renren.common.utils.HttpUtil.Params;
import io.renren.common.utils.HttpUtil.ReqParam;



/**
 * 将入参生成摘要
 * 
 * @author xiaoxb
 *
 */
public class AuthUtil {
	//这里写键值对，程序中使用ConfigProperties.props.get("")来获得值
	//接口调用,约定的一个key值,用于md5加密
	private static final String HMAC_KEY = "sign";
	private static Logger logger = LoggerFactory.getLogger(AuthUtil.class);

	public static Params getParamListWithDigest(Params params) {
		return getParamListWithDigestNew(params);
	}

	public static Params getParamListWithDigestNew(Params params) {
		logger.info("计算摘要:{}", JsonUtil.toJSONString(params));

		List<ReqParam> paramList = null;
		if (params != null) {
			paramList = params.getList();
		} else {
			paramList = new ArrayList<>();
		}
		String clientId = "yousan-test-001";
		String secret = "123456789";
		long timestamp = System.currentTimeMillis();
		String nonce = RandomUtil.getRandomChar(6);
		String hmac = Md5Util.md5(clientId + secret + timestamp + nonce);

		// 加入摘要字段
		List<ReqParam> paramList2 = new ArrayList<>(paramList);
		paramList2.add(new ReqParam("clientId", clientId));
		paramList2.add(new ReqParam("timestamp", timestamp));
		paramList2.add(new ReqParam("nonce", nonce));
		paramList2.add(new ReqParam(HMAC_KEY, hmac));
		return Params.create().add(paramList2);
	}

	public static void main(String[] args) {
		Params params = Params.create().add("", 115).add("", "说的是");
		String s = JsonUtil.toJSONString(getParamListWithDigest(params));
		System.out.println(s);
	}
}
