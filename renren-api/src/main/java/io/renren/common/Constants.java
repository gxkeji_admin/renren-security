package io.renren.common;

/**
 * 常量表
 * 
 * @author oslive
 * @version $Id: Constants.java, v 0.1 2014-2-28 上午11:18:28 oslive Exp $
 */
public interface Constants {
	
	/** 阿里云IOT域名 */
	//static final String ALIYUN_IOT_DOMAIN= "http://iot.usan365.com/yousan-iot/api/operDevice";
	static final String ALIYUN_IOT_DOMAIN= "http://iot.usan365.com/yousan-iot/api/callDevice";
	
	/** 租借成功通知 */
	static final String RENT_TEMPLATE = "rent.template";
	/** 租借归还通知 */
	static final String RETURN_TEMPLATE = "return.template";
	
	/** 项目域名 **/
	static final String	PROJECT_DOMAIN			= "https://yosan.gzgxkj.com/";
	static final String SERVER_IP = "47.93.126.20";
	static final String	QRCODE_CONTENT_UMBRELLA	= PROJECT_DOMAIN + "u";
	static final String	QRCODE_CONTENT_MACHINE	= PROJECT_DOMAIN + "m";
	static final String	QRCODE_CONTENT_CHANNCEL	= PROJECT_DOMAIN + "c";

	/** 二维码路径 */
	static final String	FILE_BASE_DIR			= "data";
	static final String	MACHINE_QR_DIR			= "/data/staticfile/qrcode/machine/";
	static final String	UMBRELLA_QR_DIR			= "/data/staticfile/qrcode/umbrella/";
	static final String	MACHINE_CHANNCEL_QR_DIR	= "/data/staticfile/qrcode/channcel/";

	/** 二维码数字内容 */
	static final long	MACHINE_QR_CONTENT		= 1000010001;
	
	static final String	MACHINE_CODE_PREFIX = "m";
	static final String UMBRELLA_CODE_PREFIX = "u";
	static final String MACHINE_CHANNCEL_CODE_PREFIX = "c";
	
	static final String MACHINE_CHANNCEL_ON = "1,2,3,4";
	static final String MACHINE_CHANNCEL_OFF = "5,6,7,8";
	
    /** 扫描二维码解析的动作 */
    public interface SCAN_QR_OPERATION {
        /** 租伞 */
        public static final String RENT = "RENT";
        /** 还伞 */
        public static final String RETURN = "RETURN";
        /** 入伞 */
        public static final String ADMIN = "ADMIN";
        /** 错误 */
        public static final String ERROR = "ERROR";
        
    }
}
