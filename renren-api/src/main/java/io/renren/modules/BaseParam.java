package io.renren.modules;

public class BaseParam {

	private String token; // token值其实是小程序的openid

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
