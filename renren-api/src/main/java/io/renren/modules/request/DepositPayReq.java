package io.renren.modules.request;

import io.renren.modules.BaseParam;

public class DepositPayReq extends BaseParam{

	/**
	 * 交易类型：1|押金，2|充值，3|退款，4|消费
	 */
	private int	tradeType;

	private Long	depositRuleId;

	public int getTradeType() {
		return tradeType;
	}

	public void setTradeType(int tradeType) {
		this.tradeType = tradeType;
	}

	public Long getDepositRuleId() {
		return depositRuleId;
	}

	public void setDepositRuleId(Long depositRuleId) {
		this.depositRuleId = depositRuleId;
	}

}
