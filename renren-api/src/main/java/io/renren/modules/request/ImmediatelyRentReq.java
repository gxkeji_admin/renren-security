package io.renren.modules.request;

import io.renren.modules.BaseParam;

public class ImmediatelyRentReq extends BaseParam{

	private String	sceneStr;

	private String	fromId;

	public String getSceneStr() {
		return sceneStr;
	}

	public void setSceneStr(String sceneStr) {
		this.sceneStr = sceneStr;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(String fromId) {
		this.fromId = fromId;
	}
}
