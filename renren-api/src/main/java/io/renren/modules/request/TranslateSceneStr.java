package io.renren.modules.request;

public class TranslateSceneStr {

	private String	machineCode;

	private String	machineChanncel;
	
	private String actionOperation;

	public String getMachineCode() {
		return machineCode;
	}

	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}

	public String getMachineChanncel() {
		return machineChanncel;
	}

	public void setMachineChanncel(String machineChanncel) {
		this.machineChanncel = machineChanncel;
	}

	public String getActionOperation() {
		return actionOperation;
	}

	public void setActionOperation(String actionOperation) {
		this.actionOperation = actionOperation;
	}

}
