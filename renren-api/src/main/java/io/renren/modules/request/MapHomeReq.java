package io.renren.modules.request;
import java.math.BigDecimal;

import io.renren.modules.BaseParam;

public class MapHomeReq extends BaseParam{
	private String	keyword;
	private BigDecimal	latitude;
	private BigDecimal	longitude;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
}
