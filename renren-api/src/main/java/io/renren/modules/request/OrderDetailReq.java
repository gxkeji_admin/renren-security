package io.renren.modules.request;

import io.renren.modules.BaseParam;

public class OrderDetailReq extends BaseParam {

	private long orderId;

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

}
