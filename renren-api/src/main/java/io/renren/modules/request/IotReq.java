package io.renren.modules.request;

public class IotReq {

	private String	productKey;		// 物联网套件产品key
	private String	deviceName;		// 设备名称
	private String	userId;			// 用户标识
	private String	orderId;		// 用户订单号
	private String	umbrellaRoadId;	// 用户订单号
	private String	oper;			// 操作
	private String	umbrellaId;		// 伞标识
	private long	time;			// 操作时间，格式：时间戳(秒)
	private String	operResult;
	
	// 扩展字段
	private Long machineId;

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUmbrellaRoadId() {
		return umbrellaRoadId;
	}

	public void setUmbrellaRoadId(String umbrellaRoadId) {
		this.umbrellaRoadId = umbrellaRoadId;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getUmbrellaId() {
		return umbrellaId;
	}

	public void setUmbrellaId(String umbrellaId) {
		this.umbrellaId = umbrellaId;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getOperResult() {
		return operResult;
	}

	public void setOperResult(String operResult) {
		this.operResult = operResult;
	}

	public Long getMachineId() {
		return machineId;
	}

	public void setMachineId(Long machineId) {
		this.machineId = machineId;
	}

}
