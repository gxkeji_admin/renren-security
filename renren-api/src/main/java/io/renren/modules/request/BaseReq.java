package io.renren.modules.request;

import io.renren.modules.BaseParam;

public class BaseReq extends BaseParam{

	private String	sceneStr;

	public String getSceneStr() {
		return sceneStr;
	}

	public void setSceneStr(String sceneStr) {
		this.sceneStr = sceneStr;
	}

}
