package io.renren.modules.request;

public class ImmediatelyReturnReq extends ImmediatelyRentReq{

	private Long	id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
