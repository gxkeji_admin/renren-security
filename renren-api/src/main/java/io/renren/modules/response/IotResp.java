package io.renren.modules.response;

public class IotResp {

	private String	msg;

	private boolean	flag;

	private String	userId;

	private String	oper;

	private String	orderId;

	private String	deviceName;

	private String	productKey;

	private String	time;

	private String	umbrellaRoadId;

	private String	operResult;

	private String	umbrellaId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUmbrellaRoadId() {
		return umbrellaRoadId;
	}

	public void setUmbrellaRoadId(String umbrellaRoadId) {
		this.umbrellaRoadId = umbrellaRoadId;
	}

	public String getOperResult() {
		return operResult;
	}

	public void setOperResult(String operResult) {
		this.operResult = operResult;
	}

	public String getUmbrellaId() {
		return umbrellaId;
	}

	public void setUmbrellaId(String umbrellaId) {
		this.umbrellaId = umbrellaId;
	}

}
