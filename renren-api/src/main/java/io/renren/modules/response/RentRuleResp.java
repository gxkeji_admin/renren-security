package io.renren.modules.response;

import java.math.BigDecimal;

public class RentRuleResp {

	private Long		id;
	/**
	 * 免费时长单位:1|分，2|小时，3|天
	 */
	private Integer		freeType;
	/**
	 * 时长：表示免费多少分钟，免费多少小时，免费多少天
	 */
	private Integer		freeValue;
	/**
	 * 计价长度：表示每n(小时/天)
	 */
	private Integer		perTypeValue;
	/**
	 * 计价单位：1|按小时计费，2|按天计费
	 */
	private Integer		rentType;
	/**
	 * 租赁价(元)
	 */
	private BigDecimal	rentValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFreeType() {
		return freeType;
	}

	public void setFreeType(Integer freeType) {
		this.freeType = freeType;
	}

	public Integer getFreeValue() {
		return freeValue;
	}

	public void setFreeValue(Integer freeValue) {
		this.freeValue = freeValue;
	}

	public Integer getPerTypeValue() {
		return perTypeValue;
	}

	public void setPerTypeValue(Integer perTypeValue) {
		this.perTypeValue = perTypeValue;
	}

	public Integer getRentType() {
		return rentType;
	}

	public void setRentType(Integer rentType) {
		this.rentType = rentType;
	}

	public BigDecimal getRentValue() {
		return rentValue;
	}

	public void setRentValue(BigDecimal rentValue) {
		this.rentValue = rentValue;
	}
}