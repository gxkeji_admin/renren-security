package io.renren.modules.response;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class OrderDetailResp {

	private Long			orderId;
	private String			chipCode;
	private Integer			orderStatus;
	private OrderDetail		orderDetail;
	private FeeDetail		feeDetail;
	private RentRuleDetail	rentRuleDetail;

	public Long getOrderId() {
		return orderId;
	}

	public String getChipCode() {
		return chipCode;
	}

	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public OrderDetail getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(OrderDetail orderDetail) {
		this.orderDetail = orderDetail;
	}

	public FeeDetail getFeeDetail() {
		return feeDetail;
	}

	public void setFeeDetail(FeeDetail feeDetail) {
		this.feeDetail = feeDetail;
	}

	public RentRuleDetail getRentRuleDetail() {
		return rentRuleDetail;
	}

	public void setRentRuleDetail(RentRuleDetail rentRuleDetail) {
		this.rentRuleDetail = rentRuleDetail;
	}

	public OrderDetailResp() {
	}

	public OrderDetail newOrderDetailInstance() {
		if (orderDetail == null)
			orderDetail = new OrderDetail();
		return orderDetail;
	}

	public FeeDetail newFeeDetailInstance() {
		if (feeDetail == null)
			feeDetail = new FeeDetail();
		return feeDetail;
	}

	public RentRuleDetail newRentRuleDetailInstance() {
		if (rentRuleDetail == null)
			rentRuleDetail = new RentRuleDetail();
		return rentRuleDetail;
	}

	public class OrderDetail {
		/**
		 * 租出地址
		 */
		private String	rentPlace;
		/**
		 * 归还地址
		 */
		private String	returnPlace;
		/**
		 * 租伞开始时间
		 */
		@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		private Date	rentTime;
		/**
		 * 还伞时间
		 */
		@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		private Date	returnTime;

		public String getRentPlace() {
			return rentPlace;
		}

		public void setRentPlace(String rentPlace) {
			this.rentPlace = rentPlace;
		}

		public String getReturnPlace() {
			return returnPlace;
		}

		public void setReturnPlace(String returnPlace) {
			this.returnPlace = returnPlace;
		}

		public Date getRentTime() {
			return rentTime;
		}

		public void setRentTime(Date rentTime) {
			this.rentTime = rentTime;
		}

		public Date getReturnTime() {
			return returnTime;
		}

		public void setReturnTime(Date returnTime) {
			this.returnTime = returnTime;
		}

	}

	public class FeeDetail {
		private Integer		costTime;
		/**
		 * 租金金额（租金总额）
		 */
		private BigDecimal	totalPrice;

		/**
		 * 余额扣除（余额支付部分金额）
		 */
		private BigDecimal	balanceDeductionPrice;

		/**
		 * 实付金额（应付金额）
		 */
		private BigDecimal	actualPrice;
		/**
		 * 折扣金额（优惠减免）
		 */
		private BigDecimal	discountPrice;

		public Integer getCostTime() {
			return costTime;
		}

		public void setCostTime(Integer costTime) {
			this.costTime = costTime;
		}

		public BigDecimal getTotalPrice() {
			return totalPrice;
		}

		public void setTotalPrice(BigDecimal totalPrice) {
			this.totalPrice = totalPrice;
		}

		public BigDecimal getBalanceDeductionPrice() {
			return balanceDeductionPrice;
		}

		public void setBalanceDeductionPrice(BigDecimal balanceDeductionPrice) {
			this.balanceDeductionPrice = balanceDeductionPrice;
		}

		public BigDecimal getActualPrice() {
			return actualPrice;
		}

		public void setActualPrice(BigDecimal actualPrice) {
			this.actualPrice = actualPrice;
		}

		public BigDecimal getDiscountPrice() {
			return discountPrice;
		}

		public void setDiscountPrice(BigDecimal discountPrice) {
			this.discountPrice = discountPrice;
		}

	}

	public class RentRuleDetail {
		/**
		 * 押金
		 */
		private BigDecimal	depositValue;
		/**
		 * 免费时长单位:1|分，2|小时，3|天
		 */
		private Integer		freeType;
		/**
		 * 时长：表示免费多少分钟，免费多少小时，免费多少天
		 */
		private Integer		freeValue;
		/**
		 * 计价长度：表示每n(小时/天)
		 */
		private Integer		perTypeValue;
		/**
		 * 计价单位：1|按小时计费，2|按天计费
		 */
		private Integer		rentType;
		/**
		 * 租赁价(元)
		 */
		private BigDecimal	rentValue;

		public BigDecimal getDepositValue() {
			return depositValue;
		}

		public void setDepositValue(BigDecimal depositValue) {
			this.depositValue = depositValue;
		}

		public Integer getFreeType() {
			return freeType;
		}

		public void setFreeType(Integer freeType) {
			this.freeType = freeType;
		}

		public Integer getFreeValue() {
			return freeValue;
		}

		public void setFreeValue(Integer freeValue) {
			this.freeValue = freeValue;
		}

		public Integer getPerTypeValue() {
			return perTypeValue;
		}

		public void setPerTypeValue(Integer perTypeValue) {
			this.perTypeValue = perTypeValue;
		}

		public Integer getRentType() {
			return rentType;
		}

		public void setRentType(Integer rentType) {
			this.rentType = rentType;
		}

		public BigDecimal getRentValue() {
			return rentValue;
		}

		public void setRentValue(BigDecimal rentValue) {
			this.rentValue = rentValue;
		}

	}
}
