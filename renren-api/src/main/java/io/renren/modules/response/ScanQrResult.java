package io.renren.modules.response;

/**
 * 
 * @ClassName: ScanUmbrellaQRResp
 * @Description: TODO<扫描二维码返回结果集>
 * @author oslive 2017年11月14日 下午11:42:40
 *
 */
public class ScanQrResult extends BaseResp {

	private long			orderId;

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
}
