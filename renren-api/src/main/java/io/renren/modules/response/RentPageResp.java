package io.renren.modules.response;

/**
 * 
 * @ClassName: ScanUmbrellaQRResp
 * @Description: TODO<扫描二维码返回结果集>
 * @author oslive 2017年11月14日 下午11:42:40
 *
 */
public class RentPageResp {

	private DepositRuleResp	depositRule;

	private RentRuleResp	rentRule;

	public DepositRuleResp getDepositRule() {
		return depositRule;
	}

	public void setDepositRule(DepositRuleResp depositRule) {
		this.depositRule = depositRule;
	}

	public RentRuleResp getRentRule() {
		return rentRule;
	}

	public void setRentRule(RentRuleResp rentRule) {
		this.rentRule = rentRule;
	}
}
