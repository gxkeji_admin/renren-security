package io.renren.modules.response;

public class RefundResultResp extends BaseResp {

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
