package io.renren.modules.response;

import java.math.BigDecimal;
import java.util.List;

import io.renren.entity.SysMachineEntity;


/**
 * 地图主页面响应bean
 * 
 * @author WLDevelop
 *
 */
public class MapDataResp {

	private String					openid;

	// 经度
	private BigDecimal				longitude;

	// 纬度
	private BigDecimal				latitude;

	// 订单id
	private Long					orderId;

	// 是否已经缴纳押金：1|未缴纳，2|已缴纳
	private Integer					deposit;

	// 是否欠费：1|欠费，2|不欠旨
	private Integer					balance;

	// 是否有进行中的订单：1|无，2|有
	private Integer					orderFlag;

	private List<SysMachineEntity>	machineList;

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Integer getDeposit() {
		return deposit;
	}

	public void setDeposit(Integer deposit) {
		this.deposit = deposit;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getOrderFlag() {
		return orderFlag;
	}

	public void setOrderFlag(Integer orderFlag) {
		this.orderFlag = orderFlag;
	}

	public List<SysMachineEntity> getMachineList() {
		return machineList;
	}

	public void setMachineList(List<SysMachineEntity> machineList) {
		this.machineList = machineList;
	}

}
