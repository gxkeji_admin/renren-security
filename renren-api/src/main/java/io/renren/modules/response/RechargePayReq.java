package io.renren.modules.response;

import io.renren.modules.BaseParam;

public class RechargePayReq extends BaseParam{

	private Long	rechargeCardId;

	/**
	 * 交易类型：1|押金，2|充值，3|退款，4|消费
	 */
	private String	tradeType;

	public Long getRechargeCardId() {
		return rechargeCardId;
	}

	public void setRechargeCardId(Long rechargeCardId) {
		this.rechargeCardId = rechargeCardId;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

}
