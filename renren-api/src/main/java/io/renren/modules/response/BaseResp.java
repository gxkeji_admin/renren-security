package io.renren.modules.response;

public class BaseResp {

	/**
	 *  UNPAY_DEPOSIT(0, "未缴纳押金"), 
	 * 	UMBRELLA_RENT(1, "扫描雨伞二维码租伞"), 
	 *	MACHINE_CHANNCEL_RENT(2, "扫描机器伞道二维码租伞"), 
	 *	MACHINE_RETURN(3, "扫描伞桶二级码还伞"), 
	 *	MACHINE_CHANNCEL_RETURN(4, "扫描机器伞道二维码还伞"),
	 *	ORDER_QR(5, "扫描订单二维码还伞")
	 */
	private int		pageCode; // 跳转的页面

	private String	token;	

	private int		resultCode; // 业务结果编码

	private String	resultMsg; // 业务结果描述

	private long	timestamp;

	public int getPageCode() {
		return pageCode;
	}

	public void setPageCode(int pageCode) {
		this.pageCode = pageCode;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}
