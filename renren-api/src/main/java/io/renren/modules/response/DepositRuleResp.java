package io.renren.modules.response;

import java.math.BigDecimal;

public class DepositRuleResp {

	private Long		id;
	/**
	 * 押金金额
	 */
	private BigDecimal	depositPrice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getDepositPrice() {
		return depositPrice;
	}

	public void setDepositPrice(BigDecimal depositPrice) {
		this.depositPrice = depositPrice;
	}

}