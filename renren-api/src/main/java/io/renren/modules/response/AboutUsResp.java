package io.renren.modules.response;

public class AboutUsResp {

	private Long	id;
	/**
	 * 字典名称
	 */
	private String	name;
	/**
	 * 字典类型
	 */
	private String	type;
	/**
	 * 字典码
	 */
	private String	code;
	/**
	 * 字典值
	 */
	private String	value;
	/**
	 * 排序
	 */
	private Integer	orderNum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

}
