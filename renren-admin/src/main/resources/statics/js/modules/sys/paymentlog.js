$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/paymentlog/list',
        datatype: "json",
        colModel: [			
			{ label: '支付ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '订单ID', name: 'orderId', index: 'order_id', width: 80 }, 			
			{ label: '昵称', name: 'nickName', width: 80 }, 			
			{ label: '商户订单号', name: 'tradeNo', index: 'trade_no', width: 80 }, 			
			{ label: '微信支付订单号', name: 'transactionId', index: 'transaction_id', width: 80 }, 			
			{ label: '交易类型', name: 'tradeType', index: 'trade_type', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-success">押金</span>';
				} else if (value === 2){
					return '<span class="label label-success">充值</span>';
				} else if (value === 3){
					return '<span class="label label-success">退款</span>';
				} else if (value === 4){
					return '<span class="label label-success">消费</span>';
				}				
			}},			
			{ label: '支付状态', name: 'payStatus', index: 'pay_status', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-success">待支付</span>';
				} else if (value === 2){
					return '<span class="label label-success">已支付</span>';
				}			
			}}, 			
			{ label: '支付时间', name: 'payTime', index: 'pay_time', width: 80 }, 			
			{ label: '付款金额', name: 'payPrice', index: 'pay_price', width: 80 }, 			
			{ label: '备注', name: 'remark', index: 'remark', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }, 			
			{ label: '更新时间', name: 'updateTime', index: 'update_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	nickName: null
        },
		showList: true,
		title: null,
		paymentLog: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.paymentLog = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.paymentLog.id == null ? "sys/paymentlog/save" : "sys/paymentlog/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.paymentLog),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/paymentlog/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			})
			;
		},
		getInfo: function(id){
			$.get(baseURL + "sys/paymentlog/info/"+id, function(r){
                vm.paymentLog = r.paymentLog;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'nickName': vm.q.nickName},
                page:page
            }).trigger("reloadGrid");
		}
	}
});