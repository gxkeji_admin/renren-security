$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/recharge/list',
        datatype: "json",
        colModel: [			
			{ label: '充值ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '昵称', name: 'nickName', width: 80 }, 			
			{ label: '所属部门', name: 'deptName', width: 80 }, 			
			{ label: '商户订单号', name: 'tradeNo', index: 'trade_no', width: 80 }, 			
			{ label: '支付订单号', name: 'transactionId', index: 'transaction_id', width: 80 }, 			
			{ label: '充值前账号余额', name: 'frontBalance', index: 'front_balance', width: 80 }, 			
			{ label: '充值金额', name: 'totalPrice', index: 'total_price', width: 80 }, 			
			{ label: '实付金额', name: 'actualPrice', index: 'actual_price', width: 80 }, 			
			{ label: '折扣金额', name: 'discountPrice', index: 'discount_price', width: 80 }, 			
			{ label: '支付时间', name: 'payTime', index: 'pay_time', width: 80 }, 			
			{ label: '支付场景', name: 'payScene', index: 'pay_scene', width: 80 }, 			
			{ label: '状态', name: 'machineStatus', index: 'machine_status', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-danger">待支付</span>';
				} else if (value === 2){
					return '<span class="label label-success">已支付</span>';
				} else if (value === 3){
					return '<span class="label label-danger">取消支付</span>';
				} else if (value === 4){
					return '<span class="label label-danger">其他</span>';
				}
			}},
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	transactionId: null
        },
		showList: true,
		title: null,
		recharge: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.recharge = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.recharge.id == null ? "sys/recharge/save" : "sys/recharge/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.recharge),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/recharge/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/recharge/info/"+id, function(r){
                vm.recharge = r.recharge;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'transactionId': vm.q.transactionId},
                page:page
            }).trigger("reloadGrid");
		}
	}
});