$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/machine/list',
        datatype: "json",
        colModel: [			
			{ label: '机器ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '所属部门', name: 'deptName', sortable: false, width: 75 },		
			{ label: '编码', name: 'machineCode', index: 'machine_code', width: 80 }, 			
			{ label: '名称', name: 'machineName', index: 'machine_name', width: 80 }, 			
			{ label: '云产品KEY', name: 'productKey', index: 'product_key', width: 80 }, 			
			{ label: '云产品NAME', name: 'deviceName', index: 'device_name', width: 80 }, 			
			{ label: '状态', name: 'machineStatus', index: 'machine_status', width: 80, formatter: function(value, options, row){
				return value === 0 ? 
					'<span class="label label-danger">离线</span>' : 
					'<span class="label label-success">在线</span>';
			}},
			{ label: '投放地址', name: 'location', index: 'location', width: 80 }, 			
			{ label: '芯片伞道数量', name: 'chipChanncelNum', index: 'chip_channcel_num', width: 80 }, 			
			{ label: '额定装伞数', name: 'fixedQuantity', index: 'fixed_quantity', width: 80 }, 			
			{ label: '剩余伞数', name: 'leftQuantity', index: 'left_quantity', width: 80 }, 			
			{ label: '电量%', name: 'battery', index: 'battery', width: 80 }, 			
			{ label: '经度', name: 'longitude', index: 'longitude', width: 80 }, 			
			{ label: '纬度', name: 'latitude', index: 'latitude', width: 80 }	
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});
var setting = {
    data: {
        simpleData: {
            enable: true,
            idKey: "deptId",
            pIdKey: "parentId",
            rootPId: -1
        },
        key: {
            url:"nourl"
        }
    }
};
var ztree;
var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
            name: null
        },
		showList: true,
		title: null,
		machine: {},
        dept:{
        	deptId:null,
            deptName:null,
            orderNum:0
        }
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.dept = {deptName:null,deptId:null,orderNum:0};
			vm.machine = {};
			vm.getDept();
			vm.initMap();
		},
        getDept: function(){
            //加载部门树
            $.get(baseURL + "sys/dept/list", function(r){
                ztree = $.fn.zTree.init($("#deptTree"), setting, r);
                var node = ztree.getNodeByParam("deptId", vm.machine.deptId);
                if(node != null){
                    ztree.selectNode(node);

                    vm.dept.deptName = node.name;
                }
            })
        },
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id);
            vm.getDept();
            vm.initMap();
		},
		saveOrUpdate: function (event) {
			var url = vm.machine.id == null ? "sys/machine/save" : "sys/machine/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.machine),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/machine/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/machine/info/"+id, function(r){
                vm.machine = r.machine;
            });
		},
        deptTree: function(){
            layer.open({
                type: 1,
                offset: '50px',
                skin: 'layui-layer-molv',
                title: "选择部门",
                area: ['300px', '450px'],
                shade: 0,
                shadeClose: false,
                content: jQuery("#deptLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    var node = ztree.getSelectedNodes();
                    //选择上级部门
                    vm.dept.deptId = node[0].deptId;
                    vm.dept.deptName = node[0].name;
                    vm.machine.deptId = node[0].deptId;

                    layer.close(index);
                }
            });
        },
        //初始化腾讯地图
        initMap: function(){
            var qqMap = new qq.maps.Map(document.getElementById("map-container"),{
                center: new qq.maps.LatLng(23.143600,113.346810),
                zoom: 18
            });
            var searchService = new qq.maps.SearchService({
                map : qqMap
            });
            //点击获取坐标
            qq.maps.event.addListener(qqMap,'click',function(event) {
                var latLng = event.latLng,
                lat = latLng.getLat().toFixed(7),
                lng = latLng.getLng().toFixed(7);
                vm.machine.latitude = lat;
                vm.machine.longitude= lng;
            });
            //搜索关键字
            $("#location").blur(function(){
                searchService.search(this.value);
            });
        },
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'machineCode': vm.q.machineCode},
                page:page
            }).trigger("reloadGrid");
		}
	}
});