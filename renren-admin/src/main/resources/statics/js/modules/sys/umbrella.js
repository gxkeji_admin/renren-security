$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/umbrella/list',
        datatype: "json",
        colModel: [			
			{ label: '雨伞ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '名称', name: 'umbrellaName', index: 'umbrella_name', width: 80 }, 	
			{ label: '所属部门', name: 'deptName', sortable: false, width: 75 },			
			{ label: '所属机器', name: 'machineName', sortable: false, width: 75 },			
			{ label: '芯片', name: 'chipCode', index: 'chip_code', width: 80 }, 			
			{ label: '状态', name: 'umbrellaStatus', index: 'umbrella_status', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-success">空闲</span>';
				} else if (value === 2){
					return '<span class="label label-danger">锁定</span>';
				} else if (value === 3){
					return '<span class="label label-danger">租出</span>';
				} else if (value === 4){
					return '<span class="label label-danger">遗失</span>';
				} 				
			}},
			{ label: '所在伞道', name: 'channcel', index: 'channcel', width: 80 }, 
			{ label: '二维码', name: 'umbrellaQrcodePath', index: 'umbrella_qrcode_path', width: 80 }, 			
			{ label: '最近一次租出时间', name: 'lastRentTime', index: 'last_rent_time', width: 80 }, 			
			{ label: '租用次数', name: 'rentTimes', index: 'rent_times', width: 80 }, 			
			{ label: '描述', name: 'desc', index: 'desc', width: 80 }	
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	chipCode: null
        },
		showList: true,
		title: null,
		umbrella: {},
		machine:{
        	deptId:null,
            machineName:null,
            orderNum:0
        }
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.umbrella = {};
		},
        getMachine: function(){
            //加载部门树
            $.get(baseURL + "sys/machine/list", function(r){
                ztree = $.fn.zTree.init($("#deptTree"), setting, r);
                var node = ztree.getNodeByParam("deptId", vm.depositRule.deptId);
                if(node != null){
                    ztree.selectNode(node);

                    vm.dept.deptName = node.name;
                }
            })
        },
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.umbrella.id == null ? "sys/umbrella/save" : "sys/umbrella/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.umbrella),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/umbrella/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/umbrella/info/"+id, function(r){
                vm.umbrella = r.umbrella;
            });
		},
		machineDrop: function(){
            layer.open({
                type: 1,
                offset: '50px',
                skin: 'layui-layer-molv',
                title: "选择部门",
                area: ['300px', '450px'],
                shade: 0,
                shadeClose: false,
                content: jQuery("#deptLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    var node = ztree.getSelectedNodes();
                    //选择上级部门
                    vm.dept.deptId = node[0].deptId;
                    vm.dept.deptName = node[0].name;
                    vm.depositRule.deptId = node[0].deptId;

                    layer.close(index);
                }
            });
        },
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
				postData:{'chipCode': vm.q.chipCode},
                page:page
            }).trigger("reloadGrid");
		}
	}
});