$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/rentrule/list',
        datatype: "json",
        colModel: [			
			{ label: '租金规则ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '名称', name: 'name', index: 'name', width: 80 }, 			
			{ label: '所属部门', name: 'deptName', sortable: false, width: 75 },		
			{ label: '免费时长单位', name: 'freeType', index: 'free_type', width: 80 }, 			
			{ label: '时长', name: 'freeValue', index: 'free_value', width: 80 }, 			
			{ label: '计价长度', name: 'perTypeValue', index: 'per_type_value', width: 80 }, 			
			{ label: '计价单位', name: 'rentType', index: 'rent_type', width: 80 }, 			
			{ label: '租赁价(元)', name: 'rentValue', index: 'rent_value', width: 80 }, 			
			{ label: '规则状态', name: 'ruleStatus', index: 'rule_status', width: 80, formatter: function(value, options, row){
				return value === 0 ? 
					'<span class="label label-danger">禁用</span>' : 
					'<span class="label label-success">正常</span>';
			}},
			{ label: '规则的描述', name: 'desc', index: 'desc', width: 80 },	
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }		
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});
var setting = {
    data: {
        simpleData: {
            enable: true,
            idKey: "deptId",
            pIdKey: "parentId",
            rootPId: -1
        },
        key: {
            url:"nourl"
        }
    }
};
var ztree;
var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
            name: null
        },
		showList: true,
		title: null,
		rentRule: {},
        dept:{
        	deptId:null,
            deptName:null,
            orderNum:0
        }
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
            vm.title = "新增";
            vm.dept = {deptName:null,deptId:null,orderNum:0};
            vm.getDept();
            vm.rentRule = {};
		},
        getDept: function(){
            //加载部门树
            $.get(baseURL + "sys/dept/list", function(r){
                ztree = $.fn.zTree.init($("#deptTree"), setting, r);
                var node = ztree.getNodeByParam("deptId", vm.rentRule.deptId);
                if(node != null){
                    ztree.selectNode(node);

                    vm.dept.deptName = node.name;
                }
            })
        },
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id);
            vm.getDept();
		},
		saveOrUpdate: function (event) {
			var url = vm.rentRule.id == null ? "sys/rentrule/save" : "sys/rentrule/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.rentRule),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/rentrule/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/rentrule/info/"+id, function(r){
                vm.rentRule = r.rentRule;
            });
		},
        deptTree: function(){
            layer.open({
                type: 1,
                offset: '50px',
                skin: 'layui-layer-molv',
                title: "选择部门",
                area: ['300px', '450px'],
                shade: 0,
                shadeClose: false,
                content: jQuery("#deptLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    var node = ztree.getSelectedNodes();
                    //选择上级部门
                    vm.dept.deptId = node[0].deptId;
                    vm.dept.deptName = node[0].name;
                    vm.rentRule.deptId = node[0].deptId;

                    layer.close(index);
                }
            });
        },
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
				postData:{'name': vm.q.name},
				page:page
            }).trigger("reloadGrid");
		}
	}
});