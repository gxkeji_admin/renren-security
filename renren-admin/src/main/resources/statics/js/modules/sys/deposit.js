$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/deposit/list',
        datatype: "json",
        colModel: [			
			{ label: '押金ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '昵称', name: 'nickName', width: 80 }, 			
			{ label: '所属部门', name: 'deptName', sortable: false, width: 75 },	
			{ label: '财付通订单号', name: 'tradeNo', index: 'trade_no', width: 80 }, 			
			{ label: '微信支付订单号', name: 'transactionId', index: 'transaction_id', width: 80 }, 			
			{ label: '付款金额', name: 'payPrice', index: 'pay_price', width: 80 }, 			
			{ label: '支付时间', name: 'payTime', index: 'pay_time', width: 80 }, 			
			{ label: '状态', name: 'depositStatus', index: 'depositStatus', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-danger">待支付</span>';
				} else if(value === 2){
					return '<span class="label label-success">已支付，未退款</span>';
				} else if(value === 3){
					return '<span class="label label-danger">取消支付</span>';
				} else if(value === 4){
					return '<span class="label label-danger">已支付，已退款</span>';
				} 
			}},
			{ label: '微信退款单号', name: 'refundId', index: 'refund_id', width: 80 }, 			
			{ label: '商户退款单号', name: 'refundNo', index: 'refund_no', width: 80 }, 			
			{ label: '退款金额', name: 'refundPrice', index: 'refund_price', width: 80 }, 			
			{ label: '退款时间', name: 'refundTime', index: 'refund_time', width: 80 }		
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	nickName: null
        },
		showList: true,
		title: null,
		deposit: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.deposit = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.deposit.id == null ? "sys/deposit/save" : "sys/deposit/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.deposit),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/deposit/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/deposit/info/"+id, function(r){
                vm.deposit = r.deposit;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'nickName': vm.q.nickName},
                page:page
            }).trigger("reloadGrid");
		}
	}
});