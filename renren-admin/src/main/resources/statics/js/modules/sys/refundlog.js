$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/refundlog/list',
        datatype: "json",
        colModel: [			
			{ label: '退款ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '昵称', name: 'nickName', width: 80 }, 			
			{ label: '押金ID', name: 'depositId', index: 'deposit_id', width: 80 }, 			
			{ label: '财付通订单号', name: 'tradeNo', index: 'trade_no', width: 80 }, 			
			{ label: '所属部门', name: 'deptName', width: 80 }, 			
			{ label: '支付金额', name: 'payPrice', index: 'pay_price', width: 80 }, 			
			{ label: '退款金额', name: 'refundPrice', index: 'refund_price', width: 80 }, 			
			{ label: '退款时间', name: 'refundTime', index: 'refund_time', width: 80 }, 			
			{ label: '支付场景', name: 'refundScene', index: 'refund_scene', width: 80 }, 			
			{ label: '交易状态', name: 'refundStatus', index: 'refund_status', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-danger">失败</span>';
				} else if (value === 2){
					return '<span class="label label-success">成功</span>';
				}
			}}, 			
			{ label: '备注', name: 'remark', index: 'remark', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }, 			
			{ label: '更新时间', name: 'updateTime', index: 'update_time', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	nickName: null
        },
		showList: true,
		title: null,
		refundLog: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.refundLog = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.refundLog.id == null ? "sys/refundlog/save" : "sys/refundlog/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.refundLog),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/refundlog/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/refundlog/info/"+id, function(r){
                vm.refundLog = r.refundLog;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'nickName': vm.q.nickName},
                page:page
            }).trigger("reloadGrid");
		}
	}
});