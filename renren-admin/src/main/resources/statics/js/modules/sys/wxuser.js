$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/wxuser/list',
        datatype: "json",
        colModel: [			
			{ label: '微信用户ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '所属部门', name: 'deptName', width: 80 },		
			{ label: '昵称', name: 'nickName', index: 'nick_name', width: 80 }, 			
			{ label: '姓别', name: 'sex', index: 'sex', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-success">男</span>';
				} else if (value === 2){
					return '<span class="label label-success">女</span>';
				} else if (value === 3){
					return '<span class="label label-danger">其他</span>';
				}
			}},	
			{ label: '所在城市', name: 'city', index: 'city', width: 80 }, 			
			{ label: '所在省份', name: 'province', index: 'province', width: 80 },
			{ label: '头像', name: 'avatar', index: 'avatar', width: 80, formatter: function(value, options, row){
				if(value != null ){
					return '<span class="label label-success"><img src='+ value +'/></span>';
				} else {
					return '<span class="label label-danger">其他</span>';
				}
			}},
			{ label: '会员等级', name: 'level', index: 'level', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-success">普通会员</span>';
				} else if (value === 2){
					return '<span class="label label-success">白银会员</span>';
				} else if (value === 3){
					return '<span class="label label-danger">黄金会员</span>';
				} else if (value === 4){
					return '<span class="label label-danger">钻石会员</span>';
				}
			}},
			{ label: '积分', name: 'bonus', index: 'bonus', width: 80 }, 			
			{ label: '余额', name: 'balance', index: 'balance', width: 80 }, 			
			{ label: '押金', name: 'deposit', index: 'deposit', width: 80 }, 			
			{ label: '是否管理员', name: 'isManager', index: 'is_manager', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-success">非管理员</span>';
				} else if (value === 2){
					return '<span class="label label-danger">管理员</span>';
				}
			}},			 			
			{ label: '租伞次数', name: 'rentTimes', index: 'rent_times', width: 80 },
			{ label: '最近一次登录时间', name: 'lastLoginTime', index: 'last_login_time', width: 80 }, 			
			{ label: '最近一次登录ip', name: 'lastLoginIp', index: 'last_login_ip', width: 80 }, 			
			{ label: '首次访问时间', name: 'createTime', index: 'create_time', width: 80 }		
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	nickName: null
        },
		showList: true,
		title: null,
		wxUser: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.wxUser = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.wxUser.id == null ? "sys/wxuser/save" : "sys/wxuser/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.wxUser),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/wxuser/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/wxuser/info/"+id, function(r){
                vm.wxUser = r.wxUser;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'nickName': vm.q.nickName},
                page:page
            }).trigger("reloadGrid");
		}
	}
});