$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/rechargecard/list',
        datatype: "json",
        colModel: [			
			{ label: '充值卡ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '所属部门', name: 'deptName', sortable: false, width: 75 },	
			{ label: '面额', name: 'totalPrice', index: 'total_price', width: 80 }, 			
			{ label: '实际售价', name: 'marketPrice', index: 'market_price', width: 80 }, 			
			{ label: '折扣金额', name: 'discountPrice', index: 'discount_price', width: 80 }, 			
			{ label: '规则状态', name: 'shelfStatus', index: 'shelf_status', width: 80, formatter: function(value, options, row){
				return value === 1 ? 
					'<span class="label label-danger">已上架</span>' : 
					'<span class="label label-success">未上架</span>';
			}},
			{ label: '描述', name: 'desc', index: 'desc', width: 80 }, 			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});
var setting = {
    data: {
        simpleData: {
            enable: true,
            idKey: "deptId",
            pIdKey: "parentId",
            rootPId: -1
        },
        key: {
            url:"nourl"
        }
    }
};
var ztree;
var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		rechargeCard: {},
        dept:{
        	deptId:null,
            deptName:null,
            orderNum:0
        }
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.dept = {deptName:null,deptId:null,orderNum:0};
			vm.getDept();
			vm.rechargeCard = {};
		},
        getDept: function(){
            //加载部门树
            $.get(baseURL + "sys/dept/list", function(r){
                ztree = $.fn.zTree.init($("#deptTree"), setting, r);
                var node = ztree.getNodeByParam("deptId", vm.rechargeCard.deptId);
                if(node != null){
                    ztree.selectNode(node);

                    vm.dept.deptName = node.name;
                }
            })
        },
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id);
            vm.getDept();
		},
		saveOrUpdate: function (event) {
			var url = vm.rechargeCard.id == null ? "sys/rechargecard/save" : "sys/rechargecard/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.rechargeCard),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/rechargecard/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/rechargecard/info/"+id, function(r){
                vm.rechargeCard = r.rechargeCard;
            });
		},
        deptTree: function(){
            layer.open({
                type: 1,
                offset: '50px',
                skin: 'layui-layer-molv',
                title: "选择部门",
                area: ['300px', '450px'],
                shade: 0,
                shadeClose: false,
                content: jQuery("#deptLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    var node = ztree.getSelectedNodes();
                    //选择上级部门
                    vm.dept.deptId = node[0].deptId;
                    vm.dept.deptName = node[0].name;
                    vm.rechargeCard.deptId = node[0].deptId;

                    layer.close(index);
                }
            });
        },
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});