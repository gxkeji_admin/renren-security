$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/order/list',
        datatype: "json",
        colModel: [			
			{ label: '订单ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '所属部门', name: 'deptName', sortable: false, width: 80 }, 			
			{ label: '昵称', name: 'nickName', sortable: false, width: 80 },		
			{ label: '支付订单号', name: 'transactionId', index: 'transaction_id', width: 80 }, 			
			{ label: '取伞源', name: 'rentMachineCode', index: 'rent_machine_code', width: 80 }, 			
			{ label: '还伞源', name: 'returnMachineCode', index: 'return_machine_code', width: 80 }, 			
			{ label: '雨伞芯片', name: 'chipCode', index: 'chip_code', width: 80 }, 			
			{ label: '租金金额', name: 'totalPrice', index: 'total_price', width: 80 }, 			
			{ label: '实付金额', name: 'actualPrice', index: 'actual_price', width: 80 }, 			
			{ label: '折扣金额', name: 'discountPrice', index: 'discount_price', width: 80 }, 			
			{ label: '支付渠道', name: 'payMethod', index: 'pay_method', width: 80, formatter: function(value, options, row){
				if(value === 'WX'){
					return '<span class="label label-success">微信</span>';
				} else if (value === 'Alipay'){
					return '<span class="label label-success">支付宝</span>';
				} else if (value === 'Balance'){
					return '<span class="label label-success">余额</span>';
				}				
			}},
			{ label: '租出地址', name: 'rentPlace', index: 'rent_place', width: 80 },
			{ label: '归还地址', name: 'returnPlace', index: 'return_place', width: 80 },	
			{ label: '开始时间', name: 'rentTime', index: 'rent_time', width: 80 },
			{ label: '还伞时间', name: 'returnTime', index: 'return_time', width: 80 }, 			
			{ label: '支付时间', name: 'payTime', index: 'pay_time', width: 80 }, 			
			{ label: '状态 ', name: 'orderStatus', index: 'order_status', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-danger">待取伞</span>';
				} else if (value === 2){
					return '<span class="label label-danger">待归还</span>';
				} else if (value === 3){
					return '<span class="label label-danger">已归还，待支付</span>';
				} else if (value === 4){
					return '<span class="label label-success">交易完成</span>';
				} else if (value === 5){
					return '<span class="label label-danger">待支付系统自动取消订单</span>';
				}				
			}}
			],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	transactionId: null
        },
		showList: true,
		title: null,
		order: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.order = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.order.id == null ? "sys/order/save" : "sys/order/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.order),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/order/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/order/info/"+id, function(r){
                vm.order = r.order;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'transactionId': vm.q.transactionId},
                page:page
            }).trigger("reloadGrid");
		}
	}
});