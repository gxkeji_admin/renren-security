$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/wxuserfault/list',
        datatype: "json",
        colModel: [			
			{ label: '故障ID', name: 'id', index: 'id', width: 50, key: true },
			{ label: '昵称', name: 'nickName', width: 80 },
			{ label: '所属部门', name: 'deptName', width: 80 }, 	
			{ label: '订单ID', name: 'orderId', index: 'order_id', width: 80 }, 			
			{ label: '所属机器', name: 'machineCode', index: 'machine_code', width: 80 }, 			
			{ label: '雨伞微芯片', name: 'chipCode', index: 'chip_code', width: 80 }, 			
			{ label: '故障类型', name: 'faultType', index: 'fault_type', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-danger">雨伞遗失</span>';
				} else if (value === 2){
					return '<span class="label label-success">还伞异常</span>';
				}			
			}},	 			
			{ label: '状态', name: 'faultStatus', index: 'fault_status', width: 80, formatter: function(value, options, row){
				if(value === 1){
					return '<span class="label label-danger">处理中</span>';
				} else if (value === 2){
					return '<span class="label label-success">完结</span>';
				}			
			}},			
			{ label: '内容', name: 'content', index: 'content', width: 80 }, 			
			{ label: '故障图片', name: 'faultPicUrl', index: 'fault_pic_url', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }, 			
			{ label: '更新时间', name: 'updateTime', index: 'update_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q:{
        	orderId: null
        },
		showList: true,
		title: null,
		wxUserFault: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.wxUserFault = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.wxUserFault.id == null ? "sys/wxuserfault/save" : "sys/wxuserfault/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.wxUserFault),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "sys/wxuserfault/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "sys/wxuserfault/info/"+id, function(r){
                vm.wxUserFault = r.wxUserFault;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
				postData:{'orderId': vm.q.orderId},
                page:page
            }).trigger("reloadGrid");
		}
	}
});