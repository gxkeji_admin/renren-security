package io.renren.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysDepositEntity;

/**
 * 押金
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysDepositService extends IService<SysDepositEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    SysDepositEntity queryById(Long id);
}

