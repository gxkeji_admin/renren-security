package io.renren.modules.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysMachineChanncelEntity;
import io.renren.entity.SysMachineEntity;
import io.renren.modules.sys.dao.SysMachineChanncelDao;
import io.renren.modules.sys.service.SysMachineChanncelService;
import io.renren.modules.sys.service.SysMachineService;


@Service("sysMachineChanncelService")
public class SysMachineChanncelServiceImpl extends ServiceImpl<SysMachineChanncelDao, SysMachineChanncelEntity> implements SysMachineChanncelService {

	@Autowired
	private SysMachineService sysMachineService;
	
    @Override
    @DataFilter(subDept = true, user = false)
    public PageUtils queryPage(Map<String, Object> params) {
    	String machineName = (String)params.get("machineName");
    	List<Long> machineIds = new ArrayList<Long>();
    	Wrapper<SysMachineChanncelEntity> machineChanncelWrapper = new EntityWrapper<SysMachineChanncelEntity>();
    	if(StringUtils.isNotBlank(machineName)) {
        	Wrapper<SysMachineEntity> wrapper = new EntityWrapper<SysMachineEntity>();
        	wrapper.like("machine_name", machineName);
    		wrapper.orderBy("machine_code", false);
    		wrapper.addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER));
    		List<SysMachineEntity> machines = sysMachineService.selectList(wrapper);
    		if(machines != null && !machines.isEmpty()) {
    			machines.forEach(machine -> {
    				if (machine != null) {
    					machineIds.add(machine.getId().longValue());
    				}
    			});
    		}
    		if(!machineIds.isEmpty()) {
        		machineChanncelWrapper.in("machine_id", machineIds);
        	}
    	}
    	
		Page<SysMachineChanncelEntity> page = this.selectPage(new Query<SysMachineChanncelEntity>(params).getPage(),
				machineChanncelWrapper);

        for(SysMachineChanncelEntity sysMachineChanncelEntity : page.getRecords()){
        	SysMachineEntity sysMachineEntity = sysMachineService.selectById(sysMachineChanncelEntity.getMachineId().longValue());
        	sysMachineChanncelEntity.setMachineName(sysMachineEntity.getMachineName());
    	};
        
        return new PageUtils(page);
    }

}
