package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysIotCallbackLogEntity;
import io.renren.modules.sys.service.SysIotCallbackLogService;



/**
 * 操作iot获取数据日志记录
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-15 18:45:53
 */
@RestController
@RequestMapping("sys/iotcallbacklog")
public class SysIotCallbackLogController {
    @Autowired
    private SysIotCallbackLogService sysIotCallbackLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:iotcallbacklog:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysIotCallbackLogService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:iotcallbacklog:info")
    public R info(@PathVariable("id") Long id){
			SysIotCallbackLogEntity iotCallbackLog = sysIotCallbackLogService.queryById(id);

        return R.ok().put("iotCallbackLog", iotCallbackLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:iotcallbacklog:save")
    public R save(@RequestBody SysIotCallbackLogEntity iotCallbackLog){
			sysIotCallbackLogService.insert(iotCallbackLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:iotcallbacklog:update")
    public R update(@RequestBody SysIotCallbackLogEntity iotCallbackLog){
			sysIotCallbackLogService.updateById(iotCallbackLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:iotcallbacklog:delete")
    public R delete(@RequestBody Long[] ids){
			sysIotCallbackLogService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
