package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysDepositEntity;

/**
 * 押金
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysDepositDao extends BaseMapper<SysDepositEntity> {
	
}
