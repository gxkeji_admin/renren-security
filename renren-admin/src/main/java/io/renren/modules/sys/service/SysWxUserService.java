package io.renren.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysWxUserEntity;

/**
 * 微信用户
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysWxUserService extends IService<SysWxUserEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    SysWxUserEntity queryById(Long id);
    
	SysWxUserEntity queryByMinaOpenid(String minaOpenid);

	SysWxUserEntity queryByOpenid(String openid);
	
	Map<String, Boolean> checkDepositAndBalance(String token);
}

