package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysPaymentLogEntity;
import io.renren.modules.sys.service.SysPaymentLogService;



/**
 * 支付关系表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/paymentlog")
public class SysPaymentLogController {
    @Autowired
    private SysPaymentLogService sysPaymentLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:paymentlog:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysPaymentLogService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:paymentlog:info")
    public R info(@PathVariable("id") Long id){
			SysPaymentLogEntity paymentLog = sysPaymentLogService.queryById(id);

        return R.ok().put("paymentLog", paymentLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:paymentlog:save")
    public R save(@RequestBody SysPaymentLogEntity paymentLog){
			sysPaymentLogService.insert(paymentLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:paymentlog:update")
    public R update(@RequestBody SysPaymentLogEntity paymentLog){
			sysPaymentLogService.updateById(paymentLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:paymentlog:delete")
    public R delete(@RequestBody Long[] ids){
			sysPaymentLogService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
