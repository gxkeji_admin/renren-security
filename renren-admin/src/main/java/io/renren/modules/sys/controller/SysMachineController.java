package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.entity.SysMachineEntity;
import io.renren.modules.sys.service.SysMachineService;



/**
 * 机器
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/machine")
public class SysMachineController {
    @Autowired
    private SysMachineService sysMachineService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:machine:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysMachineService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:machine:info")
    public R info(@PathVariable("id") Long id){
		SysMachineEntity machine = sysMachineService.selectById(id);

		return R.ok().put("machine", machine);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:machine:save")
    public R save(@RequestBody SysMachineEntity machine){
		ValidatorUtils.validateEntity(machine);
		sysMachineService.insertMachine(machine);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:machine:update")
    public R update(@RequestBody SysMachineEntity machine){
		ValidatorUtils.validateEntity(machine);
		sysMachineService.update(machine);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:machine:delete")
    public R delete(@RequestBody Long[] ids){
			sysMachineService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
