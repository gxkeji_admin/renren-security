package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysRefundLogEntity;
import io.renren.modules.sys.service.SysRefundLogService;



/**
 * 退款日志表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/refundlog")
public class SysRefundLogController {
    @Autowired
    private SysRefundLogService sysRefundLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:refundlog:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysRefundLogService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:refundlog:info")
    public R info(@PathVariable("id") Long id){
			SysRefundLogEntity refundLog = sysRefundLogService.queryById(id);

        return R.ok().put("refundLog", refundLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:refundlog:save")
    public R save(@RequestBody SysRefundLogEntity refundLog){
			sysRefundLogService.insert(refundLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:refundlog:update")
    public R update(@RequestBody SysRefundLogEntity refundLog){
			sysRefundLogService.updateById(refundLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:refundlog:delete")
    public R delete(@RequestBody Long[] ids){
			sysRefundLogService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
