package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysDepositEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.dao.SysDepositDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDepositService;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysWxUserService;


@Service("sysDepositService")
public class SysDepositServiceImpl extends ServiceImpl<SysDepositDao, SysDepositEntity> implements SysDepositService {

	@Autowired
	private SysWxUserService sysWxUserService;
	
	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysDepositEntity> page = this.selectPage(new Query<SysDepositEntity>(params).getPage(),
				new EntityWrapper<SysDepositEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));

		for (SysDepositEntity sysDepositEntity : page.getRecords()) {
			SysWxUserEntity sysWxUserEntity = sysWxUserService.queryByMinaOpenid(sysDepositEntity.getOpenid());
			sysDepositEntity.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
			SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysDepositEntity.getDeptId());
			sysDepositEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return new PageUtils(page);
	}
    
	@Override
	public SysDepositEntity queryById(Long id) {
		SysDepositEntity deposit = this.selectById(id);
		if(deposit != null) {
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", deposit.getOpenid()));
    		deposit.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(deposit.getDeptId());
    		deposit.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return deposit;
	}
}
