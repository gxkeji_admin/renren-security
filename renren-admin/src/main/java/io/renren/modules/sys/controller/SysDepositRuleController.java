package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysDepositRuleEntity;
import io.renren.modules.sys.service.SysDepositRuleService;



/**
 * 押金规则
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-30 02:50:36
 */
@RestController
@RequestMapping("sys/depositrule")
public class SysDepositRuleController {
    @Autowired
    private SysDepositRuleService sysDepositRuleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:depositrule:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDepositRuleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:depositrule:info")
    public R info(@PathVariable("id") Long id){
			SysDepositRuleEntity depositRule = sysDepositRuleService.selectById(id);

        return R.ok().put("depositRule", depositRule);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:depositrule:save")
    public R save(@RequestBody SysDepositRuleEntity depositRule){
			sysDepositRuleService.insert(depositRule);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:depositrule:update")
    public R update(@RequestBody SysDepositRuleEntity depositRule){
			sysDepositRuleService.updateById(depositRule);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:depositrule:delete")
    public R delete(@RequestBody Long[] ids){
			sysDepositRuleService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
