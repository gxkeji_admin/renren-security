package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysPaymentLogEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.dao.SysPaymentLogDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysPaymentLogService;
import io.renren.modules.sys.service.SysWxUserService;


@Service("sysPaymentLogService")
public class SysPaymentLogServiceImpl extends ServiceImpl<SysPaymentLogDao, SysPaymentLogEntity> implements SysPaymentLogService {

	@Autowired
	private SysWxUserService sysWxUserService;
	
	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysPaymentLogEntity> page = this.selectPage(new Query<SysPaymentLogEntity>(params).getPage(),
				new EntityWrapper<SysPaymentLogEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));
		for (SysPaymentLogEntity sysPaymentLogEntity : page.getRecords()) {
			SysWxUserEntity sysWxUserEntity = sysWxUserService.queryByMinaOpenid(sysPaymentLogEntity.getOpenid());
			sysPaymentLogEntity.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
			SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysPaymentLogEntity.getDeptId());
			sysPaymentLogEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return new PageUtils(page);
	}

	@Override
	public SysPaymentLogEntity queryById(Long id) {
		SysPaymentLogEntity paymentLog = this.selectById(id);
		if(paymentLog != null) {
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", paymentLog.getOpenid()));
    		paymentLog.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(paymentLog.getDeptId());
    		paymentLog.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return paymentLog;
	}
}
