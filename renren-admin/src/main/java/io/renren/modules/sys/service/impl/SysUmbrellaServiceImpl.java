package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.Constants;
import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.QrcodeUtil;
import io.renren.common.utils.Query;
import io.renren.entity.SysMachineEntity;
import io.renren.entity.SysUmbrellaEntity;
import io.renren.modules.sys.dao.SysUmbrellaDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysMachineService;
import io.renren.modules.sys.service.SysUmbrellaService;


@Service("sysUmbrellaService")
public class SysUmbrellaServiceImpl extends ServiceImpl<SysUmbrellaDao, SysUmbrellaEntity> implements SysUmbrellaService {

	@Autowired
	private SysDeptService sysDeptService;
	
	@Autowired
	private SysMachineService sysMachineService;
	
    @Override
    @DataFilter(subDept = true, user = false)
    public PageUtils queryPage(Map<String, Object> params) {
    	String chipCode = (String)params.get("chipCode");
        Page<SysUmbrellaEntity> page = this.selectPage(
                new Query<SysUmbrellaEntity>(params).getPage(),
                new EntityWrapper<SysUmbrellaEntity>().like(StringUtils.isNotBlank(chipCode),"chip_code", chipCode)
                .addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );
        
        for(SysUmbrellaEntity sysUmbrellaEntity : page.getRecords()){
        	SysMachineEntity sysMachineEntity = sysMachineService.selectById(sysUmbrellaEntity.getMachineId().longValue());
        	sysUmbrellaEntity.setMachineName(sysMachineEntity != null ? sysMachineEntity.getMachineName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysUmbrellaEntity.getDeptId() != null ? sysUmbrellaEntity.getDeptId().longValue() : "");
        	sysUmbrellaEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
    	};

        return new PageUtils(page);
    }

	@Override
	public boolean insertUmbrella(SysUmbrellaEntity param) {
		
		// 生成二维码
		String chipCode = param.getChipCode();
		String qrcode_content_umbrella = Constants.QRCODE_CONTENT_UMBRELLA + chipCode;
		param.setUmbrellaSceneUrl(qrcode_content_umbrella);
		param.setUmbrellaShortUrl(qrcode_content_umbrella);
		String localUrl = QrcodeUtil.createQrcodeWidthBottomTitle(Constants.UMBRELLA_QR_DIR,
				qrcode_content_umbrella, chipCode, chipCode);
		if (StringUtils.isNotBlank(localUrl) && localUrl.indexOf(Constants.FILE_BASE_DIR) != -1) {
			param.setUmbrellaQrcodePath(StringUtils.substringAfter(localUrl, Constants.FILE_BASE_DIR));
		} else {
			param.setUmbrellaQrcodePath(localUrl);
		}
		
		return this.insert(param);
	}

}
