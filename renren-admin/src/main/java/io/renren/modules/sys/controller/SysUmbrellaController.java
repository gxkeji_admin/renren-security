package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.entity.SysUmbrellaEntity;
import io.renren.modules.sys.service.SysUmbrellaService;



/**
 * 雨伞
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/umbrella")
public class SysUmbrellaController {
    @Autowired
    private SysUmbrellaService sysUmbrellaService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:umbrella:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysUmbrellaService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:umbrella:info")
    public R info(@PathVariable("id") Long id){
			SysUmbrellaEntity umbrella = sysUmbrellaService.selectById(id);

        return R.ok().put("umbrella", umbrella);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:umbrella:save")
    public R save(@RequestBody SysUmbrellaEntity umbrella){
		ValidatorUtils.validateEntity(umbrella);
		sysUmbrellaService.insertUmbrella(umbrella);

		return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:umbrella:update")
    public R update(@RequestBody SysUmbrellaEntity umbrella){
		ValidatorUtils.validateEntity(umbrella);
		sysUmbrellaService.updateById(umbrella);

		return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:umbrella:delete")
    public R delete(@RequestBody Long[] ids){
			sysUmbrellaService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
