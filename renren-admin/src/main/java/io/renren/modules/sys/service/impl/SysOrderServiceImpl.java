package io.renren.modules.sys.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.emum.OrderStatus;
import io.renren.entity.SysOrderEntity;
import io.renren.entity.SysRentRuleEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.dao.SysOrderDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysOrderService;
import io.renren.modules.sys.service.SysRentRuleService;
import io.renren.modules.sys.service.SysWxUserService;

@Service("sysOrderService")
public class SysOrderServiceImpl extends ServiceImpl<SysOrderDao, SysOrderEntity> implements SysOrderService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SysRentRuleService sysRentRuleService;
	
	@Autowired
	private SysDeptService sysDeptService;
	
	@Autowired
	private SysWxUserService sysWxUserService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysOrderEntity> page = this.selectPage(new Query<SysOrderEntity>(params).getPage(),
				new EntityWrapper<SysOrderEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));

        for(SysOrderEntity sysOrderEntity : page.getRecords()){
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysOrderEntity.getDeptId());
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", sysOrderEntity.getOpenid()));
    		sysOrderEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
    		sysOrderEntity.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    	}
		return new PageUtils(page);
	}
    
	@Override
	public SysOrderEntity queryById(Long id) {
		SysOrderEntity order = this.selectById(id);
		if(order != null) {
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", order.getOpenid()));
    		order.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(order.getDeptId());
    		order.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return order;
	}
    
	@Override
	public List<SysOrderEntity> queryListByStatus(String openid, int orderStatus) {
		List<SysOrderEntity> result = this.selectList(new EntityWrapper<SysOrderEntity>().eq("openid", openid)
				.eq("order_status", orderStatus).orderBy("id", Boolean.FALSE));
		return result;
	}
	
	@Override
	public List<SysOrderEntity> queryListByStatus(String openid, Integer array[]) {
		List<SysOrderEntity> result = this.selectList(new EntityWrapper<SysOrderEntity>().eq("openid", openid)
				.in("order_status", array).orderBy("id", Boolean.FALSE));
		return result;
	}

	@Override
	public void calculateOrder(SysOrderEntity order) {
		if (order != null) {
			Date startTime = order.getRentTime();
			Date returnTime = order.getReturnTime() != null ? order.getReturnTime() : new Date();
			double useTime = 0.0;
			int orderStatus = order.getOrderStatus().intValue();
			if (orderStatus == OrderStatus.SYSTEM_CANNCEL.getValue().intValue() || orderStatus == OrderStatus.WAIT_TAKE_OUT.getValue().intValue()) {
				return;
			}
			SysRentRuleEntity rentRule = sysRentRuleService.selectOne(new EntityWrapper<SysRentRuleEntity>().eq("dept_id", order.getId().longValue()));

			int freeType = rentRule.getFreeType();
			int freeValue = rentRule.getFreeValue();
			int perTypeValue = rentRule.getPerTypeValue();
			int rentType = rentRule.getRentType();
			BigDecimal rentValue = rentRule.getRentValue();

			if (OrderStatus.TO_BE_RETURN.getValue().intValue() == order.getOrderStatus().intValue() ||
			    OrderStatus.RETURN_UN_PAY.getValue().intValue() == order.getOrderStatus().intValue()) {
				// 免费时长单位:1|分，2|小时，3|天
				Date endTime = null;
				if (1 == freeType) {
					endTime = DateUtil.offsetMinute(returnTime, -freeValue);
				} else if (2 == freeType) {
					endTime = DateUtil.offsetHour(returnTime, -freeValue);
				} else if (3 == freeType) {
					endTime = DateUtil.offsetDay(returnTime, -freeValue);
				}

				if (startTime.before(endTime)) {
					// 计价单位：1|按小时计费，2|按天计费
					long diffMin = DateUtil.between(startTime, endTime, DateUnit.MINUTE, Boolean.TRUE);
					if (1 == rentType) {
						double diffHours = diffMin / 60.0 / perTypeValue;
						useTime = Math.ceil(diffHours);
					} else if (2 == rentType) {
						double diffDays = diffMin / 60.0 / 24.0 / perTypeValue;
						useTime = Math.ceil(diffDays);
					}
					
					BigDecimal totalPrice = NumberUtil.mul(useTime, rentValue);
					order.setTotalPrice(totalPrice);
				} else {
					order.setTotalPrice(BigDecimal.ZERO);
				}
			}
		}
	}
}
