package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysWxUserEntity;
import io.renren.entity.SysWxUserFaultEntity;
import io.renren.modules.sys.dao.SysWxUserFaultDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysWxUserFaultService;
import io.renren.modules.sys.service.SysWxUserService;


@Service("sysWxUserFaultService")
public class SysWxUserFaultServiceImpl extends ServiceImpl<SysWxUserFaultDao, SysWxUserFaultEntity> implements SysWxUserFaultService {

	@Autowired
	private SysDeptService sysDeptService;
	
	@Autowired
	private SysWxUserService sysWxUserService;
	
    @Override
    @DataFilter(subDept = true, user = false)
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysWxUserFaultEntity> page = this.selectPage(
                new Query<SysWxUserFaultEntity>(params).getPage(),
                new EntityWrapper<SysWxUserFaultEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
				(String) params.get(Constant.SQL_FILTER))
        );
        
        for(SysWxUserFaultEntity sysWxUserFaultEntity : page.getRecords()){
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysWxUserFaultEntity.getDeptId());
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", sysWxUserFaultEntity.getOpenid()));
    		sysWxUserFaultEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
    		sysWxUserFaultEntity.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    	}

        return new PageUtils(page);
    }

    
	@Override
	public SysWxUserFaultEntity queryById(Long id) {
		SysWxUserFaultEntity fault = this.selectById(id);
		if(fault != null) {
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", fault.getOpenid()));
    		fault.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(fault.getDeptId());
    		fault.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return fault;
	}
}
