package io.renren.modules.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysOrderEntity;

/**
 * 订单
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysOrderService extends IService<SysOrderEntity> {
	
	void calculateOrder(SysOrderEntity order);

    PageUtils queryPage(Map<String, Object> params);
    
    List<SysOrderEntity> queryListByStatus(String openid, int orderStatus);
    
    List<SysOrderEntity> queryListByStatus(String openid, Integer array[]);
    
    SysOrderEntity queryById(Long id);

}

