package io.renren.modules.sys.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.hutool.core.util.NumberUtil;
import io.renren.common.Constants;
import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.QrcodeUtil;
import io.renren.common.utils.Query;
import io.renren.entity.SysMachineChanncelEntity;
import io.renren.entity.SysMachineEntity;
import io.renren.modules.sys.dao.SysMachineDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysMachineChanncelService;
import io.renren.modules.sys.service.SysMachineService;


@Service("sysMachineService")
public class SysMachineServiceImpl extends ServiceImpl<SysMachineDao, SysMachineEntity> implements SysMachineService {

	@Autowired
	private SysDeptService sysDeptService;
	
	@Autowired
	private SysMachineChanncelService sysMachineChanncelService;
	
    @Override
    @DataFilter(subDept = true, user = false)
    public PageUtils queryPage(Map<String, Object> params) {
    	String machineCode = (String)params.get("machineCode");
        Page<SysMachineEntity> page = this.selectPage(
                new Query<SysMachineEntity>(params).getPage(),
                new EntityWrapper<SysMachineEntity>().like(StringUtils.isNotBlank(machineCode),"machine_code", machineCode)
                .addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );

        for(SysMachineEntity sysMachineEntity : page.getRecords()){
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysMachineEntity.getDeptId());
    		sysMachineEntity.setDeptName(sysDeptEntity.getName());
    	}
        
        return new PageUtils(page);
    }
    
    @Transactional
	@Override
	public boolean insertMachine(SysMachineEntity param) {
		boolean result = Boolean.FALSE;
		String nextMachineCode = this.getNextMachineCode();
		if (StringUtils.isNotEmpty(nextMachineCode)) {
			param.setMachineCode(nextMachineCode);
			param.setBattery(BigDecimal.valueOf(100.00));
			// 生成二维码
			String qrcode_content_machine = Constants.QRCODE_CONTENT_MACHINE + nextMachineCode;
			param.setMachineSceneUrl(qrcode_content_machine);
			param.setMachineShortUrl(qrcode_content_machine);
			String localUrl = QrcodeUtil.createQrcodeWidthBottomTitle(Constants.MACHINE_QR_DIR, qrcode_content_machine, nextMachineCode, nextMachineCode);
			if(StringUtils.isNotBlank(localUrl) && localUrl.indexOf(Constants.FILE_BASE_DIR) != -1) {
				param.setMachineQrcodePath(StringUtils.substringAfter(localUrl, Constants.FILE_BASE_DIR));
			} else {
				param.setMachineQrcodePath(localUrl);
			}
			param.setFixedQuantity(param.getFixedQuantity());//额定数量
			boolean insertMachineResult = this.insert(param);
			Integer chipChanncelNum = param.getChipChanncelNum();
			if(chipChanncelNum != null && insertMachineResult) {
				int num = chipChanncelNum.intValue();
				Wrapper<SysMachineEntity> wrapper = new EntityWrapper<>();
				wrapper.eq("machine_code", nextMachineCode);
				SysMachineEntity machine = this.selectOne(wrapper);
				if(num > 0 && machine != null) {
					String nextMachineCodeWithOutM = StringUtils.substringAfter(nextMachineCode, Constants.MACHINE_CODE_PREFIX);
					for(int i=1; i<= num; i++) {
						SysMachineChanncelEntity mChanncel = new SysMachineChanncelEntity();
						mChanncel.setMachineId(machine.getId());
						mChanncel.setChanncel(i);
						
						String channcelCode = Constants.MACHINE_CHANNCEL_CODE_PREFIX + nextMachineCodeWithOutM + i;
						String qrcode_content_channcel_url = Constants.QRCODE_CONTENT_CHANNCEL + channcelCode;
						String qrcode_content_channcel_short_url = Constants.QRCODE_CONTENT_CHANNCEL + channcelCode;
						mChanncel.setChanncelCode(channcelCode);
						
						mChanncel.setChanncelSceneUrl(qrcode_content_channcel_url);
						mChanncel.setChanncelShortUrl(qrcode_content_channcel_short_url);
						String localUrlOff = QrcodeUtil.createQrcodeWidthBottomTitle(Constants.MACHINE_CHANNCEL_QR_DIR, qrcode_content_channcel_url, channcelCode, channcelCode);
						if(StringUtils.isNotBlank(localUrlOff) && localUrlOff.indexOf(Constants.FILE_BASE_DIR) != -1) {
							mChanncel.setChanncelQrcodePath(StringUtils.substringAfter(localUrlOff, Constants.FILE_BASE_DIR));
						}
						boolean insertMachineChanncelResult = sysMachineChanncelService.insert(mChanncel);
						if(insertMachineResult) {
							if(insertMachineChanncelResult) {
								result = Boolean.TRUE;
							}
						}
					}
				}
			}
		} 
		return result;
	}
    
    @Transactional
	@Override
	public boolean update(SysMachineEntity param) {
		boolean result = Boolean.FALSE;
		if(param != null && param.getId() != null) {
			SysMachineEntity machine = this.selectById(param.getId().longValue());
			List<SysMachineChanncelEntity> machineChanncelList = sysMachineChanncelService.selectList(new EntityWrapper<SysMachineChanncelEntity>().eq("machine_id", param.getId().longValue()));
			if(machine != null) {
				int mcahineChanncelNum = 0;
				if(machineChanncelList == null) {
					mcahineChanncelNum = 0;
				}
				
				int inputChanncelNum = param.getChipChanncelNum(); // 界面输入伞道数量
				int createMachineChanncelNum = 0; // 需要生成的伞道数量
				if(inputChanncelNum > mcahineChanncelNum) {
					createMachineChanncelNum = inputChanncelNum;
				}
				if(createMachineChanncelNum > 0) {
					String nextMachineCodeWithOutM = StringUtils.substringAfter(machine.getMachineCode(), Constants.MACHINE_CODE_PREFIX);
					for(int i=machineChanncelList.size() + 1; i<= createMachineChanncelNum; i++) {
						SysMachineChanncelEntity mChanncel = new SysMachineChanncelEntity();
						mChanncel.setMachineId(machine.getId());
						mChanncel.setChanncel(i);
						
						String channcelCode = Constants.MACHINE_CHANNCEL_CODE_PREFIX + nextMachineCodeWithOutM + i;
						String qrcode_content_channcel_url = Constants.QRCODE_CONTENT_CHANNCEL + channcelCode;
						String qrcode_content_channcel_short_url = Constants.QRCODE_CONTENT_CHANNCEL + channcelCode;
						mChanncel.setChanncelCode(channcelCode);
						
						mChanncel.setChanncelSceneUrl(qrcode_content_channcel_url);
						mChanncel.setChanncelShortUrl(qrcode_content_channcel_short_url);
						String localUrlOff = QrcodeUtil.createQrcodeWidthBottomTitle(Constants.MACHINE_CHANNCEL_QR_DIR, qrcode_content_channcel_url, channcelCode, channcelCode);
						if(StringUtils.isNotBlank(localUrlOff) && localUrlOff.indexOf(Constants.FILE_BASE_DIR) != -1) {
							mChanncel.setChanncelQrcodePath(StringUtils.substringAfter(localUrlOff, Constants.FILE_BASE_DIR));
						}
						sysMachineChanncelService.insert(mChanncel);
					}
				}
			}
		}
		result = this.updateById(param);
		return result;
	}
	
	private String getNextMachineCode() {
		String nextMachineCode = "";
		Wrapper<SysMachineEntity> wrapper = new EntityWrapper<SysMachineEntity>();
		wrapper.orderBy("machine_code", false);
		List<SysMachineEntity> machines = this.selectList(wrapper);
		if (machines != null && !machines.isEmpty() && machines.size() > 0) {
			SysMachineEntity machine = machines.get(0);
			if (machine != null && StringUtils.isNotEmpty(machine.getMachineCode())) {
				String temp = StringUtils.substringAfter(machine.getMachineCode(), Constants.MACHINE_CODE_PREFIX);
				if (NumberUtil.isNumber(temp)) {
					long nextCode = Long.valueOf(temp).longValue() + 1L;
					nextMachineCode = Constants.MACHINE_CODE_PREFIX + nextCode;
				}
			}
		}else {
			nextMachineCode = Constants.MACHINE_CODE_PREFIX + Constants.MACHINE_QR_CONTENT;
		}
		return nextMachineCode;
	}

	@Override
	public SysMachineEntity queryMachineByStatus(String machineCode, int machineStatus) {
		return this.selectOne(new EntityWrapper<SysMachineEntity>().eq("machine_code", machineCode).eq("machine_status", machineStatus));
	}
}
