package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysRechargeCardEntity;
import io.renren.modules.sys.dao.SysRechargeCardDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysRechargeCardService;


@Service("sysRechargeCardService")
public class SysRechargeCardServiceImpl extends ServiceImpl<SysRechargeCardDao, SysRechargeCardEntity> implements SysRechargeCardService {

	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysRechargeCardEntity> page = this.selectPage(new Query<SysRechargeCardEntity>(params).getPage(),
				new EntityWrapper<SysRechargeCardEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));

		for (SysRechargeCardEntity sysRechargeCardEntity : page.getRecords()) {
			SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysRechargeCardEntity.getDeptId());
			sysRechargeCardEntity.setDeptName(sysDeptEntity.getName());
		}

		return new PageUtils(page);
	}

}
