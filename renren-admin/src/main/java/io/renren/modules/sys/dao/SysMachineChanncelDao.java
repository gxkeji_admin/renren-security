package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysMachineChanncelEntity;

/**
 * 伞道
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysMachineChanncelDao extends BaseMapper<SysMachineChanncelEntity> {
	
}
