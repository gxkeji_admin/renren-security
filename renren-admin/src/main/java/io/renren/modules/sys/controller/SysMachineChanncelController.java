package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysMachineChanncelEntity;
import io.renren.modules.sys.service.SysMachineChanncelService;



/**
 * 伞道
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/machinechanncel")
public class SysMachineChanncelController {
    @Autowired
    private SysMachineChanncelService sysMachineChanncelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:machinechanncel:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysMachineChanncelService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:machinechanncel:info")
    public R info(@PathVariable("id") Long id){
			SysMachineChanncelEntity machineChanncel = sysMachineChanncelService.selectById(id);

        return R.ok().put("machineChanncel", machineChanncel);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:machinechanncel:save")
    public R save(@RequestBody SysMachineChanncelEntity machineChanncel){
			sysMachineChanncelService.insert(machineChanncel);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:machinechanncel:update")
    public R update(@RequestBody SysMachineChanncelEntity machineChanncel){
			sysMachineChanncelService.updateById(machineChanncel);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:machinechanncel:delete")
    public R delete(@RequestBody Long[] ids){
			sysMachineChanncelService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
