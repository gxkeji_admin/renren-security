package io.renren.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysDepositRuleEntity;

/**
 * 押金规则
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-30 02:50:36
 */
public interface SysDepositRuleService extends IService<SysDepositRuleEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    SysDepositRuleEntity queryDepositRuleByDeptId(long deptId);
}

