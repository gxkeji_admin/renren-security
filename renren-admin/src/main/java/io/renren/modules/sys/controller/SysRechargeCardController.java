package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysRechargeCardEntity;
import io.renren.modules.sys.service.SysRechargeCardService;



/**
 * 充值卡
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/rechargecard")
public class SysRechargeCardController {
    @Autowired
    private SysRechargeCardService sysRechargeCardService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:rechargecard:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysRechargeCardService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:rechargecard:info")
    public R info(@PathVariable("id") Long id){
			SysRechargeCardEntity rechargeCard = sysRechargeCardService.selectById(id);

        return R.ok().put("rechargeCard", rechargeCard);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:rechargecard:save")
    public R save(@RequestBody SysRechargeCardEntity rechargeCard){
			sysRechargeCardService.insert(rechargeCard);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:rechargecard:update")
    public R update(@RequestBody SysRechargeCardEntity rechargeCard){
			sysRechargeCardService.updateById(rechargeCard);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:rechargecard:delete")
    public R delete(@RequestBody Long[] ids){
			sysRechargeCardService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
