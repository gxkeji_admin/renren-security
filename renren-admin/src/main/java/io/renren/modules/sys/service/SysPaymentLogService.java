package io.renren.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysPaymentLogEntity;

/**
 * 支付关系表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysPaymentLogService extends IService<SysPaymentLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    SysPaymentLogEntity queryById(Long id);
}

