package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysRechargeEntity;
import io.renren.modules.sys.service.SysRechargeService;



/**
 * 充值
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/recharge")
public class SysRechargeController {
    @Autowired
    private SysRechargeService sysRechargeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:recharge:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysRechargeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:recharge:info")
    public R info(@PathVariable("id") Long id){
			SysRechargeEntity recharge = sysRechargeService.queryById(id);

        return R.ok().put("recharge", recharge);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:recharge:save")
    public R save(@RequestBody SysRechargeEntity recharge){
			sysRechargeService.insert(recharge);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:recharge:update")
    public R update(@RequestBody SysRechargeEntity recharge){
			sysRechargeService.updateById(recharge);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:recharge:delete")
    public R delete(@RequestBody Long[] ids){
			sysRechargeService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
