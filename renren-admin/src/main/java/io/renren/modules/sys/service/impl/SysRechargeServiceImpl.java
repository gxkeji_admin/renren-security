package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysRechargeEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.dao.SysRechargeDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysRechargeService;
import io.renren.modules.sys.service.SysWxUserService;


@Service("sysRechargeService")
public class SysRechargeServiceImpl extends ServiceImpl<SysRechargeDao, SysRechargeEntity> implements SysRechargeService {

	@Autowired
	private SysDeptService sysDeptService;
	
	@Autowired
	private SysWxUserService sysWxUserService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysRechargeEntity> page = this.selectPage(new Query<SysRechargeEntity>(params).getPage(),
				new EntityWrapper<SysRechargeEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));
        for(SysRechargeEntity sysRechargeEntity : page.getRecords()){
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysRechargeEntity.getDeptId());
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", sysRechargeEntity.getOpenid()));
    		sysRechargeEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
    		sysRechargeEntity.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    	}
		return new PageUtils(page);
	}
    
	@Override
	public SysRechargeEntity queryById(Long id) {
		SysRechargeEntity recharge = this.selectById(id);
		if(recharge != null) {
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", recharge.getOpenid()));
    		recharge.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(recharge.getDeptId());
    		recharge.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return recharge;
	}
}
