package io.renren.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysRentRuleEntity;

/**
 * 租金规则
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-29 15:47:22
 */
public interface SysRentRuleService extends IService<SysRentRuleEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    SysRentRuleEntity queryRentRuleByDeptId(long deptId);
}

