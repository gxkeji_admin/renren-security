package io.renren.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysMachineEntity;

/**
 * 机器
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysMachineService extends IService<SysMachineEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    boolean insertMachine(SysMachineEntity param);
    
    boolean update(SysMachineEntity param);
    
	SysMachineEntity queryMachineByStatus(String machineCode, int machineStatus);
	
	
}

