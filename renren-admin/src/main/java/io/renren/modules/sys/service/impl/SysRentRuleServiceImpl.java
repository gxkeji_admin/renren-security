package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysRentRuleEntity;
import io.renren.modules.sys.dao.SysRentRuleDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysRentRuleService;


@Service("sysRentRuleService")
public class SysRentRuleServiceImpl extends ServiceImpl<SysRentRuleDao, SysRentRuleEntity> implements SysRentRuleService {

	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
    public PageUtils queryPage(Map<String, Object> params) {
    	String name = (String)params.get("name");
        Page<SysRentRuleEntity> page = this.selectPage(
                new Query<SysRentRuleEntity>(params).getPage(),
                new EntityWrapper<SysRentRuleEntity>().like(StringUtils.isNotBlank(name),"name", name)
                .addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );
        
		for(SysRentRuleEntity sysRentRuleEntity : page.getRecords()){
			SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysRentRuleEntity.getDeptId());
			sysRentRuleEntity.setDeptName(sysDeptEntity.getName());
		}

        return new PageUtils(page);
    }

	@Override
	public SysRentRuleEntity queryRentRuleByDeptId(long deptId) {
		SysRentRuleEntity result = this.selectOne(new EntityWrapper<SysRentRuleEntity>().eq("dept_id", deptId).eq("rule_status", 1));
		if(result != null) {
			return result;
		} else {
			return this.selectList(new EntityWrapper<SysRentRuleEntity>().orderBy("id", false)).get(0);
		}
	}
}
