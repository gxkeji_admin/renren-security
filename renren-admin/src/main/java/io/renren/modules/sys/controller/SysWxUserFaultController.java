package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import io.renren.entity.SysWxUserFaultEntity;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.sys.service.SysWxUserFaultService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 故障表
 *
 * @author oslive
 * @email apple_live@qq.com
 * @date 2018-05-22 23:55:47
 */
@RestController
@RequestMapping("sys/wxuserfault")
public class SysWxUserFaultController {
    @Autowired
    private SysWxUserFaultService sysWxUserFaultService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:wxuserfault:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysWxUserFaultService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:wxuserfault:info")
    public R info(@PathVariable("id") Long id){
        SysWxUserFaultEntity wxUserFault = sysWxUserFaultService.queryById(id);

        return R.ok().put("wxUserFault", wxUserFault);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:wxuserfault:save")
    public R save(@RequestBody SysWxUserFaultEntity wxUserFault){
        sysWxUserFaultService.insert(wxUserFault);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:wxuserfault:update")
    public R update(@RequestBody SysWxUserFaultEntity wxUserFault){
        ValidatorUtils.validateEntity(wxUserFault);
        sysWxUserFaultService.updateAllColumnById(wxUserFault);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:wxuserfault:delete")
    public R delete(@RequestBody Long[] ids){
        sysWxUserFaultService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
