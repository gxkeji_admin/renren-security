package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.sys.service.SysDepositService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.entity.SysDepositEntity;

/**
 * 押金
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/deposit")
public class SysDepositController {
	@Autowired
	private SysDepositService sysDepositService;

	/**
	 * 列表
	 */
	@RequestMapping("/list")
	@RequiresPermissions("sys:deposit:list")
	public R list(@RequestParam Map<String, Object> params) {
		PageUtils page = sysDepositService.queryPage(params);

		return R.ok().put("page", page);
	}

	/**
	 * 信息
	 */
	@RequestMapping("/info/{id}")
	@RequiresPermissions("sys:deposit:info")
	public R info(@PathVariable("id") Long id) {
		SysDepositEntity deposit = sysDepositService.queryById(id);

		return R.ok().put("deposit", deposit);
	}

	/**
	 * 保存
	 */
	@RequestMapping("/save")
	@RequiresPermissions("sys:deposit:save")
	public R save(@RequestBody SysDepositEntity deposit) {
		ValidatorUtils.validateEntity(deposit);
		sysDepositService.insert(deposit);

		return R.ok();
	}

	/**
	 * 修改
	 */
	@RequestMapping("/update")
	@RequiresPermissions("sys:deposit:update")
	public R update(@RequestBody SysDepositEntity deposit) {
		ValidatorUtils.validateEntity(deposit);
		sysDepositService.updateById(deposit);

		return R.ok();
	}

	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@RequiresPermissions("sys:deposit:delete")
	public R delete(@RequestBody Long[] ids) {
		sysDepositService.deleteBatchIds(Arrays.asList(ids));

		return R.ok();
	}

}
