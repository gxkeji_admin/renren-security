package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysRefundLogEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.dao.SysRefundLogDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysRefundLogService;
import io.renren.modules.sys.service.SysWxUserService;


@Service("sysRefundLogService")
public class SysRefundLogServiceImpl extends ServiceImpl<SysRefundLogDao, SysRefundLogEntity> implements SysRefundLogService {

	@Autowired
	private SysWxUserService sysWxUserService;
	
	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysRefundLogEntity> page = this.selectPage(new Query<SysRefundLogEntity>(params).getPage(),
				new EntityWrapper<SysRefundLogEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));

		for (SysRefundLogEntity sysRefundLogEntity : page.getRecords()) {
			SysWxUserEntity sysWxUserEntity = sysWxUserService.queryByMinaOpenid(sysRefundLogEntity.getOpenid());
			sysRefundLogEntity.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
			SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysRefundLogEntity.getDeptId());
			sysRefundLogEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return new PageUtils(page);
	}
    
	@Override
	public SysRefundLogEntity queryById(Long id) {
		SysRefundLogEntity refundLog = this.selectById(id);
		if(refundLog != null) {
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", refundLog.getOpenid()));
    		refundLog.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(refundLog.getDeptId());
    		refundLog.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return refundLog;
	}
}
