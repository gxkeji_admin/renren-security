package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysDepositEntity;
import io.renren.entity.SysIotCallbackLogEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.dao.SysIotCallbackLogDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysIotCallbackLogService;
import io.renren.modules.sys.service.SysWxUserService;


@Service("sysIotCallbackLogService")
public class SysIotCallbackLogServiceImpl extends ServiceImpl<SysIotCallbackLogDao, SysIotCallbackLogEntity> implements SysIotCallbackLogService {

	@Autowired
	private SysWxUserService sysWxUserService;
	
	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
		Page<SysIotCallbackLogEntity> page = this.selectPage(new Query<SysIotCallbackLogEntity>(params).getPage(),
				new EntityWrapper<SysIotCallbackLogEntity>().addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
						(String) params.get(Constant.SQL_FILTER)));
		for (SysIotCallbackLogEntity sysIotCallbackLogEntity : page.getRecords()) {
			SysWxUserEntity sysWxUserEntity = sysWxUserService.queryByMinaOpenid(sysIotCallbackLogEntity.getOpenid());
			sysIotCallbackLogEntity.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
			SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysIotCallbackLogEntity.getDeptId());
			sysIotCallbackLogEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return new PageUtils(page);
	}
    
	@Override
	public SysIotCallbackLogEntity queryById(Long id) {
		SysIotCallbackLogEntity iotCallbackLog = this.selectById(id);
		if(iotCallbackLog != null) {
    		SysWxUserEntity sysWxUserEntity = sysWxUserService.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", iotCallbackLog.getOpenid()));
    		iotCallbackLog.setNickName(sysWxUserEntity != null ? sysWxUserEntity.getNickName() : "未知");
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(iotCallbackLog.getDeptId());
    		iotCallbackLog.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return iotCallbackLog;
	}
}
