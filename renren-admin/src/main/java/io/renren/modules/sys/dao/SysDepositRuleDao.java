package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysDepositRuleEntity;

/**
 * 押金规则
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-30 02:50:36
 */
public interface SysDepositRuleDao extends BaseMapper<SysDepositRuleEntity> {
	
}
