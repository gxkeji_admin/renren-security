package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysRentRuleEntity;
import io.renren.modules.sys.service.SysRentRuleService;



/**
 * 租金规则
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-29 15:47:22
 */
@RestController
@RequestMapping("sys/rentrule")
public class SysRentRuleController {
    @Autowired
    private SysRentRuleService sysRentRuleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:rentrule:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysRentRuleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:rentrule:info")
    public R info(@PathVariable("id") Long id){
			SysRentRuleEntity rentRule = sysRentRuleService.selectById(id);

        return R.ok().put("rentRule", rentRule);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:rentrule:save")
    public R save(@RequestBody SysRentRuleEntity rentRule){
			sysRentRuleService.insert(rentRule);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:rentrule:update")
    public R update(@RequestBody SysRentRuleEntity rentRule){
			sysRentRuleService.updateById(rentRule);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:rentrule:delete")
    public R delete(@RequestBody Long[] ids){
			sysRentRuleService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
