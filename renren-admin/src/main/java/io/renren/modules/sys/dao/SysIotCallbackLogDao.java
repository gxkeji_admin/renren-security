package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysIotCallbackLogEntity;

/**
 * 操作iot获取数据日志记录
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-15 18:45:53
 */
public interface SysIotCallbackLogDao extends BaseMapper<SysIotCallbackLogEntity> {
	
}
