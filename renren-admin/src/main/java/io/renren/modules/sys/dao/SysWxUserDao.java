package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysWxUserEntity;

/**
 * 微信用户
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
public interface SysWxUserDao extends BaseMapper<SysWxUserEntity> {
	
}
