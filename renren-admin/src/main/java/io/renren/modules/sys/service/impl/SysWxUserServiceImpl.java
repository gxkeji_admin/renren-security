package io.renren.modules.sys.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.emum.DepositStatus;
import io.renren.entity.SysDepositEntity;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.dao.SysWxUserDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDepositService;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysWxUserService;


@Service("sysWxUserService")
public class SysWxUserServiceImpl extends ServiceImpl<SysWxUserDao, SysWxUserEntity> implements SysWxUserService {

	@Autowired
	private SysDepositService sysDepositService;
	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
	public PageUtils queryPage(Map<String, Object> params) {
    	String nickName = (String)params.get("nickName");
    	
		Page<SysWxUserEntity> page = this.selectPage(new Query<SysWxUserEntity>(params).getPage(),
				new EntityWrapper<SysWxUserEntity>().like(StringUtils.isNotBlank(nickName), "nick_name", nickName)
						.addFilterIfNeed(params.get(Constant.SQL_FILTER) != null,
								(String) params.get(Constant.SQL_FILTER)));

        for(SysWxUserEntity sysWxUserEntity : page.getRecords()){
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysWxUserEntity.getDeptId());
    		sysWxUserEntity.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
    	}
		return new PageUtils(page);
	}
    
	@Override
	public SysWxUserEntity queryById(Long id) {
		SysWxUserEntity wxUser = this.selectById(id);
		if(wxUser != null) {
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(wxUser.getDeptId());
    		wxUser.setDeptName(sysDeptEntity != null ? sysDeptEntity.getName() : "未知");
		}
		return wxUser;
	}

	@Override
	public SysWxUserEntity queryByMinaOpenid(String minaOpenid) {
		return this.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", minaOpenid));
	}
	
	@Override
	public SysWxUserEntity queryByOpenid(String openid) {
		return this.selectOne(new EntityWrapper<SysWxUserEntity>().eq("openid", openid));
	}
	
	/**
	 * 
	 * @Title: checkDepositAndBalance
	 * @Description: TODO<判断是否已经缴纳押金：false|未缴纳，true|已缴纳; 余额充足：true；欠费，false>
	 * @author oslive 2018年4月12日 上午12:03:14
	 *
	 * @param token
	 * @return
	 */
	@Override
	public Map<String, Boolean> checkDepositAndBalance(String token) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		if (StringUtils.isNotBlank(token)) {
			SysWxUserEntity wxUser = this.selectOne(new EntityWrapper<SysWxUserEntity>().eq("mina_openid", token));
			if (wxUser != null) {
				BigDecimal wxUserDeposit = wxUser.getDeposit();
				BigDecimal wxUserBalance = wxUser.getBalance();
				SysDepositEntity deposit = sysDepositService.selectOne(new EntityWrapper<SysDepositEntity>().eq("openid", token).eq("deposit_status", DepositStatus.LOST.getValue().intValue()));
				if(deposit != null) {
					if (wxUserDeposit != null && wxUserDeposit.compareTo(BigDecimal.ZERO) > 0) {
						result.put("deposit", Boolean.TRUE);
					}else {
						result.put("deposit", Boolean.FALSE);
					}
				} else {
					result.put("deposit", Boolean.FALSE);
				}
				if (wxUserBalance != null && wxUserBalance.compareTo(BigDecimal.ZERO) > -1) {
					result.put("balance", Boolean.TRUE);
				} else {
					result.put("balance", Boolean.FALSE);
				}
			} 
		}
		return result;
	}
}
