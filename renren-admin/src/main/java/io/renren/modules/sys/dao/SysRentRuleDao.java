package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.entity.SysRentRuleEntity;

/**
 * 租金规则
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-29 15:47:22
 */
public interface SysRentRuleDao extends BaseMapper<SysRentRuleEntity> {
	
}
