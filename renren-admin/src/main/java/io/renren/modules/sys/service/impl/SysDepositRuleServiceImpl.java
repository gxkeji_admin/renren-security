package io.renren.modules.sys.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.entity.SysDepositRuleEntity;
import io.renren.modules.sys.dao.SysDepositRuleDao;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.service.SysDepositRuleService;
import io.renren.modules.sys.service.SysDeptService;


@Service("sysDepositRuleService")
public class SysDepositRuleServiceImpl extends ServiceImpl<SysDepositRuleDao, SysDepositRuleEntity> implements SysDepositRuleService {

	@Autowired
	private SysDeptService sysDeptService;
	
    @Override
    @DataFilter(subDept = true, user = false)
    public PageUtils queryPage(Map<String, Object> params) {
    	String name = (String)params.get("name");
        Page<SysDepositRuleEntity> page = this.selectPage(
                new Query<SysDepositRuleEntity>(params).getPage(),
                new EntityWrapper<SysDepositRuleEntity>().like(StringUtils.isNotBlank(name),"name", name)
                .addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );

    	for(SysDepositRuleEntity sysDepositRuleEntity : page.getRecords()){
    		SysDeptEntity sysDeptEntity = sysDeptService.selectById(sysDepositRuleEntity.getDeptId());
    		sysDepositRuleEntity.setDeptName(sysDeptEntity.getName());
    	}
        
        return new PageUtils(page);
    }

	@Override
	public SysDepositRuleEntity queryDepositRuleByDeptId(long deptId) {
		SysDepositRuleEntity result = this.selectOne(new EntityWrapper<SysDepositRuleEntity>().eq("dept_id", deptId).eq("rule_status", 1));
		if(result != null) {
			return result;
		} else {
			return this.selectList(new EntityWrapper<SysDepositRuleEntity>().orderBy("id", false)).get(0);
		}
	}
}
