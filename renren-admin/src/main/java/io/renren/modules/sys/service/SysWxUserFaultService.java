package io.renren.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.utils.PageUtils;
import io.renren.entity.SysWxUserFaultEntity;

/**
 * 故障表
 *
 * @author oslive
 * @email apple_live@qq.com
 * @date 2018-05-22 23:55:47
 */
public interface SysWxUserFaultService extends IService<SysWxUserFaultEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    SysWxUserFaultEntity queryById(Long id);
}

