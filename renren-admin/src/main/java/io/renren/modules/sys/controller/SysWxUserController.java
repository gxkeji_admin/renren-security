package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.entity.SysWxUserEntity;
import io.renren.modules.sys.service.SysWxUserService;



/**
 * 微信用户
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@RestController
@RequestMapping("sys/wxuser")
public class SysWxUserController {
    @Autowired
    private SysWxUserService sysWxUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:wxuser:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysWxUserService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:wxuser:info")
    public R info(@PathVariable("id") Long id){
			SysWxUserEntity wxUser = sysWxUserService.queryById(id);

        return R.ok().put("wxUser", wxUser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:wxuser:save")
    public R save(@RequestBody SysWxUserEntity wxUser){
			sysWxUserService.insert(wxUser);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:wxuser:update")
    public R update(@RequestBody SysWxUserEntity wxUser){
			sysWxUserService.updateById(wxUser);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:wxuser:delete")
    public R delete(@RequestBody Long[] ids){
			sysWxUserService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
