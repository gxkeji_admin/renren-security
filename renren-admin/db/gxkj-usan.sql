/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50096
Source Host           : localhost:3306
Source Database       : gxkj-usan

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2018-10-05 10:31:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) default NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RenrenScheduler', 'TASK_2', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) default NULL,
  `JOB_GROUP` varchar(200) default NULL,
  `IS_NONCONCURRENT` varchar(1) default NULL,
  `REQUESTS_RECOVERY` varchar(1) default NULL,
  PRIMARY KEY  (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) default NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY  (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', null, 'io.renren.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200084C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C000A6D6574686F644E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000158BAF593307874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B020000787000000000000000017400047465737474000672656E72656E74000FE69C89E58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);
INSERT INTO `qrtz_job_details` VALUES ('RenrenScheduler', 'TASK_2', 'DEFAULT', null, 'io.renren.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200084C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C000A6D6574686F644E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000158C377C4607874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000274000574657374327074000FE697A0E58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000017800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RenrenScheduler', 'iZ2zegje0fzlqxpcbuz5p4Z1529841402730', '1535386215495', '15000');

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) default NULL,
  `STR_PROP_2` varchar(512) default NULL,
  `STR_PROP_3` varchar(512) default NULL,
  `INT_PROP_1` int(11) default NULL,
  `INT_PROP_2` int(11) default NULL,
  `LONG_PROP_1` bigint(20) default NULL,
  `LONG_PROP_2` bigint(20) default NULL,
  `DEC_PROP_1` decimal(13,4) default NULL,
  `DEC_PROP_2` decimal(13,4) default NULL,
  `BOOL_PROP_1` varchar(1) default NULL,
  `BOOL_PROP_2` varchar(1) default NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) default NULL,
  `NEXT_FIRE_TIME` bigint(13) default NULL,
  `PREV_FIRE_TIME` bigint(13) default NULL,
  `PRIORITY` int(11) default NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) default NULL,
  `CALENDAR_NAME` varchar(200) default NULL,
  `MISFIRE_INSTR` smallint(2) default NULL,
  `JOB_DATA` blob,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', null, '1535387400000', '1535385600000', '5', 'WAITING', 'CRON', '1522304526000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200084C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C000A6D6574686F644E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000158BAF593307874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B020000787000000000000000017400047465737474000672656E72656E74000FE69C89E58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);
INSERT INTO `qrtz_triggers` VALUES ('RenrenScheduler', 'TASK_2', 'DEFAULT', 'TASK_2', 'DEFAULT', null, '1522305000000', '-1', '5', 'PAUSED', 'CRON', '1522304527000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200084C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C000A6D6574686F644E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000158C377C4607874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000274000574657374327074000FE697A0E58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000017800);

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL auto_increment COMMENT '任务id',
  `bean_name` varchar(200) default NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) default NULL COMMENT '方法名',
  `params` varchar(2000) default NULL COMMENT '参数',
  `cron_expression` varchar(100) default NULL COMMENT 'cron表达式',
  `status` tinyint(4) default NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) default NULL COMMENT '备注',
  `create_time` datetime default NULL COMMENT '创建时间',
  PRIMARY KEY  (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1', 'testTask', 'test', 'renren', '0 0/30 * * * ?', '0', '有参数测试', '2016-12-01 23:16:46');
INSERT INTO `schedule_job` VALUES ('2', 'testTask', 'test2', null, '0 0/30 * * * ?', '1', '无参数测试', '2016-12-03 14:55:56');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL auto_increment COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) default NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) default NULL COMMENT '方法名',
  `params` varchar(2000) default NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) default NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime default NULL COMMENT '创建时间',
  PRIMARY KEY  (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4429 DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES ('1', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-03-29 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-03-29 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-03-29 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4', '1', 'testTask', 'test', 'renren', '0', null, '1070', '2018-03-29 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('5', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-03-29 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('6', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-03-29 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('7', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-03-29 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('8', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-03-29 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('9', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-03-29 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('10', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-03-30 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('11', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-03-30 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('12', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-03-30 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('13', '1', 'testTask', 'test', 'renren', '0', null, '1060', '2018-03-30 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('14', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-04-01 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('15', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-04-01 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('16', '1', 'testTask', 'test', 'renren', '0', null, '1059', '2018-04-01 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('17', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-04-01 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('18', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-04-01 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('19', '1', 'testTask', 'test', 'renren', '0', null, '1024', '2018-04-01 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('20', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-01 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('21', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-01 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('22', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-04-02 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('23', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-02 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('24', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-02 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('25', '1', 'testTask', 'test', 'renren', '0', null, '1066', '2018-04-02 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('26', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-02 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('27', '1', 'testTask', 'test', 'renren', '0', null, '1020', '2018-04-02 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('28', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-02 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('29', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-02 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('30', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-02 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('31', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-02 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('32', '1', 'testTask', 'test', 'renren', '0', null, '1016', '2018-04-02 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('33', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-02 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('34', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-02 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('35', '1', 'testTask', 'test', 'renren', '0', null, '1022', '2018-04-03 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('36', '1', 'testTask', 'test', 'renren', '0', null, '1023', '2018-04-03 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('37', '1', 'testTask', 'test', 'renren', '0', null, '1023', '2018-04-03 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('38', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-03 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('39', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-03 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('40', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('41', '1', 'testTask', 'test', 'renren', '0', null, '1024', '2018-04-03 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('42', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-03 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('43', '1', 'testTask', 'test', 'renren', '0', null, '1017', '2018-04-03 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('44', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-04-03 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('45', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-03 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('46', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-03 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('47', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('48', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('49', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-04-03 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('50', '1', 'testTask', 'test', 'renren', '0', null, '1023', '2018-04-03 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('51', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-04-03 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('52', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('53', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-04-03 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('54', '1', 'testTask', 'test', 'renren', '0', null, '1023', '2018-04-03 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('55', '1', 'testTask', 'test', 'renren', '0', null, '1016', '2018-04-03 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('56', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-03 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('57', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-04-03 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('58', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-03 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('59', '1', 'testTask', 'test', 'renren', '0', null, '1022', '2018-04-03 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('60', '1', 'testTask', 'test', 'renren', '0', null, '1024', '2018-04-03 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('61', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-03 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('62', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('63', '1', 'testTask', 'test', 'renren', '0', null, '1023', '2018-04-03 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('64', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-03 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('65', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-03 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('66', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-03 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('67', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-03 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('68', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-03 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('69', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('70', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-03 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('71', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('72', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-03 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('73', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-03 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('74', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-03 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('75', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('76', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-03 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('77', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-03 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('78', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-03 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('79', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-03 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('80', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-03 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('81', '1', 'testTask', 'test', 'renren', '0', null, '1148', '2018-04-03 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('82', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-03 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('83', '1', 'testTask', 'test', 'renren', '0', null, '1060', '2018-04-04 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('84', '1', 'testTask', 'test', 'renren', '0', null, '1231', '2018-04-04 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('85', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-04-04 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('86', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-04-04 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('87', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-04-07 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('88', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-04-07 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('89', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-07 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('90', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-07 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('91', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-07 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('92', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-04-07 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('93', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-07 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('94', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-07 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('95', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-07 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('96', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-07 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('97', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-07 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('98', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-07 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('99', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-07 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('100', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-07 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('101', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-07 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('102', '1', 'testTask', 'test', 'renren', '0', null, '1017', '2018-04-08 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('103', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-04-08 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('104', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-08 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('105', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('106', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-08 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('107', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('108', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-08 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('109', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('110', '1', 'testTask', 'test', 'renren', '0', null, '1023', '2018-04-08 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('111', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-08 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('112', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('113', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('114', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-04-08 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('115', '1', 'testTask', 'test', 'renren', '0', null, '1023', '2018-04-08 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('116', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('117', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-08 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('118', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('119', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-04-08 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('120', '1', 'testTask', 'test', 'renren', '0', null, '1024', '2018-04-08 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('121', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('122', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-08 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('123', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('124', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-08 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('125', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('126', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-04-08 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('127', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('128', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-08 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('129', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-08 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('130', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('131', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-08 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('132', '1', 'testTask', 'test', 'renren', '0', null, '1181', '2018-04-08 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('133', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('134', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('135', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('136', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-08 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('137', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-08 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('138', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('139', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('140', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-08 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('141', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-08 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('142', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-08 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('143', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-08 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('144', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-04-08 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('145', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-08 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('146', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-08 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('147', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-08 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('148', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-08 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('149', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-04-08 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('150', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-09 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('151', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-04-09 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('152', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-09 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('153', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-04-09 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('154', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-09 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('155', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('156', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('157', '1', 'testTask', 'test', 'renren', '0', null, '1016', '2018-04-09 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('158', '1', 'testTask', 'test', 'renren', '0', null, '1017', '2018-04-09 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('159', '1', 'testTask', 'test', 'renren', '0', null, '1021', '2018-04-09 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('160', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-09 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('161', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-09 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('162', '1', 'testTask', 'test', 'renren', '0', null, '1016', '2018-04-09 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('163', '1', 'testTask', 'test', 'renren', '0', null, '1017', '2018-04-09 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('164', '1', 'testTask', 'test', 'renren', '0', null, '1018', '2018-04-09 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('165', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-09 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('166', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-04-09 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('167', '1', 'testTask', 'test', 'renren', '0', null, '1015', '2018-04-09 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('168', '1', 'testTask', 'test', 'renren', '0', null, '1016', '2018-04-09 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('169', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-04-09 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('170', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-09 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('171', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-09 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('172', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-09 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('173', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('174', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-09 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('175', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-09 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('176', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-09 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('177', '1', 'testTask', 'test', 'renren', '0', null, '1177', '2018-04-09 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('178', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-09 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('179', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('180', '1', 'testTask', 'test', 'renren', '0', null, '1017', '2018-04-09 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('181', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-04-09 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('182', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('183', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('184', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('185', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('186', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-09 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('187', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('188', '1', 'testTask', 'test', 'renren', '0', null, '1021', '2018-04-09 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('189', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-09 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('190', '1', 'testTask', 'test', 'renren', '0', null, '1017', '2018-04-09 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('191', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-04-09 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('192', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-04-09 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('193', '1', 'testTask', 'test', 'renren', '0', null, '1279', '2018-04-16 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('194', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-04-16 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('195', '1', 'testTask', 'test', 'renren', '0', null, '1037', '2018-04-16 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('196', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-04-16 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('197', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-04-16 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('198', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-04-16 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('199', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-04-16 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('200', '1', 'testTask', 'test', 'renren', '0', null, '1036', '2018-04-16 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('201', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-16 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('202', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-04-17 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('203', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-17 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('204', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-04-17 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('205', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-17 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('206', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-04-17 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('207', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-17 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('208', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-17 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('209', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-04-17 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('210', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-04-17 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('211', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-17 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('212', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-04-17 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('213', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-04-17 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('214', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-04-17 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('215', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-04-17 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('216', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-04-17 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('217', '1', 'testTask', 'test', 'renren', '0', null, '1037', '2018-04-17 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('218', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-17 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('219', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-04-17 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('220', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-04-17 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('221', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-04-17 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('222', '1', 'testTask', 'test', 'renren', '0', null, '1036', '2018-04-17 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('223', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-04-17 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('224', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-04-17 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('225', '1', 'testTask', 'test', 'renren', '0', null, '1070', '2018-04-17 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('226', '1', 'testTask', 'test', 'renren', '0', null, '1063', '2018-04-17 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('227', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-04-17 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('228', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-05-15 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('229', '1', 'testTask', 'test', 'renren', '0', null, '1446', '2018-05-15 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('230', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-05-18 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('231', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-05-18 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('232', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-05-18 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('233', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-05-19 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('234', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-05-19 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('235', '1', 'testTask', 'test', 'renren', '0', null, '1060', '2018-05-19 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('236', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-05-19 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('237', '1', 'testTask', 'test', 'renren', '0', null, '1014', '2018-05-19 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('238', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-05-19 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('239', '1', 'testTask', 'test', 'renren', '0', null, '1066', '2018-05-19 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('240', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-05-19 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('241', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-05-19 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('242', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-05-19 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('243', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-05-20 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('244', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-05-20 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('245', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-05-20 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('246', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-05-20 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('247', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-05-20 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('248', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-05-20 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('249', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-05-20 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('250', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-05-20 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('251', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-05-20 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('252', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-05-20 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('253', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-05-20 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('254', '1', 'testTask', 'test', 'renren', '0', null, '1066', '2018-05-20 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('255', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-05-20 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('256', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-05-20 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('257', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-05-20 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('258', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-05-23 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('259', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-05-23 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('260', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-05-23 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('261', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-05-23 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('262', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-05-23 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('263', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-05-23 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('264', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-05-23 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('265', '1', 'testTask', 'test', 'renren', '0', null, '1036', '2018-05-23 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('266', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-05-28 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('267', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-05-28 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('268', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-05-28 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('269', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-05-28 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('270', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-05-30 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('271', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-05-30 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('272', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-05-30 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('273', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-05-30 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('274', '1', 'testTask', 'test', 'renren', '0', null, '1135', '2018-05-30 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('275', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-05-30 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('276', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-05-30 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('277', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-05-30 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('278', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-05-30 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('279', '1', 'testTask', 'test', 'renren', '0', null, '1197', '2018-05-31 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('280', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-05-31 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('281', '1', 'testTask', 'test', 'renren', '0', null, '1071', '2018-05-31 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('282', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-05-31 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('283', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-05-31 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('284', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-05-31 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('285', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-05-31 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('286', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-05-31 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('287', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-05-31 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('288', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-05-31 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('289', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-05-31 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('290', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-05-31 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('291', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-05-31 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('292', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-05-31 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('293', '1', 'testTask', 'test', 'renren', '0', null, '1037', '2018-05-31 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('294', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-05-31 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('295', '1', 'testTask', 'test', 'renren', '0', null, '1020', '2018-05-31 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('296', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-05-31 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('297', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-05-31 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('298', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-05-31 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('299', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-05-31 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('300', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-05-31 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('301', '1', 'testTask', 'test', 'renren', '0', null, '1069', '2018-05-31 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('302', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-05-31 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('303', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-05-31 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('304', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-05-31 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('305', '1', 'testTask', 'test', 'renren', '0', null, '1033', '2018-05-31 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('306', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-05-31 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('307', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-05-31 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('308', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-05-31 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('309', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-05-31 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('310', '1', 'testTask', 'test', 'renren', '0', null, '1021', '2018-05-31 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('311', '1', 'testTask', 'test', 'renren', '0', null, '1037', '2018-05-31 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('312', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-05-31 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('313', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-05-31 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('314', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-05-31 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('315', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-05-31 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('316', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-05-31 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('317', '1', 'testTask', 'test', 'renren', '0', null, '1030', '2018-05-31 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('318', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-05-31 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('319', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-05-31 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('320', '1', 'testTask', 'test', 'renren', '0', null, '1018', '2018-05-31 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('321', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-05-31 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('322', '1', 'testTask', 'test', 'renren', '0', null, '1031', '2018-05-31 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('323', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-05-31 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('324', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-01 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('325', '1', 'testTask', 'test', 'renren', '0', null, '1018', '2018-06-01 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('326', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-06-01 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('327', '1', 'testTask', 'test', 'renren', '0', null, '1018', '2018-06-01 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('328', '1', 'testTask', 'test', 'renren', '0', null, '1017', '2018-06-01 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('329', '1', 'testTask', 'test', 'renren', '0', null, '1019', '2018-06-01 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('330', '1', 'testTask', 'test', 'renren', '0', null, '1016', '2018-06-01 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('331', '1', 'testTask', 'test', 'renren', '0', null, '1013', '2018-06-01 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('332', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-06-01 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('333', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-06-01 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('334', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-06-01 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('335', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-06-01 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('336', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-06-01 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('337', '1', 'testTask', 'test', 'renren', '0', null, '1034', '2018-06-01 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('338', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-06-01 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('339', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-06-01 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('340', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-06-01 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('341', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-06-01 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('342', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-06-01 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('343', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-06-01 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('344', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-06-01 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('345', '1', 'testTask', 'test', 'renren', '0', null, '1027', '2018-06-01 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('346', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-06-01 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('347', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-06-01 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('348', '1', 'testTask', 'test', 'renren', '0', null, '1032', '2018-06-01 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('349', '1', 'testTask', 'test', 'renren', '0', null, '1035', '2018-06-01 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('350', '1', 'testTask', 'test', 'renren', '0', null, '1018', '2018-06-01 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('351', '1', 'testTask', 'test', 'renren', '0', null, '1028', '2018-06-01 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('352', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-06-01 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('353', '1', 'testTask', 'test', 'renren', '0', null, '1026', '2018-06-01 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('354', '1', 'testTask', 'test', 'renren', '0', null, '1025', '2018-06-01 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('355', '1', 'testTask', 'test', 'renren', '0', null, '1016', '2018-06-01 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('356', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-01 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('357', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-01 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('358', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-01 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('359', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-01 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('360', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-02 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('361', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-02 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('362', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-02 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('363', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-02 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('364', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-02 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('365', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-02 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('366', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-02 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('367', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-02 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('368', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-02 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('369', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-02 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('370', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-06-02 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('371', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-02 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('372', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-02 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('373', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-02 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('374', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-02 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('375', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-02 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('376', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-06-02 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('377', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-02 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('378', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-02 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('379', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-02 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('380', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-02 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('381', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-02 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('382', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-02 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('383', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-06-02 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('384', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-02 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('385', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-02 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('386', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-02 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('387', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-02 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('388', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-02 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('389', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-02 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('390', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-02 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('391', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-02 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('392', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-02 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('393', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-02 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('394', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-02 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('395', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-02 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('396', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-02 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('397', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-02 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('398', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-02 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('399', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-02 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('400', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-06-02 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('401', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-06-02 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('402', '1', 'testTask', 'test', 'renren', '0', null, '1140', '2018-06-02 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('403', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-02 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('404', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-03 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('405', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-06-03 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('406', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-03 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('407', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-03 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('408', '1', 'testTask', 'test', 'renren', '0', null, '1145', '2018-06-03 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('409', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-03 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('410', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-06-03 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('411', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-03 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('412', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-06-03 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('413', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-03 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('414', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-03 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('415', '1', 'testTask', 'test', 'renren', '0', null, '1151', '2018-06-03 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('416', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-06-03 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('417', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-03 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('418', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-03 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('419', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-06-03 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('420', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-03 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('421', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-03 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('422', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-03 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('423', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-03 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('424', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-03 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('425', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-03 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('426', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-03 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('427', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-03 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('428', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-03 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('429', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-03 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('430', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-03 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('431', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-03 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('432', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-03 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('433', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-03 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('434', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-03 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('435', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-03 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('436', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-03 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('437', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-03 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('438', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-03 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('439', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-03 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('440', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-03 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('441', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-03 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('442', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-03 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('443', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-03 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('444', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-03 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('445', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-03 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('446', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-06-03 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('447', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-03 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('448', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-03 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('449', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-03 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('450', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-03 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('451', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-03 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('452', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-04 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('453', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-04 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('454', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-04 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('455', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-04 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('456', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-04 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('457', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-04 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('458', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-04 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('459', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-04 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('460', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-04 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('461', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-04 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('462', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-04 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('463', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-04 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('464', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-06-04 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('465', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-04 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('466', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-04 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('467', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-04 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('468', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-04 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('469', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-04 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('470', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-04 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('471', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-04 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('472', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-04 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('473', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-04 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('474', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-04 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('475', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-04 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('476', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-04 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('477', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-04 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('478', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-04 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('479', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-04 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('480', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-04 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('481', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-04 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('482', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-04 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('483', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-04 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('484', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-04 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('485', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-04 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('486', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-04 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('487', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-04 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('488', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-04 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('489', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-04 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('490', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-04 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('491', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-04 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('492', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-04 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('493', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-04 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('494', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-04 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('495', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-04 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('496', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-04 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('497', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-04 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('498', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-04 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('499', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-04 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('500', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-05 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('501', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-05 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('502', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-05 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('503', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-05 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('504', '1', 'testTask', 'test', 'renren', '0', null, '1153', '2018-06-05 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('505', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-05 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('506', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-05 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('507', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-05 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('508', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-05 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('509', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-06-05 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('510', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-05 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('511', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-05 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('512', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-06-05 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('513', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-05 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('514', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-05 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('515', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-05 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('516', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-05 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('517', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-05 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('518', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-05 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('519', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-05 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('520', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-05 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('521', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-05 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('522', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-06-05 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('523', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-05 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('524', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-05 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('525', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-05 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('526', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-05 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('527', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-05 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('528', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-05 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('529', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-05 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('530', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-06-05 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('531', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-05 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('532', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-05 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('533', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-06-05 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('534', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-05 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('535', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-05 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('536', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-05 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('537', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-05 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('538', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-05 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('539', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-05 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('540', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-05 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('541', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-05 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('542', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-05 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('543', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-05 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('544', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-05 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('545', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-05 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('546', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-05 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('547', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-05 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('548', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-06 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('549', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('550', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-06-06 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('551', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-06 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('552', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-06 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('553', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-06 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('554', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-06 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('555', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-06 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('556', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('557', '1', 'testTask', 'test', 'renren', '0', null, '1147', '2018-06-06 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('558', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-06 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('559', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-06 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('560', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('561', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-06 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('562', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('563', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-06 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('564', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-06 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('565', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-06 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('566', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('567', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-06 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('568', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-06 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('569', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-06 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('570', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('571', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-06 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('572', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('573', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-06 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('574', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-06 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('575', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-06 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('576', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-06 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('577', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-06 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('578', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-06 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('579', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-06 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('580', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-06 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('581', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-06 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('582', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-06 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('583', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-06 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('584', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-06 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('585', '1', 'testTask', 'test', 'renren', '0', null, '1145', '2018-06-06 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('586', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-06 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('587', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-06 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('588', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-06 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('589', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-06 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('590', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-06 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('591', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-06 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('592', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-06 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('593', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-06 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('594', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-06 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('595', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-06 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('596', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-07 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('597', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-07 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('598', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-07 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('599', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-07 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('600', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-07 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('601', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-07 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('602', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-07 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('603', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-06-07 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('604', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-07 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('605', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-07 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('606', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-07 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('607', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-07 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('608', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-07 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('609', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-07 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('610', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-07 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('611', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-07 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('612', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-07 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('613', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-07 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('614', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-07 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('615', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-07 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('616', '1', 'testTask', 'test', 'renren', '0', null, '1147', '2018-06-07 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('617', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-07 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('618', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-07 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('619', '1', 'testTask', 'test', 'renren', '0', null, '1165', '2018-06-07 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('620', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-07 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('621', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-07 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('622', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-07 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('623', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-06-07 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('624', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-07 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('625', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-07 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('626', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-07 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('627', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-07 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('628', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-07 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('629', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-07 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('630', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-07 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('631', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-07 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('632', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-07 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('633', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-07 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('634', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-07 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('635', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-07 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('636', '1', 'testTask', 'test', 'renren', '0', null, '1140', '2018-06-07 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('637', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-07 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('638', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-07 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('639', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-07 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('640', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-07 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('641', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-07 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('642', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-06-07 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('643', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-07 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('644', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-08 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('645', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-08 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('646', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-08 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('647', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-08 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('648', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-08 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('649', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-08 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('650', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-08 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('651', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-08 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('652', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-08 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('653', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-08 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('654', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-08 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('655', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-08 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('656', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-06-08 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('657', '1', 'testTask', 'test', 'renren', '0', null, '1071', '2018-06-08 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('658', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-08 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('659', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-08 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('660', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-08 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('661', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-06-08 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('662', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('663', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-06-08 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('664', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-06-08 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('665', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-08 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('666', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-08 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('667', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-08 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('668', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-06-08 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('669', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-06-08 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('670', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-06-08 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('671', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-08 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('672', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('673', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-06-08 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('674', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-08 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('675', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('676', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-08 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('677', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('678', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-08 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('679', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-08 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('680', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-06-08 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('681', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('682', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('683', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-08 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('684', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-08 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('685', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-06-08 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('686', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-08 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('687', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('688', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('689', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('690', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-08 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('691', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-08 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('692', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-06-09 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('693', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-09 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('694', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-09 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('695', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-09 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('696', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-09 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('697', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-06-09 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('698', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-09 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('699', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-09 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('700', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-09 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('701', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('702', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-09 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('703', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-09 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('704', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-09 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('705', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-09 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('706', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-09 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('707', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('708', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('709', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-06-09 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('710', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-09 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('711', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('712', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-06-09 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('713', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-06-09 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('714', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('715', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-09 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('716', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-09 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('717', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-09 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('718', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-09 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('719', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('720', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('721', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-09 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('722', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-09 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('723', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-09 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('724', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-06-09 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('725', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-06-09 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('726', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-09 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('727', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-09 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('728', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-09 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('729', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-06-09 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('730', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-09 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('731', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-09 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('732', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-09 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('733', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-09 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('734', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-06-09 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('735', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-09 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('736', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-09 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('737', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-09 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('738', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-09 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('739', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-09 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('740', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-06-10 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('741', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-10 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('742', '1', 'testTask', 'test', 'renren', '0', null, '1256', '2018-06-10 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('743', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-06-10 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('744', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-06-10 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('745', '1', 'testTask', 'test', 'renren', '0', null, '1135', '2018-06-10 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('746', '1', 'testTask', 'test', 'renren', '0', null, '1148', '2018-06-10 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('747', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-10 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('748', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-06-10 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('749', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-06-10 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('750', '1', 'testTask', 'test', 'renren', '0', null, '1181', '2018-06-10 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('751', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-10 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('752', '1', 'testTask', 'test', 'renren', '0', null, '1151', '2018-06-10 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('753', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-10 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('754', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-10 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('755', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-10 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('756', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-10 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('757', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-10 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('758', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-10 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('759', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-10 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('760', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-10 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('761', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-10 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('762', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-10 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('763', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-06-10 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('764', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-10 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('765', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-10 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('766', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-10 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('767', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-10 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('768', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-10 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('769', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-06-10 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('770', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-10 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('771', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-10 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('772', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-10 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('773', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-10 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('774', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-10 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('775', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-10 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('776', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-10 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('777', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-10 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('778', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-10 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('779', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-10 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('780', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-10 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('781', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-10 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('782', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-10 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('783', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-10 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('784', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-10 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('785', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-10 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('786', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-10 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('787', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-10 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('788', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-11 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('789', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-11 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('790', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-11 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('791', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-11 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('792', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-11 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('793', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-11 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('794', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-11 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('795', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-11 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('796', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-06-11 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('797', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-11 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('798', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-11 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('799', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-11 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('800', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-11 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('801', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-11 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('802', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-11 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('803', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-11 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('804', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-11 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('805', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-11 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('806', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-06-11 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('807', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-11 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('808', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-11 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('809', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-11 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('810', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-11 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('811', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-11 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('812', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-11 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('813', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-11 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('814', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-11 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('815', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-11 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('816', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-11 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('817', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-11 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('818', '1', 'testTask', 'test', 'renren', '0', null, '1144', '2018-06-11 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('819', '1', 'testTask', 'test', 'renren', '0', null, '1141', '2018-06-11 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('820', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-11 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('821', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-11 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('822', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-11 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('823', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-11 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('824', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-11 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('825', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-11 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('826', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-11 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('827', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-11 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('828', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-11 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('829', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-11 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('830', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-11 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('831', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-11 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('832', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-11 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('833', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-06-11 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('834', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-11 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('835', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-11 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('836', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-12 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('837', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-12 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('838', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-12 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('839', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-12 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('840', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-12 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('841', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-12 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('842', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('843', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-12 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('844', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('845', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-12 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('846', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('847', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-12 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('848', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('849', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-12 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('850', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-12 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('851', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-12 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('852', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-12 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('853', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-12 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('854', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-12 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('855', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-12 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('856', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-12 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('857', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-12 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('858', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-12 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('859', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-06-12 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('860', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-12 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('861', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-12 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('862', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-12 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('863', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-12 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('864', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('865', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-12 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('866', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-12 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('867', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-12 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('868', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-12 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('869', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-12 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('870', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-12 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('871', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-12 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('872', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('873', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-12 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('874', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-12 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('875', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-12 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('876', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-12 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('877', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-12 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('878', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-12 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('879', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('880', '1', 'testTask', 'test', 'renren', '0', null, '1981', '2018-06-12 22:00:02');
INSERT INTO `schedule_job_log` VALUES ('881', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-12 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('882', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-12 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('883', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-12 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('884', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-13 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('885', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-13 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('886', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-13 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('887', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-13 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('888', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-13 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('889', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-13 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('890', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-13 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('891', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-13 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('892', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-13 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('893', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-13 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('894', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-13 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('895', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-13 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('896', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-13 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('897', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-06-13 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('898', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-13 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('899', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-13 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('900', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-13 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('901', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-13 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('902', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-13 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('903', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-13 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('904', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-13 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('905', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-06-13 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('906', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-13 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('907', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-13 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('908', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-06-13 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('909', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-06-13 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('910', '1', 'testTask', 'test', 'renren', '0', null, '1252', '2018-06-13 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('911', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-13 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('912', '1', 'testTask', 'test', 'renren', '0', null, '3924', '2018-06-13 14:00:02');
INSERT INTO `schedule_job_log` VALUES ('913', '1', 'testTask', 'test', 'renren', '0', null, '6147', '2018-06-13 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('914', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-13 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('915', '1', 'testTask', 'test', 'renren', '0', null, '1470', '2018-06-13 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('916', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-13 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('917', '1', 'testTask', 'test', 'renren', '0', null, '1284', '2018-06-13 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('918', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-13 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('919', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-13 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('920', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-13 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('921', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-13 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('922', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-13 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('923', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-13 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('924', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-06-13 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('925', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-13 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('926', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-13 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('927', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-13 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('928', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-13 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('929', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-13 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('930', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-13 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('931', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-13 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('932', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-14 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('933', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-14 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('934', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-14 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('935', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-14 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('936', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-14 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('937', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-06-14 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('938', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-14 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('939', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-14 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('940', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-14 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('941', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-14 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('942', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-14 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('943', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-14 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('944', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-14 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('945', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-14 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('946', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-14 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('947', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-14 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('948', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-14 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('949', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-14 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('950', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-14 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('951', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-14 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('952', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-14 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('953', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-14 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('954', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-14 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('955', '1', 'testTask', 'test', 'renren', '0', null, '1241', '2018-06-16 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('956', '1', 'testTask', 'test', 'renren', '0', null, '1037', '2018-06-16 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('957', '1', 'testTask', 'test', 'renren', '0', null, '1029', '2018-06-16 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('958', '1', 'testTask', 'test', 'renren', '0', null, '1036', '2018-06-16 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('959', '1', 'testTask', 'test', 'renren', '0', null, '1067', '2018-06-16 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('960', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-06-16 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('961', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-16 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('962', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-16 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('963', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-16 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('964', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-16 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('965', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-16 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('966', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-16 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('967', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-16 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('968', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-16 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('969', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-16 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('970', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-16 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('971', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-16 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('972', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-17 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('973', '1', 'testTask', 'test', 'renren', '0', null, '1144', '2018-06-17 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('974', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-06-17 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('975', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-17 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('976', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-17 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('977', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-17 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('978', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-17 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('979', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-06-17 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('980', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-17 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('981', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-06-17 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('982', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-17 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('983', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-06-17 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('984', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-17 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('985', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-17 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('986', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-06-17 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('987', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('988', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('989', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('990', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-17 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('991', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-17 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('992', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-17 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('993', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-17 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('994', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-17 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('995', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-17 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('996', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-17 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('997', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-17 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('998', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-17 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('999', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-17 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1000', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-17 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1001', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-17 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1002', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1003', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1004', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-17 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1005', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-17 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1006', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-17 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1007', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1008', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-17 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1009', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-17 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1010', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1011', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1012', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-17 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1013', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-17 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1014', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-17 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1015', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-17 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1016', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-17 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1017', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-17 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1018', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-17 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1019', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-17 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1020', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-18 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1021', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1022', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1023', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-18 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1024', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-18 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1025', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1026', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-18 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1027', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-18 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1028', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-18 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1029', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-18 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1030', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1031', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1032', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-18 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1033', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-18 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1034', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1035', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1036', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-06-18 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1037', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1038', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-18 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1039', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-18 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1040', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-18 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1041', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1042', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1043', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-18 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1044', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1045', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-18 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1046', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1047', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-18 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1048', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-18 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1049', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-06-18 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1050', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-18 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1051', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-18 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1052', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-18 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1053', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-18 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1054', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-18 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1055', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-06-18 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1056', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-18 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1057', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-18 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1058', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-18 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1059', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-18 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1060', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-18 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1061', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-18 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1062', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-18 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1063', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-18 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1064', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-18 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1065', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-06-18 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1066', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-18 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1067', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-18 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1068', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-06-19 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1069', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-19 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1070', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-19 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1071', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-19 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1072', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-19 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1073', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-19 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1074', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-19 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1075', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-19 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1076', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-19 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1077', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-19 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1078', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-19 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1079', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-19 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1080', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-19 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1081', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-19 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1082', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-19 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1083', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-19 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1084', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-19 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1085', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-19 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1086', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-19 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1087', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-19 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1088', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-19 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1089', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-19 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1090', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-19 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1091', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-19 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1092', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-19 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1093', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-19 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1094', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-19 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1095', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-19 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1096', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-19 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('1097', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-19 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('1098', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-06-19 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('1099', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-06-19 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('1100', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-19 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1101', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-19 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1102', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-06-19 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1103', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-19 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1104', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-19 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1105', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-19 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1106', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-19 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1107', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-19 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1108', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-19 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1109', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-19 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1110', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-19 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1111', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-19 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1112', '1', 'testTask', 'test', 'renren', '0', null, '1054', '2018-06-19 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1113', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-19 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1114', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-19 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1115', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-19 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1116', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-20 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1117', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-20 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1118', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-20 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1119', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-20 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1120', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-20 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1121', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-20 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1122', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-20 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1123', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-20 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1124', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-20 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1125', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-20 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1126', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-20 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1127', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-20 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1128', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1129', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-06-20 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1130', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-20 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1131', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-20 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1132', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-20 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1133', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-06-20 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1134', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1135', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-20 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1136', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-06-20 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1137', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1138', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-20 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1139', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-20 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1140', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-20 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1141', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1142', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-20 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1143', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-20 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1144', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-20 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1145', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-20 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1146', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1147', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-20 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1148', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-20 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1149', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-20 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1150', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-20 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1151', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-20 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1152', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-20 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1153', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1154', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-20 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1155', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-20 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1156', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-20 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1157', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-20 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1158', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1159', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-20 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1160', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-20 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1161', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-20 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1162', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-20 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1163', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-20 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1164', '1', 'testTask', 'test', 'renren', '0', null, '1157', '2018-06-21 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1165', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-21 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1166', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-21 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1167', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-21 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1168', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-06-21 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1169', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-21 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1170', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-06-21 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1171', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-21 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1172', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-21 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1173', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-06-21 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1174', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-06-21 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1175', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-21 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('1176', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-21 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('1177', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-06-21 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('1178', '1', 'testTask', 'test', 'renren', '0', null, '1063', '2018-06-21 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('1179', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-21 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('1180', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-21 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1181', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-21 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1182', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-21 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('1183', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-06-21 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1184', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-21 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1185', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-21 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1186', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-21 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1187', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-21 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1188', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-21 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1189', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-21 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1190', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-21 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1191', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-21 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1192', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-21 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1193', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-21 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1194', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-21 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1195', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-21 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1196', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-21 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1197', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-21 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1198', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-21 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1199', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-21 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1200', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-21 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1201', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-21 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1202', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-21 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1203', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-21 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1204', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-21 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1205', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-21 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1206', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-21 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1207', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-21 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1208', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-21 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1209', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-21 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1210', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-21 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1211', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-21 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1212', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-22 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1213', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-22 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1214', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-22 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1215', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-22 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1216', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-22 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1217', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-22 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1218', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-22 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1219', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-22 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1220', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-22 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1221', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-22 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1222', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-06-22 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1223', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-22 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('1224', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-22 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('1225', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-22 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('1226', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-22 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('1227', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-22 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('1228', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-06-22 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1229', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-22 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1230', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-06-22 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('1231', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-22 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1232', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-22 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1233', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-22 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1234', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-22 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('1235', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-22 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('1236', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-22 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1237', '1', 'testTask', 'test', 'renren', '0', null, '1054', '2018-06-22 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1238', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-22 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('1239', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-22 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('1240', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-22 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('1241', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-22 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('1242', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-22 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1243', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-22 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1244', '1', 'testTask', 'test', 'renren', '0', null, '1054', '2018-06-22 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('1245', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-22 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('1246', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-22 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('1247', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-22 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1248', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-22 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1249', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-22 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1250', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-22 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1251', '1', 'testTask', 'test', 'renren', '0', null, '1054', '2018-06-22 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1252', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-22 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('1253', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-22 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('1254', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-22 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('1255', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-22 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1256', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-22 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('1257', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-22 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1258', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-22 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1259', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-22 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('1260', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-23 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1261', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-06-23 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1262', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-23 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1263', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-06-23 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1264', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-06-23 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1265', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-23 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1266', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-23 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1267', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-23 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1268', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-23 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1269', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-23 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1270', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-23 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1271', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-23 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1272', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-23 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1273', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-23 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1274', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-23 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1275', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-23 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1276', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-06-23 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1277', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-23 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1278', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-23 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1279', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-23 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1280', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-23 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1281', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-06-23 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1282', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-23 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1283', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-23 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1284', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-23 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1285', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-23 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1286', '1', 'testTask', 'test', 'renren', '0', null, '1140', '2018-06-23 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1287', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-23 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1288', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-23 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1289', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-23 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1290', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-23 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1291', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-23 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1292', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-23 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1293', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-23 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1294', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-23 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1295', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-23 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1296', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-23 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1297', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-23 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1298', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-23 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1299', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-23 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1300', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-23 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1301', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-23 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1302', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-23 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1303', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-23 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1304', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-23 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1305', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-06-23 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1306', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-23 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1307', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-23 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1308', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-24 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1309', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-24 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1310', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-24 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1311', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-24 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1312', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-06-24 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1313', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-06-24 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1314', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-24 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1315', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-24 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1316', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-24 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1317', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-06-24 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1318', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-24 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1319', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-24 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('1320', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-24 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('1321', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-24 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1322', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-24 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('1323', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-24 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('1324', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-24 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1325', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-24 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('1326', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-24 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('1327', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-24 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('1328', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-24 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1329', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-24 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1330', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-24 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1331', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-24 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1332', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-24 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1333', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-24 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1334', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-24 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1335', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-24 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1336', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-24 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1337', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-06-24 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1338', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-24 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1339', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-06-24 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1340', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-24 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1341', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-24 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1342', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-06-24 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1343', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-24 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1344', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-06-24 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1345', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-24 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1346', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-24 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1347', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-24 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1348', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-06-24 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1349', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-06-24 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1350', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-24 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1351', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-24 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1352', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-06-24 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1353', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-06-24 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1354', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-24 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1355', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-24 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1356', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-25 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1357', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-25 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1358', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-25 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1359', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-25 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1360', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1361', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-25 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1362', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-25 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1363', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-25 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1364', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-25 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1365', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-06-25 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1366', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-06-25 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1367', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-25 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1368', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-25 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1369', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1370', '1', 'testTask', 'test', 'renren', '0', null, '1138', '2018-06-25 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1371', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-25 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1372', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-25 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1373', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-25 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1374', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-25 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1375', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-25 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1376', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-25 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1377', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1378', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-25 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1379', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-25 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1380', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-06-25 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1381', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-25 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1382', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-25 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1383', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-25 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1384', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1385', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-25 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1386', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-25 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1387', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-25 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1388', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-25 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1389', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-25 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1390', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1391', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-25 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1392', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1393', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1394', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-06-25 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1395', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-25 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1396', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-25 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1397', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-25 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1398', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-25 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1399', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-25 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1400', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-25 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1401', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-25 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1402', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-25 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1403', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-25 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1404', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-26 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1405', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1406', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-26 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1407', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-06-26 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1408', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1409', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-26 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1410', '1', 'testTask', 'test', 'renren', '0', null, '1153', '2018-06-26 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1411', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-26 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1412', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-06-26 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1413', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1414', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-26 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1415', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('1416', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-06-26 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('1417', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-26 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('1418', '1', 'testTask', 'test', 'renren', '0', null, '1144', '2018-06-26 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('1419', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-26 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('1420', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-26 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('1421', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-26 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('1422', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-26 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('1423', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-26 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('1424', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-06-26 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('1425', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-26 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('1426', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-26 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('1427', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-06-26 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('1428', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 12:00:01');
INSERT INTO `schedule_job_log` VALUES ('1429', '1', 'testTask', 'test', 'renren', '0', null, '1185', '2018-06-26 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('1430', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('1431', '1', 'testTask', 'test', 'renren', '0', null, '1154', '2018-06-26 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('1432', '1', 'testTask', 'test', 'renren', '0', null, '1151', '2018-06-26 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('1433', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-26 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('1434', '1', 'testTask', 'test', 'renren', '0', null, '1138', '2018-06-26 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('1435', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('1436', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('1437', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-26 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('1438', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-26 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('1439', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('1440', '1', 'testTask', 'test', 'renren', '0', null, '1154', '2018-06-26 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('1441', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-26 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('1442', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-26 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('1443', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-26 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('1444', '1', 'testTask', 'test', 'renren', '0', null, '1169', '2018-06-26 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('1445', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-06-26 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('1446', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-26 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('1447', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-26 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('1448', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-26 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('1449', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-26 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('1450', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-06-26 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('1451', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-26 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('1452', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-06-27 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1453', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-27 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1454', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-27 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1455', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-27 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1456', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-27 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1457', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-27 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1458', '1', 'testTask', 'test', 'renren', '0', null, '1063', '2018-06-27 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1459', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-27 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1460', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-27 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1461', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-27 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1462', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-06-27 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1463', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-06-27 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('1464', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-06-27 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('1465', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-27 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1466', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-27 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1467', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-27 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('1468', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-27 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('1469', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-27 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1470', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-27 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1471', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-27 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1472', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-06-27 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1473', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-27 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1474', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-27 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1475', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-27 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1476', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-27 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1477', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-27 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1478', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-27 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1479', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-27 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1480', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-27 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1481', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-06-27 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1482', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-06-27 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1483', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-27 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1484', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-06-27 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1485', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-06-27 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1486', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-27 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1487', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-27 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1488', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-27 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1489', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-27 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1490', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-27 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1491', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-27 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1492', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-06-27 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1493', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-27 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1494', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-27 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1495', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-27 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1496', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-27 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1497', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-06-27 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1498', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-27 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1499', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-27 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1500', '1', 'testTask', 'test', 'renren', '0', null, '1069', '2018-06-28 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1501', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-28 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1502', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-28 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1503', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-28 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1504', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-28 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1505', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-28 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1506', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-28 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1507', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-06-28 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1508', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-28 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1509', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-28 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1510', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-28 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1511', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-28 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1512', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-28 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1513', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-28 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1514', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-06-28 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1515', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-28 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1516', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-28 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1517', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-28 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1518', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-28 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1519', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-06-28 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1520', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-28 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1521', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-28 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1522', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-06-28 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1523', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-28 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1524', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-28 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1525', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-28 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1526', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-28 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1527', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-28 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1528', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-28 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1529', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-06-28 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1530', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-28 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1531', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-28 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1532', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-28 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1533', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-28 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1534', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-28 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1535', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-28 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1536', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-28 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1537', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-28 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1538', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-28 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1539', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-28 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1540', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-28 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1541', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-28 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1542', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-06-28 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1543', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-06-28 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1544', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-28 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1545', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-28 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1546', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-28 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1547', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-28 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1548', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-06-29 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1549', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-29 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1550', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-06-29 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1551', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-29 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1552', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-29 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1553', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-29 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1554', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-29 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1555', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-06-29 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1556', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-06-29 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1557', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-29 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1558', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-29 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1559', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-29 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1560', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-29 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1561', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-29 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1562', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-29 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1563', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-29 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1564', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-29 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1565', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-29 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1566', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-06-29 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1567', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-06-29 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1568', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-06-29 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1569', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-29 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1570', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-29 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1571', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-29 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1572', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-29 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1573', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-29 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1574', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-29 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1575', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-06-29 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1576', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-29 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1577', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-29 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1578', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-06-29 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1579', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-29 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1580', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-29 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1581', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-29 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1582', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-06-29 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1583', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-29 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1584', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-29 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1585', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-29 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('1586', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-06-29 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1587', '1', 'testTask', 'test', 'renren', '0', null, '1141', '2018-06-29 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1588', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-29 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1589', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-29 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1590', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-06-29 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1591', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-06-29 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1592', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-29 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1593', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-06-29 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1594', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-06-29 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1595', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-29 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1596', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-06-30 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1597', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-30 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1598', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-30 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1599', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-06-30 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1600', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-06-30 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1601', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-06-30 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1602', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-30 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1603', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-30 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1604', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-06-30 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1605', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-30 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1606', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-30 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1607', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-30 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1608', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-30 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1609', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-30 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1610', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-06-30 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1611', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-30 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1612', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-30 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1613', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-06-30 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1614', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-30 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1615', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-30 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1616', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-06-30 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1617', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-30 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1618', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-30 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1619', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-30 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1620', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-30 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1621', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-30 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1622', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-06-30 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1623', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-06-30 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1624', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-06-30 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1625', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-30 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1626', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-06-30 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1627', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-06-30 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1628', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-06-30 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1629', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-06-30 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1630', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-06-30 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1631', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-06-30 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1632', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-30 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1633', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-06-30 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1634', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-06-30 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1635', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-06-30 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1636', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-06-30 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1637', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-06-30 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1638', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-06-30 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1639', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-06-30 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1640', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-06-30 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1641', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-06-30 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1642', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-06-30 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1643', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-06-30 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1644', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-01 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1645', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-01 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1646', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-01 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1647', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-01 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1648', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-01 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1649', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-01 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1650', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-01 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1651', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-01 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1652', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-01 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1653', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-01 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1654', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-01 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1655', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-01 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1656', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-01 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1657', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-01 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1658', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-01 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1659', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-01 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1660', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-01 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1661', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-01 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1662', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-01 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1663', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-01 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1664', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-01 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1665', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-01 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1666', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-01 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1667', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-01 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1668', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-01 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1669', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-01 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1670', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-01 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1671', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-01 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1672', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-01 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1673', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-01 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1674', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-01 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1675', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-01 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1676', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-01 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1677', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-01 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1678', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-01 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1679', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-01 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1680', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-01 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('1681', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-01 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1682', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-01 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1683', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-01 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('1684', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-01 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1685', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-01 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1686', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-01 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('1687', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-01 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1688', '1', 'testTask', 'test', 'renren', '0', null, '1140', '2018-07-01 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1689', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-01 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1690', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-01 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1691', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-01 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1692', '1', 'testTask', 'test', 'renren', '0', null, '1144', '2018-07-02 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1693', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-02 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1694', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-02 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1695', '1', 'testTask', 'test', 'renren', '0', null, '1144', '2018-07-02 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1696', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-07-02 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1697', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-07-02 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1698', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-02 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1699', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-02 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1700', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-02 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1701', '1', 'testTask', 'test', 'renren', '0', null, '1066', '2018-07-02 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1702', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-02 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1703', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-02 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1704', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-02 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1705', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-07-02 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1706', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-02 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1707', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-02 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1708', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-02 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('1709', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-02 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1710', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-02 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1711', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-02 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1712', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-02 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1713', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-07-02 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1714', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-02 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1715', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-07-02 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('1716', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-02 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1717', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-07-02 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('1718', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-07-02 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('1719', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-02 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1720', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-02 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('1721', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-02 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('1722', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-02 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1723', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-02 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1724', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-02 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1725', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-07-02 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1726', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-02 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('1727', '1', 'testTask', 'test', 'renren', '0', null, '1067', '2018-07-02 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1728', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-02 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1729', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-07-02 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1730', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-07-02 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1731', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-07-02 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1732', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-02 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1733', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-02 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1734', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-02 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1735', '1', 'testTask', 'test', 'renren', '0', null, '1054', '2018-07-02 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1736', '1', 'testTask', 'test', 'renren', '0', null, '1067', '2018-07-02 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1737', '1', 'testTask', 'test', 'renren', '0', null, '1069', '2018-07-02 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1738', '1', 'testTask', 'test', 'renren', '0', null, '1069', '2018-07-02 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1739', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-02 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1740', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-03 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1741', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1742', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1743', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-03 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1744', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-07-03 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1745', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-07-03 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1746', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-03 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1747', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-03 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1748', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-03 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1749', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-03 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1750', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1751', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-03 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1752', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-07-03 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1753', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-07-03 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('1754', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-03 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1755', '1', 'testTask', 'test', 'renren', '0', null, '1066', '2018-07-03 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1756', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-07-03 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1757', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1758', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1759', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-07-03 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1760', '1', 'testTask', 'test', 'renren', '0', null, '1071', '2018-07-03 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1761', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-07-03 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1762', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1763', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1764', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1765', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-03 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1766', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1767', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-07-03 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1768', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1769', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-03 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1770', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-07-03 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1771', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-03 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1772', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-03 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1773', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-03 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1774', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-07-03 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1775', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-03 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('1776', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-03 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1777', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-07-03 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1778', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-03 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1779', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1780', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1781', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-03 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1782', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-07-03 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1783', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-03 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1784', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-03 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1785', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-07-03 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1786', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-03 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1787', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-03 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('1788', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-07-04 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1789', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-07-04 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1790', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-07-04 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1791', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-04 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1792', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-07-04 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1793', '1', 'testTask', 'test', 'renren', '0', null, '1220', '2018-07-04 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1794', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-04 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1795', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-04 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1796', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-04 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1797', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-07-04 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1798', '1', 'testTask', 'test', 'renren', '0', null, '1060', '2018-07-04 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1799', '1', 'testTask', 'test', 'renren', '0', null, '1140', '2018-07-04 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('1800', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-04 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1801', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-04 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1802', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-04 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1803', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-04 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1804', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-04 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1805', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-04 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1806', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-04 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1807', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-04 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1808', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-04 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1809', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-04 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1810', '1', 'testTask', 'test', 'renren', '0', null, '1169', '2018-07-04 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1811', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-04 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1812', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-04 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1813', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-04 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1814', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-04 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1815', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-04 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1816', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-04 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1817', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-04 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1818', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-04 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1819', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-07-04 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('1820', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-04 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('1821', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-04 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('1822', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-04 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('1823', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-04 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1824', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-04 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1825', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-04 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1826', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-04 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1827', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-04 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('1828', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-04 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('1829', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-04 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('1830', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-04 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('1831', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-04 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('1832', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-04 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('1833', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-04 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('1834', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-04 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1835', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-04 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('1836', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-05 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1837', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-05 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('1838', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-05 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('1839', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-05 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1840', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-05 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1841', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-05 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1842', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-05 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1843', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-05 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1844', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-07-05 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1845', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-05 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1846', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-05 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1847', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-05 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1848', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-07-05 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1849', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-05 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1850', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-05 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1851', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-05 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1852', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-05 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1853', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-05 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1854', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-05 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1855', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-07-05 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1856', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-05 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1857', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-05 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1858', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-05 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1859', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-05 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1860', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-05 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1861', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-05 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1862', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-05 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1863', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-05 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1864', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-05 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1865', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-05 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1866', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-05 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1867', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-05 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('1868', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-05 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1869', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-05 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1870', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-05 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1871', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-05 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1872', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-05 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1873', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-07-05 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('1874', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-05 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('1875', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-07-05 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('1876', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-05 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('1877', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-05 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('1878', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-05 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('1879', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-07-05 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('1880', '1', 'testTask', 'test', 'renren', '0', null, '1137', '2018-07-05 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('1881', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-05 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('1882', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-05 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('1883', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-05 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('1884', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-06 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('1885', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-06 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1886', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-06 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1887', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-06 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('1888', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-06 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('1889', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-06 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('1890', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-06 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('1891', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-06 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('1892', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-06 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('1893', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-06 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('1894', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-06 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('1895', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-06 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1896', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-06 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1897', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-06 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1898', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-06 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1899', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-06 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1900', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-06 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1901', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-06 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1902', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-06 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1903', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-06 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1904', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-06 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1905', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-06 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1906', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-06 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('1907', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-06 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('1908', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-06 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1909', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-06 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('1910', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-06 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('1911', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-06 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('1912', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-06 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('1913', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-06 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('1914', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-06 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('1915', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-06 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('1916', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-06 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('1917', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-06 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('1918', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-06 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('1919', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-06 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('1920', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-06 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('1921', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-06 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('1922', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-06 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('1923', '1', 'testTask', 'test', 'renren', '0', null, '1178', '2018-07-06 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('1924', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-07-06 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('1925', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-06 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('1926', '1', 'testTask', 'test', 'renren', '0', null, '1135', '2018-07-06 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('1927', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-06 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('1928', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-06 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('1929', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-07-06 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('1930', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-07-06 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('1931', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-07-06 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('1932', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-07 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1933', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-07 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1934', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-07 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1935', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-07-07 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1936', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-07-07 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1937', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-07 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1938', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-07 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1939', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-07-07 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1940', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-07 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1941', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-07-07 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1942', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-07 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1943', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-07-07 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('1944', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-07-07 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('1945', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-07 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1946', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-07 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1947', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-07 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1948', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-07 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1949', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-07 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1950', '1', 'testTask', 'test', 'renren', '0', null, '1176', '2018-07-07 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1951', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-07 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('1952', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-07 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('1953', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-07 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('1954', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-07 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('1955', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-07 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('1956', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-07 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('1957', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-07 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('1958', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-07 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('1959', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-07 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('1960', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-07-07 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('1961', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-07 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('1962', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-07-07 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('1963', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-07 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('1964', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-07 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('1965', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-07-07 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('1966', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-07-07 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('1967', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-07 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('1968', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-07-07 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('1969', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-07 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('1970', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-07-07 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('1971', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-07 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('1972', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-07 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('1973', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-07 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('1974', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-07 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('1975', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-07-07 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('1976', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-07 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('1977', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-07 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('1978', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-07 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('1979', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-07 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('1980', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-08 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('1981', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-08 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('1982', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-07-08 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('1983', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-08 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('1984', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-07-08 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('1985', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-07-08 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('1986', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-08 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('1987', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-07-08 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('1988', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-07-08 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('1989', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-08 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('1990', '1', 'testTask', 'test', 'renren', '0', null, '1180', '2018-07-08 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('1991', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-08 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('1992', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-08 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('1993', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-08 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('1994', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-08 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('1995', '1', 'testTask', 'test', 'renren', '0', null, '1137', '2018-07-08 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('1996', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-08 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('1997', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-08 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('1998', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-08 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('1999', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-08 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2000', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-08 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2001', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-07-08 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2002', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-08 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2003', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-08 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2004', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-08 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2005', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-08 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2006', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-08 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2007', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-08 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2008', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-08 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('2009', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-08 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2010', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-08 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2011', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-08 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2012', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-08 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2013', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-08 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2014', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-08 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2015', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-08 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2016', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-08 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2017', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-08 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2018', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-08 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2019', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-08 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2020', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-08 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2021', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-08 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2022', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-08 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2023', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-08 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2024', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-08 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2025', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-08 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2026', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-08 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2027', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-08 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2028', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-09 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2029', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-09 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2030', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-09 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2031', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2032', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-09 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2033', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-09 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2034', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-09 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2035', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-09 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2036', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-09 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2037', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-09 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2038', '1', 'testTask', 'test', 'renren', '0', null, '1138', '2018-07-09 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2039', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-09 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2040', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-09 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2041', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-09 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2042', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-09 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2043', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-09 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2044', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-09 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2045', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-09 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2046', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-09 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2047', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-09 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2048', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-09 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2049', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-09 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2050', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2051', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-09 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2052', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-09 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2053', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2054', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-09 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2055', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-09 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2056', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-09 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2057', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-07-09 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2058', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-09 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2059', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2060', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-09 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2061', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2062', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-09 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2063', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-09 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2064', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-09 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2065', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-09 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2066', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-09 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2067', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-09 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2068', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-09 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2069', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2070', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-09 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2071', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2072', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-09 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2073', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-09 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2074', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-09 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2075', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-09 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2076', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-07-10 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2077', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-10 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2078', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-10 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2079', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-10 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2080', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-10 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2081', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-10 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2082', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-10 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2083', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-10 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2084', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-10 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2085', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-10 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2086', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-10 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2087', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-10 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2088', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-10 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2089', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-10 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2090', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-10 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2091', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-10 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2092', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-10 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2093', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-10 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2094', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-10 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2095', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-10 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2096', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-10 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2097', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-10 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2098', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-10 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2099', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-10 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2100', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-10 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2101', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-10 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2102', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-10 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2103', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-07-10 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2104', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-10 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2105', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-10 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2106', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-10 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2107', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-10 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2108', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-10 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2109', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-07-10 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2110', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-10 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2111', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-10 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2112', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-10 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2113', '1', 'testTask', 'test', 'renren', '0', null, '1142', '2018-07-10 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2114', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-10 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2115', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-10 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2116', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-10 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2117', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-10 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2118', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-10 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2119', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-10 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2120', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-10 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2121', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-10 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2122', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-10 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2123', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-10 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2124', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-11 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2125', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-11 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2126', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-11 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2127', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-11 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2128', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-11 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2129', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-11 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2130', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-11 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2131', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-11 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2132', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-11 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2133', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-11 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2134', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-11 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2135', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-11 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2136', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-11 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2137', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-11 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('2138', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-11 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2139', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-11 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2140', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-11 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2141', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-11 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2142', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-11 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2143', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-11 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2144', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-11 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2145', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-11 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2146', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-11 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2147', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-07-11 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2148', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-11 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2149', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-07-11 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2150', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-11 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2151', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-11 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2152', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-11 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2153', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-11 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2154', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-11 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('2155', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-07-11 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('2156', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-11 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2157', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-11 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('2158', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-11 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('2159', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-11 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('2160', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-11 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('2161', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-11 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2162', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-11 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2163', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-11 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2164', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-11 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('2165', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-11 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2166', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-11 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('2167', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-07-11 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2168', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-11 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2169', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-11 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2170', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-07-11 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2171', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-11 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2172', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-12 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2173', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-12 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2174', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-12 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2175', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-12 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2176', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-12 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2177', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-12 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2178', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-12 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2179', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-12 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2180', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-12 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2181', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-07-12 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2182', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-12 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2183', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-12 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2184', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-12 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2185', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-12 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2186', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-12 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2187', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-12 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2188', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-12 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2189', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-12 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2190', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-12 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2191', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-12 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2192', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-12 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2193', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-12 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2194', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-12 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2195', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-12 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2196', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-12 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2197', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-12 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2198', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-12 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2199', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-12 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2200', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-12 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2201', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-12 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2202', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-12 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2203', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-12 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2204', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-12 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2205', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-12 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2206', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-12 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2207', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-12 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2208', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-12 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2209', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-12 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2210', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-12 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2211', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-12 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2212', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-12 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('2213', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-12 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('2214', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-07-12 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('2215', '1', 'testTask', 'test', 'renren', '0', null, '1157', '2018-07-12 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('2216', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-07-12 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('2217', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-12 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('2218', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-07-12 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('2219', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-12 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('2220', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-07-13 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('2221', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-07-13 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('2222', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-13 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('2223', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-07-13 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('2224', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-13 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('2225', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-13 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('2226', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-07-13 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('2227', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-13 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('2228', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-13 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2229', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-13 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2230', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-13 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2231', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-13 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2232', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-13 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2233', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-13 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2234', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-13 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2235', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-13 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2236', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-13 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2237', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-13 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2238', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-13 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2239', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-13 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2240', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-13 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2241', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-13 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2242', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-13 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2243', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-13 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2244', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-13 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2245', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-07-13 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2246', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-13 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2247', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-13 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2248', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-13 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2249', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-13 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2250', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-13 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2251', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-13 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2252', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-13 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2253', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-13 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2254', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-13 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2255', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-13 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2256', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-13 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2257', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-13 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2258', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-13 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2259', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-13 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2260', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-13 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2261', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-13 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2262', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-07-13 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2263', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-13 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2264', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-13 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2265', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-13 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2266', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-13 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2267', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-13 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2268', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-14 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2269', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-14 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2270', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-14 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2271', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-14 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('2272', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-14 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('2273', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-14 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('2274', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-07-14 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('2275', '1', 'testTask', 'test', 'renren', '0', null, '1135', '2018-07-14 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('2276', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-14 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('2277', '1', 'testTask', 'test', 'renren', '0', null, '1135', '2018-07-14 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('2278', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-14 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('2279', '1', 'testTask', 'test', 'renren', '0', null, '1148', '2018-07-14 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('2280', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-14 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('2281', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-14 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2282', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-14 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2283', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-14 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2284', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-14 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2285', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-14 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2286', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-14 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2287', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-14 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2288', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-14 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2289', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-14 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2290', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-14 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2291', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-14 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2292', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-14 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2293', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-14 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2294', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-14 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2295', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-14 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2296', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-14 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2297', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-14 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2298', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-14 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2299', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-14 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2300', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-14 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2301', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-14 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2302', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-14 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2303', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-14 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2304', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-14 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2305', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-14 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2306', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-14 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2307', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-14 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2308', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-14 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2309', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-14 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2310', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-14 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2311', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-14 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2312', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-14 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2313', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-14 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2314', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-14 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2315', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-14 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2316', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-15 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2317', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-15 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2318', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-15 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2319', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-15 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2320', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-15 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2321', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-15 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2322', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-15 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2323', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-15 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2324', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-15 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2325', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-15 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2326', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-15 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2327', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-07-15 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2328', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-07-15 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2329', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-07-15 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2330', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-15 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2331', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-15 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2332', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-15 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2333', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-07-15 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2334', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-15 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2335', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-15 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2336', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-15 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2337', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-15 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2338', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-15 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2339', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-15 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2340', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-15 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2341', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-15 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2342', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-15 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2343', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-15 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2344', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-15 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2345', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-15 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2346', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-15 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2347', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-07-15 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('2348', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-15 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('2349', '1', 'testTask', 'test', 'renren', '0', null, '1150', '2018-07-15 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('2350', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-07-15 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('2351', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-07-15 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('2352', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-15 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('2353', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-15 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('2354', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-15 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('2355', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-15 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('2356', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-15 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('2357', '1', 'testTask', 'test', 'renren', '0', null, '1141', '2018-07-15 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('2358', '1', 'testTask', 'test', 'renren', '0', null, '1142', '2018-07-15 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('2359', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-07-15 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('2360', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-15 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('2361', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-07-15 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('2362', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-15 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('2363', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-15 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('2364', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-16 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2365', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-16 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2366', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-16 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2367', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-07-16 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2368', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-16 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2369', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-16 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2370', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-16 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2371', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-16 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2372', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-16 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2373', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-16 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2374', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-16 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2375', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-16 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2376', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-16 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2377', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-16 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2378', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-16 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2379', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-16 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2380', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-16 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2381', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-16 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2382', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-16 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2383', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-16 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2384', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-16 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2385', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-16 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2386', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-16 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2387', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-16 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2388', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-07-16 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2389', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-16 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2390', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-16 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2391', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-16 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2392', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-16 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2393', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-16 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2394', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-16 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2395', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-16 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2396', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-16 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2397', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-16 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2398', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-16 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2399', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-07-16 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2400', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-16 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2401', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-16 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2402', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-16 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2403', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-16 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2404', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-16 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2405', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-16 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2406', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-16 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2407', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-16 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2408', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-16 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2409', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-16 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2410', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-16 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2411', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-16 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2412', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-17 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2413', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-17 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2414', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-17 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2415', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-17 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2416', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-17 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2417', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-17 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2418', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-17 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2419', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-17 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2420', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-17 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2421', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-17 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2422', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-17 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2423', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-17 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2424', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-17 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2425', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-17 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2426', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-17 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2427', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-17 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2428', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-17 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2429', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-17 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2430', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-17 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2431', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-17 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2432', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-17 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2433', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-17 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2434', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-17 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2435', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-17 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2436', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-17 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2437', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-17 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2438', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-17 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2439', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-17 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2440', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-17 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2441', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-17 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2442', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-17 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2443', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-17 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2444', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-17 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2445', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-17 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2446', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-17 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2447', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-17 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2448', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-17 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2449', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-17 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2450', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-17 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2451', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-17 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2452', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-17 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2453', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-17 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2454', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-17 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2455', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-17 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2456', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-07-17 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2457', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-17 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2458', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-17 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2459', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-17 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2460', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-18 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('2461', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-18 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('2462', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-18 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('2463', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-18 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('2464', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-18 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('2465', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-07-18 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('2466', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-18 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('2467', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-18 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('2468', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-18 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2469', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-18 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2470', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-18 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2471', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-18 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2472', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-18 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2473', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-18 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2474', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-18 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2475', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-18 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2476', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-18 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2477', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-18 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2478', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-18 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2479', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-18 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2480', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-18 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2481', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-18 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2482', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-18 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2483', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-18 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2484', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-18 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2485', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-18 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2486', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-18 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2487', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-18 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2488', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-07-18 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2489', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-18 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2490', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-18 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2491', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-18 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2492', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-18 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2493', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-18 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2494', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-18 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2495', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-18 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2496', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-18 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2497', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-18 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2498', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-07-18 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2499', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-18 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2500', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-18 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2501', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-18 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2502', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-18 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2503', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-18 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2504', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-18 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2505', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-18 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2506', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-18 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2507', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-18 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2508', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-19 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2509', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-19 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2510', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-19 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2511', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-19 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2512', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-19 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2513', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-19 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2514', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-07-19 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2515', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-19 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2516', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-19 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2517', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-19 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2518', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-19 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2519', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-19 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2520', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-19 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2521', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-19 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2522', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-19 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2523', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-19 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2524', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-19 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2525', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-19 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2526', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-19 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2527', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-19 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2528', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-19 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2529', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-19 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2530', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-19 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2531', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-19 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2532', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-19 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2533', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-19 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2534', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-19 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2535', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-19 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2536', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-19 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2537', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-19 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2538', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-19 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2539', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-19 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2540', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-19 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2541', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-07-19 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2542', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-19 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2543', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-19 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2544', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-19 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2545', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-19 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2546', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-19 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2547', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-19 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2548', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-19 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2549', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-19 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2550', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-19 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2551', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-19 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2552', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-19 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2553', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-19 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2554', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-19 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2555', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-19 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2556', '1', 'testTask', 'test', 'renren', '0', null, '1137', '2018-07-20 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('2557', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-20 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('2558', '1', 'testTask', 'test', 'renren', '0', null, '1153', '2018-07-20 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('2559', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-20 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('2560', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-20 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('2561', '1', 'testTask', 'test', 'renren', '0', null, '1215', '2018-07-20 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('2562', '1', 'testTask', 'test', 'renren', '0', null, '1152', '2018-07-20 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('2563', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-20 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('2564', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-07-20 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('2565', '1', 'testTask', 'test', 'renren', '0', null, '1144', '2018-07-20 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('2566', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-20 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('2567', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-20 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('2568', '1', 'testTask', 'test', 'renren', '0', null, '1149', '2018-07-20 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('2569', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2570', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-20 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2571', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-20 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2572', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-20 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2573', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-20 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2574', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-20 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2575', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-07-20 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2576', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2577', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-20 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2578', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-20 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2579', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-07-20 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2580', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2581', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-20 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2582', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-07-20 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2583', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-20 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2584', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2585', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2586', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-20 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2587', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-20 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2588', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-20 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2589', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2590', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-20 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2591', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2592', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-20 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2593', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2594', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-20 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2595', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-20 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2596', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-20 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2597', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-20 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2598', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-20 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2599', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-20 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2600', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-20 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2601', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-20 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2602', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-20 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2603', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-20 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2604', '1', 'testTask', 'test', 'renren', '0', null, '1142', '2018-07-21 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2605', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-21 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2606', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-21 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2607', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-21 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2608', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-21 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2609', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-21 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2610', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-21 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2611', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-21 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2612', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-21 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2613', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-21 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2614', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-21 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('2615', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-21 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2616', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-21 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2617', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-21 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2618', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-21 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('2619', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-21 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2620', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-21 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2621', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-21 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2622', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-21 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2623', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-21 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2624', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-21 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2625', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-21 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2626', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-21 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2627', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-21 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2628', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-21 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2629', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-21 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2630', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-07-21 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2631', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-21 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2632', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-21 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2633', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-21 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2634', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-07-21 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('2635', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-21 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2636', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-21 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2637', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-21 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2638', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-21 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2639', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-21 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2640', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-21 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2641', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-21 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2642', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-21 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2643', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-21 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2644', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-21 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2645', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-21 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2646', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-21 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2647', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-21 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2648', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-21 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2649', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-21 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2650', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-21 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2651', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-21 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2652', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-22 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2653', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-22 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2654', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-22 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2655', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-07-22 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2656', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-22 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2657', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-22 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2658', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-07-22 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2659', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-22 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2660', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-07-22 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2661', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-22 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2662', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-22 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2663', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-22 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2664', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-07-22 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2665', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-22 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2666', '1', 'testTask', 'test', 'renren', '0', null, '1067', '2018-07-22 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2667', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-07-22 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2668', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-07-22 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2669', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-07-22 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2670', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-22 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2671', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-07-22 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2672', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-22 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2673', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-07-22 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2674', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-22 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2675', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-07-22 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2676', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-07-22 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2677', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-07-22 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2678', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-22 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2679', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-07-22 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2680', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-22 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2681', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-22 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2682', '1', 'testTask', 'test', 'renren', '0', null, '1057', '2018-07-22 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2683', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-22 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2684', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-22 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2685', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-07-22 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2686', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-07-22 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2687', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-22 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2688', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-07-22 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2689', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-07-22 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2690', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-22 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2691', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-22 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2692', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-22 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2693', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-22 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2694', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-22 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2695', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-22 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2696', '1', 'testTask', 'test', 'renren', '0', null, '1057', '2018-07-22 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2697', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-07-22 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2698', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-07-22 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2699', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-07-22 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2700', '1', 'testTask', 'test', 'renren', '0', null, '1059', '2018-07-23 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2701', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-23 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2702', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-23 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2703', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-23 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2704', '1', 'testTask', 'test', 'renren', '0', null, '1057', '2018-07-23 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2705', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-23 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2706', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-07-23 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2707', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-23 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2708', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-23 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2709', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-07-23 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2710', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-23 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2711', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-23 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2712', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-23 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2713', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-23 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2714', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-07-23 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2715', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-23 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2716', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-07-23 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2717', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-23 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2718', '1', 'testTask', 'test', 'renren', '0', null, '1148', '2018-07-23 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2719', '1', 'testTask', 'test', 'renren', '0', null, '1147', '2018-07-23 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2720', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-23 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2721', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-23 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2722', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-07-23 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2723', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-23 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2724', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-23 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2725', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-23 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2726', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-23 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2727', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-23 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2728', '1', 'testTask', 'test', 'renren', '0', null, '1070', '2018-07-23 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2729', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-07-23 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2730', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-23 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('2731', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-23 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2732', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-23 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('2733', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-07-23 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('2734', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-23 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('2735', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-23 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('2736', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-07-23 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('2737', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-07-23 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('2738', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-07-23 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('2739', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-23 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2740', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-07-23 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('2741', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-23 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('2742', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-23 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('2743', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-23 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('2744', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-23 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('2745', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-23 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2746', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-23 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('2747', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-23 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2748', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-24 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('2749', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-24 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('2750', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-24 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2751', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-07-24 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('2752', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-24 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2753', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-24 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2754', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-24 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2755', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-24 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('2756', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-07-24 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('2757', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-24 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('2758', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-24 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2759', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-24 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('2760', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-24 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('2761', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-24 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2762', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-24 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2763', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-24 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2764', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-24 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2765', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-24 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2766', '1', 'testTask', 'test', 'renren', '0', null, '1162', '2018-07-24 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2767', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-24 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2768', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-24 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2769', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-24 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2770', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-24 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2771', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-24 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2772', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-24 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2773', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-24 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2774', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-07-24 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2775', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-24 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2776', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-24 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2777', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-24 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2778', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-24 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2779', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-24 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2780', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-24 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2781', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-24 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2782', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-24 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2783', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-24 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2784', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-24 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2785', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-24 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2786', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-24 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2787', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-24 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2788', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-24 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2789', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-24 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2790', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-24 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2791', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-24 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2792', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-24 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2793', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-24 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2794', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-24 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2795', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-24 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2796', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-25 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2797', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-25 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2798', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-25 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2799', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-25 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2800', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-25 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2801', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-25 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2802', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-25 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2803', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-25 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2804', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-25 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2805', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-07-25 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2806', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-25 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2807', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-25 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2808', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-25 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2809', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-25 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2810', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-25 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2811', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-25 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2812', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-25 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2813', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-25 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2814', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-25 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2815', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-25 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2816', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-25 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2817', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-25 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2818', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-25 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2819', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-25 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2820', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-25 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2821', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-25 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2822', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-25 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2823', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-25 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2824', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-25 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2825', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-25 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2826', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-25 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2827', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-25 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2828', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-25 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2829', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-25 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2830', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-25 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2831', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-25 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2832', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-25 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2833', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-25 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2834', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-25 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2835', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-25 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2836', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-25 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2837', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-25 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2838', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-25 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2839', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-25 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2840', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-25 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2841', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-25 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2842', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-25 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2843', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-25 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2844', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-26 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2845', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-26 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2846', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-26 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2847', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-26 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2848', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-26 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2849', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-07-26 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2850', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-26 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2851', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-26 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2852', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-26 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2853', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-26 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2854', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-26 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2855', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-26 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2856', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-26 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2857', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-26 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2858', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-26 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2859', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-26 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2860', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-26 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2861', '1', 'testTask', 'test', 'renren', '0', null, '1141', '2018-07-26 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2862', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-26 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2863', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-26 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2864', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-26 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2865', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-26 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2866', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-26 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2867', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-26 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2868', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-26 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2869', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-26 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2870', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-07-26 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2871', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-26 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2872', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-26 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2873', '1', 'testTask', 'test', 'renren', '0', null, '1164', '2018-07-26 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2874', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-07-26 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2875', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-26 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2876', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-26 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2877', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-26 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2878', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-26 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2879', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-26 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2880', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-26 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2881', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-26 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2882', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-26 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2883', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-26 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2884', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-26 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2885', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-26 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2886', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-26 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2887', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-07-26 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2888', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-07-26 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2889', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-26 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2890', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-26 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2891', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-07-26 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2892', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-07-27 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2893', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-27 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2894', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-27 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2895', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-27 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2896', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-27 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2897', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-27 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2898', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-27 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2899', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-27 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2900', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-27 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2901', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-27 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2902', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-27 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2903', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-07-27 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2904', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-27 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2905', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-27 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2906', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-27 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2907', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-07-27 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2908', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-27 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2909', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-27 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2910', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-07-27 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2911', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-27 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2912', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-27 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2913', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-27 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2914', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-27 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2915', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-27 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2916', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-27 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2917', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-27 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2918', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-27 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2919', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-27 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2920', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-27 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2921', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-27 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2922', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-27 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2923', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-27 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2924', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-27 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2925', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-07-27 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('2926', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-27 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2927', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-27 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2928', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-27 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2929', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-07-27 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2930', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-27 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2931', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-27 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2932', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-27 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2933', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-27 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2934', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-07-27 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2935', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-27 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2936', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-27 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2937', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-27 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2938', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-27 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2939', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-27 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2940', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-28 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2941', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-28 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2942', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-28 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2943', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-28 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2944', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-28 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2945', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-28 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2946', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-28 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2947', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-28 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2948', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-28 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2949', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-28 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2950', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-28 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2951', '1', 'testTask', 'test', 'renren', '0', null, '1039', '2018-07-28 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('2952', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-28 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('2953', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-28 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('2954', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-28 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('2955', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-28 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('2956', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-28 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2957', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-28 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('2958', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-07-28 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('2959', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-28 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('2960', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-28 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('2961', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-28 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('2962', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-28 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('2963', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-07-28 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('2964', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-28 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('2965', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-28 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('2966', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-28 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('2967', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-28 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('2968', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-28 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('2969', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-28 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('2970', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-28 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('2971', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-28 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('2972', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-28 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('2973', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-28 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('2974', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-28 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('2975', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-28 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('2976', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-28 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('2977', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-07-28 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('2978', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-28 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('2979', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-28 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('2980', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-28 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('2981', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-28 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('2982', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-28 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('2983', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-28 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2984', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-28 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('2985', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-28 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('2986', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-28 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('2987', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-28 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('2988', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-29 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('2989', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-29 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('2990', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-29 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('2991', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-07-29 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('2992', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('2993', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-29 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('2994', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('2995', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-29 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('2996', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-29 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('2997', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('2998', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-07-29 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('2999', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-29 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3000', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-07-29 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3001', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-29 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3002', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-29 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3003', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3004', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-29 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3005', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-29 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3006', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-29 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3007', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-29 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3008', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-07-29 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3009', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-29 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3010', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-29 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3011', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-29 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3012', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-29 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3013', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3014', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3015', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-29 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3016', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-07-29 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3017', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-07-29 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3018', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-07-29 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3019', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3020', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-29 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3021', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-29 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3022', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-29 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3023', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-29 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3024', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-29 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3025', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-29 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3026', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3027', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-29 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3028', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-29 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3029', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-29 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3030', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-29 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3031', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-29 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3032', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-29 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3033', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-07-29 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3034', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-29 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3035', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-29 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3036', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-30 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3037', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-30 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3038', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-30 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3039', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3040', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-30 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3041', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-30 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3042', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-30 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3043', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3044', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-07-30 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3045', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3046', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3047', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3048', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-30 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3049', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-30 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3050', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-30 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3051', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-30 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3052', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3053', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-30 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3054', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-07-30 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3055', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3056', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-07-30 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3057', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-30 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3058', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-30 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3059', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-30 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('3060', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3061', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-07-30 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3062', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-30 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3063', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-30 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3064', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-30 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3065', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-30 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3066', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-30 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3067', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-30 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3068', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-30 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3069', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-07-30 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3070', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-07-30 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3071', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-07-30 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3072', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-07-30 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3073', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-07-30 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3074', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-30 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3075', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-30 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3076', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-07-30 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3077', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-07-30 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3078', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-07-30 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('3079', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-07-30 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('3080', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-30 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3081', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-30 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3082', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-30 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3083', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-30 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3084', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-31 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3085', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-07-31 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3086', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-31 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3087', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-31 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3088', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-31 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3089', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-31 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3090', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-07-31 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3091', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-31 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3092', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-07-31 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3093', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-31 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3094', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-07-31 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3095', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-31 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3096', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-07-31 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3097', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-07-31 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3098', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-31 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3099', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-31 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3100', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-07-31 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3101', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-07-31 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3102', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-31 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3103', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-31 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3104', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-07-31 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3105', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-07-31 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3106', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-31 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3107', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-31 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3108', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-31 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3109', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-07-31 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3110', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-07-31 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3111', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-07-31 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3112', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-07-31 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3113', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-07-31 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3114', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-31 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3115', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-07-31 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3116', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-31 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3117', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-31 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3118', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-07-31 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3119', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-07-31 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3120', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-31 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3121', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-31 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3122', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-31 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3123', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-31 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3124', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-07-31 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3125', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-31 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3126', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-31 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3127', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-07-31 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3128', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-07-31 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3129', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-31 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3130', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-07-31 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3131', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-07-31 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3132', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-01 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3133', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-01 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3134', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-01 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3135', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-01 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3136', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-01 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3137', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-01 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3138', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-01 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3139', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-01 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3140', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-01 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3141', '1', 'testTask', 'test', 'renren', '0', null, '1071', '2018-08-01 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3142', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-01 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3143', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-01 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3144', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-01 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3145', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-01 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3146', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-01 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3147', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-01 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3148', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-01 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3149', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-01 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3150', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-01 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3151', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-01 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3152', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-01 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3153', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-01 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3154', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-01 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3155', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-01 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3156', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-01 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3157', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-01 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3158', '1', 'testTask', 'test', 'renren', '0', null, '1137', '2018-08-01 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3159', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-01 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3160', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-01 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3161', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-01 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3162', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-01 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3163', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-01 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3164', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-01 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3165', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-01 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3166', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-01 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3167', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-01 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3168', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-01 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3169', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-01 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3170', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-01 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3171', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-01 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3172', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-01 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3173', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-01 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3174', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-01 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3175', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-01 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3176', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-01 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3177', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-01 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3178', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-01 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3179', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-01 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3180', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-02 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3181', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-02 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3182', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-02 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3183', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-02 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3184', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-02 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3185', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-02 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3186', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-02 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3187', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-02 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3188', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-02 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3189', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-02 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3190', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-02 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3191', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-02 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3192', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-02 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3193', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-02 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3194', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-08-02 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('3195', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-02 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3196', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-02 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3197', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-02 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3198', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-02 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('3199', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-02 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3200', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-02 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3201', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-08-02 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('3202', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-02 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3203', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-02 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3204', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-02 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3205', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-02 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3206', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-02 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3207', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-02 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3208', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-02 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3209', '1', 'testTask', 'test', 'renren', '0', null, '1164', '2018-08-02 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3210', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-02 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3211', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-02 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3212', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-02 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('3213', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-02 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3214', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-02 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('3215', '1', 'testTask', 'test', 'renren', '0', null, '1057', '2018-08-02 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('3216', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-02 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3217', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-02 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3218', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-02 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3219', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-02 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3220', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-08-02 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('3221', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-02 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3222', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-02 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3223', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-08-02 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3224', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-02 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3225', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-02 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3226', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-02 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3227', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-02 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3228', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-03 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3229', '1', 'testTask', 'test', 'renren', '0', null, '1052', '2018-08-03 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3230', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-03 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3231', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-03 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3232', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-03 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3233', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-03 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3234', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-03 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3235', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-08-03 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3236', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-03 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3237', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3238', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3239', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-03 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3240', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-03 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3241', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-03 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3242', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-03 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3243', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-03 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3244', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-03 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3245', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-03 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3246', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3247', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3248', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-03 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3249', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-03 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3250', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-03 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3251', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-03 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3252', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-03 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3253', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-03 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3254', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-03 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3255', '1', 'testTask', 'test', 'renren', '0', null, '1159', '2018-08-03 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3256', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3257', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-03 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3258', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-03 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3259', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-03 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3260', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3261', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3262', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-03 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3263', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-03 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3264', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-03 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3265', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-03 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3266', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-03 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3267', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-03 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3268', '1', 'testTask', 'test', 'renren', '0', null, '1056', '2018-08-03 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3269', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-03 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3270', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-03 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3271', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-03 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3272', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-03 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3273', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-03 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3274', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-03 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3275', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-03 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3276', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-04 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3277', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-04 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3278', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-04 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3279', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-04 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3280', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-04 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3281', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-04 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3282', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-04 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3283', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-04 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3284', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-04 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3285', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-04 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3286', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-04 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3287', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-04 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3288', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-04 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3289', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-04 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3290', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-04 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3291', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-04 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3292', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-04 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3293', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-04 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3294', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-04 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3295', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-04 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3296', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-04 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3297', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-08-04 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3298', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-04 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3299', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-04 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3300', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-04 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3301', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-04 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3302', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-04 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3303', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-04 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3304', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-04 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3305', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-04 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3306', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-04 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3307', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-04 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3308', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-04 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3309', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-04 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3310', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-04 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3311', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-04 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3312', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-04 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3313', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-04 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3314', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-04 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3315', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-04 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3316', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-04 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3317', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-04 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3318', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-04 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3319', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-04 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3320', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-04 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3321', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-04 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3322', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-04 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3323', '1', 'testTask', 'test', 'renren', '0', null, '1147', '2018-08-04 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3324', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-05 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3325', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-05 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3326', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-05 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3327', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-05 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3328', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-05 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3329', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-05 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3330', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-05 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3331', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-05 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3332', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-05 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3333', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-05 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3334', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-05 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3335', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-05 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3336', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-08-05 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3337', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-05 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3338', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-05 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3339', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-05 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3340', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-05 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3341', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-05 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3342', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-05 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3343', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-05 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3344', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-05 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3345', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-05 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3346', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-05 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3347', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-05 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3348', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-05 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3349', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-05 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3350', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-05 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3351', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-05 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3352', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-05 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3353', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-05 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3354', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-05 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3355', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-05 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3356', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-05 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3357', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-05 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3358', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-05 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3359', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-05 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3360', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-05 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3361', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-05 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3362', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-05 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3363', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-05 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3364', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-05 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3365', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-05 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3366', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-05 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('3367', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-05 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('3368', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-05 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('3369', '1', 'testTask', 'test', 'renren', '0', null, '1154', '2018-08-05 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3370', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-08-05 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('3371', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-05 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3372', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-08-06 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('3373', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-06 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('3374', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-06 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('3375', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-06 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('3376', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-08-06 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('3377', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-06 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3378', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-06 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('3379', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-06 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3380', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-06 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3381', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-06 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3382', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-06 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('3383', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-06 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3384', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-06 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3385', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-06 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3386', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-06 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3387', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-06 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('3388', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-06 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('3389', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-06 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('3390', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-06 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3391', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-06 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3392', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-06 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('3393', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-06 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3394', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-06 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3395', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-06 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3396', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-06 12:00:01');
INSERT INTO `schedule_job_log` VALUES ('3397', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-06 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('3398', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-06 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3399', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-06 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3400', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-06 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3401', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-06 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('3402', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-06 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('3403', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-06 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3404', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-06 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3405', '1', 'testTask', 'test', 'renren', '0', null, '1051', '2018-08-06 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3406', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-06 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3407', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-06 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3408', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-06 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('3409', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-06 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('3410', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-06 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3411', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-06 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3412', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-06 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3413', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-06 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3414', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-06 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3415', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-06 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('3416', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-06 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3417', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-06 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3418', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-06 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3419', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-06 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3420', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-07 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3421', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-07 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3422', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3423', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3424', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3425', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-07 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3426', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-07 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3427', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-07 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3428', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-07 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3429', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-07 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3430', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-07 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3431', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-07 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3432', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-07 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3433', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-07 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3434', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3435', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-07 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3436', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3437', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3438', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3439', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3440', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-07 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3441', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-07 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3442', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-07 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3443', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-07 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3444', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-07 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3445', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-07 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3446', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-07 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3447', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-07 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3448', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-07 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3449', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-07 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3450', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-07 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3451', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-07 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3452', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-07 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3453', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-07 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3454', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-07 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3455', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-07 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3456', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-07 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3457', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-07 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3458', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-07 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3459', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-07 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3460', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-07 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('3461', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-07 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3462', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-07 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3463', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-07 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3464', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-07 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3465', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-07 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3466', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-07 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('3467', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-07 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3468', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-08 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3469', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-08 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3470', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-08 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3471', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-08 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3472', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-08 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3473', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-08 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3474', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-08 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3475', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-08 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3476', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-08 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3477', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-08 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3478', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-08 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3479', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-08 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3480', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-08-08 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('3481', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-08 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3482', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-08 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3483', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-08 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('3484', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-08 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3485', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-08 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('3486', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-08 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3487', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-08 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('3488', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-08-08 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3489', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-08 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3490', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-08 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('3491', '1', 'testTask', 'test', 'renren', '0', null, '1133', '2018-08-08 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('3492', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-08-08 12:00:01');
INSERT INTO `schedule_job_log` VALUES ('3493', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-08 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('3494', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-08 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('3495', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-08 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('3496', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-08-08 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('3497', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-08 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3498', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-08 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3499', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-08-08 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3500', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-08 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3501', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-08-08 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('3502', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-08-08 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('3503', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-08 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('3504', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-08-08 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('3505', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-08-08 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('3506', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-08-08 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('3507', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-08-08 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('3508', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-08 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('3509', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-08 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('3510', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-08 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('3511', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-08-08 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('3512', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-08 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('3513', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-08 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('3514', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-08-08 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('3515', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-08 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('3516', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-08-09 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('3517', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-09 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('3518', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-09 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('3519', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-09 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('3520', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-09 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('3521', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-09 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('3522', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-09 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('3523', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-08-09 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('3524', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-09 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('3525', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-09 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('3526', '1', 'testTask', 'test', 'renren', '0', null, '1170', '2018-08-09 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('3527', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-09 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('3528', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-09 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('3529', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-09 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('3530', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-09 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('3531', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-08-09 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('3532', '1', 'testTask', 'test', 'renren', '0', null, '1165', '2018-08-09 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('3533', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-09 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('3534', '1', 'testTask', 'test', 'renren', '0', null, '1147', '2018-08-09 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('3535', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-09 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('3536', '1', 'testTask', 'test', 'renren', '0', null, '1167', '2018-08-09 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('3537', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-09 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('3538', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-09 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('3539', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-08-09 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('3540', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-08-09 12:00:01');
INSERT INTO `schedule_job_log` VALUES ('3541', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-08-09 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('3542', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-08-09 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('3543', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-09 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('3544', '1', 'testTask', 'test', 'renren', '0', null, '1140', '2018-08-09 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('3545', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-09 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('3546', '1', 'testTask', 'test', 'renren', '0', null, '1151', '2018-08-09 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('3547', '1', 'testTask', 'test', 'renren', '0', null, '1060', '2018-08-09 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('3548', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-09 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('3549', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-08-09 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('3550', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-09 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('3551', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-09 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('3552', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-09 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3553', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-09 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3554', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-09 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3555', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-09 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3556', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-09 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3557', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-09 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3558', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-09 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3559', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-09 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3560', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-09 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3561', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-09 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3562', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-09 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3563', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-09 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('3564', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-10 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3565', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-10 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3566', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-10 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3567', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-10 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3568', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-10 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3569', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-10 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3570', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-10 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3571', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-10 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3572', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-10 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3573', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-10 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3574', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-10 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3575', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-10 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3576', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-10 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3577', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-10 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3578', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-10 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3579', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-10 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3580', '1', 'testTask', 'test', 'renren', '0', null, '1059', '2018-08-10 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3581', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-10 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3582', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-10 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3583', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-10 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3584', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-10 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3585', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-10 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3586', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-10 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3587', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-10 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3588', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-10 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3589', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-08-10 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3590', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-10 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3591', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-08-10 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3592', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-10 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3593', '1', 'testTask', 'test', 'renren', '0', null, '1069', '2018-08-10 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3594', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-10 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3595', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-08-10 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('3596', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-10 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('3597', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-08-10 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('3598', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-08-10 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('3599', '1', 'testTask', 'test', 'renren', '0', null, '1063', '2018-08-10 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('3600', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-08-10 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('3601', '1', 'testTask', 'test', 'renren', '0', null, '1065', '2018-08-10 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('3602', '1', 'testTask', 'test', 'renren', '0', null, '1165', '2018-08-10 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('3603', '1', 'testTask', 'test', 'renren', '0', null, '1068', '2018-08-10 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('3604', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-08-10 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('3605', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-08-10 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('3606', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-08-10 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('3607', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-10 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('3608', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-08-10 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('3609', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-08-10 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('3610', '1', 'testTask', 'test', 'renren', '0', null, '1063', '2018-08-10 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('3611', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-08-10 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('3612', '1', 'testTask', 'test', 'renren', '0', null, '1069', '2018-08-11 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('3613', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-11 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('3614', '1', 'testTask', 'test', 'renren', '0', null, '1067', '2018-08-11 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('3615', '1', 'testTask', 'test', 'renren', '0', null, '1066', '2018-08-11 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('3616', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-11 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('3617', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-08-11 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('3618', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-11 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('3619', '1', 'testTask', 'test', 'renren', '0', null, '1064', '2018-08-11 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('3620', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-11 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('3621', '1', 'testTask', 'test', 'renren', '0', null, '1066', '2018-08-11 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('3622', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-11 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('3623', '1', 'testTask', 'test', 'renren', '0', null, '1058', '2018-08-11 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('3624', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-11 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('3625', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-11 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3626', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-11 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3627', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-11 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3628', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-11 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3629', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-11 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3630', '1', 'testTask', 'test', 'renren', '0', null, '1063', '2018-08-11 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3631', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-11 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3632', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-11 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3633', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-11 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3634', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-11 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3635', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-11 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3636', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-08-11 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3637', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-11 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3638', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-11 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3639', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-11 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3640', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-11 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3641', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-11 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3642', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-11 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3643', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-11 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3644', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-11 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3645', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-11 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3646', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-11 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3647', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-11 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3648', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-11 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3649', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-08-11 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3650', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-11 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3651', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-11 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3652', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-11 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3653', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-11 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3654', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-08-11 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3655', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-08-11 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3656', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-11 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3657', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-11 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3658', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-11 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3659', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-11 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3660', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-12 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3661', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-12 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3662', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-12 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3663', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-12 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3664', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-12 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3665', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-08-12 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3666', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-12 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3667', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-08-12 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3668', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-12 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3669', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-12 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3670', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-12 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3671', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-12 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3672', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-12 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3673', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-12 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3674', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-12 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3675', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-12 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('3676', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-12 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('3677', '1', 'testTask', 'test', 'renren', '0', null, '1060', '2018-08-12 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('3678', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-12 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('3679', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-08-12 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('3680', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-12 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('3681', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-12 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('3682', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-08-12 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('3683', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-12 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('3684', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-08-12 12:00:01');
INSERT INTO `schedule_job_log` VALUES ('3685', '1', 'testTask', 'test', 'renren', '0', null, '1128', '2018-08-12 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('3686', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-08-12 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('3687', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-12 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('3688', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-12 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('3689', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-12 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('3690', '1', 'testTask', 'test', 'renren', '0', null, '1155', '2018-08-12 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('3691', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-12 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('3692', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-12 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('3693', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-12 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('3694', '1', 'testTask', 'test', 'renren', '0', null, '1140', '2018-08-12 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('3695', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-12 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('3696', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-12 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('3697', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-12 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('3698', '1', 'testTask', 'test', 'renren', '0', null, '1161', '2018-08-12 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('3699', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-12 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('3700', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-12 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('3701', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-12 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('3702', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-12 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('3703', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-12 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('3704', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-12 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('3705', '1', 'testTask', 'test', 'renren', '0', null, '1127', '2018-08-12 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('3706', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-12 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('3707', '1', 'testTask', 'test', 'renren', '0', null, '1154', '2018-08-12 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('3708', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-13 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('3709', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-13 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('3710', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-08-13 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('3711', '1', 'testTask', 'test', 'renren', '0', null, '1135', '2018-08-13 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('3712', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-08-13 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('3713', '1', 'testTask', 'test', 'renren', '0', null, '1057', '2018-08-13 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('3714', '1', 'testTask', 'test', 'renren', '0', null, '1138', '2018-08-13 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('3715', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-13 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('3716', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-13 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('3717', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-08-13 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('3718', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-13 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('3719', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-13 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('3720', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-13 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('3721', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-13 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('3722', '1', 'testTask', 'test', 'renren', '0', null, '1145', '2018-08-13 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('3723', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-13 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('3724', '1', 'testTask', 'test', 'renren', '0', null, '1137', '2018-08-13 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('3725', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-13 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('3726', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-13 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('3727', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-13 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('3728', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-08-13 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('3729', '1', 'testTask', 'test', 'renren', '0', null, '1143', '2018-08-13 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('3730', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-13 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('3731', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-13 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('3732', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-13 12:00:01');
INSERT INTO `schedule_job_log` VALUES ('3733', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-08-13 12:30:01');
INSERT INTO `schedule_job_log` VALUES ('3734', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-13 13:00:01');
INSERT INTO `schedule_job_log` VALUES ('3735', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-13 13:30:01');
INSERT INTO `schedule_job_log` VALUES ('3736', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-13 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('3737', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-13 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('3738', '1', 'testTask', 'test', 'renren', '0', null, '1142', '2018-08-13 15:00:01');
INSERT INTO `schedule_job_log` VALUES ('3739', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-13 15:30:01');
INSERT INTO `schedule_job_log` VALUES ('3740', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-13 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('3741', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-13 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('3742', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-13 17:00:01');
INSERT INTO `schedule_job_log` VALUES ('3743', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-13 17:30:01');
INSERT INTO `schedule_job_log` VALUES ('3744', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-13 18:00:01');
INSERT INTO `schedule_job_log` VALUES ('3745', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-13 18:30:01');
INSERT INTO `schedule_job_log` VALUES ('3746', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-13 19:00:01');
INSERT INTO `schedule_job_log` VALUES ('3747', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-13 19:30:01');
INSERT INTO `schedule_job_log` VALUES ('3748', '1', 'testTask', 'test', 'renren', '0', null, '1157', '2018-08-13 20:00:01');
INSERT INTO `schedule_job_log` VALUES ('3749', '1', 'testTask', 'test', 'renren', '0', null, '1124', '2018-08-13 20:30:01');
INSERT INTO `schedule_job_log` VALUES ('3750', '1', 'testTask', 'test', 'renren', '0', null, '1160', '2018-08-13 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('3751', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-13 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('3752', '1', 'testTask', 'test', 'renren', '0', null, '1153', '2018-08-13 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('3753', '1', 'testTask', 'test', 'renren', '0', null, '1125', '2018-08-13 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('3754', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-13 23:00:01');
INSERT INTO `schedule_job_log` VALUES ('3755', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-08-13 23:30:01');
INSERT INTO `schedule_job_log` VALUES ('3756', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-08-14 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('3757', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-14 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('3758', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-08-14 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('3759', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-08-14 01:30:01');
INSERT INTO `schedule_job_log` VALUES ('3760', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-14 02:00:01');
INSERT INTO `schedule_job_log` VALUES ('3761', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-14 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('3762', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-14 03:00:01');
INSERT INTO `schedule_job_log` VALUES ('3763', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-14 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('3764', '1', 'testTask', 'test', 'renren', '0', null, '1126', '2018-08-14 04:00:01');
INSERT INTO `schedule_job_log` VALUES ('3765', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-14 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('3766', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-14 05:00:01');
INSERT INTO `schedule_job_log` VALUES ('3767', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-14 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('3768', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-14 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('3769', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-14 06:30:01');
INSERT INTO `schedule_job_log` VALUES ('3770', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-14 07:00:01');
INSERT INTO `schedule_job_log` VALUES ('3771', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-14 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('3772', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-14 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('3773', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-14 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('3774', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-14 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('3775', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-08-14 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('3776', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-14 10:00:01');
INSERT INTO `schedule_job_log` VALUES ('3777', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-14 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3778', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-14 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3779', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-14 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3780', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-14 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3781', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-14 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3782', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-14 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3783', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-14 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3784', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-14 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3785', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-14 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3786', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-14 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3787', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-14 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3788', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-14 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3789', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-14 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3790', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-14 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3791', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-14 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3792', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-14 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3793', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-14 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3794', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-14 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3795', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-14 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3796', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-14 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3797', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-14 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3798', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-14 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3799', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-14 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3800', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-14 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3801', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-14 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3802', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-14 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3803', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-14 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3804', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-15 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3805', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-15 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3806', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-15 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3807', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-15 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3808', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-15 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3809', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-15 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3810', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-15 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3811', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-15 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3812', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-15 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3813', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-15 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3814', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-15 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3815', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-15 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3816', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-15 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3817', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-15 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3818', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-15 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3819', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-15 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3820', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-15 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3821', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-15 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3822', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-15 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3823', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-15 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3824', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-15 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3825', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-15 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3826', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-15 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3827', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-15 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3828', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-15 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3829', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-15 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3830', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-15 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3831', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-15 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3832', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-15 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3833', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-15 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3834', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-15 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3835', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-15 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3836', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-15 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3837', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-15 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3838', '1', 'testTask', 'test', 'renren', '0', null, '1072', '2018-08-15 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3839', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-15 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3840', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-15 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3841', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-15 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3842', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-15 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3843', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-15 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3844', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-15 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3845', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-15 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3846', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-15 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3847', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-08-15 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3848', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-15 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3849', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-15 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3850', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-15 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3851', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-15 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3852', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-16 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3853', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3854', '1', 'testTask', 'test', 'renren', '0', null, '1047', '2018-08-16 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3855', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-16 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3856', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-16 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3857', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-16 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3858', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-16 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3859', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-16 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3860', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-16 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3861', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-16 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3862', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-16 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3863', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3864', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-16 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3865', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-16 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3866', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3867', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3868', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-16 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3869', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-16 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3870', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3871', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3872', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3873', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-16 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3874', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-16 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3875', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-16 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3876', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-16 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3877', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-16 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3878', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-16 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3879', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-16 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3880', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3881', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-16 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3882', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-16 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3883', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-16 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3884', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-16 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3885', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-16 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3886', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-16 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3887', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-16 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3888', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-16 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3889', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-16 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3890', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-16 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3891', '1', 'testTask', 'test', 'renren', '0', null, '1136', '2018-08-16 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3892', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-16 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3893', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-16 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3894', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-16 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3895', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-16 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3896', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-16 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3897', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-16 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3898', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-16 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3899', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-16 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3900', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-17 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3901', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-17 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3902', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-17 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3903', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-17 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3904', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-17 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3905', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-17 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3906', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-17 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3907', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-17 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3908', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-08-17 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3909', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-17 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3910', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-17 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3911', '1', 'testTask', 'test', 'renren', '0', null, '1070', '2018-08-17 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3912', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-17 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3913', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-17 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3914', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-17 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3915', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-17 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3916', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-17 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3917', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-17 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3918', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-17 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3919', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-17 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3920', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-17 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3921', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-17 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3922', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-17 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3923', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-17 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3924', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-17 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3925', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-17 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3926', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-17 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3927', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-17 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3928', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-17 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3929', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-17 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3930', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-17 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3931', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-17 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3932', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-08-17 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3933', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-17 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3934', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-17 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3935', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-17 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3936', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-17 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3937', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-17 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3938', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-17 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3939', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-17 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3940', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-17 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3941', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-17 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3942', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-17 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3943', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-17 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3944', '1', 'testTask', 'test', 'renren', '0', null, '1132', '2018-08-17 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3945', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-17 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3946', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-17 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3947', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-17 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3948', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-18 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3949', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-18 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3950', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-18 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3951', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-18 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('3952', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-18 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('3953', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-18 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('3954', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-18 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('3955', '1', 'testTask', 'test', 'renren', '0', null, '1040', '2018-08-18 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('3956', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-18 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('3957', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-18 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('3958', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-18 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('3959', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-18 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('3960', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-18 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('3961', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-18 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('3962', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-18 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('3963', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-18 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('3964', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-18 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('3965', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-18 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3966', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-18 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('3967', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-18 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('3968', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-18 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('3969', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-18 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('3970', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-18 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('3971', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-18 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('3972', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-18 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('3973', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-18 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('3974', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-18 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('3975', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-18 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('3976', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-18 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('3977', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-18 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('3978', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-18 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3979', '1', 'testTask', 'test', 'renren', '0', null, '1129', '2018-08-18 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('3980', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-18 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('3981', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-18 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('3982', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-18 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('3983', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-18 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('3984', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-18 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('3985', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-18 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('3986', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-18 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('3987', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-18 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('3988', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-18 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('3989', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-18 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('3990', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-08-18 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('3991', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-18 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('3992', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-18 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('3993', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-18 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('3994', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-18 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('3995', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-18 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('3996', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-19 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('3997', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-19 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('3998', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-19 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('3999', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4000', '1', 'testTask', 'test', 'renren', '0', null, '1053', '2018-08-19 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4001', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-19 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4002', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4003', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-19 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4004', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-19 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4005', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-19 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4006', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-19 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4007', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-19 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4008', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4009', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-19 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4010', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-19 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4011', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4012', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-19 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4013', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-19 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('4014', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-19 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4015', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-19 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('4016', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-19 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4017', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-19 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4018', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-08-19 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4019', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-19 11:30:01');
INSERT INTO `schedule_job_log` VALUES ('4020', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-19 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4021', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-19 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4022', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-19 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4023', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-19 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4024', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-19 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4025', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-19 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4026', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-19 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4027', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-19 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4028', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-19 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4029', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-19 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4030', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-19 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4031', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-19 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4032', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-19 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4033', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-19 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4034', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-19 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4035', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4036', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4037', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-19 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4038', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4039', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-19 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4040', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-19 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4041', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-19 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4042', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-19 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4043', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-19 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4044', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-20 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('4045', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-20 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('4046', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-20 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('4047', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-20 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4048', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-20 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4049', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4050', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-20 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4051', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-20 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4052', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4053', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-20 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4054', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-20 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4055', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-20 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4056', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4057', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-20 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4058', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4059', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-20 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4060', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-20 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4061', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('4062', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-20 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4063', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-08-20 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('4064', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-20 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4065', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-20 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4066', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-20 11:00:01');
INSERT INTO `schedule_job_log` VALUES ('4067', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-20 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4068', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-20 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4069', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-20 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4070', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-20 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4071', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-20 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4072', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-20 14:00:01');
INSERT INTO `schedule_job_log` VALUES ('4073', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-20 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4074', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-20 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4075', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-20 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4076', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-20 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4077', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-20 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4078', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-20 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4079', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-20 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4080', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-20 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4081', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4082', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-20 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4083', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-20 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4084', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-20 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4085', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-08-20 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4086', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-20 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4087', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-20 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4088', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4089', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-20 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4090', '1', 'testTask', 'test', 'renren', '0', null, '1061', '2018-08-20 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4091', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-20 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4092', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-21 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('4093', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-21 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('4094', '1', 'testTask', 'test', 'renren', '0', null, '1044', '2018-08-21 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('4095', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-21 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4096', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-21 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4097', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-21 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4098', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-21 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4099', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-21 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4100', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-21 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4101', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-21 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4102', '1', 'testTask', 'test', 'renren', '0', null, '1045', '2018-08-21 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4103', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-21 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4104', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-08-21 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4105', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-21 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4106', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-21 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4107', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-21 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4108', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-21 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4109', '1', 'testTask', 'test', 'renren', '0', null, '1130', '2018-08-21 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('4110', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-21 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4111', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-21 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('4112', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-08-21 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4113', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-21 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4114', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-21 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4115', '1', 'testTask', 'test', 'renren', '0', null, '1050', '2018-08-21 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4116', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-21 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4117', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-21 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4118', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-21 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4119', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-21 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4120', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-21 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4121', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-21 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4122', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-21 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4123', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-21 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4124', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-21 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4125', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-21 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4126', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-21 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4127', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-21 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4128', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-21 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4129', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-21 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4130', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-21 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4131', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-21 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4132', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-21 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4133', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-21 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4134', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-21 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4135', '1', 'testTask', 'test', 'renren', '0', null, '1122', '2018-08-21 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4136', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-21 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4137', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-21 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4138', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-21 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4139', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-21 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4140', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-22 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('4141', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-22 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('4142', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-22 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('4143', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-22 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4144', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-22 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4145', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-22 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4146', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-22 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4147', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-22 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4148', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-22 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4149', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-22 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4150', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-22 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4151', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-22 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4152', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-22 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4153', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-22 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4154', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-22 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4155', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-22 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4156', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-22 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4157', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-22 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('4158', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-22 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4159', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-22 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('4160', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-22 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4161', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-22 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4162', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-22 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4163', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-22 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4164', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-22 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4165', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-22 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4166', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-22 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4167', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-22 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4168', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-22 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4169', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-22 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4170', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-22 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4171', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-22 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4172', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-22 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4173', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-22 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4174', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-22 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4175', '1', 'testTask', 'test', 'renren', '0', null, '1085', '2018-08-22 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4176', '1', 'testTask', 'test', 'renren', '0', null, '1082', '2018-08-22 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4177', '1', 'testTask', 'test', 'renren', '0', null, '1086', '2018-08-22 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4178', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-22 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4179', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-22 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4180', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-22 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4181', '1', 'testTask', 'test', 'renren', '0', null, '1073', '2018-08-22 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4182', '1', 'testTask', 'test', 'renren', '0', null, '1084', '2018-08-22 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4183', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-22 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4184', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-22 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4185', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-22 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4186', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-22 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4187', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-22 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4188', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-23 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('4189', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-23 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('4190', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-23 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('4191', '1', 'testTask', 'test', 'renren', '0', null, '1123', '2018-08-23 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4192', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-23 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4193', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-23 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4194', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-23 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4195', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-23 03:30:01');
INSERT INTO `schedule_job_log` VALUES ('4196', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-23 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4197', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-23 04:30:01');
INSERT INTO `schedule_job_log` VALUES ('4198', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-23 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4199', '1', 'testTask', 'test', 'renren', '0', null, '1155', '2018-08-23 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4200', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-08-23 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4201', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-23 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4202', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-23 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4203', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-23 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4204', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-23 08:00:01');
INSERT INTO `schedule_job_log` VALUES ('4205', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-23 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('4206', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-23 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('4207', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-23 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('4208', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-23 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4209', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-23 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4210', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-23 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4211', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-23 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4212', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-23 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4213', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-23 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4214', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-23 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4215', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-23 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4216', '1', 'testTask', 'test', 'renren', '0', null, '1062', '2018-08-23 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4217', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-23 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4218', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-23 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4219', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-23 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4220', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-23 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4221', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-23 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4222', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-23 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4223', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-23 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4224', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-23 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4225', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-23 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4226', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-23 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4227', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-23 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4228', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-23 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4229', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-23 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4230', '1', 'testTask', 'test', 'renren', '0', null, '1106', '2018-08-23 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4231', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-23 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4232', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-23 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4233', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-08-23 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4234', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-23 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4235', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-23 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4236', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-24 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('4237', '1', 'testTask', 'test', 'renren', '0', null, '1121', '2018-08-24 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('4238', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-24 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('4239', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-24 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4240', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-24 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4241', '1', 'testTask', 'test', 'renren', '0', null, '1087', '2018-08-24 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4242', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-24 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4243', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-24 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4244', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-24 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4245', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-24 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4246', '1', 'testTask', 'test', 'renren', '0', null, '1046', '2018-08-24 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4247', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-24 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4248', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-24 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4249', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-24 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4250', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-24 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4251', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-08-24 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4252', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-08-24 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4253', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-24 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('4254', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-24 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4255', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-24 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('4256', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-24 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4257', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-24 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4258', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-24 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4259', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-24 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4260', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-24 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4261', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-08-24 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4262', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-24 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4263', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-24 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4264', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-08-24 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4265', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-24 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4266', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-24 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4267', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-24 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4268', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-24 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4269', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-24 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('4270', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-24 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4271', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-24 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4272', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-24 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4273', '1', 'testTask', 'test', 'renren', '0', null, '1131', '2018-08-24 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4274', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-24 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4275', '1', 'testTask', 'test', 'renren', '0', null, '1150', '2018-08-24 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4276', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-24 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4277', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-24 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4278', '1', 'testTask', 'test', 'renren', '0', null, '1113', '2018-08-24 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4279', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-24 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4280', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-24 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4281', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-24 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4282', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-24 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4283', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-24 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4284', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('4285', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('4286', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-25 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('4287', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-25 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4288', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-25 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4289', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-25 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4290', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-25 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4291', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-25 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4292', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-25 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4293', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-25 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4294', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-25 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4295', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4296', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-25 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4297', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4298', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-25 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4299', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4300', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-25 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4301', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-25 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('4302', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4303', '1', 'testTask', 'test', 'renren', '0', null, '1103', '2018-08-25 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('4304', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-25 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4305', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-08-25 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4306', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4307', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4308', '1', 'testTask', 'test', 'renren', '0', null, '1139', '2018-08-25 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4309', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4310', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-25 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4311', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4312', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4313', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-25 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4314', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4315', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4316', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-08-25 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4317', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4318', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-25 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4319', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4320', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-25 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4321', '1', 'testTask', 'test', 'renren', '0', null, '1048', '2018-08-25 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4322', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-25 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4323', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-25 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4324', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-25 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4325', '1', 'testTask', 'test', 'renren', '0', null, '1083', '2018-08-25 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4326', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-08-25 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4327', '1', 'testTask', 'test', 'renren', '0', null, '1102', '2018-08-25 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4328', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-25 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4329', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-08-25 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4330', '1', 'testTask', 'test', 'renren', '0', null, '1101', '2018-08-25 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4331', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-25 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4332', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-26 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('4333', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-08-26 00:30:01');
INSERT INTO `schedule_job_log` VALUES ('4334', '1', 'testTask', 'test', 'renren', '0', null, '1115', '2018-08-26 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('4335', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-26 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4336', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-26 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4337', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-26 02:30:01');
INSERT INTO `schedule_job_log` VALUES ('4338', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-26 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4339', '1', 'testTask', 'test', 'renren', '0', null, '1120', '2018-08-26 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4340', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-26 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4341', '1', 'testTask', 'test', 'renren', '0', null, '1055', '2018-08-26 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4342', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-26 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4343', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-26 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('4344', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-26 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('4345', '1', 'testTask', 'test', 'renren', '0', null, '1074', '2018-08-26 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4346', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-26 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4347', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-26 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('4348', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-26 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4349', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-26 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('4350', '1', 'testTask', 'test', 'renren', '0', null, '1080', '2018-08-26 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4351', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-26 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('4352', '1', 'testTask', 'test', 'renren', '0', null, '1038', '2018-08-26 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4353', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-26 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('4354', '1', 'testTask', 'test', 'renren', '0', null, '1099', '2018-08-26 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4355', '1', 'testTask', 'test', 'renren', '0', null, '1088', '2018-08-26 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4356', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-26 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4357', '1', 'testTask', 'test', 'renren', '0', null, '1114', '2018-08-26 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4358', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-26 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4359', '1', 'testTask', 'test', 'renren', '0', null, '1041', '2018-08-26 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4360', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-26 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4361', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-26 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4362', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-26 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4363', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-26 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4364', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-26 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4365', '1', 'testTask', 'test', 'renren', '0', null, '1112', '2018-08-26 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4366', '1', 'testTask', 'test', 'renren', '0', null, '1075', '2018-08-26 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4367', '1', 'testTask', 'test', 'renren', '0', null, '1076', '2018-08-26 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4368', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-26 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4369', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-26 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4370', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-26 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4371', '1', 'testTask', 'test', 'renren', '0', null, '1119', '2018-08-26 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4372', '1', 'testTask', 'test', 'renren', '0', null, '1089', '2018-08-26 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4373', '1', 'testTask', 'test', 'renren', '0', null, '1116', '2018-08-26 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4374', '1', 'testTask', 'test', 'renren', '0', null, '1111', '2018-08-26 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('4375', '1', 'testTask', 'test', 'renren', '0', null, '1117', '2018-08-26 21:30:01');
INSERT INTO `schedule_job_log` VALUES ('4376', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-26 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4377', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-26 22:30:01');
INSERT INTO `schedule_job_log` VALUES ('4378', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-26 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4379', '1', 'testTask', 'test', 'renren', '0', null, '1145', '2018-08-26 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4380', '1', 'testTask', 'test', 'renren', '0', null, '1108', '2018-08-27 00:00:01');
INSERT INTO `schedule_job_log` VALUES ('4381', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-27 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('4382', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-27 01:00:01');
INSERT INTO `schedule_job_log` VALUES ('4383', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-27 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('4384', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-27 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('4385', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-27 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('4386', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-27 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('4387', '1', 'testTask', 'test', 'renren', '0', null, '1042', '2018-08-27 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('4388', '1', 'testTask', 'test', 'renren', '0', null, '1107', '2018-08-27 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('4389', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-27 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('4390', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-27 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('4391', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-27 05:30:01');
INSERT INTO `schedule_job_log` VALUES ('4392', '1', 'testTask', 'test', 'renren', '0', null, '1049', '2018-08-27 06:00:01');
INSERT INTO `schedule_job_log` VALUES ('4393', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-27 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('4394', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-27 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('4395', '1', 'testTask', 'test', 'renren', '0', null, '1144', '2018-08-27 07:30:01');
INSERT INTO `schedule_job_log` VALUES ('4396', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-27 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('4397', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-27 08:30:01');
INSERT INTO `schedule_job_log` VALUES ('4398', '1', 'testTask', 'test', 'renren', '0', null, '1134', '2018-08-27 09:00:01');
INSERT INTO `schedule_job_log` VALUES ('4399', '1', 'testTask', 'test', 'renren', '0', null, '1109', '2018-08-27 09:30:01');
INSERT INTO `schedule_job_log` VALUES ('4400', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-27 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('4401', '1', 'testTask', 'test', 'renren', '0', null, '1105', '2018-08-27 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('4402', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-27 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('4403', '1', 'testTask', 'test', 'renren', '0', null, '1098', '2018-08-27 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('4404', '1', 'testTask', 'test', 'renren', '0', null, '1118', '2018-08-27 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('4405', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-27 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('4406', '1', 'testTask', 'test', 'renren', '0', null, '1079', '2018-08-27 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('4407', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-27 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('4408', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-27 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('4409', '1', 'testTask', 'test', 'renren', '0', null, '1095', '2018-08-27 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('4410', '1', 'testTask', 'test', 'renren', '0', null, '1110', '2018-08-27 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('4411', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-27 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4412', '1', 'testTask', 'test', 'renren', '0', null, '1097', '2018-08-27 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('4413', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-27 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('4414', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-27 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('4415', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-27 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('4416', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-27 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('4417', '1', 'testTask', 'test', 'renren', '0', null, '1096', '2018-08-27 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('4418', '1', 'testTask', 'test', 'renren', '0', null, '1094', '2018-08-27 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('4419', '1', 'testTask', 'test', 'renren', '0', null, '1093', '2018-08-27 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('4420', '1', 'testTask', 'test', 'renren', '0', null, '1100', '2018-08-27 20:00:00');
INSERT INTO `schedule_job_log` VALUES ('4421', '1', 'testTask', 'test', 'renren', '0', null, '1077', '2018-08-27 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('4422', '1', 'testTask', 'test', 'renren', '0', null, '1078', '2018-08-27 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('4423', '1', 'testTask', 'test', 'renren', '0', null, '1104', '2018-08-27 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('4424', '1', 'testTask', 'test', 'renren', '0', null, '1043', '2018-08-27 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('4425', '1', 'testTask', 'test', 'renren', '0', null, '1081', '2018-08-27 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4426', '1', 'testTask', 'test', 'renren', '0', null, '1090', '2018-08-27 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('4427', '1', 'testTask', 'test', 'renren', '0', null, '1092', '2018-08-27 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('4428', '1', 'testTask', 'test', 'renren', '0', null, '1091', '2018-08-28 00:00:00');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL auto_increment,
  `param_key` varchar(50) default NULL COMMENT 'key',
  `param_value` varchar(2000) default NULL COMMENT 'value',
  `status` tinyint(4) default '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) default NULL COMMENT '备注',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `param_key` (`param_key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'CLOUD_STORAGE_CONFIG_KEY', '{\"type\":1,\"qiniuDomain\":\"http://p96gov5rj.bkt.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuAccessKey\":\"exdHFa2eCcQH5hHf-gSz7SCSVoov3T1HQU1HtZh8\",\"qiniuSecretKey\":\"5CESil3MudOx19Hg9F5zTqv1kuVBC1xl3KZpGOWN\",\"qiniuBucketName\":\"gxkeji-yosan\",\"aliyunDomain\":\"\",\"aliyunPrefix\":\"\",\"aliyunEndPoint\":\"\",\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qcloudBucketName\":\"\"}', '0', '云存储配置信息');
INSERT INTO `sys_config` VALUES ('2', 'test', 'zhj', '1', 'test');

-- ----------------------------
-- Table structure for sys_deposit
-- ----------------------------
DROP TABLE IF EXISTS `sys_deposit`;
CREATE TABLE `sys_deposit` (
  `id` bigint(20) NOT NULL auto_increment,
  `openid` varchar(28) NOT NULL default '' COMMENT '付款人openid',
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `trade_no` varchar(32) default NULL COMMENT '财付通订单号',
  `transaction_id` varchar(32) default NULL COMMENT '微信支付订单号',
  `pay_price` decimal(10,2) NOT NULL default '0.00' COMMENT '付款金额',
  `pay_time` datetime default NULL COMMENT '支付时间',
  `pay_scene` varchar(12) default NULL COMMENT '支付场景',
  `deposit_status` tinyint(1) NOT NULL default '1' COMMENT '押金交易状态:1|待支付，2|已支付，未退款，3|取消支付，4|已支付，已退款，5|用户报失，系统扣押金',
  `refund_id` varchar(32) default NULL COMMENT '微信退款单号',
  `refund_no` bigint(20) default NULL COMMENT '商户退款单号',
  `refund_price` decimal(10,2) default '0.00' COMMENT '退款金额',
  `refund_time` datetime default NULL COMMENT '退款时间',
  `refund_scene` varchar(12) default NULL COMMENT '支付场景',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `sys_deposit_openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='押金';

-- ----------------------------
-- Records of sys_deposit
-- ----------------------------
INSERT INTO `sys_deposit` VALUES ('1', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '3', '1010415695420256258', '4200000127201806232848254116', '0.01', '2018-06-23 14:54:15', 'JSAPI', '2', null, null, '0.00', null, '', null, null, null, '2018-06-23 15:05:06', null);

-- ----------------------------
-- Table structure for sys_deposit_rule
-- ----------------------------
DROP TABLE IF EXISTS `sys_deposit_rule`;
CREATE TABLE `sys_deposit_rule` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL COMMENT '名称',
  `dept_id` bigint(20) NOT NULL COMMENT '部门id',
  `deposit_price` decimal(10,2) NOT NULL default '0.00' COMMENT '押金金额',
  `rule_status` tinyint(1) NOT NULL default '1' COMMENT '规则状态：0|过期, 1|执行',
  `desc` varchar(200) default '' COMMENT '规则的描述',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='押金规则';

-- ----------------------------
-- Records of sys_deposit_rule
-- ----------------------------
INSERT INTO `sys_deposit_rule` VALUES ('1', '上海分公司-押金', '3', '0.01', '1', '上海分公司-押金', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL auto_increment,
  `parent_id` bigint(20) default NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) default NULL COMMENT '部门名称',
  `order_num` int(11) default NULL COMMENT '排序',
  `del_flag` tinyint(4) default '0' COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY  (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '广州共想科技有限公司', '0', '0');
INSERT INTO `sys_dept` VALUES ('2', '1', '北京分公司', '1', '0');
INSERT INTO `sys_dept` VALUES ('3', '1', '上海分公司', '2', '0');
INSERT INTO `sys_dept` VALUES ('4', '3', '技术部', '0', '0');
INSERT INTO `sys_dept` VALUES ('5', '3', '销售部', '1', '0');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL COMMENT '字典名称',
  `type` varchar(100) NOT NULL COMMENT '字典类型',
  `code` varchar(100) NOT NULL COMMENT '字典码',
  `value` varchar(1000) NOT NULL COMMENT '字典值',
  `order_num` int(11) default '0' COMMENT '排序',
  `remark` varchar(255) default NULL COMMENT '备注',
  `del_flag` tinyint(4) default '0' COMMENT '删除标记  -1：已删除  0：正常',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `type` (`type`,`code`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='数据字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '小程序配置', 'mini', 'appid', 'wxbd2f253d4bf3b1f0', '1', '小程序appid', '0');
INSERT INTO `sys_dict` VALUES ('2', '小程序配置', 'mini', 'secret', '4235b13e1c00bcb4c375ffccae81ce9f', '2', '小程序密钥', '0');
INSERT INTO `sys_dict` VALUES ('3', '小程序配置', 'mini', 'token', 'gxkjys', '3', '小程序token', '0');
INSERT INTO `sys_dict` VALUES ('4', '小程序配置', 'mini', 'aesKey', '7TQpP7GkVsaYmm3NpZrntjE9FRss7H5J2tlNQzCn6lo', '4', '小程序aesKey', '0');
INSERT INTO `sys_dict` VALUES ('5', '小程序配置', 'mini', 'msgDataFormat', 'JSON', '5', '小程序消息格式', '0');
INSERT INTO `sys_dict` VALUES ('6', '小程序支付配置', 'minipay', 'appid', 'wxbd2f253d4bf3b1f0', '6', '小程序支付appid', '0');
INSERT INTO `sys_dict` VALUES ('7', '小程序支付配置', 'minipay', 'mchid', '1501765151', '7', '小程序支付商户号', '0');
INSERT INTO `sys_dict` VALUES ('8', '小程序支付配置', 'minipay', 'mchkey', 'gxkjchanpin0bcb4c375ffccae81ce9f', '8', '小程序支付商户密钥', '0');
INSERT INTO `sys_dict` VALUES ('9', '小程序支付配置', 'minipay', 'subappid', '暂无', '9', '小程序支付子商户appid', '-1');
INSERT INTO `sys_dict` VALUES ('10', '小程序支付配置', 'minipay', 'submchid', '暂无', '10', '小程序支付子商户支付密钥', '-1');
INSERT INTO `sys_dict` VALUES ('11', '小程序支付配置', 'minipay', 'keypath', '/data/project/cert/apiclient_cert.p12', '11', '小程序支付商户证书路径配置', '0');
INSERT INTO `sys_dict` VALUES ('12', '小程序支付配置', 'minipay', 'notifyurl', 'https://yosan.gzgxkj.com/yosan-api/wechat/rent/pay/notify', '12', '小程序支付通知URL', '0');
INSERT INTO `sys_dict` VALUES ('13', '故障类型', 'fault', '1', '雨伞遗失', '13', null, '0');
INSERT INTO `sys_dict` VALUES ('14', '故障类型', 'fault', '2', '还伞异常', '14', null, '0');
INSERT INTO `sys_dict` VALUES ('15', '公司信息', 'about', '微信公众号', 'gxkeji', '15', '关于我们', '0');
INSERT INTO `sys_dict` VALUES ('16', '公司信息', 'about', '联系电话', '020-28135588', '16', '关于我们', '0');
INSERT INTO `sys_dict` VALUES ('17', '公司信息', 'about', '电子邮箱', 'apple_live@qq.com', '17', '关于我们', '0');
INSERT INTO `sys_dict` VALUES ('18', '公司信息', 'about', '官网', 'http://www.gzgxkj.com', '18', '关于我们', '0');

-- ----------------------------
-- Table structure for sys_iot_callback_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_iot_callback_log`;
CREATE TABLE `sys_iot_callback_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `product_key` varchar(50) NOT NULL COMMENT '产品key',
  `device_name` varchar(50) NOT NULL COMMENT '设备名称',
  `openid` varchar(28) NOT NULL COMMENT '用户id',
  `order_id` varchar(50) default NULL COMMENT '订单标识',
  `message_type` varchar(30) default NULL COMMENT '消息类型：RENT|借，RETURN|还，ADMIN|管理员',
  `req_time` datetime default NULL COMMENT '请求时间',
  `resp_time` datetime default NULL COMMENT '请求到获取结果花费时间',
  `req_str` varchar(512) default NULL COMMENT '请求参数',
  `resp_result` varchar(512) default '' COMMENT '响应结果',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='操作iot获取数据日志记录';

-- ----------------------------
-- Records of sys_iot_callback_log
-- ----------------------------
INSERT INTO `sys_iot_callback_log` VALUES ('1', '3', 'dsfa', 'asdf', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1', 'RENT', '2018-05-22 20:58:39', '2018-05-20 20:58:42', '1', '3', '3', '2018-05-20 20:58:46', null, '2018-05-31 20:58:49', null);
INSERT INTO `sys_iot_callback_log` VALUES ('2', null, '4o5HzG9GaWx', '868575026628083', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '3', 'RENT', '2018-06-23 16:24:38', '2018-06-23 16:20:13', '{\"deviceName\":\"868575026628083\",\"machineId\":5,\"oper\":\"RENT\",\"orderId\":\"3\",\"productKey\":\"4o5HzG9GaWx\",\"time\":1529742277,\"umbrellaRoadId\":\"1\",\"userId\":\"ovz-Z5TGHM53mtWSyzXiyrXS4LcQ\"}', '{\"machineId\":5,\"operResult\":2,\"umbrellaId\":0,\"orderId\":\"3\",\"umbrellaRoadId\":\"1\",\"oper\":\"RENT\",\"time\":1529742013,\"productKey\":\"4o5HzG9GaWx\",\"deviceName\":\"868575026628083\",\"userId\":\"ovz-Z5TGHM53mtWSyzXiyrXS4LcQ\"}', null, '2018-06-23 16:24:38', null, null, null);
INSERT INTO `sys_iot_callback_log` VALUES ('3', null, '4o5HzG9GaWx', '868575026628083', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '3', 'RENT', '2018-06-23 23:46:21', '2018-06-23 16:20:13', '{\"deviceName\":\"868575026628083\",\"machineId\":5,\"oper\":\"RENT\",\"orderId\":\"3\",\"productKey\":\"4o5HzG9GaWx\",\"time\":1529768781,\"umbrellaRoadId\":\"1\",\"userId\":\"ovz-Z5TGHM53mtWSyzXiyrXS4LcQ\"}', '{\"machineId\":5,\"operResult\":2,\"umbrellaId\":0,\"orderId\":\"3\",\"umbrellaRoadId\":\"1\",\"oper\":\"RENT\",\"time\":1529742013,\"productKey\":\"4o5HzG9GaWx\",\"deviceName\":\"868575026628083\",\"userId\":\"ovz-Z5TGHM53mtWSyzXiyrXS4LcQ\"}', null, '2018-06-23 23:46:21', null, null, null);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `username` varchar(50) default NULL COMMENT '用户名',
  `operation` varchar(50) default NULL COMMENT '用户操作',
  `method` varchar(200) default NULL COMMENT '请求方法',
  `params` varchar(5000) default NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) default NULL COMMENT 'IP地址',
  `create_date` datetime default NULL COMMENT '创建时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":42,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"globe\",\"orderNum\":0}', '209', '0:0:0:0:0:0:0:1', '2018-03-29 16:05:55');
INSERT INTO `sys_log` VALUES ('2', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":41,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"globe\",\"orderNum\":0}', '134', '0:0:0:0:0:0:0:1', '2018-03-29 16:05:55');
INSERT INTO `sys_log` VALUES ('3', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '42', '1822', '0:0:0:0:0:0:0:1', '2018-03-29 16:06:15');
INSERT INTO `sys_log` VALUES ('4', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":41,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"globe\",\"orderNum\":0}', '147', '0:0:0:0:0:0:0:1', '2018-03-29 16:06:33');
INSERT INTO `sys_log` VALUES ('5', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":43,\"parentId\":41,\"parentName\":\"业务管理\",\"name\":\"规则管理\",\"type\":0,\"orderNum\":0}', '102', '0:0:0:0:0:0:0:1', '2018-03-29 16:07:42');
INSERT INTO `sys_log` VALUES ('6', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"type\":1,\"orderNum\":0}', '96', '0:0:0:0:0:0:0:1', '2018-03-29 16:10:21');
INSERT INTO `sys_log` VALUES ('7', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":41,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"fa globe\",\"orderNum\":0}', '103', '0:0:0:0:0:0:0:1', '2018-03-29 18:51:19');
INSERT INTO `sys_log` VALUES ('8', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":41,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"fa fa-cog\",\"orderNum\":0}', '72', '0:0:0:0:0:0:0:1', '2018-03-29 18:52:13');
INSERT INTO `sys_log` VALUES ('9', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":41,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"fa bar\",\"orderNum\":0}', '74', '0:0:0:0:0:0:0:1', '2018-03-29 18:53:17');
INSERT INTO `sys_log` VALUES ('10', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":41,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"bar\",\"orderNum\":0}', '70', '0:0:0:0:0:0:0:1', '2018-03-29 18:53:28');
INSERT INTO `sys_log` VALUES ('11', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":41,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"业务管理\",\"type\":0,\"icon\":\"fa fa-tasks\",\"orderNum\":0}', '74', '0:0:0:0:0:0:0:1', '2018-03-29 18:59:01');
INSERT INTO `sys_log` VALUES ('12', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"sys:rentrule:list,sys:rentrule:info\",\"type\":2,\"orderNum\":0}', '92', '0:0:0:0:0:0:0:1', '2018-03-29 19:03:45');
INSERT INTO `sys_log` VALUES ('13', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"sys:rentrule:list,sys:rentrule:info\",\"type\":1,\"orderNum\":0}', '88', '0:0:0:0:0:0:0:1', '2018-03-29 19:04:40');
INSERT INTO `sys_log` VALUES ('14', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"\",\"type\":1,\"orderNum\":0}', '80', '0:0:0:0:0:0:0:1', '2018-03-29 19:06:28');
INSERT INTO `sys_log` VALUES ('15', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"\",\"type\":1,\"orderNum\":0}', '83', '0:0:0:0:0:0:0:1', '2018-03-29 19:07:12');
INSERT INTO `sys_log` VALUES ('16', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":45,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"a\",\"perms\":\"sdfasdf\",\"type\":2,\"orderNum\":0}', '80', '0:0:0:0:0:0:0:1', '2018-03-29 19:09:12');
INSERT INTO `sys_log` VALUES ('17', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":45,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"查看\",\"perms\":\"sys:rentrule:list,sys:rentrule:info\",\"type\":2,\"orderNum\":0}', '86', '0:0:0:0:0:0:0:1', '2018-03-29 19:09:57');
INSERT INTO `sys_log` VALUES ('18', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":45,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"新增\",\"perms\":\"sys:rentrule:save\",\"type\":2,\"orderNum\":0}', '84', '0:0:0:0:0:0:0:1', '2018-03-29 19:10:42');
INSERT INTO `sys_log` VALUES ('19', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":45,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"查看\",\"perms\":\"sys:rentrule:list,sys:rentrule:info\",\"type\":2,\"orderNum\":0}', '82', '0:0:0:0:0:0:0:1', '2018-03-29 19:11:06');
INSERT INTO `sys_log` VALUES ('20', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":46,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"新增\",\"perms\":\"sys:rentrule:save\",\"type\":2,\"orderNum\":0}', '90', '0:0:0:0:0:0:0:1', '2018-03-29 19:11:34');
INSERT INTO `sys_log` VALUES ('21', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":47,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"修改\",\"perms\":\"sys:rentrule:update\",\"type\":2,\"orderNum\":0}', '84', '0:0:0:0:0:0:0:1', '2018-03-29 19:12:45');
INSERT INTO `sys_log` VALUES ('22', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":48,\"parentId\":44,\"parentName\":\"租金管理\",\"name\":\"删除\",\"perms\":\"sys:rentrule:delete\",\"type\":2,\"orderNum\":0}', '88', '0:0:0:0:0:0:0:1', '2018-03-29 19:13:20');
INSERT INTO `sys_log` VALUES ('23', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"\",\"type\":1,\"orderNum\":0}', '83', '0:0:0:0:0:0:0:1', '2018-03-29 19:13:33');
INSERT INTO `sys_log` VALUES ('24', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"\",\"type\":1,\"icon\":\"fa fa-gave\",\"orderNum\":0}', '84', '0:0:0:0:0:0:0:1', '2018-03-29 19:17:48');
INSERT INTO `sys_log` VALUES ('25', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"sys:rentrule:list,sys:rentrule:info\",\"type\":1,\"icon\":\"fa fa-gavel\",\"orderNum\":0}', '80', '0:0:0:0:0:0:0:1', '2018-03-29 19:18:12');
INSERT INTO `sys_log` VALUES ('26', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":43,\"parentId\":41,\"parentName\":\"业务管理\",\"name\":\"规则管理\",\"type\":0,\"icon\":\"fa fa-star\",\"orderNum\":0}', '83', '0:0:0:0:0:0:0:1', '2018-03-29 19:19:52');
INSERT INTO `sys_log` VALUES ('27', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"\",\"type\":1,\"icon\":\"fa fa-gavel\",\"orderNum\":0}', '77', '0:0:0:0:0:0:0:1', '2018-03-29 19:20:44');
INSERT INTO `sys_log` VALUES ('28', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":44,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"租金管理\",\"url\":\"modules/sys/rentrule.html\",\"perms\":\"1\",\"type\":1,\"icon\":\"fa fa-gavel\",\"orderNum\":0}', '80', '0:0:0:0:0:0:0:1', '2018-03-29 19:21:06');
INSERT INTO `sys_log` VALUES ('29', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":49,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"押金规则\",\"url\":\"modules/sys/depositrule.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '103', '0:0:0:0:0:0:0:1', '2018-03-30 02:55:34');
INSERT INTO `sys_log` VALUES ('30', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":84,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"充值卡\",\"url\":\"modules/sys/rechargecard.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '85', '0:0:0:0:0:0:0:1', '2018-04-01 18:54:11');
INSERT INTO `sys_log` VALUES ('31', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":84,\"parentId\":43,\"parentName\":\"规则管理\",\"name\":\"充值规则\",\"url\":\"modules/sys/rechargecard.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '74', '0:0:0:0:0:0:0:1', '2018-04-01 18:58:33');
INSERT INTO `sys_log` VALUES ('32', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":104,\"parentId\":41,\"parentName\":\"业务管理\",\"name\":\"硬件管理\",\"url\":\"a\",\"type\":1,\"icon\":\"fa fa-star\",\"orderNum\":0}', '91', '0:0:0:0:0:0:0:1', '2018-04-01 20:18:53');
INSERT INTO `sys_log` VALUES ('33', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":104,\"parentId\":41,\"parentName\":\"业务管理\",\"name\":\"硬件管理\",\"url\":\"\",\"type\":0,\"icon\":\"fa fa-star\",\"orderNum\":0}', '83', '0:0:0:0:0:0:0:1', '2018-04-01 20:19:10');
INSERT INTO `sys_log` VALUES ('34', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":59,\"parentId\":104,\"parentName\":\"硬件管理\",\"name\":\"机器\",\"url\":\"modules/sys/machine.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '77', '0:0:0:0:0:0:0:1', '2018-04-01 20:22:15');
INSERT INTO `sys_log` VALUES ('35', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":59,\"parentId\":104,\"parentName\":\"硬件管理\",\"name\":\"机器管理\",\"url\":\"modules/sys/machine.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '78', '0:0:0:0:0:0:0:1', '2018-04-01 20:23:19');
INSERT INTO `sys_log` VALUES ('36', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":94,\"parentId\":104,\"parentName\":\"硬件管理\",\"name\":\"雨伞\",\"url\":\"modules/sys/umbrella.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '103', '127.0.0.1', '2018-04-01 23:34:29');
INSERT INTO `sys_log` VALUES ('37', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":94,\"parentId\":104,\"parentName\":\"硬件管理\",\"name\":\"雨伞管理\",\"url\":\"modules/sys/umbrella.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '90', '0:0:0:0:0:0:0:1', '2018-04-01 23:34:59');
INSERT INTO `sys_log` VALUES ('38', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":105,\"parentId\":41,\"parentName\":\"业务管理\",\"name\":\"运营概况\",\"type\":0,\"orderNum\":0}', '97', '0:0:0:0:0:0:0:1', '2018-04-02 18:49:06');
INSERT INTO `sys_log` VALUES ('39', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":105,\"parentId\":41,\"parentName\":\"业务管理\",\"name\":\"运营概况\",\"type\":0,\"icon\":\"fa fa-star\",\"orderNum\":0}', '83', '0:0:0:0:0:0:0:1', '2018-04-02 18:49:26');
INSERT INTO `sys_log` VALUES ('40', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":54,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"押金列表\",\"url\":\"modules/sys/deposit.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '92', '0:0:0:0:0:0:0:1', '2018-04-02 18:50:01');
INSERT INTO `sys_log` VALUES ('41', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":64,\"parentId\":104,\"parentName\":\"硬件管理\",\"name\":\"伞道\",\"url\":\"modules/sys/machinechanncel.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '97', '0:0:0:0:0:0:0:1', '2018-04-04 00:23:12');
INSERT INTO `sys_log` VALUES ('42', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":64,\"parentId\":104,\"parentName\":\"硬件管理\",\"name\":\"伞道管理\",\"url\":\"modules/sys/machinechanncel.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '91', '0:0:0:0:0:0:0:1', '2018-04-04 00:23:44');
INSERT INTO `sys_log` VALUES ('43', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '66', '147', '0:0:0:0:0:0:0:1', '2018-04-04 01:44:07');
INSERT INTO `sys_log` VALUES ('44', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '67', '119', '0:0:0:0:0:0:0:1', '2018-04-04 01:44:21');
INSERT INTO `sys_log` VALUES ('45', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '68', '115', '0:0:0:0:0:0:0:1', '2018-04-04 01:44:33');
INSERT INTO `sys_log` VALUES ('46', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":79,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"充值\",\"url\":\"modules/sys/recharge.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '103', '0:0:0:0:0:0:0:1', '2018-04-04 01:52:36');
INSERT INTO `sys_log` VALUES ('47', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":79,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"充值列表\",\"url\":\"modules/sys/recharge.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '86', '0:0:0:0:0:0:0:1', '2018-04-04 01:54:03');
INSERT INTO `sys_log` VALUES ('48', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":69,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"订单列表\",\"url\":\"modules/sys/order.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '103', '0:0:0:0:0:0:0:1', '2018-04-07 16:27:18');
INSERT INTO `sys_log` VALUES ('49', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":99,\"parentId\":41,\"parentName\":\"业务管理\",\"name\":\"微信用户\",\"url\":\"modules/sys/wxuser.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '84', '0:0:0:0:0:0:0:1', '2018-04-07 16:30:10');
INSERT INTO `sys_log` VALUES ('50', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '101', '173', '0:0:0:0:0:0:0:1', '2018-04-07 16:41:47');
INSERT INTO `sys_log` VALUES ('51', 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '103', '107', '0:0:0:0:0:0:0:1', '2018-04-07 16:41:58');
INSERT INTO `sys_log` VALUES ('52', 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '{\"userId\":2,\"username\":\"13560464310\",\"password\":\"8fb32d90cf0fa4cb91d2a44e6f93df8885ebd02e1a84ec43508d0caeb78dcdb1\",\"salt\":\"bvnZYTKWAV2jNgKHU91q\",\"email\":\"apple_live@qq.com\",\"mobile\":\"123456\",\"status\":1,\"roleIdList\":[],\"createTime\":\"May 18, 2018 10:42:27 PM\",\"deptId\":2,\"deptName\":\"北京分公司\"}', '358', '0:0:0:0:0:0:0:1', '2018-05-18 22:42:27');
INSERT INTO `sys_log` VALUES ('53', 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '{\"roleId\":1,\"roleName\":\"北京分公司\",\"remark\":\"北京分公司权限\",\"deptId\":2,\"deptName\":\"北京分公司\",\"menuIdList\":[1,4,23,24,25,26,74,75,76,77,78,41,43,44,45,46,47,48,49,50,51,52,53,84,85,86,87,88,99,100,102,104,59,60,61,62,63,64,65,94,95,96,97,98,105,54,55,56,57,58,69,70,71,72,73,79,80,81,82,83],\"deptIdList\":[],\"createTime\":\"May 18, 2018 10:45:23 PM\"}', '968', '0:0:0:0:0:0:0:1', '2018-05-18 22:45:24');
INSERT INTO `sys_log` VALUES ('54', 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '{\"userId\":2,\"username\":\"13560464310\",\"salt\":\"bvnZYTKWAV2jNgKHU91q\",\"email\":\"apple_live@qq.com\",\"mobile\":\"123456\",\"status\":1,\"roleIdList\":[1],\"createTime\":\"May 18, 2018 10:42:27 PM\",\"deptId\":2,\"deptName\":\"北京分公司\"}', '260', '0:0:0:0:0:0:0:1', '2018-05-18 22:45:36');
INSERT INTO `sys_log` VALUES ('55', 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '{\"roleId\":1,\"roleName\":\"北京分公司\",\"remark\":\"北京分公司权限\",\"deptId\":2,\"deptName\":\"北京分公司\",\"menuIdList\":[1,74,75,76,77,78,89,90,91,92,93,41,43,44,45,46,47,48,49,50,51,52,53,84,85,86,87,88,99,100,102,104,59,60,61,62,63,64,65,94,95,96,97,98,105,54,55,56,57,58,69,70,71,72,73,79,80,81,82,83],\"deptIdList\":[],\"createTime\":\"May 18, 2018 10:45:23 PM\"}', '920', '0:0:0:0:0:0:0:1', '2018-05-18 22:50:55');
INSERT INTO `sys_log` VALUES ('56', 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '{\"userId\":2,\"username\":\"13560464310\",\"salt\":\"bvnZYTKWAV2jNgKHU91q\",\"email\":\"apple_live@qq.com\",\"mobile\":\"123456\",\"status\":1,\"roleIdList\":[1],\"createTime\":\"May 18, 2018 10:42:27 PM\",\"deptId\":2,\"deptName\":\"北京分公司\"}', '296', '0:0:0:0:0:0:0:1', '2018-05-18 22:53:57');
INSERT INTO `sys_log` VALUES ('57', 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '{\"userId\":2,\"username\":\"13560464310\",\"salt\":\"bvnZYTKWAV2jNgKHU91q\",\"email\":\"apple_live@qq.com\",\"mobile\":\"123456\",\"status\":1,\"roleIdList\":[1],\"createTime\":\"May 18, 2018 10:42:27 PM\",\"deptId\":2,\"deptName\":\"北京分公司\"}', '265', '0:0:0:0:0:0:0:1', '2018-05-19 10:06:59');
INSERT INTO `sys_log` VALUES ('58', 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '{\"roleId\":1,\"roleName\":\"北京分公司\",\"remark\":\"北京分公司权限\",\"deptId\":2,\"deptName\":\"北京分公司\",\"menuIdList\":[1,74,75,76,77,78,89,90,91,92,93,41,43,44,45,46,47,48,49,50,51,52,53,84,85,86,87,88,99,100,102,104,59,60,61,62,63,64,65,94,95,96,97,98,105,54,55,56,57,58,69,70,71,72,73,79,80,81,82,83],\"deptIdList\":[2],\"createTime\":\"May 18, 2018 10:45:23 PM\"}', '12372', '0:0:0:0:0:0:0:1', '2018-05-19 17:52:43');
INSERT INTO `sys_log` VALUES ('59', 'admin', '保存配置', 'io.renren.modules.sys.controller.SysConfigController.save()', '{\"id\":2,\"paramKey\":\"test\",\"paramValue\":\"zhj\",\"remark\":\"test\"}', '227', '0:0:0:0:0:0:0:1', '2018-05-20 19:32:05');
INSERT INTO `sys_log` VALUES ('60', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":74,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"支付关系表\",\"url\":\"modules/sys/paymentlog.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '101', '0:0:0:0:0:0:0:1', '2018-05-20 19:34:41');
INSERT INTO `sys_log` VALUES ('61', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":89,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"退款日志表\",\"url\":\"modules/sys/refundlog.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '85', '0:0:0:0:0:0:0:1', '2018-05-20 19:34:59');
INSERT INTO `sys_log` VALUES ('62', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":74,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"支付日志表\",\"url\":\"modules/sys/paymentlog.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '96', '0:0:0:0:0:0:0:1', '2018-05-20 20:04:36');
INSERT INTO `sys_log` VALUES ('63', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '{\"menuId\":106,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"IOT日志\",\"url\":\"modules/sys/iotcallbacklog.html\",\"type\":1,\"orderNum\":7}', '81', '0:0:0:0:0:0:0:1', '2018-05-20 21:00:25');
INSERT INTO `sys_log` VALUES ('64', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":106,\"parentId\":104,\"parentName\":\"硬件管理\",\"name\":\"IOT日志\",\"url\":\"modules/sys/iotcallbacklog.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":7}', '92', '0:0:0:0:0:0:0:1', '2018-05-20 21:01:16');
INSERT INTO `sys_log` VALUES ('65', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":107,\"parentId\":106,\"parentName\":\"IOT日志\",\"name\":\"查看\",\"perms\":\"sys:iotcallbacklog:list,sys:iotcallbacklog:info\",\"type\":2,\"orderNum\":6}', '78', '0:0:0:0:0:0:0:1', '2018-05-20 21:13:43');
INSERT INTO `sys_log` VALUES ('66', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":111,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"故障管理\",\"url\":\"modules/sys/wxuserfault.html\",\"type\":1,\"icon\":\"larry-10109\",\"orderNum\":6}', '115', '0:0:0:0:0:0:0:1', '2018-05-23 00:12:56');
INSERT INTO `sys_log` VALUES ('67', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '{\"menuId\":111,\"parentId\":105,\"parentName\":\"运营概况\",\"name\":\"故障管理\",\"url\":\"modules/sys/wxuserfault.html\",\"type\":1,\"icon\":\"fa fa-file-code-o\",\"orderNum\":6}', '81', '0:0:0:0:0:0:0:1', '2018-05-23 00:13:45');

-- ----------------------------
-- Table structure for sys_machine
-- ----------------------------
DROP TABLE IF EXISTS `sys_machine`;
CREATE TABLE `sys_machine` (
  `id` bigint(20) NOT NULL auto_increment,
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  `machine_code` varchar(20) NOT NULL COMMENT '编码',
  `machine_name` varchar(30) NOT NULL COMMENT '名称',
  `product_key` varchar(128) default '' COMMENT '阿里云产品KEY',
  `device_name` varchar(128) default '' COMMENT '阿里去产品NAME',
  `machine_status` tinyint(1) NOT NULL default '1' COMMENT '机器状态：0|离线，1|在线',
  `location` varchar(100) NOT NULL COMMENT '投放地址',
  `machine_qrcode_path` varchar(256) default NULL COMMENT '本地机器二维码路径',
  `machine_scene_url` varchar(256) default NULL COMMENT '机器二维码内容',
  `machine_short_url` varchar(256) default NULL COMMENT '机器短二维码内容',
  `chip_channcel_num` int(3) NOT NULL default '0' COMMENT '芯片伞道数量',
  `fixed_quantity` int(3) NOT NULL COMMENT '额定装伞数',
  `left_quantity` int(3) NOT NULL default '0' COMMENT '剩余伞数',
  `battery` decimal(10,2) NOT NULL default '0.00' COMMENT '电量%',
  `longitude` decimal(10,7) default NULL COMMENT '经度',
  `latitude` decimal(10,7) default NULL COMMENT '纬度',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `machine_code` (`machine_code`),
  KEY `idx_device_name` USING BTREE (`device_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='机器';

-- ----------------------------
-- Records of sys_machine
-- ----------------------------
INSERT INTO `sys_machine` VALUES ('5', '3', 'm10001036', '共想科技投放点', '4o5HzG9GaWx', '868575026628083', '1', '广州白云区国际单位二期C3-309', '\\staticfile\\qrcode\\machine\\m1000010002.jpg', 'https://mina.usan365.com/mm1000010002', 'https://mina.usan365.com/mm1000010002', '8', '30', '30', '100.00', '113.3447487', '23.1435170', null, null, null, null, null);
INSERT INTO `sys_machine` VALUES ('6', '2', 'm10001022', 'a', 'a', 'a', '1', '2', '\\staticfile\\qrcode\\machine\\m10001022.jpg', 'https://yosan.gzgxkj.com/mm10001022', 'https://yosan.gzgxkj.com/mm10001022', '4', '2', '2', '100.00', '113.3469880', '23.1432605', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_machine_channcel
-- ----------------------------
DROP TABLE IF EXISTS `sys_machine_channcel`;
CREATE TABLE `sys_machine_channcel` (
  `id` bigint(20) NOT NULL auto_increment,
  `machine_id` bigint(20) NOT NULL COMMENT '机器id',
  `channcel_code` varchar(20) default NULL COMMENT '编码',
  `channcel` tinyint(2) NOT NULL COMMENT '机器-伞道：1|表示第一条通道，2|表示第二条通道',
  `channcel_qrcode_path` varchar(256) NOT NULL COMMENT '本地伞道二维码路径：1234开锁，5678关锁',
  `channcel_scene_url` varchar(256) NOT NULL COMMENT '伞道二维码内容：1234开锁，5678关锁',
  `channcel_short_url` varchar(256) NOT NULL COMMENT '伞道短二维码内容：1234开锁，5678关锁',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `channcel_code` (`channcel_code`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='伞道';

-- ----------------------------
-- Records of sys_machine_channcel
-- ----------------------------
INSERT INTO `sys_machine_channcel` VALUES ('2', '6', 'c100010361', '1', '\\staticfile\\qrcode\\channcel\\c100010221.jpg', 'https://yosan.gzgxkj.com/cc100010221', 'https://yosan.gzgxkj.com/cc100010221', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('3', '6', 'c100010362', '2', '\\staticfile\\qrcode\\channcel\\c100010222.jpg', 'https://yosan.gzgxkj.com/cc100010222', 'https://yosan.gzgxkj.com/cc100010222', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('4', '6', 'c100010363', '3', '\\staticfile\\qrcode\\channcel\\c100010223.jpg', 'https://yosan.gzgxkj.com/cc100010223', 'https://yosan.gzgxkj.com/cc100010223', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('5', '6', 'c100010364', '4', '\\staticfile\\qrcode\\channcel\\c100010224.jpg', 'https://yosan.gzgxkj.com/cc100010224', 'https://yosan.gzgxkj.com/cc100010224', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('6', '6', 'c100010365', '5', '\\staticfile\\qrcode\\channcel\\c100010225.jpg', 'https://yosan.gzgxkj.com/cc100010225', 'https://yosan.gzgxkj.com/cc100010225', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('7', '6', 'c100010366', '6', '\\staticfile\\qrcode\\channcel\\c100010226.jpg', 'https://yosan.gzgxkj.com/cc100010226', 'https://yosan.gzgxkj.com/cc100010226', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('8', '5', 'c100010211', '1', '\\staticfile\\qrcode\\channcel\\c100010211.jpg', 'https://yosan.gzgxkj.com/cc100010211', 'https://yosan.gzgxkj.com/cc100010211', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('9', '5', 'c100010212', '2', '\\staticfile\\qrcode\\channcel\\c100010212.jpg', 'https://yosan.gzgxkj.com/cc100010212', 'https://yosan.gzgxkj.com/cc100010212', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('10', '5', 'c100010213', '3', '\\staticfile\\qrcode\\channcel\\c100010213.jpg', 'https://yosan.gzgxkj.com/cc100010213', 'https://yosan.gzgxkj.com/cc100010213', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('11', '5', 'c100010214', '4', '\\staticfile\\qrcode\\channcel\\c100010214.jpg', 'https://yosan.gzgxkj.com/cc100010214', 'https://yosan.gzgxkj.com/cc100010214', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('12', '5', 'c100010215', '5', '\\staticfile\\qrcode\\channcel\\c100010215.jpg', 'https://yosan.gzgxkj.com/cc100010215', 'https://yosan.gzgxkj.com/cc100010215', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('13', '5', 'c100010216', '6', '\\staticfile\\qrcode\\channcel\\c100010216.jpg', 'https://yosan.gzgxkj.com/cc100010216', 'https://yosan.gzgxkj.com/cc100010216', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('14', '5', 'c100010217', '7', '\\staticfile\\qrcode\\channcel\\c100010217.jpg', 'https://yosan.gzgxkj.com/cc100010217', 'https://yosan.gzgxkj.com/cc100010217', null, null, null, null, null);
INSERT INTO `sys_machine_channcel` VALUES ('15', '5', 'c100010218', '8', '\\staticfile\\qrcode\\channcel\\c100010218.jpg', 'https://yosan.gzgxkj.com/cc100010218', 'https://yosan.gzgxkj.com/cc100010218', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL auto_increment,
  `parent_id` bigint(20) default NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) default NULL COMMENT '菜单名称',
  `url` varchar(200) default NULL COMMENT '菜单URL',
  `perms` varchar(500) default NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) default NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) default NULL COMMENT '菜单图标',
  `order_num` int(11) default NULL COMMENT '排序',
  PRIMARY KEY  (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'fa fa-cog', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '管理员管理', 'modules/sys/user.html', null, '1', 'fa fa-user', '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', 'modules/sys/role.html', null, '1', 'fa fa-user-secret', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'modules/sys/menu.html', null, '1', 'fa fa-th-list', '3');
INSERT INTO `sys_menu` VALUES ('5', '1', 'SQL监控', 'druid/sql.html', null, '1', 'fa fa-bug', '4');
INSERT INTO `sys_menu` VALUES ('6', '1', '定时任务', 'modules/job/schedule.html', null, '1', 'fa fa-tasks', '5');
INSERT INTO `sys_menu` VALUES ('7', '6', '查看', null, 'sys:schedule:list,sys:schedule:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('8', '6', '新增', null, 'sys:schedule:save', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('9', '6', '修改', null, 'sys:schedule:update', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '6', '删除', null, 'sys:schedule:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '6', '暂停', null, 'sys:schedule:pause', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '6', '恢复', null, 'sys:schedule:resume', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('13', '6', '立即执行', null, 'sys:schedule:run', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '6', '日志列表', null, 'sys:schedule:log', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('27', '1', '参数管理', 'modules/sys/config.html', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', '1', 'fa fa-sun-o', '6');
INSERT INTO `sys_menu` VALUES ('29', '1', '系统日志', 'modules/sys/log.html', 'sys:log:list', '1', 'fa fa-file-text-o', '7');
INSERT INTO `sys_menu` VALUES ('30', '1', '文件上传', 'modules/oss/oss.html', 'sys:oss:all', '1', 'fa fa-file-image-o', '6');
INSERT INTO `sys_menu` VALUES ('31', '1', '部门管理', 'modules/sys/dept.html', null, '1', 'fa fa-file-code-o', '1');
INSERT INTO `sys_menu` VALUES ('32', '31', '查看', null, 'sys:dept:list,sys:dept:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('33', '31', '新增', null, 'sys:dept:save,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('34', '31', '修改', null, 'sys:dept:update,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('35', '31', '删除', null, 'sys:dept:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('36', '1', '字典管理', 'modules/sys/dict.html', null, '1', 'fa fa-bookmark-o', '6');
INSERT INTO `sys_menu` VALUES ('37', '36', '查看', null, 'sys:dict:list,sys:dict:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('38', '36', '新增', null, 'sys:dict:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('39', '36', '修改', null, 'sys:dict:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('40', '36', '删除', null, 'sys:dict:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('41', '0', '业务管理', null, null, '0', 'fa fa-tasks', '0');
INSERT INTO `sys_menu` VALUES ('43', '41', '规则管理', null, null, '0', 'fa fa-star', '0');
INSERT INTO `sys_menu` VALUES ('44', '43', '租金管理', 'modules/sys/rentrule.html', '', '1', 'fa fa-gavel', '0');
INSERT INTO `sys_menu` VALUES ('45', '44', '查看', null, 'sys:rentrule:list,sys:rentrule:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('46', '44', '新增', null, 'sys:rentrule:save', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('47', '44', '修改', null, 'sys:rentrule:update', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('48', '44', '删除', null, 'sys:rentrule:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('49', '43', '押金规则', 'modules/sys/depositrule.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('50', '49', '查看', null, 'sys:depositrule:list,sys:depositrule:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('51', '49', '新增', null, 'sys:depositrule:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('52', '49', '修改', null, 'sys:depositrule:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('53', '49', '删除', null, 'sys:depositrule:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('54', '105', '押金列表', 'modules/sys/deposit.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('55', '54', '查看', null, 'sys:deposit:list,sys:deposit:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('56', '54', '新增', null, 'sys:deposit:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('57', '54', '修改', null, 'sys:deposit:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('58', '54', '删除', null, 'sys:deposit:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('59', '104', '机器管理', 'modules/sys/machine.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('60', '59', '查看', null, 'sys:machine:list,sys:machine:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('61', '59', '新增', null, 'sys:machine:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('62', '59', '修改', null, 'sys:machine:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('63', '59', '删除', null, 'sys:machine:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('64', '104', '伞道管理', 'modules/sys/machinechanncel.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('65', '64', '查看', null, 'sys:machinechanncel:list,sys:machinechanncel:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('69', '105', '订单列表', 'modules/sys/order.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('70', '69', '查看', null, 'sys:order:list,sys:order:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('71', '69', '新增', null, 'sys:order:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('72', '69', '修改', null, 'sys:order:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('73', '69', '删除', null, 'sys:order:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('74', '105', '支付日志表', 'modules/sys/paymentlog.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('75', '74', '查看', null, 'sys:paymentlog:list,sys:paymentlog:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('76', '74', '新增', null, 'sys:paymentlog:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('77', '74', '修改', null, 'sys:paymentlog:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('78', '74', '删除', null, 'sys:paymentlog:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('79', '105', '充值列表', 'modules/sys/recharge.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('80', '79', '查看', null, 'sys:recharge:list,sys:recharge:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('81', '79', '新增', null, 'sys:recharge:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('82', '79', '修改', null, 'sys:recharge:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('83', '79', '删除', null, 'sys:recharge:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('84', '43', '充值规则', 'modules/sys/rechargecard.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('85', '84', '查看', null, 'sys:rechargecard:list,sys:rechargecard:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('86', '84', '新增', null, 'sys:rechargecard:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('87', '84', '修改', null, 'sys:rechargecard:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('88', '84', '删除', null, 'sys:rechargecard:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('89', '105', '退款日志表', 'modules/sys/refundlog.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('90', '89', '查看', null, 'sys:refundlog:list,sys:refundlog:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('91', '89', '新增', null, 'sys:refundlog:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('92', '89', '修改', null, 'sys:refundlog:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('93', '89', '删除', null, 'sys:refundlog:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('94', '104', '雨伞管理', 'modules/sys/umbrella.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('95', '94', '查看', null, 'sys:umbrella:list,sys:umbrella:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('96', '94', '新增', null, 'sys:umbrella:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('97', '94', '修改', null, 'sys:umbrella:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('98', '94', '删除', null, 'sys:umbrella:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('99', '41', '微信用户', 'modules/sys/wxuser.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('100', '99', '查看', null, 'sys:wxuser:list,sys:wxuser:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('102', '99', '修改', null, 'sys:wxuser:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('104', '41', '硬件管理', '', null, '0', 'fa fa-star', '0');
INSERT INTO `sys_menu` VALUES ('105', '41', '运营概况', null, null, '0', 'fa fa-star', '0');
INSERT INTO `sys_menu` VALUES ('106', '104', 'IOT日志', 'modules/sys/iotcallbacklog.html', null, '1', 'fa fa-file-code-o', '7');
INSERT INTO `sys_menu` VALUES ('107', '106', '查看', null, 'sys:iotcallbacklog:list,sys:iotcallbacklog:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('108', '106', '新增', null, 'sys:iotcallbacklog:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('109', '106', '修改', null, 'sys:iotcallbacklog:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('110', '106', '删除', null, 'sys:iotcallbacklog:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('111', '105', '故障管理', 'modules/sys/wxuserfault.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('112', '111', '查看', null, 'sys:wxuserfault:list,sys:wxuserfault:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('113', '111', '新增', null, 'sys:wxuserfault:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('114', '111', '修改', null, 'sys:wxuserfault:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('115', '111', '删除', null, 'sys:wxuserfault:delete', '2', null, '6');

-- ----------------------------
-- Table structure for sys_order
-- ----------------------------
DROP TABLE IF EXISTS `sys_order`;
CREATE TABLE `sys_order` (
  `id` bigint(20) NOT NULL auto_increment,
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  `openid` varchar(28) NOT NULL default '' COMMENT '用户openid',
  `unionid` varchar(64) default '' COMMENT '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段',
  `transaction_id` varchar(32) default NULL COMMENT '支付订单号',
  `rent_machine_code` varchar(20) NOT NULL COMMENT '取伞源',
  `return_machine_code` varchar(20) default '' COMMENT '还伞源',
  `chip_code` varchar(20) NOT NULL default 'unuse' COMMENT '雨伞内置微芯片code',
  `total_price` decimal(10,2) NOT NULL default '0.00' COMMENT '租金金额',
  `actual_price` decimal(10,2) NOT NULL default '0.00' COMMENT '实付金额',
  `discount_price` decimal(10,2) NOT NULL default '0.00' COMMENT '折扣金额',
  `pay_method` varchar(10) NOT NULL default 'WX' COMMENT '支付方式：WX|微信支付，Alipay|支付宝支付，Balance|余额支付',
  `rent_place` varchar(256) NOT NULL COMMENT '租出地址',
  `return_place` varchar(256) default NULL COMMENT '归还地址',
  `rent_time` datetime default NULL COMMENT '租伞开始时间',
  `return_time` datetime default NULL COMMENT '还伞时间',
  `pay_time` datetime default NULL COMMENT '支付时间',
  `order_status` tinyint(1) unsigned NOT NULL COMMENT '付款交易状态:1|待取伞，2|待归还，3|已归还，待支付，4|交易完成，5|用户遗失，6|待支付系统自动取消订单, ',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `chip_code` (`chip_code`),
  KEY `openid` USING BTREE (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of sys_order
-- ----------------------------
INSERT INTO `sys_order` VALUES ('1', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '', null, 'm10001021', '', 'unuse', '0.00', '0.00', '0.00', 'WX', '广州白云区国际单位二期C3-309', '广州白云区国际单位二期C3-309', '2018-06-21 21:02:08', '2018-06-22 21:02:49', '2018-06-22 21:02:53', '4', null, '2018-05-13 14:40:14', null, '2018-05-13 14:40:14', null);
INSERT INTO `sys_order` VALUES ('3', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '', null, 'm10001036', '', 'unuse', '0.00', '0.00', '0.00', 'WX', '广州白云区国际单位二期C3-309', null, null, null, null, '1', null, '2018-06-23 15:57:28', null, '2018-06-23 15:57:28', null);

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss` (
  `id` bigint(20) NOT NULL auto_increment,
  `url` varchar(200) default NULL COMMENT 'URL地址',
  `create_date` datetime default NULL COMMENT '创建时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Records of sys_oss
-- ----------------------------
INSERT INTO `sys_oss` VALUES ('1', 'http://p96gov5rj.bkt.clouddn.com/upload/20180523/97d700a1b120407ba1d189e3f15b1dc5.png', '2018-05-23 19:06:42');

-- ----------------------------
-- Table structure for sys_payment_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_payment_log`;
CREATE TABLE `sys_payment_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `openid` varchar(28) NOT NULL COMMENT '用户openid',
  `trade_no` varchar(32) NOT NULL COMMENT '商户订单号(提交至微信的本地订单号，非系统真正订单号)',
  `transaction_id` varchar(32) default NULL COMMENT '微信支付订单号',
  `trade_type` tinyint(1) unsigned NOT NULL default '1' COMMENT '交易类型：1|押金，2|充值，3|退款，4|消费',
  `pay_status` tinyint(1) unsigned NOT NULL default '1' COMMENT '支付状态：1|待支付，2|已支付',
  `pay_time` datetime default NULL COMMENT '支付时间',
  `pay_price` decimal(10,2) NOT NULL default '0.00' COMMENT '付款金额',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `sys_payment_log_idx_1` (`order_id`),
  KEY `sys_payment_log_idx_2` (`trade_no`),
  KEY `sys_payment_log_idx_3` (`pay_status`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COMMENT='支付关系表';

-- ----------------------------
-- Records of sys_payment_log
-- ----------------------------
INSERT INTO `sys_payment_log` VALUES ('52', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010406732268105729', null, '1', '1', null, '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付押金日志  金额=0.01', '2018-06-23 14:18:32', null, '2018-06-23 14:18:32', null);
INSERT INTO `sys_payment_log` VALUES ('53', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010415695420256258', '4200000127201806232848254116', '1', '2', '2018-06-23 14:54:15', '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付押金日志  金额=0.01', '2018-06-23 14:54:09', null, '2018-06-23 15:15:12', null);
INSERT INTO `sys_payment_log` VALUES ('54', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010938288019623938', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-25 01:30:45', null, '2018-06-25 01:30:45', null);
INSERT INTO `sys_payment_log` VALUES ('55', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010939759012368385', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-25 01:36:36', null, '2018-06-25 01:36:36', null);
INSERT INTO `sys_payment_log` VALUES ('56', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1012022799914336257', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-28 01:20:13', null, '2018-06-28 01:20:13', null);
INSERT INTO `sys_payment_log` VALUES ('57', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1012023273765830658', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-28 01:22:06', null, '2018-06-28 01:22:06', null);
INSERT INTO `sys_payment_log` VALUES ('58', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1012023780936876033', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-28 01:24:07', null, '2018-06-28 01:24:07', null);
INSERT INTO `sys_payment_log` VALUES ('59', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1012028714092691458', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-28 01:43:43', null, '2018-06-28 01:43:43', null);
INSERT INTO `sys_payment_log` VALUES ('60', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1012029088497238017', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-28 01:45:12', null, '2018-06-28 01:45:12', null);
INSERT INTO `sys_payment_log` VALUES ('61', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1012030227875725313', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-06-28 01:49:44', null, '2018-06-28 01:49:44', null);
INSERT INTO `sys_payment_log` VALUES ('62', '2', '3', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1013117762496462849', null, '2', '1', null, '10.00', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 支付充值日志  金额=10.00', '2018-07-01 01:51:12', null, '2018-07-01 01:51:12', null);
INSERT INTO `sys_payment_log` VALUES ('63', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010415695420256258', null, '3', '2', null, '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 退款押金日志  金额=0.01', '2018-07-01 15:00:18', null, '2018-07-01 15:00:18', null);
INSERT INTO `sys_payment_log` VALUES ('64', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010415695420256258', null, '3', '2', null, '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 退款押金日志  金额=0.01', '2018-07-01 15:00:57', null, '2018-07-01 15:00:57', null);
INSERT INTO `sys_payment_log` VALUES ('65', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010415695420256258', null, '3', '2', null, '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 退款押金日志  金额=0.01', '2018-07-01 15:01:09', null, '2018-07-01 15:01:09', null);
INSERT INTO `sys_payment_log` VALUES ('66', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010415695420256258', null, '3', '2', null, '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 退款押金日志  金额=0.01', '2018-07-05 20:14:19', null, '2018-07-05 20:14:19', null);
INSERT INTO `sys_payment_log` VALUES ('67', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010415695420256258', null, '3', '2', null, '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 退款押金日志  金额=0.01', '2018-07-05 20:20:43', null, '2018-07-05 20:20:43', null);
INSERT INTO `sys_payment_log` VALUES ('68', '1', null, 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1010415695420256258', null, '3', '2', null, '0.01', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ 退款押金日志  金额=0.01', '2018-07-05 20:20:58', null, '2018-07-05 20:20:58', null);

-- ----------------------------
-- Table structure for sys_recharge
-- ----------------------------
DROP TABLE IF EXISTS `sys_recharge`;
CREATE TABLE `sys_recharge` (
  `id` bigint(20) NOT NULL auto_increment,
  `openid` varchar(28) NOT NULL default '' COMMENT '付款人openid',
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `trade_no` varchar(32) default NULL COMMENT '商户订单号(提交至微信的本地订单号，非系统真正订单号)',
  `transaction_id` varchar(32) default NULL COMMENT '微信支付订单号',
  `front_balance` decimal(10,2) NOT NULL default '0.00' COMMENT '充值前账号余额',
  `total_price` decimal(10,2) NOT NULL default '0.00' COMMENT '充值金额',
  `actual_price` decimal(10,2) NOT NULL default '0.00' COMMENT '实付金额',
  `discount_price` decimal(10,2) NOT NULL default '0.00' COMMENT '折扣金额',
  `pay_time` datetime default NULL COMMENT '支付时间',
  `pay_scene` varchar(12) NOT NULL COMMENT '支付场景',
  `recharge_status` tinyint(1) NOT NULL default '1' COMMENT '充值状态:1|待支付，2|已支付，3|取消支付，4|其他',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `trade_no` (`trade_no`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='充值';

-- ----------------------------
-- Records of sys_recharge
-- ----------------------------
INSERT INTO `sys_recharge` VALUES ('1', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '3', null, null, '0.00', '0.00', '0.00', '0.00', '2018-05-20 16:24:36', '', '1', null, '2018-05-20 16:24:39', null, '2018-05-20 16:24:42', null);
INSERT INTO `sys_recharge` VALUES ('2', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '3', null, null, '109.00', '5.00', '10.00', '0.00', null, 'JSAPI', '2', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_recharge_card
-- ----------------------------
DROP TABLE IF EXISTS `sys_recharge_card`;
CREATE TABLE `sys_recharge_card` (
  `id` bigint(20) NOT NULL auto_increment,
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `total_price` decimal(10,2) NOT NULL default '0.00' COMMENT '面额（充值时使用这个字段）',
  `market_price` decimal(10,2) NOT NULL default '0.00' COMMENT '实际售价',
  `discount_price` decimal(10,2) NOT NULL default '0.00' COMMENT '折扣金额',
  `shelf_status` tinyint(1) NOT NULL default '1' COMMENT '上架状态:1|已上架，2|未上架',
  `desc` varchar(200) default '' COMMENT '描述',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='充值卡';

-- ----------------------------
-- Records of sys_recharge_card
-- ----------------------------
INSERT INTO `sys_recharge_card` VALUES ('1', '3', '0.02', '0.01', '5.00', '1', '1', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_refund_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_refund_log`;
CREATE TABLE `sys_refund_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `openid` varchar(28) NOT NULL default '' COMMENT '收款人openid',
  `deposit_id` bigint(20) default NULL COMMENT '押金ID',
  `trade_no` varchar(100) NOT NULL COMMENT '财付通订单号',
  `dept_id` bigint(20) NOT NULL COMMENT '部门',
  `pay_price` decimal(10,2) NOT NULL default '0.00' COMMENT '支付金额',
  `refund_price` decimal(10,2) NOT NULL default '0.00' COMMENT '退款金额',
  `refund_time` datetime NOT NULL COMMENT '退款时间',
  `refund_scene` varchar(12) default NULL COMMENT '支付场景',
  `refund_status` tinyint(1) NOT NULL default '1' COMMENT '退款交易状态：1|失败，2|成功',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='退款日志表';

-- ----------------------------
-- Records of sys_refund_log
-- ----------------------------
INSERT INTO `sys_refund_log` VALUES ('1', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1', '', '3', '10.00', '10.00', '2018-05-20 19:55:14', null, '2', null, '2018-05-20 19:55:19', null, '2018-05-20 19:55:21', null);

-- ----------------------------
-- Table structure for sys_rent_rule
-- ----------------------------
DROP TABLE IF EXISTS `sys_rent_rule`;
CREATE TABLE `sys_rent_rule` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL default '' COMMENT '名称',
  `dept_id` bigint(20) NOT NULL COMMENT '部门id',
  `free_type` tinyint(1) NOT NULL default '2' COMMENT '免费时长单位:1|分，2|小时，3|天',
  `free_value` int(10) NOT NULL default '0' COMMENT '时长：表示免费多少分钟，免费多少小时，免费多少天',
  `per_type_value` int(10) NOT NULL default '0' COMMENT '计价长度：表示每n(小时/天)',
  `rent_type` tinyint(1) NOT NULL default '1' COMMENT '计价单位：1|按小时计费，2|按天计费',
  `rent_value` decimal(10,2) NOT NULL default '1.00' COMMENT '租赁价(元)',
  `rule_status` tinyint(1) NOT NULL default '1' COMMENT '规则状态：0|过期, 1|执行',
  `desc` varchar(200) default '' COMMENT '规则的描述',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='租金规则';

-- ----------------------------
-- Records of sys_rent_rule
-- ----------------------------
INSERT INTO `sys_rent_rule` VALUES ('2', '343', '2', '1', '11', '11', '11', '11.00', '1', '1', null, null, null, null, null);
INSERT INTO `sys_rent_rule` VALUES ('3', '12', '3', '1', '1', '1', '1', '1.00', '1', '1', null, null, null, null, null);
INSERT INTO `sys_rent_rule` VALUES ('4', '11111', '4', '1', '1', '1', '1', '1.00', '0', '1', null, null, null, null, null);
INSERT INTO `sys_rent_rule` VALUES ('5', '11', '5', '1', '23', '2', '2', '2.00', '1', '2', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL auto_increment,
  `role_name` varchar(100) default NULL COMMENT '角色名称',
  `remark` varchar(100) default NULL COMMENT '备注',
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `create_time` datetime default NULL COMMENT '创建时间',
  PRIMARY KEY  (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '北京分公司', '北京分公司权限', '2', '2018-05-18 22:45:23');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `id` bigint(20) NOT NULL auto_increment,
  `role_id` bigint(20) default NULL COMMENT '角色ID',
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色与部门对应关系';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('1', '1', '2');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL auto_increment,
  `role_id` bigint(20) default NULL COMMENT '角色ID',
  `menu_id` bigint(20) default NULL COMMENT '菜单ID',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('121', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('122', '1', '74');
INSERT INTO `sys_role_menu` VALUES ('123', '1', '75');
INSERT INTO `sys_role_menu` VALUES ('124', '1', '76');
INSERT INTO `sys_role_menu` VALUES ('125', '1', '77');
INSERT INTO `sys_role_menu` VALUES ('126', '1', '78');
INSERT INTO `sys_role_menu` VALUES ('127', '1', '89');
INSERT INTO `sys_role_menu` VALUES ('128', '1', '90');
INSERT INTO `sys_role_menu` VALUES ('129', '1', '91');
INSERT INTO `sys_role_menu` VALUES ('130', '1', '92');
INSERT INTO `sys_role_menu` VALUES ('131', '1', '93');
INSERT INTO `sys_role_menu` VALUES ('132', '1', '41');
INSERT INTO `sys_role_menu` VALUES ('133', '1', '43');
INSERT INTO `sys_role_menu` VALUES ('134', '1', '44');
INSERT INTO `sys_role_menu` VALUES ('135', '1', '45');
INSERT INTO `sys_role_menu` VALUES ('136', '1', '46');
INSERT INTO `sys_role_menu` VALUES ('137', '1', '47');
INSERT INTO `sys_role_menu` VALUES ('138', '1', '48');
INSERT INTO `sys_role_menu` VALUES ('139', '1', '49');
INSERT INTO `sys_role_menu` VALUES ('140', '1', '50');
INSERT INTO `sys_role_menu` VALUES ('141', '1', '51');
INSERT INTO `sys_role_menu` VALUES ('142', '1', '52');
INSERT INTO `sys_role_menu` VALUES ('143', '1', '53');
INSERT INTO `sys_role_menu` VALUES ('144', '1', '84');
INSERT INTO `sys_role_menu` VALUES ('145', '1', '85');
INSERT INTO `sys_role_menu` VALUES ('146', '1', '86');
INSERT INTO `sys_role_menu` VALUES ('147', '1', '87');
INSERT INTO `sys_role_menu` VALUES ('148', '1', '88');
INSERT INTO `sys_role_menu` VALUES ('149', '1', '99');
INSERT INTO `sys_role_menu` VALUES ('150', '1', '100');
INSERT INTO `sys_role_menu` VALUES ('151', '1', '102');
INSERT INTO `sys_role_menu` VALUES ('152', '1', '104');
INSERT INTO `sys_role_menu` VALUES ('153', '1', '59');
INSERT INTO `sys_role_menu` VALUES ('154', '1', '60');
INSERT INTO `sys_role_menu` VALUES ('155', '1', '61');
INSERT INTO `sys_role_menu` VALUES ('156', '1', '62');
INSERT INTO `sys_role_menu` VALUES ('157', '1', '63');
INSERT INTO `sys_role_menu` VALUES ('158', '1', '64');
INSERT INTO `sys_role_menu` VALUES ('159', '1', '65');
INSERT INTO `sys_role_menu` VALUES ('160', '1', '94');
INSERT INTO `sys_role_menu` VALUES ('161', '1', '95');
INSERT INTO `sys_role_menu` VALUES ('162', '1', '96');
INSERT INTO `sys_role_menu` VALUES ('163', '1', '97');
INSERT INTO `sys_role_menu` VALUES ('164', '1', '98');
INSERT INTO `sys_role_menu` VALUES ('165', '1', '105');
INSERT INTO `sys_role_menu` VALUES ('166', '1', '54');
INSERT INTO `sys_role_menu` VALUES ('167', '1', '55');
INSERT INTO `sys_role_menu` VALUES ('168', '1', '56');
INSERT INTO `sys_role_menu` VALUES ('169', '1', '57');
INSERT INTO `sys_role_menu` VALUES ('170', '1', '58');
INSERT INTO `sys_role_menu` VALUES ('171', '1', '69');
INSERT INTO `sys_role_menu` VALUES ('172', '1', '70');
INSERT INTO `sys_role_menu` VALUES ('173', '1', '71');
INSERT INTO `sys_role_menu` VALUES ('174', '1', '72');
INSERT INTO `sys_role_menu` VALUES ('175', '1', '73');
INSERT INTO `sys_role_menu` VALUES ('176', '1', '79');
INSERT INTO `sys_role_menu` VALUES ('177', '1', '80');
INSERT INTO `sys_role_menu` VALUES ('178', '1', '81');
INSERT INTO `sys_role_menu` VALUES ('179', '1', '82');
INSERT INTO `sys_role_menu` VALUES ('180', '1', '83');

-- ----------------------------
-- Table structure for sys_umbrella
-- ----------------------------
DROP TABLE IF EXISTS `sys_umbrella`;
CREATE TABLE `sys_umbrella` (
  `id` bigint(20) NOT NULL auto_increment,
  `dept_id` bigint(20) default NULL COMMENT '部门id',
  `machine_id` bigint(20) default NULL COMMENT '机器id',
  `chip_code` varchar(20) NOT NULL COMMENT '雨伞内置微芯片code',
  `umbrella_name` varchar(30) NOT NULL COMMENT '名称',
  `umbrella_status` tinyint(1) NOT NULL default '2' COMMENT '雨伞状态：1|空闲，2|锁定，3|租出, 4|遗失',
  `channcel` tinyint(2) NOT NULL COMMENT '机器-伞道：1|表示第一条通道，2|表示第二条通道',
  `umbrella_qrcode_path` varchar(256) NOT NULL COMMENT '本地雨伞二维码路径',
  `umbrella_scene_url` varchar(256) NOT NULL COMMENT '雨伞二维码内容',
  `umbrella_short_url` varchar(256) NOT NULL COMMENT '雨伞短二维码内容',
  `last_rent_time` datetime default NULL COMMENT '最近一次租出时间',
  `rent_times` int(11) default '0' COMMENT '租用次数',
  `desc` varchar(200) default NULL COMMENT '描述',
  `remark` varchar(100) default NULL,
  `create_time` datetime default NULL,
  `create_by` bigint(20) default NULL,
  `update_time` datetime default NULL,
  `update_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `chip_code` (`chip_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='雨伞';

-- ----------------------------
-- Records of sys_umbrella
-- ----------------------------
INSERT INTO `sys_umbrella` VALUES ('1', null, '5', '2965128', '共想科技-u1000010184', '1', '1', '\\staticfile\\qrcode\\umbrella\\1.jpg', 'https://mina.usan365.com/u1', 'https://mina.usan365.com/u1', null, '0', '0', null, null, null, null, null);
INSERT INTO `sys_umbrella` VALUES ('2', null, '5', '1052952', '共想科技-u1000010186', '1', '1', '/staticfile/qrcode/umbrella/1052952.jpg', 'https://yosan.gzgxkj.com/u1052952', 'https://yosan.gzgxkj.com/u1052952', null, '0', '0', null, null, null, null, null);
INSERT INTO `sys_umbrella` VALUES ('3', null, '5', '2964096', '共想科技-u1000010179', '1', '1', '/staticfile/qrcode/umbrella/2964096.jpg', 'https://yosan.gzgxkj.com/u2964096', 'https://yosan.gzgxkj.com/u2964096', null, '0', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) default NULL COMMENT '密码',
  `salt` varchar(20) default NULL COMMENT '盐',
  `email` varchar(100) default NULL COMMENT '邮箱',
  `mobile` varchar(100) default NULL COMMENT '手机号',
  `status` tinyint(4) default NULL COMMENT '状态  0：禁用   1：正常',
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `create_time` datetime default NULL COMMENT '创建时间',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e1153123d7d180ceeb820d577ff119876678732a68eef4e6ffc0b1f06a01f91b', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', '1', '1', '2016-11-11 11:11:11');
INSERT INTO `sys_user` VALUES ('2', '13560464310', '8fb32d90cf0fa4cb91d2a44e6f93df8885ebd02e1a84ec43508d0caeb78dcdb1', 'bvnZYTKWAV2jNgKHU91q', 'apple_live@qq.com', '123456', '1', '2', '2018-05-18 22:42:27');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` bigint(20) default NULL COMMENT '用户ID',
  `role_id` bigint(20) default NULL COMMENT '角色ID',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('3', '2', '1');

-- ----------------------------
-- Table structure for sys_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_wx_user`;
CREATE TABLE `sys_wx_user` (
  `id` bigint(20) NOT NULL auto_increment COMMENT '用户id',
  `dept_id` bigint(20) default NULL COMMENT '部门ID',
  `appid` varchar(25) NOT NULL default 'wxd9e6088604e08cda' COMMENT '公众号appid',
  `openid` varchar(28) default NULL COMMENT '用户在公众号的唯一标示',
  `mina_appid` varchar(25) NOT NULL default 'wx7cb45a366a8752d4' COMMENT '小程序appid',
  `mina_openid` varchar(28) default NULL COMMENT '小程序openid',
  `unionid` varchar(64) default '' COMMENT '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段',
  `subscribe` tinyint(1) default '1' COMMENT '关注：1|为关注，0|未关注',
  `nick_name` varchar(20) default NULL COMMENT '昵称',
  `sex` tinyint(1) default NULL COMMENT '姓别：1|男，2|女，3|未知',
  `language` varchar(16) default NULL COMMENT '语言',
  `city` varchar(55) default NULL COMMENT '所在城市',
  `province` varchar(55) default '' COMMENT '所在省份',
  `country` varchar(55) default '' COMMENT '所在国家',
  `avatar` varchar(256) default NULL COMMENT '头像',
  `subscribe_time` int(11) default NULL COMMENT '关注时间',
  `level` tinyint(1) NOT NULL default '1' COMMENT '会员等级：1|普通会员，2|白银会员，3|黄金会员，4|钻石会员',
  `bonus` int(11) NOT NULL default '0' COMMENT '积分',
  `balance` decimal(10,2) NOT NULL default '0.00' COMMENT '余额',
  `deposit` decimal(10,2) NOT NULL default '0.00' COMMENT '押金',
  `is_manager` tinyint(1) default '1' COMMENT '是否管理员：1|非管理员，2|管理员',
  `rent_times` int(11) default '0' COMMENT '租伞次数',
  `remark` varchar(100) default NULL COMMENT '备注',
  `longitude` decimal(10,7) default NULL COMMENT '经度',
  `latitude` decimal(10,7) default NULL COMMENT '纬度',
  `last_login_time` datetime default NULL COMMENT '最近一次登录时间',
  `last_login_ip` varchar(15) default NULL COMMENT '最近一次登录ip',
  `create_by` bigint(20) default NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) default NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `openid` (`openid`),
  UNIQUE KEY `mina_openid` (`mina_openid`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='微信用户';

-- ----------------------------
-- Records of sys_wx_user
-- ----------------------------
INSERT INTO `sys_wx_user` VALUES ('3', '3', 'wxbd2f253d4bf3b1f0', null, 'wxbd2f253d4bf3b1f0', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', 'oepMz0rLhRkOryZ7K28QAMnXVqak', '1', '张君君[发呆]', '1', 'zh_CN', 'Guangzhou', 'Guangdong', 'China', 'https://wx.qlogo.cn/mmopen/vi_32/Buqic80tx4jd2X5nWH3xdLLbsgPMyjR0SL3HGDHnW4oib35A3jIIKs6RThL0DQibXoEdLYbIm5vkmQHJSbPhKp4Mg/132', null, '1', '0', '109.00', '0.01', '1', '0', null, null, null, null, null, null, '2018-05-03 03:01:39', null, '2018-06-23 15:05:15');
INSERT INTO `sys_wx_user` VALUES ('4', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5cqXeaE4QKoS3T2Wd6wMo6A', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-02 23:14:37', null, '2018-06-02 23:14:37');
INSERT INTO `sys_wx_user` VALUES ('5', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5UBTVr1d5ZRYL9lZHQQKR5w', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-03 19:32:17', null, '2018-06-03 19:32:17');
INSERT INTO `sys_wx_user` VALUES ('6', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5W2A9orI98znOgFnxMk-KWA', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-03 21:10:42', null, '2018-06-03 21:10:42');
INSERT INTO `sys_wx_user` VALUES ('7', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5dxVfp3bYjCxBKk_vx2SVw8', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-04 11:47:44', null, '2018-06-04 11:47:44');
INSERT INTO `sys_wx_user` VALUES ('8', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5RYlXTUecmQTMH3rqLHizmY', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-07 13:44:37', null, '2018-06-07 13:44:37');
INSERT INTO `sys_wx_user` VALUES ('9', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5a59ehx4gwG-8-oWq3JLovY', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-07 21:06:07', null, '2018-06-07 21:06:07');
INSERT INTO `sys_wx_user` VALUES ('10', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5ZoXelNRxFBN1_rDAdDrFKI', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-10 18:20:45', null, '2018-06-10 18:20:45');
INSERT INTO `sys_wx_user` VALUES ('11', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5RNNOgCdEHWSFbVb16J2UxQ', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-10 21:27:22', null, '2018-06-10 21:27:22');
INSERT INTO `sys_wx_user` VALUES ('12', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5f23VYVX-wYCpM6-6MN2ku4', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-12 00:24:47', null, '2018-06-12 00:24:47');
INSERT INTO `sys_wx_user` VALUES ('13', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5c0yl07rhqYVfWPhXnFFF4k', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-12 18:42:34', null, '2018-06-12 18:42:34');
INSERT INTO `sys_wx_user` VALUES ('14', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Sf0RseYpvnDZGFFfCOvlc8', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-12 19:16:51', null, '2018-06-12 19:16:51');
INSERT INTO `sys_wx_user` VALUES ('15', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5WqR5mRS2tawlQCMjBmVG2E', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-15 10:53:53', null, '2018-06-15 10:53:53');
INSERT INTO `sys_wx_user` VALUES ('16', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5a09RNn6h0Vd72V2WgoW7ww', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-16 07:07:20', null, '2018-06-16 07:07:20');
INSERT INTO `sys_wx_user` VALUES ('17', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5e-xLOW7oBwigXjztSRiD4Y', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-16 18:34:05', null, '2018-06-16 18:34:05');
INSERT INTO `sys_wx_user` VALUES ('18', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5U5wEK-25-gmVWKQ917AlQI', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-18 11:59:32', null, '2018-06-18 11:59:32');
INSERT INTO `sys_wx_user` VALUES ('19', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5eX8erW_nHe4LqNXQ0ApiIw', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-19 20:11:09', null, '2018-06-19 20:11:09');
INSERT INTO `sys_wx_user` VALUES ('20', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5ZG1EO15ObcXWyHirddJP0k', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-19 20:40:43', null, '2018-06-19 20:40:43');
INSERT INTO `sys_wx_user` VALUES ('21', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5WsXel0r-fTMFDXnEvfQ2Dg', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-20 14:36:00', null, '2018-06-20 14:36:00');
INSERT INTO `sys_wx_user` VALUES ('22', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5dYh3Anj07fJIyqIjHXNt0Y', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-22 13:33:02', null, '2018-06-22 13:33:02');
INSERT INTO `sys_wx_user` VALUES ('23', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5TbuLoJy5xxuKIyCR-lPyX8', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-22 14:32:55', null, '2018-06-22 14:32:55');
INSERT INTO `sys_wx_user` VALUES ('24', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5QGJsoUXkS7U2Rl6Hjoz0qs', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-23 18:01:39', null, '2018-06-23 18:01:39');
INSERT INTO `sys_wx_user` VALUES ('25', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5c2Y2-drM-p3caFeND6skN8', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-25 15:37:09', null, '2018-06-25 15:37:09');
INSERT INTO `sys_wx_user` VALUES ('26', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5ZzG1mMl68fupbAY1evvNsU', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-25 17:21:45', null, '2018-06-25 17:21:45');
INSERT INTO `sys_wx_user` VALUES ('27', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5f5lKxvtxG0EytuMYrBrzRM', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-25 17:26:29', null, '2018-06-25 17:26:29');
INSERT INTO `sys_wx_user` VALUES ('28', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5ReCVc3LFnbHv-ES8qy7Ym4', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-25 17:55:33', null, '2018-06-25 17:55:33');
INSERT INTO `sys_wx_user` VALUES ('29', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5RQ5sUYv5nvLMEaFZDVGWzo', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-06-29 11:11:35', null, '2018-06-29 11:11:35');
INSERT INTO `sys_wx_user` VALUES ('30', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5f7VeLnMMV2CSB2QpFHb30U', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-03 12:35:33', null, '2018-07-03 12:35:33');
INSERT INTO `sys_wx_user` VALUES ('31', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Rz0mq1217FvtNqkVqO89BA', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-09 16:18:25', null, '2018-07-09 16:18:25');
INSERT INTO `sys_wx_user` VALUES ('32', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5VXMQSwb-2WpeXkyey-VZm0', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-09 17:27:42', null, '2018-07-09 17:27:42');
INSERT INTO `sys_wx_user` VALUES ('33', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5UQ9KPv11e0zNICj8WEkAWI', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-11 10:34:38', null, '2018-07-11 10:34:38');
INSERT INTO `sys_wx_user` VALUES ('34', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5eDruDwUiv9JJhbq7l5dn_Q', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-12 01:16:25', null, '2018-07-12 01:16:25');
INSERT INTO `sys_wx_user` VALUES ('35', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5WvrhyBVSUGYS218Adh03w0', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-13 09:49:26', null, '2018-07-13 09:49:26');
INSERT INTO `sys_wx_user` VALUES ('36', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Qo-xTtWZTq0QYK0FOg-LII', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-13 16:42:55', null, '2018-07-13 16:42:55');
INSERT INTO `sys_wx_user` VALUES ('37', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5bwZVOcX969-LjUvJQTr-GA', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-19 14:39:59', null, '2018-07-19 14:39:59');
INSERT INTO `sys_wx_user` VALUES ('38', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5cSN-g8LQR1nRd0b0Vded1w', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-25 14:41:40', null, '2018-07-25 14:41:40');
INSERT INTO `sys_wx_user` VALUES ('39', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5el2rwFiaAYiopu2GLLw5D0', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-26 12:26:09', null, '2018-07-26 12:26:09');
INSERT INTO `sys_wx_user` VALUES ('40', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5fpdiXJgY-DkAlF6nvD53nw', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-07-27 17:06:24', null, '2018-07-27 17:06:24');
INSERT INTO `sys_wx_user` VALUES ('41', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5TY-SWmKDEr5zaYsBrdUupI', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-02 15:36:26', null, '2018-08-02 15:36:26');
INSERT INTO `sys_wx_user` VALUES ('42', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5RBPAeiFINb-sOC4FSaKRVY', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-03 07:04:06', null, '2018-08-03 07:04:06');
INSERT INTO `sys_wx_user` VALUES ('43', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Xlnl4Z6MOJSgLo7HRaoDng', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-03 15:03:47', null, '2018-08-03 15:03:47');
INSERT INTO `sys_wx_user` VALUES ('44', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5UYHDdx6q1SSp26ugxDjRm8', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-03 16:01:41', null, '2018-08-03 16:01:41');
INSERT INTO `sys_wx_user` VALUES ('45', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5dEKVq7rwi3gKRnjjGfOntk', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-04 15:34:46', null, '2018-08-04 15:34:46');
INSERT INTO `sys_wx_user` VALUES ('46', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5d9b7nC4wFvYq_jwt5rtPdA', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-05 20:32:22', null, '2018-08-05 20:32:22');
INSERT INTO `sys_wx_user` VALUES ('47', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5aK969rwqUaj5AU8SogYxJU', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-06 00:52:44', null, '2018-08-06 00:52:44');
INSERT INTO `sys_wx_user` VALUES ('48', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5VVaKtkaKYoAGhgRYYQACF0', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-06 16:42:13', null, '2018-08-06 16:42:13');
INSERT INTO `sys_wx_user` VALUES ('49', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5bIu_nYJDdqEBoBaJbMNeHY', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-07 16:49:00', null, '2018-08-07 16:49:00');
INSERT INTO `sys_wx_user` VALUES ('50', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5U94IRHEXavDxU-USDFm7tg', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-09 17:23:54', null, '2018-08-09 17:23:54');
INSERT INTO `sys_wx_user` VALUES ('51', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5WIgzHcATMaXaQIXJk06Kxo', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-10 00:41:44', null, '2018-08-10 00:41:44');
INSERT INTO `sys_wx_user` VALUES ('52', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5e-99M4sJX9DnRjU7rGkKSU', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-10 09:33:42', null, '2018-08-10 09:33:42');
INSERT INTO `sys_wx_user` VALUES ('53', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5VrPLluDk0mDjTzihRTk9oE', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-10 19:21:20', null, '2018-08-10 19:21:20');
INSERT INTO `sys_wx_user` VALUES ('54', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Ti8eoc-C0yQxlkZGbH45QE', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-13 22:12:58', null, '2018-08-13 22:12:58');
INSERT INTO `sys_wx_user` VALUES ('55', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5SkSfGXa6Yn_5mKB1Fi4OEI', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-14 17:44:56', null, '2018-08-14 17:44:56');
INSERT INTO `sys_wx_user` VALUES ('56', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5WmsRCEmMQVM4Rt_e2yJ618', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-16 11:00:13', null, '2018-08-16 11:00:13');
INSERT INTO `sys_wx_user` VALUES ('57', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Y5LnOMqFpJg3TZ_C5xVaTc', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-16 13:53:34', null, '2018-08-16 13:53:34');
INSERT INTO `sys_wx_user` VALUES ('58', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5dLoGLV0Fta9E-NRBeO38-g', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-17 08:31:09', null, '2018-08-17 08:31:09');
INSERT INTO `sys_wx_user` VALUES ('59', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Xua4-VKl03ODsaDXzdcnak', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-17 10:52:57', null, '2018-08-17 10:52:57');
INSERT INTO `sys_wx_user` VALUES ('60', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5QQNTelb5rtt7aBYSLVTh_M', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-17 17:36:26', null, '2018-08-17 17:36:26');
INSERT INTO `sys_wx_user` VALUES ('61', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5edax6e6OxVevhP56H2YKms', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-17 18:08:44', null, '2018-08-17 18:08:44');
INSERT INTO `sys_wx_user` VALUES ('62', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Vkga6ETZxLv80Zk-fW46Uc', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-17 20:38:30', null, '2018-08-17 20:38:30');
INSERT INTO `sys_wx_user` VALUES ('63', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5cYhExDhPeJ_xpD-GSjy9Ok', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-19 17:42:45', null, '2018-08-19 17:42:45');
INSERT INTO `sys_wx_user` VALUES ('64', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5fJ6r7tFp7IIiCYywrQEDrg', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-21 19:40:29', null, '2018-08-21 19:40:29');
INSERT INTO `sys_wx_user` VALUES ('65', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5YJciinsbeqkYVzQ8r8B-W0', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-22 11:46:45', null, '2018-08-22 11:46:45');
INSERT INTO `sys_wx_user` VALUES ('66', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5XVfTDrqUPMVjWFARkPCJ1Y', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-22 18:41:51', null, '2018-08-22 18:41:51');
INSERT INTO `sys_wx_user` VALUES ('67', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5f2NdRs2hGdNCpD1sk3YAqY', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-23 17:29:19', null, '2018-08-23 17:29:19');
INSERT INTO `sys_wx_user` VALUES ('68', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5RzInHy33wLxpBL01tjHImo', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-23 17:33:40', null, '2018-08-23 17:33:40');
INSERT INTO `sys_wx_user` VALUES ('69', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5cscibTemNYGI1oMo9ojdXk', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-24 10:16:46', null, '2018-08-24 10:16:46');
INSERT INTO `sys_wx_user` VALUES ('70', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5fPdsSRmmrtUEeCipE848pM', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-25 15:37:44', null, '2018-08-25 15:37:44');
INSERT INTO `sys_wx_user` VALUES ('71', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5WoQz_iLuwr5050I1OGu-iQ', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-26 17:39:42', null, '2018-08-26 17:39:42');
INSERT INTO `sys_wx_user` VALUES ('72', null, 'wxbd2f253d4bf3b1f0', null, 'wx7cb45a366a8752d4', 'ovz-Z5Q4E9FT_6lOwG8r4J2DgD_4', '', '1', null, null, null, null, '', '', null, null, '1', '0', '0.00', '0.00', '1', '0', null, null, null, null, null, null, '2018-08-27 11:49:32', null, '2018-08-27 11:49:32');

-- ----------------------------
-- Table structure for sys_wx_user_fault
-- ----------------------------
DROP TABLE IF EXISTS `sys_wx_user_fault`;
CREATE TABLE `sys_wx_user_fault` (
  `id` bigint(20) NOT NULL auto_increment,
  `openid` varchar(28) NOT NULL default '' COMMENT '用户openid',
  `order_id` bigint(20) default NULL COMMENT '订单ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  `machine_code` varchar(20) default NULL COMMENT '机器',
  `chip_code` varchar(20) NOT NULL COMMENT '雨伞内置微芯片code',
  `fault_type` tinyint(1) NOT NULL default '1' COMMENT '故障类型：1|雨伞遗失，2|还伞异常',
  `fault_status` tinyint(1) NOT NULL default '1' COMMENT '故障单状态：1|处理中，2|完结',
  `content` text COMMENT '内容',
  `fault_pic_url` varchar(256) NOT NULL default '' COMMENT '故障图片',
  `remark` varchar(100) default NULL,
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='故障表';

-- ----------------------------
-- Records of sys_wx_user_fault
-- ----------------------------
INSERT INTO `sys_wx_user_fault` VALUES ('1', 'ovz-Z5TGHM53mtWSyzXiyrXS4LcQ', '1321515', '3', '1000100012', '4124', '1', '1', '4654654', 'http://www.gzgxkj.com/image/header_logo.png', '12', '0', '2018-05-23 00:27:27', '0', '2018-05-23 00:27:29');

-- ----------------------------
-- Table structure for tb_token
-- ----------------------------
DROP TABLE IF EXISTS `tb_token`;
CREATE TABLE `tb_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime default NULL COMMENT '过期时间',
  `update_time` datetime default NULL COMMENT '更新时间',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户Token';

-- ----------------------------
-- Records of tb_token
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `user_id` bigint(20) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) default NULL COMMENT '密码',
  `create_time` datetime default NULL COMMENT '创建时间',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'mark', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');
