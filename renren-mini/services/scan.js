/**
 * 扫描相关服务
 */
const util = require('../utils/util.js');
const api = require('../config/api.js');

function scanQrcode(token) {
  wx.scanCode({
    success: (scanRes) => {
      // 正在获取密码通知
      wx.showLoading({
        title: '正在拼命加载...',
        mask: true
      })
      util.request(api.scanQrcode, {
        token: token,
        sceneStr: scanRes.result
      }, 'PUT').then(res => {
        if (res.code == 0 && res.msg == 'success') {
          var resData = res.result;
          if (resData.pageCode == 1000) { // 地图主页面
            if (resData.resultCode == 110 //机器状态不正确
              ||
              resData.resultCode == 109 //非法的二维码
              ||
              resData.resultCode == 108 //您有尚未归还的雨伞，请还伞后再借伞
              ||
              resData.resultCode == 107 //您尚未租借雨伞，请扫描借伞二维码
            ) {
              util.showToast(resData);
            }
          } else if (resData.pageCode == 2000) { // 充值押金页面
            if (resData.resultCode == 104) { // 您未缴纳押金，请先缴纳押金
              util.confirmNavigateTo(resData, '../deposit/index');
            }
          } else if (resData.pageCode == 3000) { // 充值余额页面
            if (resData.resultCode == 105) { // 您的余额不足，请先充值
              util.confirmNavigateTo(resData, '../charge/index');
            }
          } else if (resData.pageCode == 4000) { // 订单详情页面
            if (resData.resultCode == 103) { // 您有正在进行中的订单
              util.navigateTo('../charge/index');
            }
          } else if (resData.pageCode == 5000) { // 扫描机器伞道二维码租/还伞页面
            if (resData.resultCode == 0) { // 请求成功，跳转租伞页面
              // 携带密码和车号跳转到密码页
              util.navigateTo('../rentpage/index?scene_str=' + scanRes.result);
            }
          }
        } else if (res.code == 500) {
          wx.showModal({
            title: '提示',
            content: res.msg,
            showCancel: false
          });
        }
      });
      // 请求密码成功隐藏等待框
      wx.hideLoading();
    }
  });
}

module.exports = {
  scanQrcode
};