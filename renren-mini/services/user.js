/**
 * 用户相关服务
 */
const util = require('../utils/util.js');
const api = require('../config/api.js');
const login = require('../services/login.js');

// 获取用户信息
function getUserInfo() {
  return new Promise(function (resolve, reject) {
    util.getUserInfo().then((auth) => {
      console.info("==>授权状态：" + auth);
      let session = null;
      let authData = auth;
      login.loginByWeixin().then((res) => {
        session = res;
      }).then(() => {
        var data = {
          sessionKey: session.sessionKey,
          signature: authData.signature,
          rawData: authData.rawData,
          encryptedData: authData.encryptedData,
          iv: authData.iv,
          token: session.openid
        }
        util.request(api.user, data, 'PUT').then(resData => { // 再请求一次，与数据库数据同步。获取更详细的用户信息
          // 缓存-用户信息
          var userInfo = {
            avatarUrl: resData.result.avatar,
            nickName: resData.result.nickName,
            balance: resData.result.balance,
            deposit: resData.result.deposit
          }
          wx.setStorageSync({
            key: "userInfo",
            data: userInfo
          })
          resolve(userInfo);
        }).catch((errData) => {
          reject(errData);
        });
      }).catch((err) => {
        reject(err);
      });
    });
  });
}

// 获取用户信息
function refreshUserInfo() {
  return new Promise(function (resolve, reject) {
    util.request(api.userInfo, { token: wx.getStorageSync('token')}, 'GET').then(res => { // 再请求一次，与数据库数据同步。获取更详细的用户信息
      // 缓存-用户信息
      var userInfo = {
        avatarUrl: res.result.avatar,
        nickName: res.result.nickName,
        balance: res.result.balance,
        deposit: res.result.deposit
      }
      wx.setStorageSync({
        key: "userInfo",
        data: userInfo
      })
      resolve(userInfo);
    }).catch((errData) => {
      reject(errData);
    });
  });
}

module.exports = {
  getUserInfo,
  refreshUserInfo
};