/**
 * 支付相关服务
 */
const util = require('../utils/util.js');
const api = require('../config/api.js');
function payOrder(res) {
  return new Promise(function (resolve, reject) {
    const payParam = res;
    wx.requestPayment({
      'timeStamp': payParam.timeStamp,
      'nonceStr': payParam.nonceStr,
      'package': payParam.package,
      'signType': payParam.signType,
      'paySign': payParam.paySign,
      'success': function (res) {
        resolve(res);
      },
      'fail': function (res) {
        reject(res);
      },
      'complete': function (res) {
        reject(res);
      }
    });
  });
}

module.exports = {
  payOrder,
};











