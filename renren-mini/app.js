
// app.js
var login = require('./services/login.js');
App({
  //beasUrl: 'http://localhost:8081/yosan-api/wechat/rent/',
  beasUrl: 'https://yosan.gzgxkj.com/yosan-api/wechat/rent/',

  onLaunch: function () {
    //获取用户的登录信息
    login.checkLogin().then(res => {
      console.log('app login')
      this.globalData.token = wx.getStorageSync('token');
    }).catch(() => {

    });
  },

  globalData: {
    userInfo: {
      nickName: 'Hi,游客',
      userName: '点击去登录',
      avatarUrl: '/images/login_btn.png',
      balance: 0,
      deposit: 0
    },
    token: '',
    longitude: '',
    latitude: ''
  }
})