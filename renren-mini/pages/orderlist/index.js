// pages/orderlist/index.js
const api = require('../../config/api.js');
const util = require('../../utils/util.js');
var app = getApp();
var page = 1;//分页标识，第几次下拉，用户传给后台获取新的下拉数据
Page({
  data: {
    orderList: [],//订单列表数组
    token: wx.getStorageSync('token'),
    loadmorehidden: true
  },

  // 页面加载
  onLoad: function () {
    this.clearCache();//清本页缓存
    this.getArticles(page);//第一次加载数据
  },

  // 下拉刷新
  onPullDownRefresh: function () {
    this.clearCache();
    this.getArticles(page);//第一次加载数据
  },

  // 页面上拉触底事件（上拉加载更多）
  onReachBottom: function () {
    this.setData({
      loadmorehidden: false
    })
    this.getArticles(page);//后台获取新数据并追加渲染
  },

  // 清缓存
  clearCache: function () {
    this.setData({
      orderList: [], //文章列表数组清空
      page: 1//分页标识归零
    });
  },

  /**************** 界面点击 *****************/
  // 文章点击跳转详情页
  onArticle: function () {
    //业务逻辑
  },

  /**************** 网络请求 *****************/
  /**
   * 获取文章列表
   * @param {int} pg  分页标识 默认0
   */
  getArticles: function (pg) {
    //设置默认值
    var _this = this; 
    var postData = {
      token: _this.data.token,
      page: page + ''
    }

    util.request(api.orderList, postData, 'PUT').then(function (res) {
      if (res.result.list.length > 0) {
        var tmpArr = _this.data.orderList;
        // 这一步实现了上拉加载更多
        tmpArr.push.apply(tmpArr, res.result.list);
        _this.setData({
          orderList: tmpArr,
          loadmorehidden: true
        })
      } else {
        _this.setData({
          loadmorehidden: true
        })
      }
      var totalPage = res.result.totalPage;
      if (totalPage > page){
        page++;
      }

      _this.setData({
        page: page
      })
      
      setTimeout(function () {
      }, 500)
      console.info("==>page " + page);
    }, function (err) {
      console.error("==>" + err);
    });
  },
})