// pages/about/index.js
const api = require('../../config/api.js');
const util = require('../../utils/util.js');
var app = getApp();
Page({
  data: {
    token: wx.getStorageSync('token'),
    list: {}
  },
  onLoad: function (options) {
    var _this = this;
    // 设置本页导航标题
    wx.setNavigationBarTitle({
      title: '关于我们'
    });
    _this.getAboutUs();
  },

  // 下拉刷新
  onPullDownRefresh: function () {
    this.clearCache();
    console.info("==> 下拉刷新");
    this.getAboutUs(this.data.orderId);;//第一次加载数据
  },

  // 清缓存
  clearCache: function () {
    this.setData({
      list: {}  //清空
    });
  },

  /**************** 网络请求 *****************/
  /**
   * 获取关于我们
   *
   */
  getAboutUs: function () {
    var _this = this;
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    util.request(api.aboutUs, { token: _this.data.token }, 'GET').then(function (res) {
      _this.setData({
        list: res.result
      });
    }, function (err) {
      console.error("==>" + err);
    });
    wx.hideLoading();
  }
});
