// pages/server/index.js
var app = getApp();
var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
Page({
  data: {
    tabs: ["如何提现", "订单问题", "使用指南"],
    activeIndex: 1,
    sliderOffset: 0,
    sliderLeft: 0,
    isFold: false,
    list: [
      {
        id: '1',
        name: '1.押金如何提现',
        open: false,
        pages: '我是一个很长的文字1'
      },
      {
        id: '2',
        name: '2.提现需要多久到账',
        open: false,
        pages: '我是一个很长的文字2'
      },
      {
        id: '3',
        name: '3.提现后钱退到哪里',
        open: false,
        pages: '我是一个很长的文字3'
      },
      {
        id: '4',
        name: '4.提现超过5个工作日未到帐',
        open: false,
        pages: '我是一个很长的文字4'
      }
    ]
  },
  onLoad: function () {
    // 设置本页导航标题
    wx.setNavigationBarTitle({
      title: '常见问题'
    })
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  flodFn: function (e) {
    console.info(e.target.id);
    this.setData({
      isFold: !this.isFold
    });
  },
  kindToggle: function (e) {
    var id = e.currentTarget.id, list = this.data.list;
    for (var i = 0, len = list.length; i < len; ++i) {
      if (list[i].id == id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list: list
    });
  }
});