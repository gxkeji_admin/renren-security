//index.js
const api = require('../../config/api.js');
var util = require('../../utils/util.js');
var login = require('../../services/login.js');
var scan = require('../../services/scan.js');
var app = getApp();
Page({
  data: {
    scale: 15,
    latitude: 0,
    longitude: 0,
    token: wx.getStorageSync('token'),
    inputShowed: false,
    inputVal: "",
    markers: [],
    _markers: []
  },

  showInput: function() {
    this.setData({
      inputShowed: true
    });
    wx.navigateTo({
      url: '../search/index'
    })
  },
  hideInput: function() {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
    console.info("inputVal = >" + 3);
  },
  clearInput: function() {
    this.setData({
      inputVal: ""
    });
    console.info("inputVal = >" + 4);
  },
  inputTyping: function(e) {
    this.setData({
      inputVal: e.detail.value
    });
    console.info("inputVal = >" + this.data.inputVal);
  },

  // 页面加载
  onLoad: function(options) {
    var _this = this;

    // 1.获取并设置当前位置经纬度
    util.getLocation().then(res => {
      app.globalData.longitude = res.longitude;
      app.globalData.latitude = res.latitude;
    }).catch((err) => {
      console.log(err)
    });

    //2.设置地图控件的位置及大小，通过设备宽高定位
    wx.getSystemInfo({
      success: (res) => {
        _this.setData({
          controls: [{
              id: 1,
              iconPath: '/images/location_btn.png',
              position: {
                left: 10,
                top: res.windowHeight - 135,
                width: 75.5,
                height: 75.5
              },
              clickable: true
            },
            {
              id: 2,
              iconPath: '/images/scan_btn.png',
              position: {
                left: res.windowWidth / 2 - 76,
                top: res.windowHeight - 120,
                width: 163,
                height: 60
              },
              clickable: true
            },
            {
              id: 3,
              iconPath: '/images/center_btn.png',
              position: {
                left: res.windowWidth - 85,
                top: res.windowHeight - 138,
                width: 92,
                height: 75.5
              },
              clickable: true
            },
            {
              id: 4,
              iconPath: '/images/marker_btn.png',
              position: {
                left: res.windowWidth / 2 - 14,
                top: res.windowHeight / 2 - 30,
                width: 26,
                height: 40
              },
              clickable: true
            },
            {
              id: 5,
              iconPath: '/images/server_btn.png',
              position: {
                left: 10,
                top: res.windowHeight - 205,
                width: 75.5,
                height: 75.5
              },
              clickable: true
            }
          ]
        })
      }
    });
  },

  // 获取地图机器图标
  getMachineList: function(token, longitude, latitude) {
    // 4.请求服务器，显示附近的单车，用markers标记
    //登录远程服务器
    util.request(api.indexUrl, {
      token: token,
      longitude: longitude,
      latitude: latitude
    }, 'PUT').then(res => {
      var machineList = res.result.machineList;
      if (machineList.length > 0) {
        for (var i = 0; i < machineList.length; i++) {
          machineList[i].id = i;
          machineList[i].iconPath = '/images/markers.png';
          machineList[i].width = '45';
          machineList[i].height = '45';
        }
        this.setData({
          markers: machineList
        })
      } else {
        reject(res);
      }
    }).catch((err) => {
      console.error(err);
    });
  },

  // 页面显示
  onShow: function() {
    // 1.创建地图上下文，移动当前位置到地图中心
    this.mapCtx = wx.createMapContext("yosanMap");
    this.movetoPosition()
  },
  // 地图控件点击事件
  bindcontroltap: function(e) {
    // 判断点击的是哪个控件 e.controlId代表控件的id，在页面加载时的第3步设置的id
    switch (e.controlId) {
      // 点击定位控件
      case 1:
        this.movetoPosition();
        break;
        // 点击立即用车，判断当前是否正在计费
      case 2:
        if (this.timer === "" || this.timer === undefined) {
          // 没有在计费就扫码
          scan.scanQrcode(this.data.token);
          // 当前已经在计费就回退到计费页
        } else {
          wx.navigateBack({
            delta: 1
          });
        }
        break;
        // 点击头像控件，跳转到个人中心
      case 3:
        wx.navigateTo({
          url: '../center/index'
        });
        break;
        // 点击客服中心，跳转到客服中心
      case 5:
        wx.navigateTo({
          url: '../server/index'
        });
        break;
      default:
        break;
    }
  },
  // 地图视野改变事件
  bindregionchange: function(e) {
    // 拖动地图，获取附件单车位置
    if (e.type == "begin") {
      if (this.data.token == '') {
        login.loginByWeixin().then(res => {
          app.globalData.token = res.openid;
          this.setData({
            token: app.globalData.token
          });
          this.getMachineList(app.globalData.token, app.globalData.longitude, app.globalData.latitude);
        }).catch((err) => {
          console.log(err)
        });
      } else {
        this.getMachineList(this.data.token, app.globalData.longitude, app.globalData.latitude);
      }
      // 停止拖动，显示单车位置
    } else if (e.type == "end") {
      this.setData({
        markers: this.data._markers
      });
    }
  },
  // 地图标记点击事件，连接用户位置和点击的单车位置
  bindmarkertap: function(e) {
    let _markers = this.data.markers;
    let markerId = e.markerId;
    let currMaker = _markers[markerId];
    this.setData({
      polyline: [{
        points: [{
          longitude: this.data.longitude,
          latitude: this.data.latitude
        }, {
          longitude: currMaker.longitude,
          latitude: currMaker.latitude
        }],
        color: "#FF0000DD",
        width: 1,
        dottedLine: true

      }],
      scale: 15
    })
  },
  // 定位函数，移动位置到地图中心
  movetoPosition: function() {
    this.mapCtx.moveToLocation();
  }
})