// pages/rentpage/index.js
var app = getApp();
Page({
  data: {
    token: wx.getStorageSync('token'),
    sceneStr: '',
    info: {
      rentRule: {
        freeType: 0,
        freeValue: 0,
        perTypeValue: 0,
        rentType: 0,
        rentValue: 0
      },
      depositRule: {
        depositPrice: 0
      }
    }
  },
  onLoad: function (options) {
    var _this = this;
    var sceneStr = options.scene_str;
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    if (_this.data.token && sceneStr) {
      wx.request({
        url: app.beasUrl + 'rentPage',
        data: {
          token: _this.data.token,
          sceneStr: sceneStr
        },
        method: 'PUT',
        header: { 'content-type': 'application/json', 'token': this.data.token }, // 设置请求的 header
        success: function (res) {
          wx.hideLoading();
          if (res.data.code == 0 && res.data.msg == 'success') {
            _this.setData({
              sceneStr: sceneStr,
              info: {
                rentRule: {
                  freeType: res.data.result.rentRule.freeType,
                  freeValue: res.data.result.rentRule.freeValue,
                  perTypeValue: res.data.result.rentRule.perTypeValue,
                  rentType: res.data.result.rentRule.rentType,
                  rentValue: res.data.result.rentRule.rentValue
                },
                depositRule: {
                  depositPrice: res.data.result.depositRule.depositPrice
                }
              }
            });
          }
        },
        fail: function (err) {
          console.log(err)
        }
      });
    } else {
      // 没有token, 吓屎我了
    }
  },

  immediatelyRent: function (e) {
    var formId = e.detail.formId;
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    if (this.data.token && this.data.sceneStr) {
      wx.request({
        url: app.beasUrl + "immediatelyRent",
        data: {
          token: this.data.token,
          sceneStr: this.data.sceneStr,
          fromId: formId
        },
        method: 'PUT',
        header: { 'content-type': 'application/json', 'token': this.data.token }, // 设置请求的 header
        success: function (res) {
          var resData = res.data;
          if (resData.code == 0 && resData.msg == 'success') {
            if (resData.result.pageCode == 1000) {
              wx.showModal({
                title: '提示',
                content: resData.result.resultMsg,
                success: function (res) {
                  wx.redirectTo({
                    url: '../index/index'
                  })
                }
              });
            } else if (resData.data.actionCode == 400) {
              var endTime = setInterval(() => {
                percent++;
                if (percent == 100) clearInterval(startTime);
                that.setData({
                  percent: percent
                })
              }, 20);
              if (resData.data.resultCode == 903) {
                wx.redirectTo({
                  url: '../rent_suc/rent_suc'
                })
              } else if (resData.data.resultCode == 904) {
                wx.redirectTo({
                  url: '../return_suc/return_suc?id=' + resData.data.id
                })
              }
            } else if (resData.data.actionCode == 600 || resData.data.actionCode == 700) {
              var endTime = setInterval(() => {
                percent++;
                if (percent == 100) clearInterval(startTime);
                that.setData({
                  percent: percent
                })
              }, 20);
              if (resData.data.resultCode == 901) {
                /*wx.showModal({
                  title: '提示',
                  content: '租伞成功',
                  success: function (res) {
                    wx.navigateTo({
                      url: '../rent_suc/rent_suc'
                    })
                  }
                }); 隐藏租伞成功弹出框 */
                wx.redirectTo({
                  url: '../rent_suc/rent_suc'
                })
              } else if (resData.data.resultCode == 902) {
                /*wx.showModal({
                  title: '提示',
                  content: '还伞成功',
                  success: function (res) {
                    wx.navigateTo({
                      url: '../return_suc/return_suc?id=' + resData.data.id
                    })
                  }
                }); 隐藏还伞成功弹出框 */
                wx.redirectTo({
                  url: '../return_suc/return_suc?id=' + resData.data.id
                })
              }
            } else if (resData.data.actionCode == 800) {
              if (resData.data.resultCode == 801) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 802) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 803) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 804) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 805) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              }
              else if (resData.data.resultCode == 20001) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 20002) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 20003) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 20004) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 20005) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 20006) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 20007) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else if (resData.data.resultCode == 3000) {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              } else {
                wx.showModal({
                  title: '提示',
                  content: resData.data.resultMsg,
                  success: function (res) {
                    wx.redirectTo({
                      url: '../map_index/map_index'
                    })
                  }
                });
              }
            } else {
              wx.showModal({
                title: '提示',
                content: '系统繁忙，请稍后重试',
                showCancel: false
              });
            }
          } else if (res.data.code == 500) { // 系统异常，请联系管理员
            wx.hideLoading();
            wx.showModal({
              title: '提示',
              content: res.data.msg,
              showCancel: false,
              success: function (alertRes) {
                if (alertRes.confirm) {
                  wx.redirectTo({
                    url: '../index/index',
                  })
                }
              }
            });
          }
        },
        fail: function (err) { // 系统异常，请联系管理员
          wx.hideLoading();
          wx.showModal({
            title: '提示',
            content: res.data.msg,
            showCancel: false,
            success: function (alertRes) {
              if (alertRes.confirm) {
                wx.redirectTo({
                  url: '../index/index',
                })
              }
            }
          });
        },
        complete: function (e){
          wx.hideLoading();
        }
      })
    } else {
      // 没有token, 吓屎我了
    }
  },

  // 点击去首页
  moveToHome: function () {
    wx.redirectTo({
      url: '../index/index'
    })
  }
});