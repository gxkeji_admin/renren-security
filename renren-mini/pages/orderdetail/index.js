var app = getApp();
Page({
  data: {
    token: wx.getStorageSync('token'),
    loadmorehidden: true,
    orderId: 0
  },
  onLoad: function (options) {
    console.log(options);
    var _this = this;
    var orderId = options.orderId;
    _this.setData({
      orderId: orderId
    })
    _this.getOrderDetail(_this.data.orderId);
  },

  // 下拉刷新
  onPullDownRefresh: function () {
    console.info("==> 下拉刷新");
    this.getOrderDetail(this.data.orderId);;//第一次加载数据
  },

  /**************** 网络请求 *****************/
  /**
   * 获取订单详情
   * @param {int} orderId 订单id
   */
  getOrderDetail: function (orderId) {
    var _this = this;
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: app.beasUrl + 'user/order/detail',
      data: {
        token: _this.data.token,
        orderId: orderId
      },
      method: 'PUT',
      header: { 'content-type': 'application/json', 'token': _this.data.token }, // 设置请求的 header
      success: function (res) {
        var resData = res.data;
        console.log(resData)
        if (res.data.code == 0 && res.data.msg == 'success') {
          _this.setData({
            orderDetail: resData.result.orderDetail,
            feeDetail: resData.result.feeDetail,
            orderStatus: resData.result.orderStatus,
            orderId: resData.result.orderId,
            chipCode: resData.result.chipCode
          })
        }
        wx: wx.hideLoading();
      },
      fail: function (err) {
        console.log(err)
      }
    })
  },

  /**
   * 获取订单详情
   * @param {int} orderId 订单id
   */
  orderLost: function (e) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '报失将会优先扣减余额和押金！',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '加载中',
            mask: true
          });
          wx.request({
            url: app.beasUrl + '/user/order/lost',
            data: {
              token: _this.data.token,
              orderId: _this.data.orderId
            },
            method: 'PUT',
            header: { 'content-type': 'application/json' },
            success: function (res) {
              var resData = res.data;
              console.log(resData)
              if (resData.code == 200) {
                wx.showModal({
                  title: '提示',
                  content: '报失成功！',
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      wx.navigateTo({
                        url: '../orderlist/index'
                      })
                    }
                  }
                });
              }
              wx: wx.hideLoading();
            },
            fail: function (err) {
              console.log(err)
            }
          })
        }
      },
    })
  },
})