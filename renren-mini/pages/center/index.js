// pages/my/index.js
const api = require('../../config/api.js');
var util = require('../../utils/util.js');
var login = require('../../services/login.js');
var userUtil = require('../../services/user.js');
var app = getApp();
Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    token: wx.getStorageSync('token'),
    userInfo: {}
  },
  // 页面加载
  onLoad: function () {
    var _this = this;
    // 设置本页导航标题
    wx.setNavigationBarTitle({
      title: '个人中心'
    })
    // 页面初始化
  },

  onShow: function () {
    let userInfo = wx.getStorageSync('userInfo');
    let token = this.data.token;
    // 页面显示
    if (userInfo && token) {
      app.globalData.userInfo = userInfo;
      app.globalData.token = token;
    }
    this.setData({
      userInfo: app.globalData.userInfo,
    });
  },

  bindGetUserInfo(e) {
    var _this = this;
    if (e.detail.userInfo) {
      //用户按了允许授权按钮
      userUtil.getUserInfo().then(res => {
        _this.setData({
          userInfo: res
        });
        app.globalData.userInfo = res;
        app.globalData.token = _this.data.token;
      }).catch((err) => {
        console.log(err)
      });
    } else {
      //用户按了拒绝按钮
      wx.showModal({
        title: '警告通知',
        content: '您点击了拒绝授权,将无法正常显示个人信息,点击确定重新获取授权。',
        success: function (res) {
          if (res.confirm) {
            wx.openSetting({
              success: (res) => {
                if (res.authSetting["scope.userInfo"]) {//如果用户重新同意了授权登录
                  userUtil.getUserInfo(e.detail).then(res => {
                    _this.setData({
                      userInfo: res
                    });
                    app.globalData.userInfo = res;
                    app.globalData.token = _this.data.token;
                  }).catch((err) => {
                    console.log(err)
                  });
                }
              }
            })
          }
        }
      });
    }
  },

  // 跳转至钱包
  movetoWallet: function () {
    wx.navigateTo({
      url: '../wallet/index'
    })
  },
})