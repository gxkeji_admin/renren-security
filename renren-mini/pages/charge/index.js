// pages/charge/index.js
//获取应用实例
const api = require('../../config/api.js');
const util = require('../../utils/util.js');
var pay = require('../../services/pay.js');
var user = require('../../services/user.js');
var app = getApp();
Page({
  data: {
    btndisabled: true,
    token: wx.getStorageSync('token')
  },
  // 页面加载
  onLoad: function (options) {
    var _this = this;
    wx.setNavigationBarTitle({
      title: '充值余额'
    })
    util.request(api.rechargeCardList, { token: _this.data.token }, 'PUT').then(function (res) {
      _this.setData({
        list: res.result.list
      });
    }, function (err) {
      console.error("==>" + err);
    });
  },

  select: function (e) {
    this.setData({
      moneyid: e.currentTarget.dataset.moneyid
    })
  },

  recharge: function () {
    var _this = this;
    if (this.data.btndisabled && _this.data.moneyid != null) {
      this.setData({ btndisabled: false })
      var data = {
        token: _this.data.token,
        tradeType: '2', // 1|押金，2|充值，3|退款，4|消费'
        rechargeCardId: _this.data.moneyid
      };
      util.request(api.payRecharge, data, 'PUT').then(function (payParamRes) {
        const payParam = payParamRes.result;
        return new Promise(function (resolve, reject) {
          pay.payOrder(payParam).then(res => {
            user.refreshUserInfo().then(function (res) {
              wx.redirectTo({
                url: '../wallet/index',
                success: function (res) {
                  wx.showToast({
                    title: "充值成功",
                    icon: "success",
                    duration: 2000
                  })
                }
              });
            }).catch((err) => {
              console.log(err)
            });
          }).catch((err) => {
            console.log(err)
          });
        });
      }), function (err) {
        console.error("==>" + err);
      }
    }
  },

  // 存储输入的充值金额
  bindInput: function (res) {
    this.setData({
      inputValue: res.detail.value
    })
  },
  // 充值
  charge: function () {
    // 必须输入大于0的数字
    if (parseInt(this.data.inputValue) <= 0 || isNaN(this.data.inputValue)) {
      wx.showModal({
        title: "警告",
        content: "咱是不是还得给你钱？！！",
        showCancel: false,
        confirmText: "不不不不"
      })
    } else {
      wx.redirectTo({
        url: '../wallet/index',
        success: function (res) {
          wx.showToast({
            title: "充值成功",
            icon: "success",
            duration: 2000
          })
        }
      })
    }
  },

  cancle: function() {
    wx.navigateBack();
  }
})