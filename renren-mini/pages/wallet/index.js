// pages/wallet/index.js
const api = require('../../config/api.js');
const util = require('../../utils/util.js');
var app = getApp();
Page({
  data: {
    token: wx.getStorageSync('token'),
    userInfo: {
      avatarUrl: "/images/logo.png",
      nickName: "未授权",
      balance: 0,
      deposit: 0
    },
    ticket: 0
  },
  // 页面加载
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '我的钱包'
    })
    this.getUserInfoFromServer();
  },
  // 页面加载完成，更新本地存储的overage
  onReady: function () {
    wx.getStorage({
      key: 'userInfo',
      success: (res) => {
        wx.hideLoading();
        this.setData({
          userInfo: {
            avatarUrl: res.data.avatarUrl,
            nickName: res.data.nickName,
            balance: res.data.balance,
            deposit: res.data.deposit
          }
        })
      },
      fail: function (err) {
        this.getUserInfoFromServer();
      }
    });
  },
  // 页面显示完成，获取本地存储的overage
  onShow: function () {
    wx.getStorage({
      key: 'userInfo',
      success: (res) => {
        wx.hideLoading();
        this.setData({
          userInfo: {
            avatarUrl: res.data.avatarUrl,
            nickName: res.data.nickName,
            balance: res.data.balance,
            deposit: res.data.deposit
          }
        })
      },
      fail: function (err) {
        this.getUserInfoFromServer();
      }
    });
  },
  // 余额说明
  overageDesc: function () {
    wx.showModal({
      title: "",
      content: "充值余额0.00元+活动赠送余额0.00元",
      showCancel: false,
      confirmText: "我知道了",
    })
  },
  // 跳转到充值页面
  movetoCharge: function () {
    // 关闭当前页面，跳转到指定页面，返回时将不会回到当前页面
    wx.redirectTo({
      url: '../charge/index'
    })
  },
  // 用车券
  showTicket: function () {
    wx.showModal({
      title: "",
      content: "你没有用车券了",
      showCancel: false,
      confirmText: "好吧",
    })
  },
  // 押金退还
  showDeposit: function () {
    wx.showModal({
      title: "",
      content: "押金会立即退回，退款后，您将不能使用共享晴雨伞确认要进行此退款吗？",
      cancelText: "继续使用",
      cancelColor: "#b9dd08",
      confirmText: "押金退款",
      confirmColor: "#ccc",
      success: (res) => {
        if (res.confirm) {
          util.request(api.refundDeposit, { token: wx.getStorageSync('token')}, 'PUT').then(function (res) {
            var resData = res.result;
            if (resData.code == '200') {
              wx.showModal({
                title: '提示',
                content: '退款成功',
                showCancel: false,
                success: function (res) {
                  if (res.confirm) {
                    that.userInfo();
                  }
                }
              })
            } else {
              wx.showModal({
                title: '提示',
                content: resData.msg,
                showCancel: false
              })
            }
          }, function (err) {
            console.error("==>" + err);
          });
        }
      }
    })
  },

  // 押金退还
  showOrderList: function () {
    wx.navigateTo({
      url: '../orderlist/index'
    })
  },
  
  // 关于我们
  showInvcode: function () {
    wx.navigateTo({
      url: '../about/index'
    })
  },

  // 获取缓存失败，直接请求服务器获取用户信息
  getUserInfoFromServer: function() {
    wx.request({
      url: app.beasUrl + 'user/info',
      method: 'GET',
      data: {
        token: this.data.token
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: (userRes) => {
        if (userRes.data.code == 0 && userRes.data.msg == 'success') {
          this.setData({
            userInfo: {
              avatarUrl: userRes.data.result.avatar,
              nickName: userRes.data.result.nickName,
              balance: userRes.data.result.balance,
              deposit: userRes.data.result.deposit
            },
          });
          wx.setStorageSync('userInfo', this.data.userInfo);
        }
      }
    });
  }
});