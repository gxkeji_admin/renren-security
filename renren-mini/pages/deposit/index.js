var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token: wx.getStorageSync('token'),
    depositRuleId: 0,
    depositPrice: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '充值押金'
    })
    wx.request({
      url: app.beasUrl + 'deposit/rule',
      data: {
        token: this.data.token
      },
      method: 'PUT',
      header: { 'content-type': 'application/json', 'token': this.data.token }, // 设置请求的 header
      success: (res) => {
        if (res.data.code == 0 && res.data.msg == 'success') {
          this.setData({
            depositRuleId: res.data.result.id,
            depositPrice: res.data.result.depositPrice
          })
        } else {
          wx.showModal({
            title: '提示',
            content: res.msg,
            showCancel: false
          })
        }
      },
      fail: function (err) {
        console.log(err)
      }
    })
  },

  // 缴纳押金
  deposit: function () {
    var token = wx.getStorageSync('token');
    wx.request({
      url: app.beasUrl + 'pay/deposit',
      method: 'PUT',
      data: {
        token: token,
        tradeType: '1', // 1|押金，2|充值，3|退款，4|消费
        depositRuleId: this.data.depositRuleId
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (returnRes) {
        if (returnRes.data.code == 0 && returnRes.data.msg == 'success') {
        console.info("==>" + returnRes.data.result);
          wx.requestPayment({
            'timeStamp': returnRes.data.result.timeStamp,
            'nonceStr': returnRes.data.result.nonceStr,
            'package': returnRes.data.result.package,
            'signType': 'MD5',
            'paySign': returnRes.data.result.paySign,
            'success': function (res) {
              wx.request({
                url: app.beasUrl + 'user/info',
                data: {
                  token: token
                },
                method: 'GET',
                header: { 'content-type': 'application/json' },
                success: function (res) {
                  var resData = res.data;
                  if (resData.code == 0 && resData.msg == 'success') {
                    wx.setStorageSync('userInfo', resData.data);
                    wx.redirectTo({
                      url: '../wallet/index'
                    })
                  } else {
                    wx.showModal({
                      title: '提示',
                      content: resData.msg,
                      showCancel: false
                    })
                  }
                },
                fail: function (err) {
                 // console.log(err)
                }
              })
            },
            'fail': function (res) {
             // console.log(returnRes.data.data.id);
            }
          })
        }
        //console.info(returnRes);
      }
    })
  },

  cancle: function () {
    wx.navigateBack();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})