function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/**
 * 封装微信的的request
 */
function request(url, data = {}, method) {
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: { 'content-type': 'application/json', 'token': wx.getStorageSync('token') }, // 设置请求的 header
      success: function (res) {
        if (res.data.code == 0 && res.data.msg == 'success') {
          resolve(res.data);
        } else {
          reject(res.data);
        }
      },
      fail: function (err) {
        reject(err)
        console.log("failed")
      }
    })
  });
}

/**
 * 检查微信会话是否过期
 */
function checkSession() {
  return new Promise(function (resolve, reject) {
    wx.checkSession({
      success: function () {
        resolve(true); // 未过期
      },
      fail: function () {
        reject(false); // 已过期
      }
    })
  });
}

/**
 * 获取用户信息
 */
function getUserInfo() {
  return new Promise(function (resolve, reject) {
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {// 查看是否授权
          wx.getUserInfo({
            withCredentials: true,
            success: res => {
              resolve(res);
            },
            fail: err => {
              reject(err);
            }
          });
        } else {
          reject(res); // 未授权
        }
      },
      fail: function () {
        reject(false); // 未授权
      }
    });
  });
}

/**
 * 调用微信登录
 */
function login() {
  return new Promise(function (resolve, reject) {
    wx.login({
      success: function (res) {
        if (res.code) {
          //登录远程服务器
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    });
  });
}

function showErrorToast(msg) {
  wx.showToast({
    title: msg,
    image: '/images/icon_error.png'
  })
}

function getLocation() {
  return new Promise(function (resolve, reject) {
    wx.getLocation({
      type: "gcj02",
      success: function (res) {
        resolve(res);
      },
      fail: function (err) {
        reject(err); // 已过期
      }
    });
  });
}

function redirectTo(url) {
  wx.redirectTo({
    url: url
  });
}

function navigateTo(url) {
  wx.navigateTo({
    url: url
  });
}

function confirmRedirectTo(resData, url) {
  wx.showModal({
    title: '提示',
    content: resData.resultMsg,
    showCancel: true,
    success: function (res) {
      if (res.confirm) {
        this.redirectTo(url);
      }
    }
  });
}

function confirmNavigateTo(resData, url) {
  wx.showModal({
    title: '提示',
    content: resData.resultMsg,
    showCancel: true,
    success: function (depositRes) {
      if (depositRes.confirm) {
        this.navigateTo(url);
      }
    }
  });
}

function showToast(resData) {
  wx.showModal({
    title: '提示',
    content: resData.resultMsg,
    showCancel: false
  });
}

module.exports = {
  formatTime,
  request,
  showErrorToast,
  login,
  getLocation,
  redirectTo,
  navigateTo,
  confirmRedirectTo,
  confirmNavigateTo,
  showToast,
  getUserInfo
}
