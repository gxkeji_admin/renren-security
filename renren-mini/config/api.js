//var baseUrl = 'http://localhost:8081/yosan-api/wechat/rent/';
var baseUrl='https://yosan.gzgxkj.com/yosan-api/wechat/rent/';

module.exports = {
  indexUrl: baseUrl + 'index', //首页数据接口
  loginUrl: baseUrl + 'login',  //微信登录接口
  user: baseUrl + 'user',  //用户信息接口

  userInfo: baseUrl + 'user/info', //我的信息接口

  location: baseUrl + 'location',  //上报地理位置接口

  scanQrcode: baseUrl + 'scan',  //扫一扫二维码接口

  rentPage: baseUrl + 'rentPage',  //租伞页面接口

  ImmediatelyRent: baseUrl + 'immediatelyRent',  //立即租伞接口

  immediatelyReturn: baseUrl + 'immediatelyReturn',  //立即还伞接口

  rentSuc: baseUrl + 'rentSuc',  //租伞成功界面接口
  returnSuc: baseUrl + 'returnSuc',  //还伞成功界面接口

  orderDetail: baseUrl + 'user/order/detail',  //订单详情接口
  orderLost: baseUrl + 'user/order/lost',  //订单报失接口

  orderList: baseUrl + 'user/order/list', //订单列表接口

  rechargeCardList: baseUrl + 'recharge/card/list', // 充值卡列表接口

  depositRule: baseUrl + 'deposit/rule', // 押金规则接口
  tradeDetailList: baseUrl + 'trade/detail/list', // 交易详情接口

  nearby: baseUrl + 'nearby', // 附近网点接口

  faultList: baseUrl + 'fault/list', // 故障类型列表接口

  upload: baseUrl + 'upload', // 上传文件接口

  aboutUs: baseUrl + 'about/us', // 关于我们接口

  payDeposit: baseUrl + 'pay/deposit', //微信支付押金接口

  payRecharge: baseUrl + 'pay/recharge', //微信支付充值接口

  payNotify: baseUrl + 'pay/notify', //微信支付回调通知接口

  refundDeposit: baseUrl + 'refund/deposit', //微信退款接口

};