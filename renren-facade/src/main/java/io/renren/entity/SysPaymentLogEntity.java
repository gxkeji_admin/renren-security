package io.renren.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 支付关系表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_payment_log")
public class SysPaymentLogEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单id
	 */
	private Long orderId;
	/**
	 * 付款人openid
	 */
	@NotBlank(message="付款人openid不能为空")
	private String openid;
	/**
	 * 部门ID
	 */
	@NotNull(message="部门ID不能为空")
	private Long deptId;
	/**
	 * 商户订单号(提交至微信的本地订单号，非系统真正订单号)
	 */
	private String tradeNo;
	/**
	 * 微信支付订单号
	 */
	private String transactionId;
	/**
	 * 交易类型：1|押金，2|充值，3|退款，4|消费
	 */
	private Integer tradeType;
	/**
	 * 支付状态：1|待支付，2|已支付
	 */
	private Integer payStatus;
	/**
	 * 支付时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date payTime;
	/**
	 * 付款金额
	 */
	private BigDecimal payPrice;

	/**
	 * 设置：订单id
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单id
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：付款人openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：付款人openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：商户订单号(提交至微信的本地订单号，非系统真正订单号)
	 */
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	/**
	 * 获取：商户订单号(提交至微信的本地订单号，非系统真正订单号)
	 */
	public String getTradeNo() {
		return tradeNo;
	}
	/**
	 * 设置：微信支付订单号
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * 获取：微信支付订单号
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * 设置：交易类型：1|押金，2|充值，3|退款，4|消费
	 */
	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型：1|押金，2|充值，3|退款，4|消费
	 */
	public Integer getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：支付状态：1|待支付，2|已支付
	 */
	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}
	/**
	 * 获取：支付状态：1|待支付，2|已支付
	 */
	public Integer getPayStatus() {
		return payStatus;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	/**
	 * 设置：付款金额
	 */
	public void setPayPrice(BigDecimal payPrice) {
		this.payPrice = payPrice;
	}
	/**
	 * 获取：付款金额
	 */
	public BigDecimal getPayPrice() {
		return payPrice;
	}
}
