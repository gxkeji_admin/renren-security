package io.renren.entity;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 操作iot获取数据日志记录
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-15 18:45:53
 */
@TableName("sys_iot_callback_log")
public class SysIotCallbackLogEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 产品key
	 */
	private String productKey;
	/**
	 * 设备名称
	 */
	private String deviceName;
	/**
	 * 付款人openid
	 */
	@NotBlank(message="付款人openid不能为空")
	private String openid;
	/**
	 * 部门ID
	 */
	@NotNull(message="部门ID不能为空")
	private Long deptId;
	/**
	 * 订单标识
	 */
	private String orderId;
	/**
	 * 消息类型：RENT|借，RETURN|还，ADMIN|管理员
	 */
	private String messageType;
	/**
	 * 请求时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date reqTime;
	/**
	 * 请求到获取结果花费时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date respTime;
	/**
	 * 请求参数
	 */
	private String reqStr;
	/**
	 * 响应结果
	 */
	private String respResult;

	/**
	 * 设置：产品key
	 */
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}
	/**
	 * 获取：产品key
	 */
	public String getProductKey() {
		return productKey;
	}
	/**
	 * 设置：设备名称
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	/**
	 * 获取：设备名称
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * 设置：付款人openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：付款人openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：订单标识
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单标识
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * 设置：消息类型：RENT|借，RETURN|还，ADMIN|管理员
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	/**
	 * 获取：消息类型：RENT|借，RETURN|还，ADMIN|管理员
	 */
	public String getMessageType() {
		return messageType;
	}
	/**
	 * 设置：请求时间
	 */
	public void setReqTime(Date reqTime) {
		this.reqTime = reqTime;
	}
	/**
	 * 获取：请求时间
	 */
	public Date getReqTime() {
		return reqTime;
	}
	/**
	 * 设置：请求到获取结果花费时间
	 */
	public void setRespTime(Date respTime) {
		this.respTime = respTime;
	}
	/**
	 * 获取：请求到获取结果花费时间
	 */
	public Date getRespTime() {
		return respTime;
	}
	/**
	 * 设置：请求参数
	 */
	public void setReqStr(String reqStr) {
		this.reqStr = reqStr;
	}
	/**
	 * 获取：请求参数
	 */
	public String getReqStr() {
		return reqStr;
	}
	/**
	 * 设置：响应结果
	 */
	public void setRespResult(String respResult) {
		this.respResult = respResult;
	}
	/**
	 * 获取：响应结果
	 */
	public String getRespResult() {
		return respResult;
	}
}
