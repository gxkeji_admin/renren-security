package io.renren.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 微信用户
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_wx_user")
public class SysWxUserEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	private Long deptId;
	/**
	 * 公众号appid
	 */
	private String appid;
	/**
	 * 用户在公众号的唯一标示
	 */
	private String openid;
	/**
	 * 小程序appid
	 */
	private String minaAppid;
	/**
	 * 小程序openid
	 */
	private String minaOpenid;
	/**
	 * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
	 */
	private String unionid;
	/**
	 * 关注：1|为关注，0|未关注
	 */
	private Integer subscribe;
	/**
	 * 昵称
	 */
	private String nickName;
	/**
	 * 姓别：1|男，2|女，3|未知
	 */
	private Integer sex;
	/**
	 * 语言
	 */
	private String language;
	/**
	 * 所在城市
	 */
	private String city;
	/**
	 * 所在省份
	 */
	private String province;
	/**
	 * 所在国家
	 */
	private String country;
	/**
	 * 头像
	 */
	private String avatar;
	/**
	 * 关注时间
	 */
	private Integer subscribeTime;
	/**
	 * 会员等级：1|普通会员，2|白银会员，3|黄金会员，4|钻石会员
	 */
	private Integer level;
	/**
	 * 积分
	 */
	private Integer bonus;
	/**
	 * 余额
	 */
	private BigDecimal balance;
	/**
	 * 押金
	 */
	private BigDecimal deposit;
	/**
	 * 是否管理员：1|非管理员，2|管理员
	 */
	private Integer isManager;
	/**
	 * 租伞次数
	 */
	private Integer rentTimes;
	/**
	 * 经度
	 */
	private BigDecimal longitude;
	/**
	 * 纬度
	 */
	private BigDecimal latitude;
	/**
	 * 最近一次登录时间
	 */
	private Date lastLoginTime;
	/**
	 * 最近一次登录ip
	 */
	private String lastLoginIp;

	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：公众号appid
	 */
	public void setAppid(String appid) {
		this.appid = appid;
	}
	/**
	 * 获取：公众号appid
	 */
	public String getAppid() {
		return appid;
	}
	/**
	 * 设置：用户在公众号的唯一标示
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：用户在公众号的唯一标示
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：小程序appid
	 */
	public void setMinaAppid(String minaAppid) {
		this.minaAppid = minaAppid;
	}
	/**
	 * 获取：小程序appid
	 */
	public String getMinaAppid() {
		return minaAppid;
	}
	/**
	 * 设置：小程序openid
	 */
	public void setMinaOpenid(String minaOpenid) {
		this.minaOpenid = minaOpenid;
	}
	/**
	 * 获取：小程序openid
	 */
	public String getMinaOpenid() {
		return minaOpenid;
	}
	/**
	 * 设置：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
	 */
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	/**
	 * 获取：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
	 */
	public String getUnionid() {
		return unionid;
	}
	/**
	 * 设置：关注：1|为关注，0|未关注
	 */
	public void setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
	}
	/**
	 * 获取：关注：1|为关注，0|未关注
	 */
	public Integer getSubscribe() {
		return subscribe;
	}
	/**
	 * 设置：昵称
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	/**
	 * 获取：昵称
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * 设置：姓别：1|男，2|女，3|未知
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * 获取：姓别：1|男，2|女，3|未知
	 */
	public Integer getSex() {
		return sex;
	}
	/**
	 * 设置：语言
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * 获取：语言
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * 设置：所在城市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：所在城市
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：所在省份
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * 获取：所在省份
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * 设置：所在国家
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * 获取：所在国家
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * 设置：头像
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	/**
	 * 获取：头像
	 */
	public String getAvatar() {
		return avatar;
	}
	/**
	 * 设置：关注时间
	 */
	public void setSubscribeTime(Integer subscribeTime) {
		this.subscribeTime = subscribeTime;
	}
	/**
	 * 获取：关注时间
	 */
	public Integer getSubscribeTime() {
		return subscribeTime;
	}
	/**
	 * 设置：会员等级：1|普通会员，2|白银会员，3|黄金会员，4|钻石会员
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
	/**
	 * 获取：会员等级：1|普通会员，2|白银会员，3|黄金会员，4|钻石会员
	 */
	public Integer getLevel() {
		return level;
	}
	/**
	 * 设置：积分
	 */
	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}
	/**
	 * 获取：积分
	 */
	public Integer getBonus() {
		return bonus;
	}
	/**
	 * 设置：余额
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * 获取：余额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：押金
	 */
	public void setDeposit(BigDecimal deposit) {
		this.deposit = deposit;
	}
	/**
	 * 获取：押金
	 */
	public BigDecimal getDeposit() {
		return deposit;
	}
	/**
	 * 设置：是否管理员：1|非管理员，2|管理员
	 */
	public void setIsManager(Integer isManager) {
		this.isManager = isManager;
	}
	/**
	 * 获取：是否管理员：1|非管理员，2|管理员
	 */
	public Integer getIsManager() {
		return isManager;
	}
	/**
	 * 设置：租伞次数
	 */
	public void setRentTimes(Integer rentTimes) {
		this.rentTimes = rentTimes;
	}
	/**
	 * 获取：租伞次数
	 */
	public Integer getRentTimes() {
		return rentTimes;
	}
	/**
	 * 设置：经度
	 */
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	/**
	 * 获取：经度
	 */
	public BigDecimal getLongitude() {
		return longitude;
	}
	/**
	 * 设置：纬度
	 */
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	/**
	 * 获取：纬度
	 */
	public BigDecimal getLatitude() {
		return latitude;
	}
	/**
	 * 设置：最近一次登录时间
	 */
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	/**
	 * 获取：最近一次登录时间
	 */
	public Date getLastLoginTime() {
		return lastLoginTime;
	}
	/**
	 * 设置：最近一次登录ip
	 */
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}
	/**
	 * 获取：最近一次登录ip
	 */
	public String getLastLoginIp() {
		return lastLoginIp;
	}
}
