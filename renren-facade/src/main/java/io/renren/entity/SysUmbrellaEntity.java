package io.renren.entity;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 雨伞
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_umbrella")
public class SysUmbrellaEntity extends SysBaseEntity {
	private static final long	serialVersionUID	= 1L;

	/**
	 * 部门id
	 */
	private Long				deptId;
	/**
	 * 机器id
	 */
	@NotNull(message="所属机器不能为空")
	@Min(value = 1, message = "只能输入大于1的数字")
	private Long				machineId;
	/**
	 * 雨伞内置微芯片code
	 */
	@NotBlank(message="芯片不能为空")
	private String				chipCode;
	/**
	 * 名称
	 */
	@NotBlank(message="名称不能为空")
	private String				umbrellaName;
	/**
	 * 雨伞状态：1|空闲，2|锁定，3|租出, 4|遗失
	 */
	@Min(value = 1, message = "只能输入大于1的数字")
	@NotNull(message="请选择状态")
	private Integer				umbrellaStatus;
	/**
	 * 机器-伞道：1|表示第一条通道，2|表示第二条通道
	 */
	@NotNull(message="所在伞道不能为空")
	@Min(value = 1, message = "只能输入大于1的数字")
	private Integer				channcel;
	/**
	 * 本地雨伞二维码路径
	 */
	private String				umbrellaQrcodePath;
	/**
	 * 雨伞二维码内容
	 */
	private String				umbrellaSceneUrl;
	/**
	 * 雨伞短二维码内容
	 */
	private String				umbrellaShortUrl;
	/**
	 * 最近一次租出时间
	 */
	private Date				lastRentTime;
	/**
	 * 租用次数
	 */
	private Integer				rentTimes;
	/**
	 * 描述
	 */
	private String				desc;
	/**
	 * 机器名称
	 */
	@TableField(exist = false)
	private String				machineName;

	/**
	 * 设置：部门id
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	/**
	 * 获取：部门id
	 */
	public Long getDeptId() {
		return deptId;
	}

	/**
	 * 设置：机器id
	 */
	public void setMachineId(Long machineId) {
		this.machineId = machineId;
	}

	/**
	 * 获取：机器id
	 */
	public Long getMachineId() {
		return machineId;
	}

	/**
	 * 设置：雨伞内置微芯片code
	 */
	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}

	/**
	 * 获取：雨伞内置微芯片code
	 */
	public String getChipCode() {
		return chipCode;
	}

	/**
	 * 设置：名称
	 */
	public void setUmbrellaName(String umbrellaName) {
		this.umbrellaName = umbrellaName;
	}

	/**
	 * 获取：名称
	 */
	public String getUmbrellaName() {
		return umbrellaName;
	}

	/**
	 * 设置：雨伞状态：1|空闲，2|锁定，3|租出, 4|遗失
	 */
	public void setUmbrellaStatus(Integer umbrellaStatus) {
		this.umbrellaStatus = umbrellaStatus;
	}

	/**
	 * 获取：雨伞状态：1|空闲，2|锁定，3|租出, 4|遗失
	 */
	public Integer getUmbrellaStatus() {
		return umbrellaStatus;
	}

	/**
	 * 设置：机器-伞道：1|表示第一条通道，2|表示第二条通道
	 */
	public void setChanncel(Integer channcel) {
		this.channcel = channcel;
	}

	/**
	 * 获取：机器-伞道：1|表示第一条通道，2|表示第二条通道
	 */
	public Integer getChanncel() {
		return channcel;
	}

	/**
	 * 设置：本地雨伞二维码路径
	 */
	public void setUmbrellaQrcodePath(String umbrellaQrcodePath) {
		this.umbrellaQrcodePath = umbrellaQrcodePath;
	}

	/**
	 * 获取：本地雨伞二维码路径
	 */
	public String getUmbrellaQrcodePath() {
		return umbrellaQrcodePath;
	}

	/**
	 * 设置：雨伞二维码内容
	 */
	public void setUmbrellaSceneUrl(String umbrellaSceneUrl) {
		this.umbrellaSceneUrl = umbrellaSceneUrl;
	}

	/**
	 * 获取：雨伞二维码内容
	 */
	public String getUmbrellaSceneUrl() {
		return umbrellaSceneUrl;
	}

	/**
	 * 设置：雨伞短二维码内容
	 */
	public void setUmbrellaShortUrl(String umbrellaShortUrl) {
		this.umbrellaShortUrl = umbrellaShortUrl;
	}

	/**
	 * 获取：雨伞短二维码内容
	 */
	public String getUmbrellaShortUrl() {
		return umbrellaShortUrl;
	}

	/**
	 * 设置：最近一次租出时间
	 */
	public void setLastRentTime(Date lastRentTime) {
		this.lastRentTime = lastRentTime;
	}

	/**
	 * 获取：最近一次租出时间
	 */
	public Date getLastRentTime() {
		return lastRentTime;
	}

	/**
	 * 设置：租用次数
	 */
	public void setRentTimes(Integer rentTimes) {
		this.rentTimes = rentTimes;
	}

	/**
	 * 获取：租用次数
	 */
	public Integer getRentTimes() {
		return rentTimes;
	}

	/**
	 * 设置：描述
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * 获取：描述
	 */
	public String getDesc() {
		return desc;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
}
