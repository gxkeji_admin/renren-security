package io.renren.entity;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 机器
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_machine")
public class SysMachineEntity extends SysBaseEntity {
	private static final long	serialVersionUID	= 1L;

	/**
	 * 部门ID
	 */
	@NotNull(message="所属部门不能为空")
	private Long				deptId;
	/**
	 * 编码
	 */
	private String				machineCode;
	/**
	 * 名称
	 */
	@NotBlank(message="名称不能为空")
	private String				machineName;
	/**
	 * 阿里云产品KEY
	 */
	@NotBlank(message="阿里云产品KEY不能为空")
	private String				productKey;
	/**
	 * 阿里去产品NAME
	 */
	@NotBlank(message="阿里去产品NAME不能为空")
	private String				deviceName;
	/**
	 * 机器状态：0|离线，1|在线
	 */
	@NotNull(message="请选择状态")
	private Integer				machineStatus;
	/**
	 * 投放地址
	 */
	@NotBlank(message="投放地址不能为空")
	private String				location;
	/**
	 * 本地机器二维码路径
	 */
	private String				machineQrcodePath;
	/**
	 * 机器二维码内容
	 */
	private String				machineSceneUrl;
	/**
	 * 机器短二维码内容
	 */
	private String				machineShortUrl;
	/**
	 * 芯片伞道数量
	 */
	@NotNull(message="芯片伞道数量不能为空")
	@Min(value = 1, message = "只能输入大于1的数字")
	private Integer				chipChanncelNum;
	/**
	 * 额定装伞数
	 */
	private Integer				fixedQuantity;
	/**
	 * 剩余伞数
	 */
	private Integer				leftQuantity;
	/**
	 * 电量%
	 */
	private BigDecimal			battery;
	/**
	 * 经度
	 */
	@NotNull(message="经度不能为空")
	private BigDecimal			longitude;
	/**
	 * 纬度
	 */
	@NotNull(message="纬度不能为空")
	private BigDecimal			latitude;

	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}

	/**
	 * 设置：编码
	 */
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}

	/**
	 * 获取：编码
	 */
	public String getMachineCode() {
		return machineCode;
	}

	/**
	 * 设置：名称
	 */
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	/**
	 * 获取：名称
	 */
	public String getMachineName() {
		return machineName;
	}

	/**
	 * 设置：阿里云产品KEY
	 */
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	/**
	 * 获取：阿里云产品KEY
	 */
	public String getProductKey() {
		return productKey;
	}

	/**
	 * 设置：阿里去产品NAME
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * 获取：阿里去产品NAME
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * 设置：机器状态：0|离线，1|在线
	 */
	public void setMachineStatus(Integer machineStatus) {
		this.machineStatus = machineStatus;
	}

	/**
	 * 获取：机器状态：0|离线，1|在线
	 */
	public Integer getMachineStatus() {
		return machineStatus;
	}

	/**
	 * 设置：投放地址
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 获取：投放地址
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * 设置：本地机器二维码路径
	 */
	public void setMachineQrcodePath(String machineQrcodePath) {
		this.machineQrcodePath = machineQrcodePath;
	}

	/**
	 * 获取：本地机器二维码路径
	 */
	public String getMachineQrcodePath() {
		return machineQrcodePath;
	}

	/**
	 * 设置：机器二维码内容
	 */
	public void setMachineSceneUrl(String machineSceneUrl) {
		this.machineSceneUrl = machineSceneUrl;
	}

	/**
	 * 获取：机器二维码内容
	 */
	public String getMachineSceneUrl() {
		return machineSceneUrl;
	}

	/**
	 * 设置：机器短二维码内容
	 */
	public void setMachineShortUrl(String machineShortUrl) {
		this.machineShortUrl = machineShortUrl;
	}

	/**
	 * 获取：机器短二维码内容
	 */
	public String getMachineShortUrl() {
		return machineShortUrl;
	}

	/**
	 * 设置：芯片伞道数量
	 */
	public void setChipChanncelNum(Integer chipChanncelNum) {
		this.chipChanncelNum = chipChanncelNum;
	}

	/**
	 * 获取：芯片伞道数量
	 */
	public Integer getChipChanncelNum() {
		return chipChanncelNum;
	}

	/**
	 * 设置：额定装伞数
	 */
	public void setFixedQuantity(Integer fixedQuantity) {
		this.fixedQuantity = fixedQuantity;
	}

	/**
	 * 获取：额定装伞数
	 */
	public Integer getFixedQuantity() {
		return fixedQuantity;
	}

	/**
	 * 设置：剩余伞数
	 */
	public void setLeftQuantity(Integer leftQuantity) {
		this.leftQuantity = leftQuantity;
	}

	/**
	 * 获取：剩余伞数
	 */
	public Integer getLeftQuantity() {
		return leftQuantity;
	}

	/**
	 * 设置：电量%
	 */
	public void setBattery(BigDecimal battery) {
		this.battery = battery;
	}

	/**
	 * 获取：电量%
	 */
	public BigDecimal getBattery() {
		return battery;
	}

	/**
	 * 设置：经度
	 */
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	/**
	 * 获取：经度
	 */
	public BigDecimal getLongitude() {
		return longitude;
	}

	/**
	 * 设置：纬度
	 */
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	/**
	 * 获取：纬度
	 */
	public BigDecimal getLatitude() {
		return latitude;
	}
}
