package io.renren.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 充值卡
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_recharge_card")
public class SysRechargeCardEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	private Long deptId;
	/**
	 * 面额（充值时使用这个字段）
	 */
	private BigDecimal totalPrice;
	/**
	 * 实际售价
	 */
	private BigDecimal marketPrice;
	/**
	 * 折扣金额
	 */
	private BigDecimal discountPrice;
	/**
	 * 上架状态:1|已上架，2|未上架
	 */
	private Integer shelfStatus;
	/**
	 * 描述
	 */
	private String desc;

	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：面额（充值时使用这个字段）
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：面额（充值时使用这个字段）
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：实际售价
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * 获取：实际售价
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	/**
	 * 设置：折扣金额
	 */
	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：折扣金额
	 */
	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：上架状态:1|已上架，2|未上架
	 */
	public void setShelfStatus(Integer shelfStatus) {
		this.shelfStatus = shelfStatus;
	}
	/**
	 * 获取：上架状态:1|已上架，2|未上架
	 */
	public Integer getShelfStatus() {
		return shelfStatus;
	}
	/**
	 * 设置：描述
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * 获取：描述
	 */
	public String getDesc() {
		return desc;
	}
}
