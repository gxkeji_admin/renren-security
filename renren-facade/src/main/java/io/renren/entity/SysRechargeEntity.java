package io.renren.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 充值
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_recharge")
public class SysRechargeEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 付款人openid
	 */
	private String openid;
	/**
	 * 部门ID
	 */
	private Long deptId;
	/**
	 * 商户订单号(提交至微信的本地订单号，非系统真正订单号)
	 */
	private String tradeNo;
	/**
	 * 微信支付订单号
	 */
	private String transactionId;
	/**
	 * 充值前账号余额
	 */
	private BigDecimal frontBalance;
	/**
	 * 充值金额
	 */
	private BigDecimal totalPrice;
	/**
	 * 实付金额
	 */
	private BigDecimal actualPrice;
	/**
	 * 折扣金额
	 */
	private BigDecimal discountPrice;
	/**
	 * 支付时间
	 */
	private Date payTime;
	/**
	 * 支付场景
	 */
	private String payScene;
	/**
	 * 充值状态:1|待支付，2|已支付，3|取消支付，4|其他
	 */
	private Integer rechargeStatus;

	/**
	 * 设置：付款人openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：付款人openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：商户订单号(提交至微信的本地订单号，非系统真正订单号)
	 */
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	/**
	 * 获取：商户订单号(提交至微信的本地订单号，非系统真正订单号)
	 */
	public String getTradeNo() {
		return tradeNo;
	}
	/**
	 * 设置：微信支付订单号
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * 获取：微信支付订单号
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * 设置：充值前账号余额
	 */
	public void setFrontBalance(BigDecimal frontBalance) {
		this.frontBalance = frontBalance;
	}
	/**
	 * 获取：充值前账号余额
	 */
	public BigDecimal getFrontBalance() {
		return frontBalance;
	}
	/**
	 * 设置：充值金额
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：充值金额
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：实付金额
	 */
	public void setActualPrice(BigDecimal actualPrice) {
		this.actualPrice = actualPrice;
	}
	/**
	 * 获取：实付金额
	 */
	public BigDecimal getActualPrice() {
		return actualPrice;
	}
	/**
	 * 设置：折扣金额
	 */
	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：折扣金额
	 */
	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	/**
	 * 设置：支付场景
	 */
	public void setPayScene(String payScene) {
		this.payScene = payScene;
	}
	/**
	 * 获取：支付场景
	 */
	public String getPayScene() {
		return payScene;
	}
	/**
	 * 设置：充值状态:1|待支付，2|已支付，3|取消支付，4|其他
	 */
	public void setRechargeStatus(Integer rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}
	/**
	 * 获取：充值状态:1|待支付，2|已支付，3|取消支付，4|其他
	 */
	public Integer getRechargeStatus() {
		return rechargeStatus;
	}
}
