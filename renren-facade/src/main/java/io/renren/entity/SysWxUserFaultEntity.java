package io.renren.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 故障表
 * 
 * @author oslive
 * @email apple_live@qq.com
 * @date 2018-05-22 23:55:47
 */
@TableName("sys_wx_user_fault")
public class SysWxUserFaultEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户openid
	 */
	private String openid;
	/**
	 * 订单ID
	 */
	private Long orderId;
	/**
	 * 部门ID
	 */
	private Long deptId;
	/**
	 * 机器
	 */
	private String machineCode;
	/**
	 * 雨伞内置微芯片code
	 */
	private String chipCode;
	/**
	 * 故障类型：1|雨伞遗失，2|还伞异常
	 */
	private Integer faultType;
	/**
	 * 故障单状态：1|处理中，2|完结
	 */
	private Integer faultStatus;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 故障图片
	 */
	private String faultPicUrl;
	/**
	 * 
	 */
	private String remark;
	/**
	 * 
	 */
	private Long createBy;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Long updateBy;
	/**
	 * 
	 */
	private Date updateTime;

	/**
	 * 设置：用户openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：用户openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：订单ID
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单ID
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：机器
	 */
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}
	/**
	 * 获取：机器
	 */
	public String getMachineCode() {
		return machineCode;
	}
	/**
	 * 设置：雨伞内置微芯片code
	 */
	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}
	/**
	 * 获取：雨伞内置微芯片code
	 */
	public String getChipCode() {
		return chipCode;
	}
	/**
	 * 设置：故障类型：1|雨伞遗失，2|还伞异常
	 */
	public void setFaultType(Integer faultType) {
		this.faultType = faultType;
	}
	/**
	 * 获取：故障类型：1|雨伞遗失，2|还伞异常
	 */
	public Integer getFaultType() {
		return faultType;
	}
	/**
	 * 设置：故障单状态：1|处理中，2|完结
	 */
	public void setFaultStatus(Integer faultStatus) {
		this.faultStatus = faultStatus;
	}
	/**
	 * 获取：故障单状态：1|处理中，2|完结
	 */
	public Integer getFaultStatus() {
		return faultStatus;
	}
	/**
	 * 设置：内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：故障图片
	 */
	public void setFaultPicUrl(String faultPicUrl) {
		this.faultPicUrl = faultPicUrl;
	}
	/**
	 * 获取：故障图片
	 */
	public String getFaultPicUrl() {
		return faultPicUrl;
	}
	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：
	 */
	public Long getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：
	 */
	public Long getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
