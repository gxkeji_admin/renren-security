package io.renren.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 伞道
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_machine_channcel")
public class SysMachineChanncelEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 机器id
	 */
	private Long machineId;
	/**
	 * 编码
	 */
	private String channcelCode;
	/**
	 * 机器-伞道：1|表示第一条通道，2|表示第二条通道
	 */
	private Integer channcel;
	/**
	 * 本地伞道二维码路径：1234开锁，5678关锁
	 */
	private String channcelQrcodePath;
	/**
	 * 伞道二维码内容：1234开锁，5678关锁
	 */
	private String channcelSceneUrl;
	/**
	 * 伞道短二维码内容：1234开锁，5678关锁
	 */
	private String channcelShortUrl;
	/**
	 * 部门名称
	 */
	@TableField(exist = false)
	private String				machineName;

	/**
	 * 设置：机器id
	 */
	public void setMachineId(Long machineId) {
		this.machineId = machineId;
	}
	/**
	 * 获取：机器id
	 */
	public Long getMachineId() {
		return machineId;
	}
	/**
	 * 设置：编码
	 */
	public void setChanncelCode(String channcelCode) {
		this.channcelCode = channcelCode;
	}
	/**
	 * 获取：编码
	 */
	public String getChanncelCode() {
		return channcelCode;
	}
	/**
	 * 设置：机器-伞道：1|表示第一条通道，2|表示第二条通道
	 */
	public void setChanncel(Integer channcel) {
		this.channcel = channcel;
	}
	/**
	 * 获取：机器-伞道：1|表示第一条通道，2|表示第二条通道
	 */
	public Integer getChanncel() {
		return channcel;
	}
	/**
	 * 设置：本地伞道二维码路径：1234开锁，5678关锁
	 */
	public void setChanncelQrcodePath(String channcelQrcodePath) {
		this.channcelQrcodePath = channcelQrcodePath;
	}
	/**
	 * 获取：本地伞道二维码路径：1234开锁，5678关锁
	 */
	public String getChanncelQrcodePath() {
		return channcelQrcodePath;
	}
	/**
	 * 设置：伞道二维码内容：1234开锁，5678关锁
	 */
	public void setChanncelSceneUrl(String channcelSceneUrl) {
		this.channcelSceneUrl = channcelSceneUrl;
	}
	/**
	 * 获取：伞道二维码内容：1234开锁，5678关锁
	 */
	public String getChanncelSceneUrl() {
		return channcelSceneUrl;
	}
	/**
	 * 设置：伞道短二维码内容：1234开锁，5678关锁
	 */
	public void setChanncelShortUrl(String channcelShortUrl) {
		this.channcelShortUrl = channcelShortUrl;
	}
	/**
	 * 获取：伞道短二维码内容：1234开锁，5678关锁
	 */
	public String getChanncelShortUrl() {
		return channcelShortUrl;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	
}
