package io.renren.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 订单
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_order")
public class SysOrderEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	private Long deptId;
	/**
	 * 用户openid
	 */
	private String openid;
	/**
	 * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
	 */
	private String unionid;
	/**
	 * 微信支付订单号
	 */
	private String transactionId;
	/**
	 * 取伞源
	 */
	private String rentMachineCode;
	/**
	 * 还伞源
	 */
	private String returnMachineCode;
	/**
	 * 雨伞内置微芯片code
	 */
	private String chipCode;
	/**
	 * 租金金额
	 */
	private BigDecimal totalPrice;
	/**
	 * 实付金额
	 */
	private BigDecimal actualPrice;
	/**
	 * 折扣金额
	 */
	private BigDecimal discountPrice;
	/**
	 * 支付方式：WX|微信支付，Alipay|支付宝支付，Balance|余额支付
	 */
	private String payMethod;
	/**
	 * 租出地址
	 */
	private String rentPlace;
	/**
	 * 归还地址
	 */
	private String returnPlace;
	/**
	 * 租伞开始时间
	 */
	private Date rentTime;
	/**
	 * 还伞时间
	 */
	private Date returnTime;
	/**
	 * 支付时间
	 */
	private Date payTime;
	/**
	 * 付款交易状态:1|待取伞，2|待归还，3|已归还，待支付，4|交易完成，5|待支付系统自动取消订单, 
	 */
	private Integer orderStatus;

	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：用户openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：用户openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
	 */
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	/**
	 * 获取：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
	 */
	public String getUnionid() {
		return unionid;
	}
	/**
	 * 设置：微信支付订单号
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * 获取：微信支付订单号
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * 设置：取伞源
	 */
	public void setRentMachineCode(String rentMachineCode) {
		this.rentMachineCode = rentMachineCode;
	}
	/**
	 * 获取：取伞源
	 */
	public String getRentMachineCode() {
		return rentMachineCode;
	}
	/**
	 * 设置：还伞源
	 */
	public void setReturnMachineCode(String returnMachineCode) {
		this.returnMachineCode = returnMachineCode;
	}
	/**
	 * 获取：还伞源
	 */
	public String getReturnMachineCode() {
		return returnMachineCode;
	}
	/**
	 * 设置：雨伞内置微芯片code
	 */
	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}
	/**
	 * 获取：雨伞内置微芯片code
	 */
	public String getChipCode() {
		return chipCode;
	}
	/**
	 * 设置：租金金额
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：租金金额
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：实付金额
	 */
	public void setActualPrice(BigDecimal actualPrice) {
		this.actualPrice = actualPrice;
	}
	/**
	 * 获取：实付金额
	 */
	public BigDecimal getActualPrice() {
		return actualPrice;
	}
	/**
	 * 设置：折扣金额
	 */
	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：折扣金额
	 */
	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：支付方式：WX|微信支付，Alipay|余额支付，Balance|余额支付
	 */
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	/**
	 * 获取：支付方式：WX|微信支付，Alipay|余额支付，Balance|余额支付
	 */
	public String getPayMethod() {
		return payMethod;
	}
	/**
	 * 设置：租出地址
	 */
	public void setRentPlace(String rentPlace) {
		this.rentPlace = rentPlace;
	}
	/**
	 * 获取：租出地址
	 */
	public String getRentPlace() {
		return rentPlace;
	}
	/**
	 * 设置：归还地址
	 */
	public void setReturnPlace(String returnPlace) {
		this.returnPlace = returnPlace;
	}
	/**
	 * 获取：归还地址
	 */
	public String getReturnPlace() {
		return returnPlace;
	}
	/**
	 * 设置：租伞开始时间
	 */
	public void setRentTime(Date rentTime) {
		this.rentTime = rentTime;
	}
	/**
	 * 获取：租伞开始时间
	 */
	public Date getRentTime() {
		return rentTime;
	}
	/**
	 * 设置：还伞时间
	 */
	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}
	/**
	 * 获取：还伞时间
	 */
	public Date getReturnTime() {
		return returnTime;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	/**
	 * 设置：付款交易状态:1|待取伞，2|待归还，3|已归还，待支付，4|交易完成，5|待支付系统自动取消订单, 
	 */
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * 获取：付款交易状态:1|待取伞，2|待归还，3|已归还，待支付，4|交易完成，5|待支付系统自动取消订单, 
	 */
	public Integer getOrderStatus() {
		return orderStatus;
	}
}
