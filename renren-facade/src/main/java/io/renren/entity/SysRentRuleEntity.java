package io.renren.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 租金规则
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-29 15:47:22
 */
@TableName("sys_rent_rule")
public class SysRentRuleEntity extends SysBaseEntity {
	private static final long	serialVersionUID	= 1L;

	/**
	 * 名称
	 */
	private String				name;
	/**
	 * 部门id
	 */
	private Long				deptId;
	/**
	 * 免费时长单位:1|分，2|小时，3|天
	 */
	private Integer				freeType;
	/**
	 * 时长：表示免费多少分钟，免费多少小时，免费多少天
	 */
	private Integer				freeValue;
	/**
	 * 计价长度：表示每n(小时/天)
	 */
	private Integer				perTypeValue;
	/**
	 * 计价单位：1|按小时计费，2|按天计费
	 */
	private Integer				rentType;
	/**
	 * 租赁价(元)
	 */
	private BigDecimal			rentValue;
	/**
	 * 规则状态：0|过期, 1|执行
	 */
	private Integer				ruleStatus;
	/**
	 * 规则的描述
	 */
	private String				desc;

	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置：部门id
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	/**
	 * 获取：部门id
	 */
	public Long getDeptId() {
		return deptId;
	}

	/**
	 * 设置：免费时长单位:1|分，2|小时，3|天
	 */
	public void setFreeType(Integer freeType) {
		this.freeType = freeType;
	}

	/**
	 * 获取：免费时长单位:1|分，2|小时，3|天
	 */
	public Integer getFreeType() {
		return freeType;
	}

	/**
	 * 设置：时长：表示免费多少分钟，免费多少小时，免费多少天
	 */
	public void setFreeValue(Integer freeValue) {
		this.freeValue = freeValue;
	}

	/**
	 * 获取：时长：表示免费多少分钟，免费多少小时，免费多少天
	 */
	public Integer getFreeValue() {
		return freeValue;
	}

	/**
	 * 设置：计价长度：表示每n(小时/天)
	 */
	public void setPerTypeValue(Integer perTypeValue) {
		this.perTypeValue = perTypeValue;
	}

	/**
	 * 获取：计价长度：表示每n(小时/天)
	 */
	public Integer getPerTypeValue() {
		return perTypeValue;
	}

	/**
	 * 设置：计价单位：1|按小时计费，2|按天计费
	 */
	public void setRentType(Integer rentType) {
		this.rentType = rentType;
	}

	/**
	 * 获取：计价单位：1|按小时计费，2|按天计费
	 */
	public Integer getRentType() {
		return rentType;
	}

	/**
	 * 设置：租赁价(元)
	 */
	public void setRentValue(BigDecimal rentValue) {
		this.rentValue = rentValue;
	}

	/**
	 * 获取：租赁价(元)
	 */
	public BigDecimal getRentValue() {
		return rentValue;
	}

	/**
	 * 设置：规则状态：0|过期, 1|执行
	 */
	public void setRuleStatus(Integer ruleStatus) {
		this.ruleStatus = ruleStatus;
	}

	/**
	 * 获取：规则状态：0|过期, 1|执行
	 */
	public Integer getRuleStatus() {
		return ruleStatus;
	}

	/**
	 * 设置：规则的描述
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * 获取：规则的描述
	 */
	public String getDesc() {
		return desc;
	}
}
