package io.renren.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 押金规则
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-03-30 02:50:36
 */
@TableName("sys_deposit_rule")
public class SysDepositRuleEntity extends SysBaseEntity {
	private static final long	serialVersionUID	= 1L;

	/**
	 * 名称
	 */
	private String				name;
	/**
	 * 部门id
	 */
	private Long				deptId;
	/**
	 * 押金金额
	 */
	private BigDecimal			depositPrice;
	/**
	 * 规则状态：0|过期, 1|执行
	 */
	private Integer				ruleStatus;
	/**
	 * 规则的描述
	 */
	private String				desc;

	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置：部门id
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	/**
	 * 获取：部门id
	 */
	public Long getDeptId() {
		return deptId;
	}

	/**
	 * 设置：押金金额
	 */
	public void setDepositPrice(BigDecimal depositPrice) {
		this.depositPrice = depositPrice;
	}

	/**
	 * 获取：押金金额
	 */
	public BigDecimal getDepositPrice() {
		return depositPrice;
	}

	/**
	 * 设置：规则状态：0|过期, 1|执行
	 */
	public void setRuleStatus(Integer ruleStatus) {
		this.ruleStatus = ruleStatus;
	}

	/**
	 * 获取：规则状态：0|过期, 1|执行
	 */
	public Integer getRuleStatus() {
		return ruleStatus;
	}

	/**
	 * 设置：规则的描述
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * 获取：规则的描述
	 */
	public String getDesc() {
		return desc;
	}
}
