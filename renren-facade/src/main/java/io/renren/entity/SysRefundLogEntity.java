package io.renren.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 退款日志表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_refund_log")
public class SysRefundLogEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 收款人openid
	 */
	private String openid;
	/**
	 * 押金ID
	 */
	private Long depositId;
	/**
	 * 财付通订单号
	 */
	private String tradeNo;
	/**
	 * 部门
	 */
	private Long deptId;
	/**
	 * 支付金额
	 */
	private BigDecimal payPrice;
	/**
	 * 退款金额
	 */
	private BigDecimal refundPrice;
	/**
	 * 退款时间
	 */
	private Date refundTime;
	/**
	 * 支付场景
	 */
	private String refundScene;
	/**
	 * 退款交易状态：1|失败，2|成功
	 */
	private Integer refundStatus;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：收款人openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：收款人openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：押金ID
	 */
	public void setDepositId(Long depositId) {
		this.depositId = depositId;
	}
	/**
	 * 获取：押金ID
	 */
	public Long getDepositId() {
		return depositId;
	}
	/**
	 * 设置：财付通订单号
	 */
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	/**
	 * 获取：财付通订单号
	 */
	public String getTradeNo() {
		return tradeNo;
	}
	/**
	 * 设置：部门
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：支付金额
	 */
	public void setPayPrice(BigDecimal payPrice) {
		this.payPrice = payPrice;
	}
	/**
	 * 获取：支付金额
	 */
	public BigDecimal getPayPrice() {
		return payPrice;
	}
	/**
	 * 设置：退款金额
	 */
	public void setRefundPrice(BigDecimal refundPrice) {
		this.refundPrice = refundPrice;
	}
	/**
	 * 获取：退款金额
	 */
	public BigDecimal getRefundPrice() {
		return refundPrice;
	}
	/**
	 * 设置：退款时间
	 */
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	/**
	 * 获取：退款时间
	 */
	public Date getRefundTime() {
		return refundTime;
	}
	/**
	 * 设置：支付场景
	 */
	public void setRefundScene(String refundScene) {
		this.refundScene = refundScene;
	}
	/**
	 * 获取：支付场景
	 */
	public String getRefundScene() {
		return refundScene;
	}
	/**
	 * 设置：退款交易状态：1|失败，2|成功
	 */
	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}
	/**
	 * 获取：退款交易状态：1|失败，2|成功
	 */
	public Integer getRefundStatus() {
		return refundStatus;
	}
}
