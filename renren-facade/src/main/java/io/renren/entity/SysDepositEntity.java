package io.renren.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 押金
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-01 11:58:26
 */
@TableName("sys_deposit")
public class SysDepositEntity extends SysBaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 付款人openid
	 */
	@NotBlank(message="付款人openid不能为空")
	private String openid;
	/**
	 * 部门ID
	 */
	@NotNull(message="部门ID不能为空")
	private Long deptId;
	/**
	 * 财付通订单号
	 */
	private String tradeNo;
	/**
	 * 微信支付订单号
	 */
	private String transactionId;
	/**
	 * 付款金额
	 */
	private BigDecimal payPrice;
	/**
	 * 支付时间
	 */
	private Date payTime;
	/**
	 * 支付场景
	 */
	private String payScene;
	/**
	 * 押金交易状态:1|待支付，2|已支付，未退款，3|取消支付，4|已支付，已退款
	 */
	private Integer depositStatus;
	/**
	 * 微信退款单号
	 */
	private String refundId;
	/**
	 * 商户退款单号
	 */
	private Long refundNo;
	/**
	 * 退款金额
	 */
	private BigDecimal refundPrice;
	/**
	 * 退款时间
	 */
	private Date refundTime;
	/**
	 * 支付场景
	 */
	private String refundScene;
	/**
	 * 设置：付款人openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：付款人openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：财付通订单号
	 */
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	/**
	 * 获取：财付通订单号
	 */
	public String getTradeNo() {
		return tradeNo;
	}
	/**
	 * 设置：微信支付订单号
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * 获取：微信支付订单号
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * 设置：付款金额
	 */
	public void setPayPrice(BigDecimal payPrice) {
		this.payPrice = payPrice;
	}
	/**
	 * 获取：付款金额
	 */
	public BigDecimal getPayPrice() {
		return payPrice;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	/**
	 * 设置：支付场景
	 */
	public void setPayScene(String payScene) {
		this.payScene = payScene;
	}
	/**
	 * 获取：支付场景
	 */
	public String getPayScene() {
		return payScene;
	}
	/**
	 * 设置：押金交易状态:1|待支付，2|已支付，未退款，3|取消支付，4|已支付，已退款
	 */
	public void setDepositStatus(Integer depositStatus) {
		this.depositStatus = depositStatus;
	}
	/**
	 * 获取：押金交易状态:1|待支付，2|已支付，未退款，3|取消支付，4|已支付，已退款
	 */
	public Integer getDepositStatus() {
		return depositStatus;
	}
	/**
	 * 设置：微信退款单号
	 */
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}
	/**
	 * 获取：微信退款单号
	 */
	public String getRefundId() {
		return refundId;
	}
	/**
	 * 设置：商户退款单号
	 */
	public void setRefundNo(Long refundNo) {
		this.refundNo = refundNo;
	}
	/**
	 * 获取：商户退款单号
	 */
	public Long getRefundNo() {
		return refundNo;
	}
	/**
	 * 设置：退款金额
	 */
	public void setRefundPrice(BigDecimal refundPrice) {
		this.refundPrice = refundPrice;
	}
	/**
	 * 获取：退款金额
	 */
	public BigDecimal getRefundPrice() {
		return refundPrice;
	}
	/**
	 * 设置：退款时间
	 */
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	/**
	 * 获取：退款时间
	 */
	public Date getRefundTime() {
		return refundTime;
	}
	/**
	 * 设置：支付场景
	 */
	public void setRefundScene(String refundScene) {
		this.refundScene = refundScene;
	}
	/**
	 * 获取：支付场景
	 */
	public String getRefundScene() {
		return refundScene;
	}
}
