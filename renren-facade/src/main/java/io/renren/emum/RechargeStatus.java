package io.renren.emum;

public enum RechargeStatus {

	UNPAID(2, "待支付"),
	PAY(1, "已支付"), 
	CANCEL_PAY(2, "取消支付"), 
	OTHER(3, "其他");
	
	private final Integer value;
	
	private final String msg;

	private RechargeStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
