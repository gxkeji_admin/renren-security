package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 * 
 * 
 */
public enum DepositStatus {
	
	UNPAID (1, "待支付"), 
	PAY_BUT_NOREFUND(2, "已支付，未退款"), 
	CANCEL_PAY(3, "取消支付"), 
	PAY_AND_REFUND(4, "已支付，已退款"),
	LOST(5, "用户报失，系统扣押金");

	private final Integer value;
	
	private final String msg;

	private DepositStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}

