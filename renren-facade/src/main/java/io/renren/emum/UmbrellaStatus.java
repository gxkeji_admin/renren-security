package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 * 
 * 
 */
public enum UmbrellaStatus {
		
	FREE(1, "空闲"), 
	LOCKED(2, "锁定"), 
	RENT_OUT(3, "租出"),
	LOST(4, "已遗失");
	
	private final Integer value;
	
	private final String msg;

	private UmbrellaStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
	
    public static String getMsg(int umbrellaStatus) {
        for (UmbrellaStatus u : UmbrellaStatus.values()) {
            if (u.value.intValue() == umbrellaStatus) {
                return u.getMsg();
            }
        }
        return null;
    }
}
