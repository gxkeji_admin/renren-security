package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 * 
 * 
 */
public enum OrderStatus {
	
	WAIT_TAKE_OUT(1, "待取伞"),
	TO_BE_RETURN(2, "待归还"), 
	RETURN_UN_PAY(3, "已归还，待支付"), 
	DONE(4, "交易完成"), 
	LOST(5, "用户遗失"),
	SYSTEM_CANNCEL(6, "待支付系统自动取消订单");

	private final Integer value;
	
	private final String msg;

	private OrderStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
