package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 */
public enum AliyunIotStatus {
	RESULT_FLAG_TRUE(20000, "返回结果成功"),
	RESULT_FLAG_FALSE(20001, "返回结果失败"),
	
	TAKE_OUT_TIMEOUT(20002,"取伞超时，请及时移出雨伞"),
	TAKE_OUT_NOT_ACTION(20003, "取伞超时，请及时移动雨伞"), 
	
	MACHINE_NOT_ONLINE(20004, "设备不在线"),
	DUPLICATE_OPERATION(20005, "请等待其他用户操作，请稍后重试"),
	
	TO_BE_RETURN_TIMEOUT(20006, "还伞超时，请重新操作还伞流程"),
	TO_BE_RETURN_NOT_ACTION(20007, "还伞超时，请重新操作还伞流程"),
	
	ADMIN_INPUT_UMBRELLA_TO_MACHINE(3000, "管理员入库"),
	ADMIN_OUTPUT_UMBRELLA_FROM_MACHINE(3001, "管理员出伞");
	

	private final Integer value;
	
	private final String msg;

	private AliyunIotStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
