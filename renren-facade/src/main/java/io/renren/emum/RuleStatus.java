package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 * 
 * 
 */
public enum RuleStatus {
	
	DISABLED(0, "过期"), 
	EXECUTING(1, "执行中"); //默认值
	private final Integer value;
	
	private final String msg;

	private RuleStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}

