package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 * 
 * 
 */
public enum PayLogStatus {
	
	UNPAID (1, "待支付"), 
	PAY(2, "已支付");
	
	private final Integer value;
	
	private final String msg;

	private PayLogStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
