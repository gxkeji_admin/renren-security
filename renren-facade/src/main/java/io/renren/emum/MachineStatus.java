package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 * 
 * 
 */
public enum MachineStatus {
	
	OFF_LINE(0, "离线"), 
	ON_LINE(1, "在线");

	private final Integer value;
	
	private final String msg;

	private MachineStatus(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
