package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 */
public enum PageCode {
	
	MAP_INDEX_PAGE(1000, "地图主页面"),
	
	DEPOSIT_PAGE(2000, "充值押金页面"),
	
	RECHARGE_PAGE(3000, "充值页面"),
	
	ORDER_DETAIL_PAGE(4000,"订单详情页面"),
	
	MACHINE_CHANNCEL_RENT_PAGE(5000, "扫描机器伞道二维码租伞页面"), 
	
	MACHINE_CHANNCEL_RETURN_PAGE(6000, "扫描机器伞道二维码还伞页面"),
	
	RENT_SUC(7000, "租伞成功页面"),
	
	RETURN_SUC(8000, "还伞成功页面");

	private final Integer value;
	
	private final String msg;

	private PageCode(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
