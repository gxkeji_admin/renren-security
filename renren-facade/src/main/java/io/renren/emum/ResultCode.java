package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 * 
 * 
 */
public enum ResultCode {
	
	BUSY(-1, "系统繁忙"),
	OK(0, "请求成功"),
	
	MACHINE_CHANNCEL_RENT_PAGE_SUC(100, "扫描机器伞道二维码租伞,成功！"),
	MACHINE_CHANNCEL_RETURN_PAGE_SUC(101, "扫描机器伞道二维码还伞，成功！"),
	HELP_RETURN(102, "第三方扫描机器伞道二维码还伞，成功！"),
	
	TO_BE_RETURN(103, "您有正在进行中的订单！"),
	UNPAY_DEPOSIT(104, "您未缴纳押金，请先缴纳押金！"),
	ARREARAGE(105, "您的余额不足，请先充值！"), 
	
	ERROR_TO_BE_RETURN(106, "您已归还雨伞，请勿重复操作！"),
	ERROR_CHANNCEL_ON(107, "您尚未租借雨伞，请扫描借伞二维码！"),
	ERROR_CHANNCEL_OFF(108, "您有尚未归还的雨伞，请还伞后再借伞！"),
	ERROR_QRCODE(109, "非法的二维码！"),
	ERROR_MACHINE_STATUS(110, "机器状态不正确！"),
	ERROR_UMBRELLA_STATUS(111, "雨伞状态不正确！"),
	ERROR_UMBRELLA_RETURN(112, "还伞失败，请将感应区下的雨伞移入伞道内！"),
	ERROR_INPUT_UMBRELLA_QR(113, "管理员扫码入库，请扫还伞二维码！"),
	ERROR_OUTPUT_UMBRELLA_QR(114, "管理员扫码出伞，请扫租伞二维码！"),
	ERROR_NOT_ADMIN(115, "请使用管理账号扫描二维码！");
	
	private final Integer value;
	
	private final String msg;

	private ResultCode(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
