package io.renren.emum;

/**
 * 
 * @author oslive
 * @version 2016年5月20日 下午3:19:19
 */
public enum TradeType {

	BIZ_DEPOSIT(1, "押金"), 
	BIZ_RECHARGE(2, "充值"), 
	BIZ_REFUND(3, "退款"),
	BIZ_CONSUME_PAY(4, "消费");

	private final Integer	value;

	private final String	msg;

	private TradeType(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer getValue() {
		return value;
	}

	public String getMsg() {
		return msg;
	}
}
